/*
 * Copyright 2014, Javier Moreno.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 * And have fun ;-)
 */
package com.centraldecompras.acceso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;


// select * from cc_aut_users;
// -- insert into cc_aut_users VALUES ('XXXXZZZZCCCC','admin@gmail','admin','admin',0);
// pr   

@Entity
@Table(name="CC_AUT_users", 
       indexes = {@Index(columnList = "username"), @Index(columnList = "email")}) 
@NamedQueries({        
    @NamedQuery(name = "UserCentral.findByUsername", query = ""
            + " SELECT u "
            + "   FROM UserCentral as u "
            + "  WHERE trim(u.username) = :username"),
    
    @NamedQuery(name = "UserCentral.findByEmail", query = ""
            + " SELECT u "
            + "   FROM UserCentral as u "
            + "  WHERE u.email = :email"),
    
    @NamedQuery(name = "UserCentral.findCustomUserDetails", query = ""
            + " SELECT u "
            + "   FROM UserCentral as u "
            + "  WHERE u.username = :username")
})
public class UserCentral implements UserDetails, Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
    @NotNull
    @Column(name = "user_id", length = 36, nullable = false, updatable = false)
    private String id;
    
    @Version
    private int version;
    
    @Column(length = 64, nullable = false, unique = true)
    private String username;
    
    @Column(length = 32, nullable = false, unique = false)
    private String md5password;
    
    @Column(length = 32)
    private String apellido1;
    @Column(length = 32)
    private String apellido2;
    @Column(length = 32)
    private String nombre;
    
    private Boolean perfilPublico;
    //@ElementCollection
    //@CollectionTable(name = "user_roles", joinColumns = {@JoinColumn(name="user_id")})
    //private List<String> roles = new ArrayList<>();
    
    @Column(length = 128, nullable = true, unique = false)
    private String email;

    @Column(length = 15, nullable = true, unique = false)
    private String estadoEnvíoCorreo;
    
    @Column(length = 128, nullable = true, unique = false)
    private String descripcionEstadoEnvíoCorreo;
    
    @OneToMany(targetEntity = UsrSoc.class, mappedBy = "userCentral", fetch = FetchType.EAGER)
    private Collection<UsrSoc> prsSocList = new ArrayList<UsrSoc>();
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoriaAcceso atributosAuditoria;
    
    public UserCentral() {
    }

    public UserCentral(String id, String username, String md5password, String email, DatosAuditoriaAcceso atributosAuditoria) {
        this.id = id;
        this.username = username;
        this.md5password = md5password;
        this.email = email;
        this.atributosAuditoria = atributosAuditoria;
    }

    public UserCentral(String id, String username, String md5password, String apellido1, String nombre, Boolean perfilPublico, String email, String estadoEnvíoCorreo, String descripcionEstadoEnvíoCorreo, DatosAuditoriaAcceso atributosAuditoria) {
        this.id = id;
        this.username = username;
        this.md5password = md5password;
        this.apellido1 = apellido1;
        this.nombre = nombre;
        this.perfilPublico = perfilPublico;
        this.email = email;
        this.estadoEnvíoCorreo = estadoEnvíoCorreo;
        this.descripcionEstadoEnvíoCorreo = descripcionEstadoEnvíoCorreo;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    @Override
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getMd5password() {
        return md5password;
    }

    public void setMd5password(String md5password) {
        this.md5password = md5password;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getPerfilPublico() {
        return perfilPublico;
    }

    public void setPerfilPublico(Boolean perfilPublico) {
        this.perfilPublico = perfilPublico;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //return AuthorityUtils.createAuthorityList((String[]) roles.toArray());
        return AuthorityUtils.createAuthorityList(new String[]{"Rol_noValido1","Rol_noValido2"} );
    }

    @Override
    public String getPassword() {
        return md5password;
    }

    public String getEstadoEnvíoCorreo() {
        return estadoEnvíoCorreo;
    }

    public void setEstadoEnvíoCorreo(String estadoEnvíoCorreo) {
        this.estadoEnvíoCorreo = estadoEnvíoCorreo;
    }

    public String getDescripcionEstadoEnvíoCorreo() {
        return descripcionEstadoEnvíoCorreo;
    }

    public void setDescripcionEstadoEnvíoCorreo(String descripcionEstadoEnvíoCorreo) {
        this.descripcionEstadoEnvíoCorreo = descripcionEstadoEnvíoCorreo;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<UsrSoc> getPrsSocList() {
        return prsSocList;
    }

    public void setPrsSocList(Collection<UsrSoc> prsSocList) {
        this.prsSocList = prsSocList;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoriaAcceso getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoriaAcceso atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.username);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserCentral other = (UserCentral) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        return true;
    }
/*
    @Override
    public String toString() {
        return "UserCentral{" + "id=" + id + ", version=" + version + ", username=" + username + ", md5password=" + md5password + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", nombre=" + nombre + ", perfilPublico=" + perfilPublico + ", email=" + email + ", estadoEnv\u00edoCorreo=" + estadoEnvíoCorreo + ", descripcionEstadoEnv\u00edoCorreo=" + descripcionEstadoEnvíoCorreo + ", prsSocList=" + prsSocList + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

*/
    
}
