package com.centraldecompras.acceso;

import java.util.List;

public class FiltroSoc {
     String nombre;
     String identificador;
     List perfiles;  

     int coincidenciaNombre;         //1=Empieza por, 2=Contiene, 3=Finaliza con, 4=Es igual a
     int coincidenciaIdentificador;  //1=Empieza por, 2=Contiene, 3=Finaliza con, 4=Es igual a
    
     int  numeroRegistrosPagina;
     int paginaActual; 

    public FiltroSoc(String nombre, String identificador, List perfiles, int coincidenciaNombre, int coincidenciaIdentificador, int numeroRegistrosPagina, int paginaActual) {
        this.nombre = nombre;
        this.identificador = identificador;
        this.perfiles = perfiles;
        this.coincidenciaNombre = coincidenciaNombre;
        this.coincidenciaIdentificador = coincidenciaIdentificador;
        this.numeroRegistrosPagina = numeroRegistrosPagina;
        this.paginaActual = paginaActual;
    }

    public FiltroSoc() {
    }

    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public List getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List perfiles) {
        this.perfiles = perfiles;
    }

    public int getCoincidenciaNombre() {
        return coincidenciaNombre;
    }

    public void setCoincidenciaNombre(int coincidenciaNombre) {
        this.coincidenciaNombre = coincidenciaNombre;
    }

    public int getCoincidenciaIdentificador() {
        return coincidenciaIdentificador;
    }

    public void setCoincidenciaIdentificador(int coincidenciaIdentificador) {
        this.coincidenciaIdentificador = coincidenciaIdentificador;
    }

    public int getNumeroRegistrosPagina() {
        return numeroRegistrosPagina;
    }

    public void setNumeroRegistrosPagina(int numeroRegistrosPagina) {
        this.numeroRegistrosPagina = numeroRegistrosPagina;
    }

    public int getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(int paginaActual) {
        this.paginaActual = paginaActual;
    }
    
    
}
