package com.centraldecompras.acceso;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_AUT_UsrSoc", 
     uniqueConstraints = {
         @UniqueConstraint(columnNames={"userCentral", "sociedad"})
     }
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsrSoc.findAll", query = ""
            + " SELECT d "
            + "   FROM UsrSoc d"),
    @NamedQuery(name = "UsrSoc.findUsrSocByUserCentral", query = ""
            + " SELECT d "
            + "   FROM UsrSoc d "
            + "  WHERE d.userCentral  = :userCentral"),
    @NamedQuery(name = "PrsSoc.findBySociedad", query = ""
            + " SELECT d "
            + "   FROM UsrSoc d "
            + "  WHERE d.sociedad = :sociedad"),
    @NamedQuery(name = "UsrSoc.findUsrSocByUS", query = ""
            + " SELECT d "
            + "   FROM UsrSoc d "
            + "  WHERE d.userCentral = :userCentral "
            + "    AND d.sociedad = :sociedad") 
})
public class UsrSoc implements Serializable {

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idUsrSoc;

    @Version
    private int version;

    // Atributos Básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = UserCentral.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "userCentral")
    private UserCentral userCentral;

    @ManyToOne(targetEntity = Sociedad.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "sociedad")
    private Sociedad sociedad;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoriaAcceso atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public UsrSoc() {

    }

    public UsrSoc(String idUsrSoc, UserCentral userCentral, Sociedad sociedad, DatosAuditoriaAcceso atributosAuditoria) {
        this.idUsrSoc = idUsrSoc;
        this.userCentral = userCentral;
        this.sociedad = sociedad;
        this.atributosAuditoria = atributosAuditoria;
    }

    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    
    public String getIdUsrSoc() {
        return this.idUsrSoc;
    }

    public void setIdUsrSoc(String idUsrSoc) {
        this.idUsrSoc = idUsrSoc;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UserCentral getUserCentral() {
        return this.userCentral;
    }

    public void setUserCentral(UserCentral userCentral) {
        this.userCentral = userCentral;
    }

    public Sociedad getSociedad() {
        return this.sociedad;
    }

    public void setSociedad(Sociedad sociedad) {
        this.sociedad = sociedad;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoriaAcceso getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoriaAcceso atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }


    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.userCentral != null ? this.userCentral.hashCode() : 0);
        hash = 71 * hash + (this.sociedad != null ? this.sociedad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsrSoc other = (UsrSoc) obj;
        if (this.userCentral != other.userCentral && (this.userCentral == null || !this.userCentral.equals(other.userCentral))) {
            return false;
        }
        if (this.sociedad != other.sociedad && (this.sociedad == null || !this.sociedad.equals(other.sociedad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PrsSoc{" + "idPrsSoc=" + idUsrSoc + ", version=" + version + ", persona=" + userCentral + ", sociedad=" + sociedad + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


}
