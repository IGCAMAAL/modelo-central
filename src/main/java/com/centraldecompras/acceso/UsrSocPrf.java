/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso;

import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.Perfil;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_AUT_UsrSocPrf", 
     uniqueConstraints = {
         @UniqueConstraint(columnNames={"prsSoc", "perfil"})
     }
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsrSocPrf.findAll", query = ""
            + " SELECT d "
            + "   FROM UsrSocPrf d"),
    @NamedQuery(name = "UsrSocPrf.findByUsrSoc", query = ""
            + " SELECT d "
            + "   FROM UsrSocPrf d "
            + "  WHERE d.usrSoc = :usrSoc"),
    @NamedQuery(name = "UsrSocPrf.findByPerfil", query = ""
            + " SELECT d "
            + "   FROM UsrSocPrf d "
            + "  WHERE d.perfil = :perfil"),
    @NamedQuery(name = "UsrSocPrf.findUsrSocByUP", query = ""
            + " SELECT d "
            + "   FROM UsrSocPrf d "
            + "  WHERE d.usrSoc = :usrSoc "
            + "   AND d.perfil = :perfil")        
})
public class UsrSocPrf implements Serializable {

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idUsrSocPrf;

    @Version
    private int version;

    // Atributos de Sesion,  proceso o estado registro * * * * * * * * * * * * *
    @NotNull
    @Column(name = "estadoSesion", nullable = false, updatable = true)
    private Boolean estadoSesion;

    // Atributos Básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    private String mail;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @JoinColumn(name = "prsSoc", updatable = false)
    @ManyToOne(targetEntity = UsrSoc.class, fetch = FetchType.EAGER)
    private UsrSoc usrSoc;

    @JoinColumn(name = "perfil", updatable = false)
    @ManyToOne(targetEntity = Perfil.class, fetch = FetchType.EAGER)
    private Perfil perfil;

    @JoinColumn(name = "asociadoAdministrador", updatable = true)
    @ManyToOne(targetEntity = UsrSocPrf.class)
    private UsrSocPrf asociadoAdministrador;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoriaAcceso atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public UsrSocPrf() {
    }

    public UsrSocPrf(String idUsrSocPrf, Boolean estadoSesion, String mail, UsrSoc usrSoc, Perfil perfil, UsrSocPrf asociadoAdministrador, DatosAuditoriaAcceso atributosAuditoria) {
        this.idUsrSocPrf = idUsrSocPrf;
        this.estadoSesion = estadoSesion;
        this.mail = mail;
        this.usrSoc = usrSoc;
        this.perfil = perfil;
        this.asociadoAdministrador = asociadoAdministrador;
        this.atributosAuditoria = atributosAuditoria;
    }
      
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    
    public String getIdUsrSocPrf() {
        return idUsrSocPrf;
    }

    public void setIdUsrSocPrf(String idUsrSocPrf) {
        this.idUsrSocPrf = idUsrSocPrf;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    // Metodos de Sesion o proceso * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Boolean isEstadoSesion() {
        return estadoSesion;
    }

    public void setEstadoSesion(Boolean estadoSesion) {
        this.estadoSesion = estadoSesion;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UsrSoc getUsrSoc() {
        return usrSoc;
    }

    public void setUsrSoc(UsrSoc usrSoc) {
        this.usrSoc = usrSoc;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public UsrSocPrf getAsociadoAdministrador() {
        return asociadoAdministrador;
    }

    public void setAsociadoAdministrador(UsrSocPrf asociadoAdministrador) {
        this.asociadoAdministrador = asociadoAdministrador;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoriaAcceso getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoriaAcceso atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.mail);
        hash = 47 * hash + Objects.hashCode(this.usrSoc);
        hash = 47 * hash + Objects.hashCode(this.perfil);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsrSocPrf other = (UsrSocPrf) obj;
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.usrSoc, other.usrSoc)) {
            return false;
        }
        if (!Objects.equals(this.perfil, other.perfil)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UsrSocPrf{" + "idUsrSocPrf=" + idUsrSocPrf + ", version=" + version + ", estadoSesion=" + estadoSesion + ", mail=" + mail + ", usrSoc=" + usrSoc + ", perfil=" + perfil + ", asociadoAdministrador=" + asociadoAdministrador + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
