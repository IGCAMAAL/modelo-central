/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso.service;

import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class UsrSocServiceImpl implements UsrSocService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UsrSocServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;
    
    @Transactional
     public void create_C(UsrSoc usrSoc) {
        em.persist(usrSoc);
    }
    public void create(UsrSoc usrSoc) {
        em.persist(usrSoc);
    }

    public void edit(UsrSoc usrSoc) throws NonexistentEntityException, Exception {
        try {
            UsrSoc persistentPrsSoc = em.find(UsrSoc.class, usrSoc.getIdUsrSoc());
            UserCentral userCentralOld = persistentPrsSoc.getUserCentral();
            UserCentral userCentralNew = usrSoc.getUserCentral();
            Sociedad sociedadOld = persistentPrsSoc.getSociedad();
            Sociedad sociedadNew = usrSoc.getSociedad();

            if (userCentralNew != null) {
                userCentralNew = em.getReference(userCentralNew.getClass(), userCentralNew.getId());
                usrSoc.setUserCentral(userCentralNew);
            }
            if (sociedadNew != null) {
                sociedadNew = em.getReference(sociedadNew.getClass(), sociedadNew.getIdSociedad());
                usrSoc.setSociedad(sociedadNew);
            }
            usrSoc = em.merge(usrSoc);
            if (userCentralOld != null && !userCentralOld.equals(userCentralNew)) {
                userCentralOld.getPrsSocList().remove(usrSoc);
                userCentralOld = em.merge(userCentralOld);
            }
            if (userCentralNew != null && !userCentralNew.equals(userCentralOld)) {
                userCentralNew.getPrsSocList().add(usrSoc);
                userCentralNew = em.merge(userCentralNew);
            }
            if (sociedadOld != null && !sociedadOld.equals(sociedadNew)) {
                sociedadOld.getPrsSocList().remove(usrSoc);
                sociedadOld = em.merge(sociedadOld);
            }
            if (sociedadNew != null && !sociedadNew.equals(sociedadOld)) {
                sociedadNew.getPrsSocList().add(usrSoc);
                sociedadNew = em.merge(sociedadNew);
            }
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usrSoc.getIdUsrSoc();
                if (findUsrSoc(id) == null) {
                    throw new NonexistentEntityException("The prsSoc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        UsrSoc usrSoc;
        try {
            usrSoc = em.getReference(UsrSoc.class, id);
            usrSoc.getIdUsrSoc();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The prsSoc with id " + id + " no longer exists.", enfe);
        }
        UserCentral userCentral = usrSoc.getUserCentral();
        if (userCentral != null) {
            userCentral.getPrsSocList().remove(usrSoc);
            userCentral = em.merge(userCentral);
        }
        Sociedad sociedad = usrSoc.getSociedad();
        if (sociedad != null) {
            sociedad.getPrsSocList().remove(usrSoc);
            sociedad = em.merge(sociedad);
        }
        em.remove(usrSoc);
    }

    public List<UsrSoc> findPrsSocEntities() {
        return findUsrSocEntities(true, -1, -1);
    }

    public List<UsrSoc> findPrsSocEntities(int maxResults, int firstResult) {
        return findUsrSocEntities(false, maxResults, firstResult);
    }

    private List<UsrSoc> findUsrSocEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UsrSoc.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public UsrSoc findUsrSoc(String id) {
        return em.find(UsrSoc.class, id);
    }

    public int getUsrSocCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UsrSoc> rt = cq.from(UsrSoc.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<UsrSoc> findUsrSocByUserCentral(UserCentral userCentral) {
        return em.createNamedQuery("UsrSoc.findUsrSocByUserCentral")
                .setParameter("userCentral", userCentral)
                .getResultList();
    }

    public UsrSoc findPrsSocByUS(UserCentral userCentral, Sociedad sociedad) {
        UsrSoc prsSoc = null;

        try {
            prsSoc = (UsrSoc) em.createNamedQuery("UsrSoc.findUsrSocByUS")
                    .setParameter("userCentral", userCentral)
                    .setParameter("sociedad", sociedad)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing to do
        }
        return prsSoc;
    }

    public List<UsrSoc> findPrsSocByS(Sociedad sociedad) {
        List<UsrSoc> prsSoc = null;

        try {
            prsSoc = (List<UsrSoc>) em.createNamedQuery("PrsSoc.findBySociedad")
                    .setParameter("sociedad", sociedad)
                    .getResultList();
        } catch (Exception ex) {
            // Nothing to do
        }
        return prsSoc;
    }


}
