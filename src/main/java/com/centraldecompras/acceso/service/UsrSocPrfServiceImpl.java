/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso.service;

import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class UsrSocPrfServiceImpl implements UsrSocPrfService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UsrSocPrfServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(UsrSocPrf usrSocPrf) {
        Perfil perfil = usrSocPrf.getPerfil();
        if (perfil != null) {
            perfil = em.getReference(perfil.getClass(), perfil.getIdPerfil());
            usrSocPrf.setPerfil(perfil);
        }
        UsrSocPrf asociadoAdministrador = usrSocPrf.getAsociadoAdministrador();
        if (asociadoAdministrador != null) {
            asociadoAdministrador = em.getReference(asociadoAdministrador.getClass(), asociadoAdministrador.getIdUsrSocPrf());
            usrSocPrf.setAsociadoAdministrador(asociadoAdministrador);
        }
        em.persist(usrSocPrf);
        if (perfil != null) {
            perfil.getUsrSocPrfList().add(usrSocPrf);
            perfil = em.merge(perfil);
        }
        if (asociadoAdministrador != null) {
            UsrSocPrf oldAsociadoAdministradorOfAsociadoAdministrador = asociadoAdministrador.getAsociadoAdministrador();
            if (oldAsociadoAdministradorOfAsociadoAdministrador != null) {
                oldAsociadoAdministradorOfAsociadoAdministrador.setAsociadoAdministrador(null);
                oldAsociadoAdministradorOfAsociadoAdministrador = em.merge(oldAsociadoAdministradorOfAsociadoAdministrador);
            }
            asociadoAdministrador.setAsociadoAdministrador(usrSocPrf);
            asociadoAdministrador = em.merge(asociadoAdministrador);
        }
    }

    public void edit(UsrSocPrf usrSocPrf) throws NonexistentEntityException, Exception {
        try {
            UsrSocPrf persistentUsrSocPrf = em.find(UsrSocPrf.class, usrSocPrf.getIdUsrSocPrf());
            Perfil perfilOld = persistentUsrSocPrf.getPerfil();
            Perfil perfilNew = usrSocPrf.getPerfil();
            UsrSocPrf asociadoAdministradorOld = persistentUsrSocPrf.getAsociadoAdministrador();
            UsrSocPrf asociadoAdministradorNew = usrSocPrf.getAsociadoAdministrador();
            if (perfilNew != null) {
                perfilNew = em.getReference(perfilNew.getClass(), perfilNew.getIdPerfil());
                usrSocPrf.setPerfil(perfilNew);
            }
            if (asociadoAdministradorNew != null) {
                asociadoAdministradorNew = em.getReference(asociadoAdministradorNew.getClass(), asociadoAdministradorNew.getIdUsrSocPrf());
                usrSocPrf.setAsociadoAdministrador(asociadoAdministradorNew);
            }
            usrSocPrf = em.merge(usrSocPrf);
            if (perfilOld != null && !perfilOld.equals(perfilNew)) {
                perfilOld.getUsrSocPrfList().remove(usrSocPrf);
                perfilOld = em.merge(perfilOld);
            }
            if (perfilNew != null && !perfilNew.equals(perfilOld)) {
                perfilNew.getUsrSocPrfList().add(usrSocPrf);
                perfilNew = em.merge(perfilNew);
            }
            if (asociadoAdministradorOld != null && !asociadoAdministradorOld.equals(asociadoAdministradorNew)) {
                asociadoAdministradorOld.setAsociadoAdministrador(null);
                asociadoAdministradorOld = em.merge(asociadoAdministradorOld);
            }
            if (asociadoAdministradorNew != null && !asociadoAdministradorNew.equals(asociadoAdministradorOld)) {
                UsrSocPrf oldAsociadoAdministradorOfAsociadoAdministrador = asociadoAdministradorNew.getAsociadoAdministrador();
                if (oldAsociadoAdministradorOfAsociadoAdministrador != null) {
                    oldAsociadoAdministradorOfAsociadoAdministrador.setAsociadoAdministrador(null);
                    oldAsociadoAdministradorOfAsociadoAdministrador = em.merge(oldAsociadoAdministradorOfAsociadoAdministrador);
                }
                asociadoAdministradorNew.setAsociadoAdministrador(usrSocPrf);
                asociadoAdministradorNew = em.merge(asociadoAdministradorNew);
            }
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usrSocPrf.getIdUsrSocPrf();
                if (findUsrSocPrf(id) == null) {
                    throw new NonexistentEntityException("The usrSocPrf with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }
    
    @Transactional
    public void edit_B(UsrSocPrf usrSocPrf) throws NonexistentEntityException, Exception {
        try {
            UsrSocPrf persistentUsrSocPrf = em.find(UsrSocPrf.class, usrSocPrf.getIdUsrSocPrf());
            Perfil perfilOld = persistentUsrSocPrf.getPerfil();
            Perfil perfilNew = usrSocPrf.getPerfil();
            UsrSocPrf asociadoAdministradorOld = persistentUsrSocPrf.getAsociadoAdministrador();
            UsrSocPrf asociadoAdministradorNew = usrSocPrf.getAsociadoAdministrador();
            if (perfilNew != null) {
                perfilNew = em.getReference(perfilNew.getClass(), perfilNew.getIdPerfil());
                usrSocPrf.setPerfil(perfilNew);
            }
            if (asociadoAdministradorNew != null) {
                asociadoAdministradorNew = em.getReference(asociadoAdministradorNew.getClass(), asociadoAdministradorNew.getIdUsrSocPrf());
                usrSocPrf.setAsociadoAdministrador(asociadoAdministradorNew);
            }
            usrSocPrf = em.merge(usrSocPrf);
            if (perfilOld != null && !perfilOld.equals(perfilNew)) {
                perfilOld.getUsrSocPrfList().remove(usrSocPrf);
                perfilOld = em.merge(perfilOld);
            }
            if (perfilNew != null && !perfilNew.equals(perfilOld)) {
                perfilNew.getUsrSocPrfList().add(usrSocPrf);
                perfilNew = em.merge(perfilNew);
            }
            if (asociadoAdministradorOld != null && !asociadoAdministradorOld.equals(asociadoAdministradorNew)) {
                asociadoAdministradorOld.setAsociadoAdministrador(null);
                asociadoAdministradorOld = em.merge(asociadoAdministradorOld);
            }
            if (asociadoAdministradorNew != null && !asociadoAdministradorNew.equals(asociadoAdministradorOld)) {
                UsrSocPrf oldAsociadoAdministradorOfAsociadoAdministrador = asociadoAdministradorNew.getAsociadoAdministrador();
                if (oldAsociadoAdministradorOfAsociadoAdministrador != null) {
                    oldAsociadoAdministradorOfAsociadoAdministrador.setAsociadoAdministrador(null);
                    oldAsociadoAdministradorOfAsociadoAdministrador = em.merge(oldAsociadoAdministradorOfAsociadoAdministrador);
                }
                asociadoAdministradorNew.setAsociadoAdministrador(usrSocPrf);
                asociadoAdministradorNew = em.merge(asociadoAdministradorNew);
            }
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usrSocPrf.getIdUsrSocPrf();
                if (findUsrSocPrf(id) == null) {
                    throw new NonexistentEntityException("The usrSocPrf with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        UsrSocPrf usrSocPrf;
        try {
            usrSocPrf = em.getReference(UsrSocPrf.class, id);
            usrSocPrf.getIdUsrSocPrf();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The usrSocPrf with id " + id + " no longer exists.", enfe);
        }
        Perfil perfil = usrSocPrf.getPerfil();
        if (perfil != null) {
            perfil.getUsrSocPrfList().remove(usrSocPrf);
            perfil = em.merge(perfil);
        }
        UsrSocPrf asociadoAdministrador = usrSocPrf.getAsociadoAdministrador();
        if (asociadoAdministrador != null) {
            asociadoAdministrador.setAsociadoAdministrador(null);
            asociadoAdministrador = em.merge(asociadoAdministrador);
        }
        em.remove(usrSocPrf);
    }

    public List<UsrSocPrf> findUsrSocPrfEntities() {
        return findUsrSocPrfEntities(true, -1, -1);
    }

    public List<UsrSocPrf> findUsrSocPrfEntities(int maxResults, int firstResult) {
        return findUsrSocPrfEntities(false, maxResults, firstResult);
    }

    public List<UsrSocPrf> findUsrSocPrfEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UsrSocPrf.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public UsrSocPrf findUsrSocPrf(String id) {
        return em.find(UsrSocPrf.class, id);
    }

    public int getUsrSocPrfCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UsrSocPrf> rt = cq.from(UsrSocPrf.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<UsrSocPrf> findByUsrSoc(UsrSoc usrSoc) {
        return em.createNamedQuery("UsrSocPrf.findByUsrSoc")
                .setParameter("usrSoc", usrSoc)
                .getResultList();
    }

    public UsrSocPrf findUsrSocByUP(UsrSoc usrSoc, Perfil perfil) {
        UsrSocPrf reply = null;
        try {
            reply = (UsrSocPrf) em.createNamedQuery("UsrSocPrf.findUsrSocByUP")
                    .setParameter("usrSoc", usrSoc)
                    .setParameter("perfil", perfil)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothig to do
        }
        
        return reply;
    }

    @Transactional
    public void create_C(UsrSocPrf usrSocPrf) {
        Perfil perfil = usrSocPrf.getPerfil();
        if (perfil != null) {
            perfil = em.getReference(perfil.getClass(), perfil.getIdPerfil());
            usrSocPrf.setPerfil(perfil);
        }
        UsrSocPrf asociadoAdministrador = usrSocPrf.getAsociadoAdministrador();
        if (asociadoAdministrador != null) {
            asociadoAdministrador = em.getReference(asociadoAdministrador.getClass(), asociadoAdministrador.getIdUsrSocPrf());
            usrSocPrf.setAsociadoAdministrador(asociadoAdministrador);
        }
        em.persist(usrSocPrf);
        if (perfil != null) {
            perfil.getUsrSocPrfList().add(usrSocPrf);
            perfil = em.merge(perfil);
        }
        if (asociadoAdministrador != null) {
            UsrSocPrf oldAsociadoAdministradorOfAsociadoAdministrador = asociadoAdministrador.getAsociadoAdministrador();
            if (oldAsociadoAdministradorOfAsociadoAdministrador != null) {
                oldAsociadoAdministradorOfAsociadoAdministrador.setAsociadoAdministrador(null);
                oldAsociadoAdministradorOfAsociadoAdministrador = em.merge(oldAsociadoAdministradorOfAsociadoAdministrador);
            }
            asociadoAdministrador.setAsociadoAdministrador(usrSocPrf);
            asociadoAdministrador = em.merge(asociadoAdministrador);
        }
    }

}
