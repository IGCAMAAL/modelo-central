package com.centraldecompras.acceso.service;

import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserCentralServiceImpl implements UserCentralService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UserCentralServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(UserCentral userCentral) {
        if (userCentral.getPrsSocList() == null) {
            userCentral.setPrsSocList(new ArrayList<UsrSoc>());
        }
        Collection<UsrSoc> attachedPrsSocList = new ArrayList<UsrSoc>();
        for (UsrSoc prsSocListPrsSocToAttach : userCentral.getPrsSocList()) {
            prsSocListPrsSocToAttach = em.getReference(prsSocListPrsSocToAttach.getClass(), prsSocListPrsSocToAttach.getIdUsrSoc());
            attachedPrsSocList.add(prsSocListPrsSocToAttach);
        }
        userCentral.setPrsSocList(attachedPrsSocList);
        em.persist(userCentral);
        for (UsrSoc prsSocListPrsSoc : userCentral.getPrsSocList()) {
            UserCentral oldPersonaOfPrsSocListPrsSoc = prsSocListPrsSoc.getUserCentral();
            prsSocListPrsSoc.setUserCentral(userCentral);
            prsSocListPrsSoc = em.merge(prsSocListPrsSoc);
            if (oldPersonaOfPrsSocListPrsSoc != null) {
                oldPersonaOfPrsSocListPrsSoc.getPrsSocList().remove(prsSocListPrsSoc);
                oldPersonaOfPrsSocListPrsSoc = em.merge(oldPersonaOfPrsSocListPrsSoc);
            }
        }
    }

    
    public void edit(UserCentral userCentral) throws NonexistentEntityException, Exception {
        try {
            UserCentral persistentPersona = em.find(UserCentral.class, userCentral.getId());
            Collection<UsrSoc> prsSocListOld = persistentPersona.getPrsSocList();
            Collection<UsrSoc> prsSocListNew = userCentral.getPrsSocList();
            Collection<UsrSoc> attachedPrsSocListNew = new ArrayList<UsrSoc>();
            for (UsrSoc prsSocListNewPrsSocToAttach : prsSocListNew) {
                prsSocListNewPrsSocToAttach = em.getReference(prsSocListNewPrsSocToAttach.getClass(), prsSocListNewPrsSocToAttach.getIdUsrSoc());
                attachedPrsSocListNew.add(prsSocListNewPrsSocToAttach);
            }
            prsSocListNew = attachedPrsSocListNew;
            userCentral.setPrsSocList(prsSocListNew);
            userCentral = em.merge(userCentral);
            for (UsrSoc prsSocListOldPrsSoc : prsSocListOld) {
                if (!prsSocListNew.contains(prsSocListOldPrsSoc)) {
                    prsSocListOldPrsSoc.setUserCentral(null);
                    prsSocListOldPrsSoc = em.merge(prsSocListOldPrsSoc);
                }
            }
            for (UsrSoc prsSocListNewPrsSoc : prsSocListNew) {
                if (!prsSocListOld.contains(prsSocListNewPrsSoc)) {
                    UserCentral oldPersonaOfPrsSocListNewPrsSoc = prsSocListNewPrsSoc.getUserCentral();
                    prsSocListNewPrsSoc.setUserCentral(userCentral);
                    prsSocListNewPrsSoc = em.merge(prsSocListNewPrsSoc);
                    if (oldPersonaOfPrsSocListNewPrsSoc != null && !oldPersonaOfPrsSocListNewPrsSoc.equals(userCentral)) {
                        oldPersonaOfPrsSocListNewPrsSoc.getPrsSocList().remove(prsSocListNewPrsSoc);
                        oldPersonaOfPrsSocListNewPrsSoc = em.merge(oldPersonaOfPrsSocListNewPrsSoc);
                    }
                }
            }

        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = userCentral.getId();
                if (findUserCentral(id) == null) {
                    throw new NonexistentEntityException("The persona with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    @Transactional
    public void edit_B(UserCentral userCentral) throws NonexistentEntityException, Exception {
        try {
            UserCentral persistentPersona = em.find(UserCentral.class, userCentral.getId());
            Collection<UsrSoc> prsSocListOld = persistentPersona.getPrsSocList();
            Collection<UsrSoc> prsSocListNew = userCentral.getPrsSocList();
            Collection<UsrSoc> attachedPrsSocListNew = new ArrayList<UsrSoc>();
            for (UsrSoc prsSocListNewPrsSocToAttach : prsSocListNew) {
                prsSocListNewPrsSocToAttach = em.getReference(prsSocListNewPrsSocToAttach.getClass(), prsSocListNewPrsSocToAttach.getIdUsrSoc());
                attachedPrsSocListNew.add(prsSocListNewPrsSocToAttach);
            }
            prsSocListNew = attachedPrsSocListNew;
            userCentral.setPrsSocList(prsSocListNew);
            userCentral = em.merge(userCentral);
            for (UsrSoc prsSocListOldPrsSoc : prsSocListOld) {
                if (!prsSocListNew.contains(prsSocListOldPrsSoc)) {
                    prsSocListOldPrsSoc.setUserCentral(null);
                    prsSocListOldPrsSoc = em.merge(prsSocListOldPrsSoc);
                }
            }
            for (UsrSoc prsSocListNewPrsSoc : prsSocListNew) {
                if (!prsSocListOld.contains(prsSocListNewPrsSoc)) {
                    UserCentral oldPersonaOfPrsSocListNewPrsSoc = prsSocListNewPrsSoc.getUserCentral();
                    prsSocListNewPrsSoc.setUserCentral(userCentral);
                    prsSocListNewPrsSoc = em.merge(prsSocListNewPrsSoc);
                    if (oldPersonaOfPrsSocListNewPrsSoc != null && !oldPersonaOfPrsSocListNewPrsSoc.equals(userCentral)) {
                        oldPersonaOfPrsSocListNewPrsSoc.getPrsSocList().remove(prsSocListNewPrsSoc);
                        oldPersonaOfPrsSocListNewPrsSoc = em.merge(oldPersonaOfPrsSocListNewPrsSoc);
                    }
                }
            }

        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = userCentral.getId();
                if (findUserCentral(id) == null) {
                    throw new NonexistentEntityException("The persona with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }
    
    
    
    public void destroy(String id) throws NonexistentEntityException {
        UserCentral userCentral;
        try {
            userCentral = em.getReference(UserCentral.class, id);
            userCentral.getId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The persona with id " + id + " no longer exists.", enfe);
        }
        Collection<UsrSoc> prsSocList = userCentral.getPrsSocList();
        for (UsrSoc prsSocListPrsSoc : prsSocList) {
            prsSocListPrsSoc.setUserCentral(null);
            prsSocListPrsSoc = em.merge(prsSocListPrsSoc);
        }
        em.remove(userCentral);
    }

    public List<UserCentral> findUsrCentralEntities() {
        return findUsrCentralEntities(true, -1, -1);
    }

    public List<UserCentral> findUsrCentralEntities(int maxResults, int firstResult) {
        return findUsrCentralEntities(false, maxResults, firstResult);
    }

    private List<UserCentral> findUsrCentralEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UserCentral.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public UserCentral findUsrCentral(String id) {
        return em.find(UserCentral.class, id);
    }

    public int getUsrCentralCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UserCentral> rt = cq.from(UserCentral.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public UserCentral findUserCentral(String id) {
        return em.find(UserCentral.class, id);
    }

    public UserCentral findUserCentralBy_UN(String username) {
        UserCentral reply = null;
        try {
            reply = (UserCentral) em.createNamedQuery("UserCentral.findByUsername")
                    .setParameter("username", username.trim())
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing to do
        }
        return reply;
    }
   
    @Transactional
    public void create_C(UserCentral userCentral) {
        if (userCentral.getPrsSocList() == null) {
            userCentral.setPrsSocList(new ArrayList<UsrSoc>());
        }
        Collection<UsrSoc> attachedPrsSocList = new ArrayList<UsrSoc>();
        for (UsrSoc prsSocListPrsSocToAttach : userCentral.getPrsSocList()) {
            prsSocListPrsSocToAttach = em.getReference(prsSocListPrsSocToAttach.getClass(), prsSocListPrsSocToAttach.getIdUsrSoc());
            attachedPrsSocList.add(prsSocListPrsSocToAttach);
        }
        userCentral.setPrsSocList(attachedPrsSocList);
        em.persist(userCentral);
        for (UsrSoc prsSocListPrsSoc : userCentral.getPrsSocList()) {
            UserCentral oldPersonaOfPrsSocListPrsSoc = prsSocListPrsSoc.getUserCentral();
            prsSocListPrsSoc.setUserCentral(userCentral);
            prsSocListPrsSoc = em.merge(prsSocListPrsSoc);
            if (oldPersonaOfPrsSocListPrsSoc != null) {
                oldPersonaOfPrsSocListPrsSoc.getPrsSocList().remove(prsSocListPrsSoc);
                oldPersonaOfPrsSocListPrsSoc = em.merge(oldPersonaOfPrsSocListPrsSoc);
            }
        }
    }

}
