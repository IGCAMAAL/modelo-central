/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.centraldecompras.acceso.service.interfaces;

import com.centraldecompras.acceso.Perfil;



/**
 *
 * @author ciberado
 */
public interface PerfilService {

    void create(Perfil perfil);
    
    void create_C(Perfil perfil);
    
    Perfil findPerfil(String id);

    Perfil findPerfilBy_CP(String codPerfil);
    
    int getPerfilCount();
}
