package com.centraldecompras.acceso.service.interfaces;


import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import java.util.List;



/**
 *
 * @author ciberado
 */
public interface UsrSocService {

    void create(UsrSoc empleadosSociedad) throws PreexistingEntityException, Exception ;
    
    public void edit(UsrSoc empleadosSociedad) throws NonexistentEntityException, Exception;
    
    void destroy(String id) throws NonexistentEntityException;
    
    UsrSoc findUsrSoc(String id);
    
    public List<UsrSoc> findUsrSocByUserCentral(UserCentral persona);
    
    List<UsrSoc> findPrsSocByS(Sociedad sociedad);
            
    public UsrSoc findPrsSocByUS(UserCentral persona, Sociedad sociedad);
    
    void create_C(UsrSoc empleadosSociedad) throws PreexistingEntityException, Exception ;

}
