/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.centraldecompras.acceso.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import java.util.List;



/**
 *
 * @author ciberado
 */
public interface UsrSocPrfService {

    void create(UsrSocPrf usrSocPrf) throws PreexistingEntityException, Exception ;
    
    public void edit(UsrSocPrf usrSocPrf) throws NonexistentEntityException, Exception;
    
    void destroy(String id) throws NonexistentEntityException;
    
    UsrSocPrf findUsrSocPrf(String id);
    
    List<UsrSocPrf> findByUsrSoc(UsrSoc usrSoc);
    
    UsrSocPrf findUsrSocByUP(UsrSoc UsrSoc, Perfil perfil);
    
    List<UsrSocPrf> findUsrSocPrfEntities(boolean all, int maxResults, int firstResult);
    
    void create_C(UsrSocPrf usrSocPrf) throws PreexistingEntityException, Exception ;
    
    void edit_B(UsrSocPrf usrSocPrf) throws NonexistentEntityException, Exception;
    
}
