/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso.service.interfaces;

import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface SectoresAutorizadosService {

    void create(SectoresAutorizados sector) throws PreexistingEntityException, Exception;

    void create_C(SectoresAutorizados sector) throws PreexistingEntityException, Exception;

    void edit(SectoresAutorizados sector) throws PreexistingEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    SectoresAutorizados findSectoresAutorizados(String id);

    List<SectoresAutorizados> findSectorByPrsSocPrf(UsrSocPrf usrSocPrf);

    List<SectoresAutorizados> findUSP_By_Sid(Sector sector);

    SectoresAutorizados findUsrSocBy_USP_Sid(UsrSocPrf prsSocPrf, Sector sector);

}
