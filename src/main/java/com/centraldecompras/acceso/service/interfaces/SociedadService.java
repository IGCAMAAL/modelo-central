/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.FiltroSoc;
import java.util.List;
import org.apache.commons.beanutils.DynaBean;

/**
 *
 * @author ciberado
 */
public interface SociedadService {

    Sociedad findSociedad(String id);

    void create(Sociedad sociedad) throws PreexistingEntityException, Exception;

    void edit(Sociedad sociedad) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;
    
    Sociedad findSociedadBy_TNR_NR(String nifnrf, String tipoIdentificador);
    
    List<Sociedad> findSociedadEntities(boolean all, int maxResults, int firstResult);

    DynaBean findUsrSocPrf_Pag_WithFilter(FiltroSoc filtroSoc);

    void create_C(Sociedad sociedad) throws PreexistingEntityException, Exception;

}
