/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso.service.interfaces;

import com.centraldecompras.acceso.Sector;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;

/**
 *
 * @author ciberado
 */
public interface SectorService {

    void create(Sector sector) throws PreexistingEntityException, Exception;

    void create_C(Sector sector);

    void edit(Sector sector) throws PreexistingEntityException, Exception;

    Sector findSector(String id);
}
