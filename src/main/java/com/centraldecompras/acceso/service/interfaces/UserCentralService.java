package com.centraldecompras.acceso.service.interfaces;

import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;

public interface UserCentralService {

    void create(UserCentral userCentral);

    void edit(UserCentral userCentral) throws NonexistentEntityException, Exception;

    void edit_B(UserCentral userCentral) throws NonexistentEntityException, Exception;

    public void destroy(String id) throws NonexistentEntityException;

    UserCentral findUserCentral(String id);

    UserCentral findUserCentralBy_UN(String username);

    void create_C(UserCentral userCentral);

}
