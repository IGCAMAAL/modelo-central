package com.centraldecompras.acceso.service;

import com.centraldecompras.acceso.service.interfaces.PerfilService;
import com.centraldecompras.zglobal.exceptions.IllegalOrphanException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.UsrSocPrf;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class PerfilServiceImpl implements PerfilService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PerfilServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(Perfil perfil) {
        if (perfil.getUsrSocPrfList() == null) {
            perfil.setPrsSocPrfList(new ArrayList<UsrSocPrf>());
        }
        Collection<UsrSocPrf> attachedPrsSocPrfList = new ArrayList<UsrSocPrf>();
        for (UsrSocPrf prsSocPrfListPrsSocPrfToAttach : perfil.getUsrSocPrfList()) {
            prsSocPrfListPrsSocPrfToAttach = em.getReference(prsSocPrfListPrsSocPrfToAttach.getClass(), prsSocPrfListPrsSocPrfToAttach.getIdUsrSocPrf());
            attachedPrsSocPrfList.add(prsSocPrfListPrsSocPrfToAttach);
        }
        perfil.setPrsSocPrfList(attachedPrsSocPrfList);
        em.persist(perfil);
        for (UsrSocPrf prsSocPrfListPrsSocPrf : perfil.getUsrSocPrfList()) {
            Perfil oldPerfilOfPrsSocPrfListPrsSocPrf = prsSocPrfListPrsSocPrf.getPerfil();
            prsSocPrfListPrsSocPrf.setPerfil(perfil);
            prsSocPrfListPrsSocPrf = em.merge(prsSocPrfListPrsSocPrf);
            if (oldPerfilOfPrsSocPrfListPrsSocPrf != null) {
                oldPerfilOfPrsSocPrfListPrsSocPrf.getUsrSocPrfList().remove(prsSocPrfListPrsSocPrf);
                oldPerfilOfPrsSocPrfListPrsSocPrf = em.merge(oldPerfilOfPrsSocPrfListPrsSocPrf);
            }
        }
    }

    @Transactional
    public void create(Perfil perfil) {
        if (perfil.getUsrSocPrfList() == null) {
            perfil.setPrsSocPrfList(new ArrayList<UsrSocPrf>());
        }
        Collection<UsrSocPrf> attachedPrsSocPrfList = new ArrayList<UsrSocPrf>();
        for (UsrSocPrf prsSocPrfListPrsSocPrfToAttach : perfil.getUsrSocPrfList()) {
            prsSocPrfListPrsSocPrfToAttach = em.getReference(prsSocPrfListPrsSocPrfToAttach.getClass(), prsSocPrfListPrsSocPrfToAttach.getIdUsrSocPrf());
            attachedPrsSocPrfList.add(prsSocPrfListPrsSocPrfToAttach);
        }
        perfil.setPrsSocPrfList(attachedPrsSocPrfList);
        em.persist(perfil);
        for (UsrSocPrf prsSocPrfListPrsSocPrf : perfil.getUsrSocPrfList()) {
            Perfil oldPerfilOfPrsSocPrfListPrsSocPrf = prsSocPrfListPrsSocPrf.getPerfil();
            prsSocPrfListPrsSocPrf.setPerfil(perfil);
            prsSocPrfListPrsSocPrf = em.merge(prsSocPrfListPrsSocPrf);
            if (oldPerfilOfPrsSocPrfListPrsSocPrf != null) {
                oldPerfilOfPrsSocPrfListPrsSocPrf.getUsrSocPrfList().remove(prsSocPrfListPrsSocPrf);
                oldPerfilOfPrsSocPrfListPrsSocPrf = em.merge(oldPerfilOfPrsSocPrfListPrsSocPrf);
            }
        }
    }

    @Transactional
    public void edit(Perfil perfil) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            Perfil persistentPerfil = em.find(Perfil.class, perfil.getIdPerfil());
            Collection<UsrSocPrf> prsSocPrfListOld = persistentPerfil.getUsrSocPrfList();
            Collection<UsrSocPrf> prsSocPrfListNew = perfil.getUsrSocPrfList();
            List<String> illegalOrphanMessages = null;
            for (UsrSocPrf prsSocPrfListOldPrsSocPrf : prsSocPrfListOld) {
                if (!prsSocPrfListNew.contains(prsSocPrfListOldPrsSocPrf)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PrsSocPrf " + prsSocPrfListOldPrsSocPrf + " since its perfil field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<UsrSocPrf> attachedPrsSocPrfListNew = new ArrayList<UsrSocPrf>();
            for (UsrSocPrf prsSocPrfListNewPrsSocPrfToAttach : prsSocPrfListNew) {
                prsSocPrfListNewPrsSocPrfToAttach = em.getReference(prsSocPrfListNewPrsSocPrfToAttach.getClass(), prsSocPrfListNewPrsSocPrfToAttach.getIdUsrSocPrf());
                attachedPrsSocPrfListNew.add(prsSocPrfListNewPrsSocPrfToAttach);
            }
            prsSocPrfListNew = attachedPrsSocPrfListNew;
            perfil.setPrsSocPrfList(prsSocPrfListNew);
            perfil = em.merge(perfil);
            for (UsrSocPrf prsSocPrfListNewPrsSocPrf : prsSocPrfListNew) {
                if (!prsSocPrfListOld.contains(prsSocPrfListNewPrsSocPrf)) {
                    Perfil oldPerfilOfPrsSocPrfListNewPrsSocPrf = prsSocPrfListNewPrsSocPrf.getPerfil();
                    prsSocPrfListNewPrsSocPrf.setPerfil(perfil);
                    prsSocPrfListNewPrsSocPrf = em.merge(prsSocPrfListNewPrsSocPrf);
                    if (oldPerfilOfPrsSocPrfListNewPrsSocPrf != null && !oldPerfilOfPrsSocPrfListNewPrsSocPrf.equals(perfil)) {
                        oldPerfilOfPrsSocPrfListNewPrsSocPrf.getUsrSocPrfList().remove(prsSocPrfListNewPrsSocPrf);
                        oldPerfilOfPrsSocPrfListNewPrsSocPrf = em.merge(oldPerfilOfPrsSocPrfListNewPrsSocPrf);
                    }
                }
            }
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = perfil.getIdPerfil();
                if (findPerfil(id) == null) {
                    throw new NonexistentEntityException("The perfil with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    @Transactional
    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        Perfil perfil;
        try {
            perfil = em.getReference(Perfil.class, id);
            perfil.getIdPerfil();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The perfil with id " + id + " no longer exists.", enfe);
        }
        List<String> illegalOrphanMessages = null;
        Collection<UsrSocPrf> prsSocPrfListOrphanCheck = perfil.getUsrSocPrfList();
        for (UsrSocPrf prsSocPrfListOrphanCheckPrsSocPrf : prsSocPrfListOrphanCheck) {
            if (illegalOrphanMessages == null) {
                illegalOrphanMessages = new ArrayList<String>();
            }
            illegalOrphanMessages.add("This Perfil (" + perfil + ") cannot be destroyed since the PrsSocPrf " + prsSocPrfListOrphanCheckPrsSocPrf + " in its prsSocPrfList field has a non-nullable perfil field.");
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        em.remove(perfil);
    }

    @Transactional
    public List<Perfil> findPerfilEntities() {
        return findPerfilEntities(true, -1, -1);
    }

    @Transactional
    public List<Perfil> findPerfilEntities(int maxResults, int firstResult) {
        return findPerfilEntities(false, maxResults, firstResult);
    }

    @Transactional
    private List<Perfil> findPerfilEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Perfil.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    @Transactional
    public Perfil findPerfil(String id) {
        return em.find(Perfil.class, id);
    }

    @Transactional
    public int getPerfilCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Perfil> rt = cq.from(Perfil.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    @Transactional
    public Perfil findPerfilBy_CP(String codPerfil) {
        Perfil perfil = null;
        try {
            perfil = (Perfil) em.createNamedQuery("Perfil.findPerfilBy_CP")
                    .setParameter("codPerfil", codPerfil)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Perfil con codPerfil=" + codPerfil ); 
        }
        return perfil;
    }
}
