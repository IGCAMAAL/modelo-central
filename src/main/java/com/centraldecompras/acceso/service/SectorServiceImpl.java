/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso.service;

import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class SectorServiceImpl implements SectorService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SectorServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(Sector sector) {
        em.persist(sector);
    }

    @Transactional
    public void create(Sector sector) {
        em.persist(sector);
    }

    @Transactional
    public void edit(Sector sector) throws NonexistentEntityException, Exception {
        try {
            sector = em.merge(sector);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = sector.getIdSector();
                if (findSector(id) == null) {
                    throw new NonexistentEntityException("The sector with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    @Transactional
    public void destroy(String id) throws NonexistentEntityException {
        Sector sector;
        try {
            sector = em.getReference(Sector.class, id);
            sector.getIdSector();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The sector with id " + id + " no longer exists.", enfe);
        }
        em.remove(sector);
    }

    @Transactional
    public List<Sector> findSectorEntities() {
        return findSectorEntities(true, -1, -1);
    }

    @Transactional
    public List<Sector> findSectorEntities(int maxResults, int firstResult) {
        return findSectorEntities(false, maxResults, firstResult);
    }

    @Transactional
    private List<Sector> findSectorEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Sector.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    @Transactional
    @Override
    public Sector findSector(String id) {
        return em.find(Sector.class, id);
    }

    @Transactional
    public int getSectorCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Sector> rt = cq.from(Sector.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }


  
}
