/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso.service;

import com.centraldecompras.acceso.FiltroSoc;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class SociedadServiceImpl implements SociedadService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SociedadServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(Sociedad sociedad) throws PreexistingEntityException, Exception {
        if (sociedad.getPrsSocList() == null) {
            sociedad.setPrsSocList(new ArrayList<UsrSoc>());
        }
        if (sociedad.getDistribuidoras() == null) {
            sociedad.setDistribuidoras(new ArrayList<Sociedad>());
        }
        if (sociedad.getFabricantes() == null) {
            sociedad.setFabricantes(new ArrayList<Sociedad>());
        }
        try {

            Collection<UsrSoc> attachedPrsSocList = new ArrayList<UsrSoc>();
            for (UsrSoc prsSocListPrsSocToAttach : sociedad.getPrsSocList()) {
                prsSocListPrsSocToAttach = em.getReference(prsSocListPrsSocToAttach.getClass(), prsSocListPrsSocToAttach.getIdUsrSoc());
                attachedPrsSocList.add(prsSocListPrsSocToAttach);
            }
            sociedad.setPrsSocList(attachedPrsSocList);

            Collection<Sociedad> attachedDistribuidoras = new ArrayList<Sociedad>();
            for (Sociedad distribuidorasSociedadToAttach : sociedad.getDistribuidoras()) {
                distribuidorasSociedadToAttach = em.getReference(distribuidorasSociedadToAttach.getClass(), distribuidorasSociedadToAttach.getIdSociedad());
                attachedDistribuidoras.add(distribuidorasSociedadToAttach);
            }
            sociedad.setDistribuidoras(attachedDistribuidoras);

            Collection<Sociedad> attachedFabricantes = new ArrayList<Sociedad>();
            for (Sociedad fabricantesSociedadToAttach : sociedad.getFabricantes()) {
                fabricantesSociedadToAttach = em.getReference(fabricantesSociedadToAttach.getClass(), fabricantesSociedadToAttach.getIdSociedad());
                attachedFabricantes.add(fabricantesSociedadToAttach);
            }
            sociedad.setFabricantes(attachedFabricantes);

            em.persist(sociedad);

            for (UsrSoc prsSocListPrsSoc : sociedad.getPrsSocList()) {
                Sociedad oldSociedadOfPrsSocListPrsSoc = prsSocListPrsSoc.getSociedad();
                prsSocListPrsSoc.setSociedad(sociedad);
                prsSocListPrsSoc = em.merge(prsSocListPrsSoc);
                if (oldSociedadOfPrsSocListPrsSoc != null) {
                    oldSociedadOfPrsSocListPrsSoc.getPrsSocList().remove(prsSocListPrsSoc);
                    oldSociedadOfPrsSocListPrsSoc = em.merge(oldSociedadOfPrsSocListPrsSoc);
                }
            }

            for (Sociedad distribuidorasSociedad : sociedad.getDistribuidoras()) {
                distribuidorasSociedad.getDistribuidoras().add(sociedad);
                distribuidorasSociedad = em.merge(distribuidorasSociedad);
            }
            for (Sociedad fabricantesSociedad : sociedad.getFabricantes()) {
                fabricantesSociedad.getDistribuidoras().add(sociedad);
                fabricantesSociedad = em.merge(fabricantesSociedad);
            }
        } catch (Exception ex) {
            if (findSociedad(sociedad.getIdSociedad()) != null) {
                throw new PreexistingEntityException("Sociedad " + sociedad + " already exists.", ex);
            }
            throw ex;
        }
    }

    
    public void edit(Sociedad sociedad) throws NonexistentEntityException, Exception {
        try {
            Sociedad persistentSociedad = em.find(Sociedad.class, sociedad.getIdSociedad());
            Collection<UsrSoc> prsSocListOld = persistentSociedad.getPrsSocList();
            Collection<UsrSoc> prsSocListNew = sociedad.getPrsSocList();
            Collection<Sociedad> distribuidorasOld = persistentSociedad.getDistribuidoras();
            Collection<Sociedad> distribuidorasNew = sociedad.getDistribuidoras();
            Collection<Sociedad> fabricantesOld = persistentSociedad.getFabricantes();
            Collection<Sociedad> fabricantesNew = sociedad.getFabricantes();

            Collection<UsrSoc> attachedPrsSocListNew = new ArrayList<UsrSoc>();
            for (UsrSoc prsSocListNewPrsSocToAttach : prsSocListNew) {
                prsSocListNewPrsSocToAttach = em.getReference(prsSocListNewPrsSocToAttach.getClass(), prsSocListNewPrsSocToAttach.getIdUsrSoc());
                attachedPrsSocListNew.add(prsSocListNewPrsSocToAttach);
            }
            prsSocListNew = attachedPrsSocListNew;
            sociedad.setPrsSocList(prsSocListNew);

            Collection<Sociedad> attachedDistribuidorasNew = new ArrayList<Sociedad>();
            for (Sociedad distribuidorasNewSociedadToAttach : distribuidorasNew) {
                distribuidorasNewSociedadToAttach = em.getReference(distribuidorasNewSociedadToAttach.getClass(), distribuidorasNewSociedadToAttach.getIdSociedad());
                attachedDistribuidorasNew.add(distribuidorasNewSociedadToAttach);
            }
            distribuidorasNew = attachedDistribuidorasNew;
            sociedad.setDistribuidoras(distribuidorasNew);

            Collection<Sociedad> attachedFabricantesNew = new ArrayList<Sociedad>();
            for (Sociedad fabricantesNewSociedadToAttach : fabricantesNew) {
                fabricantesNewSociedadToAttach = em.getReference(fabricantesNewSociedadToAttach.getClass(), fabricantesNewSociedadToAttach.getIdSociedad());
                attachedFabricantesNew.add(fabricantesNewSociedadToAttach);
            }
            fabricantesNew = attachedFabricantesNew;
            sociedad.setFabricantes(fabricantesNew);

            sociedad = em.merge(sociedad);

            for (UsrSoc prsSocListOldPrsSoc : prsSocListOld) {
                if (!prsSocListNew.contains(prsSocListOldPrsSoc)) {
                    prsSocListOldPrsSoc.setSociedad(null);
                    prsSocListOldPrsSoc = em.merge(prsSocListOldPrsSoc);
                }
            }

            for (UsrSoc prsSocListNewPrsSoc : prsSocListNew) {
                if (!prsSocListOld.contains(prsSocListNewPrsSoc)) {
                    Sociedad oldSociedadOfPrsSocListNewPrsSoc = prsSocListNewPrsSoc.getSociedad();
                    prsSocListNewPrsSoc.setSociedad(sociedad);
                    prsSocListNewPrsSoc = em.merge(prsSocListNewPrsSoc);
                    if (oldSociedadOfPrsSocListNewPrsSoc != null && !oldSociedadOfPrsSocListNewPrsSoc.equals(sociedad)) {
                        oldSociedadOfPrsSocListNewPrsSoc.getPrsSocList().remove(prsSocListNewPrsSoc);
                        oldSociedadOfPrsSocListNewPrsSoc = em.merge(oldSociedadOfPrsSocListNewPrsSoc);
                    }
                }
            }

            for (Sociedad distribuidorasOldSociedad : distribuidorasOld) {
                if (!distribuidorasNew.contains(distribuidorasOldSociedad)) {
                    distribuidorasOldSociedad.getDistribuidoras().remove(sociedad);
                    distribuidorasOldSociedad = em.merge(distribuidorasOldSociedad);
                }
            }
            for (Sociedad distribuidorasNewSociedad : distribuidorasNew) {
                if (!distribuidorasOld.contains(distribuidorasNewSociedad)) {
                    distribuidorasNewSociedad.getDistribuidoras().add(sociedad);
                    distribuidorasNewSociedad = em.merge(distribuidorasNewSociedad);
                }
            }

            for (Sociedad fabricantesOldSociedad : fabricantesOld) {
                if (!fabricantesNew.contains(fabricantesOldSociedad)) {
                    fabricantesOldSociedad.getDistribuidoras().remove(sociedad);
                    fabricantesOldSociedad = em.merge(fabricantesOldSociedad);
                }
            }
            for (Sociedad fabricantesNewSociedad : fabricantesNew) {
                if (!fabricantesOld.contains(fabricantesNewSociedad)) {
                    fabricantesNewSociedad.getDistribuidoras().add(sociedad);
                    fabricantesNewSociedad = em.merge(fabricantesNewSociedad);
                }
            }

        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = sociedad.getIdSociedad();
                if (findSociedad(id) == null) {
                    throw new NonexistentEntityException("The sociedad with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    
    public void destroy(String id) throws NonexistentEntityException {
        Sociedad sociedad;
        try {
            sociedad = em.getReference(Sociedad.class, id);
            sociedad.getIdSociedad();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The sociedad with id " + id + " no longer exists.", enfe);
        }

        Collection<UsrSoc> prsSocList = sociedad.getPrsSocList();
        for (UsrSoc prsSocListPrsSoc : prsSocList) {
            prsSocListPrsSoc.setSociedad(null);
            prsSocListPrsSoc = em.merge(prsSocListPrsSoc);
        }

        Collection<Sociedad> distribuidoras = sociedad.getDistribuidoras();
        for (Sociedad distribuidorasSociedad : distribuidoras) {
            distribuidorasSociedad.getDistribuidoras().remove(sociedad);
            distribuidorasSociedad = em.merge(distribuidorasSociedad);
        }
        Collection<Sociedad> fabricantes = sociedad.getFabricantes();
        for (Sociedad fabricantesSociedad : fabricantes) {
            fabricantesSociedad.getDistribuidoras().remove(sociedad);
            fabricantesSociedad = em.merge(fabricantesSociedad);
        }
        em.remove(sociedad);
    }

    
    @Transactional
    public void create_C(Sociedad sociedad) throws PreexistingEntityException, Exception {
        if (sociedad.getPrsSocList() == null) {
            sociedad.setPrsSocList(new ArrayList<UsrSoc>());
        }
        if (sociedad.getDistribuidoras() == null) {
            sociedad.setDistribuidoras(new ArrayList<Sociedad>());
        }
        if (sociedad.getFabricantes() == null) {
            sociedad.setFabricantes(new ArrayList<Sociedad>());
        }
        try {

            Collection<UsrSoc> attachedPrsSocList = new ArrayList<UsrSoc>();
            for (UsrSoc prsSocListPrsSocToAttach : sociedad.getPrsSocList()) {
                prsSocListPrsSocToAttach = em.getReference(prsSocListPrsSocToAttach.getClass(), prsSocListPrsSocToAttach.getIdUsrSoc());
                attachedPrsSocList.add(prsSocListPrsSocToAttach);
            }
            sociedad.setPrsSocList(attachedPrsSocList);

            Collection<Sociedad> attachedDistribuidoras = new ArrayList<Sociedad>();
            for (Sociedad distribuidorasSociedadToAttach : sociedad.getDistribuidoras()) {
                distribuidorasSociedadToAttach = em.getReference(distribuidorasSociedadToAttach.getClass(), distribuidorasSociedadToAttach.getIdSociedad());
                attachedDistribuidoras.add(distribuidorasSociedadToAttach);
            }
            sociedad.setDistribuidoras(attachedDistribuidoras);

            Collection<Sociedad> attachedFabricantes = new ArrayList<Sociedad>();
            for (Sociedad fabricantesSociedadToAttach : sociedad.getFabricantes()) {
                fabricantesSociedadToAttach = em.getReference(fabricantesSociedadToAttach.getClass(), fabricantesSociedadToAttach.getIdSociedad());
                attachedFabricantes.add(fabricantesSociedadToAttach);
            }
            sociedad.setFabricantes(attachedFabricantes);

            em.persist(sociedad);

            for (UsrSoc prsSocListPrsSoc : sociedad.getPrsSocList()) {
                Sociedad oldSociedadOfPrsSocListPrsSoc = prsSocListPrsSoc.getSociedad();
                prsSocListPrsSoc.setSociedad(sociedad);
                prsSocListPrsSoc = em.merge(prsSocListPrsSoc);
                if (oldSociedadOfPrsSocListPrsSoc != null) {
                    oldSociedadOfPrsSocListPrsSoc.getPrsSocList().remove(prsSocListPrsSoc);
                    oldSociedadOfPrsSocListPrsSoc = em.merge(oldSociedadOfPrsSocListPrsSoc);
                }
            }

            for (Sociedad distribuidorasSociedad : sociedad.getDistribuidoras()) {
                distribuidorasSociedad.getDistribuidoras().add(sociedad);
                distribuidorasSociedad = em.merge(distribuidorasSociedad);
            }
            for (Sociedad fabricantesSociedad : sociedad.getFabricantes()) {
                fabricantesSociedad.getDistribuidoras().add(sociedad);
                fabricantesSociedad = em.merge(fabricantesSociedad);
            }
        } catch (Exception ex) {
            if (findSociedad(sociedad.getIdSociedad()) != null) {
                throw new PreexistingEntityException("Sociedad " + sociedad + " already exists.", ex);
            }
            throw ex;
        }
    }

    
    public List<Sociedad> findSociedadEntities() {
        return findSociedadEntities(true, -1, -1);
    }

    
    public List<Sociedad> findSociedadEntities(int maxResults, int firstResult) {
        return findSociedadEntities(false, maxResults, firstResult);
    }

    
    public List<Sociedad> findSociedadEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Sociedad.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    
    public Sociedad findSociedad(String id) {
        return em.find(Sociedad.class, id);
    }
 
    
    public int getSociedadCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Sociedad> rt = cq.from(Sociedad.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    
    public Sociedad findSociedadBy_TNR_NR(String nifnrf, String tipoIdentificador) {
        Sociedad sociedad = null;
        try {
            sociedad = (Sociedad) em.createNamedQuery("Sociedad.findSociedadBy_TNR_NR")
                    .setParameter("nifnrf", nifnrf)
                    .setParameter("tipoIdentificador", tipoIdentificador)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Sociedad con nifnrf=" + nifnrf + " y tipoIdentificador=" + tipoIdentificador); 
        }
        return sociedad;
    }

    
    public DynaBean findUsrSocPrf_Pag_WithFilter(FiltroSoc filtroSoc) {
        DynaBean reply = null;
        List<String> idSociedades = null;

        StringBuilder nombreBuscado = new StringBuilder();
        StringBuilder identificadorBuscado = new StringBuilder();
        StringBuilder codper = new StringBuilder();
        int ultimaPagina = 0;

        if (filtroSoc.getNombre().isEmpty() || filtroSoc.getNombre().equals("")) {
            filtroSoc.setNombre(null);
        }
        if (filtroSoc.getIdentificador().isEmpty() || filtroSoc.getIdentificador().equals("")) {
            filtroSoc.setIdentificador(null);
        }
        if (filtroSoc.getPerfiles().isEmpty() || filtroSoc.getPerfiles().equals("")) {
            filtroSoc.setPerfiles(null);
        }

        Long resultadosmax = findUsrSocPrf_Con_WithFilter(filtroSoc);

        int leidos = resultadosmax.intValue();
        ultimaPagina = (leidos + filtroSoc.getNumeroRegistrosPagina() - 1) / filtroSoc.getNumeroRegistrosPagina();

        if (filtroSoc.getPaginaActual() > ultimaPagina) {
            filtroSoc.setPaginaActual(ultimaPagina);
        }

        if (filtroSoc.getNombre() != null) {
            switch (filtroSoc.getCoincidenciaNombre()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(filtroSoc.getNombre()).append("%");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append("%").append(filtroSoc.getNombre()).append("%");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append("%").append(filtroSoc.getNombre());
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(filtroSoc.getNombre());
                    break;
            }
        }

        if (filtroSoc.getIdentificador() != null) {
            switch (filtroSoc.getCoincidenciaIdentificador()) {
                case 1:
                    identificadorBuscado = (identificadorBuscado).append(filtroSoc.getIdentificador()).append("%");
                    break;
                case 2:
                    identificadorBuscado = (identificadorBuscado).append("%").append(filtroSoc.getIdentificador()).append("%");
                    break;
                case 3:
                    identificadorBuscado = (identificadorBuscado).append("%").append(filtroSoc.getIdentificador());
                    break;
                case 4:
                    identificadorBuscado = (identificadorBuscado).append(filtroSoc.getIdentificador());
                    break;
            }
        }

        try {

            // Filtro: 3- Nombre, identificador, perfiles
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() != null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_NIP")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

            // Filtro: 2- Nombre, identificador
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() == null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_NI")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

            // Filtro: 2- Nombre, perfiles
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() != null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_NP")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

            // Filtro: 2- identificador, perfiles
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() != null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_IP")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

            // Filtro: 1- Nombre
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() == null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_N")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

            // Filtro: 1- identificador
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() == null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_I")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

            // Filtro: 1- Nombre, identificador, perfiles
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() != null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_P")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

            // Filtro: 0- Nombre, identificador, perfiles
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() == null) {
                idSociedades = (List<String>) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .setFirstResult((filtroSoc.getPaginaActual() - 1) * filtroSoc.getNumeroRegistrosPagina())
                        .setMaxResults(filtroSoc.getNumeroRegistrosPagina())
                        .getResultList();
            }

        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Sociedad con nifnrf=" + filtroSoc.getIdentificador());
        }

        DynaProperty[] props = new DynaProperty[]{
            new DynaProperty("totalPaginas", Integer.TYPE),
            new DynaProperty("totalRegistros", Integer.TYPE),
            new DynaProperty("paginaActual", Integer.TYPE),
            new DynaProperty("idSociedades", List.class),};
        BasicDynaClass dynaClass = new BasicDynaClass("datosSocUsuPrf", null, props);

        try {
            reply = dynaClass.newInstance();
            reply.set("totalPaginas", ultimaPagina);
            reply.set("totalRegistros", leidos);
            reply.set("paginaActual", filtroSoc.getPaginaActual());
            reply.set("idSociedades", idSociedades);
        } catch (IllegalAccessException | InstantiationException ex) {
            try {
                throw Exception(ex.getMessage());
            } catch (java.lang.Exception ex1) {
                Logger.getLogger(SociedadServiceImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return reply;
    }


    public Long findUsrSocPrf_Con_WithFilter(FiltroSoc filtroSoc) {
        Long reply = null;

        StringBuilder nombreBuscado = new StringBuilder();
        StringBuilder identificadorBuscado = new StringBuilder();

        if (filtroSoc.getNombre() != null) {
            switch (filtroSoc.getCoincidenciaNombre()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(filtroSoc.getNombre()).append("%");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append("%").append(filtroSoc.getNombre()).append("%");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append("%").append(filtroSoc.getNombre());
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(filtroSoc.getNombre());
                    break;
            }
        }

        if (filtroSoc.getIdentificador() != null) {
            switch (filtroSoc.getCoincidenciaIdentificador()) {
                case 1:
                    identificadorBuscado = (identificadorBuscado).append(filtroSoc.getIdentificador()).append("%");
                    break;
                case 2:
                    identificadorBuscado = (identificadorBuscado).append("%").append(filtroSoc.getIdentificador()).append("%");
                    break;
                case 3:
                    identificadorBuscado = (identificadorBuscado).append("%").append(filtroSoc.getIdentificador());
                    break;
                case 4:
                    identificadorBuscado = (identificadorBuscado).append(filtroSoc.getIdentificador());
                    break;
            }
        }


        try {

            // Filtro: 3- Nombre, identificador, perfiles
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() != null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_NIP_COUNT")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

            // Filtro: 2- Nombre, identificador
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() == null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_NI_COUNT")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

            // Filtro: 2- Nombre, perfiles
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() != null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_NP_COUNT")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

            // Filtro: 2- identificador, perfiles
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() != null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_IP_COUNT")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

            // Filtro: 1- Nombre
            if (filtroSoc.getNombre() != null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() == null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_N_COUNT")
                        .setParameter("nombre", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

            // Filtro: 1- identificador
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() != null && filtroSoc.getPerfiles() == null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_I_COUNT")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        .setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

            // Filtro: 1- Nombre, identificador, perfiles
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() != null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter_P_COUNT")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        .setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

            // Filtro: 0- Nombre, identificador, perfiles
            if (filtroSoc.getNombre() == null && filtroSoc.getIdentificador() == null && filtroSoc.getPerfiles() == null) {
                reply = (Long) em.createNamedQuery("Sociedad.findContUsrSocPrfWith_Filter__COUNT")
                        //.setParameter("nombre",(nombreBuscado==null)?null: nombreBuscado.toString())
                        //.setParameter("nifnrf", identificadorBuscado.toString())
                        //.setParameter("perfiles", filtroSoc.getPerfiles())
                        .getSingleResult();
            }

        } catch (Exception ex) {
            log.warn(ex+ ".............No se ha encontrado Sociedad con nifnrf=" + filtroSoc.getIdentificador());
        }        


        return reply;
    }

    public Long findUsrSocPrf_Con_WithFilter_VIEJO(FiltroSoc filtroSoc) {
        List<Long> reply = null;

        StringBuilder nombreBuscado = new StringBuilder();
        StringBuilder identificadorBuscado = new StringBuilder();
        StringBuilder codper = new StringBuilder();

        StringBuilder qry = new StringBuilder(
                "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
                + "    WHERE ");

        StringBuilder orderBy = new StringBuilder(" ORDER BY  s.nombre, s.id ) AS xyz");
        //StringBuilder poxMaxResult = new StringBuilder(" WHERE xyz.rid BETWEEN " + (filtroSoc.getPaginaActual()-1)* filtroSoc.getNumeroRegistrosPagina() + " AND " +  (filtroSoc.getPaginaActual())* filtroSoc.getNumeroRegistrosPagina());

        if (filtroSoc.getNombre() != null) {
            switch (filtroSoc.getCoincidenciaNombre()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(" s.nombre ").append(" LIKE '").append(filtroSoc.getNombre()).append("%'");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append(" s.nombre ").append(" LIKE '%").append(filtroSoc.getNombre()).append("%'");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append(" s.nombre ").append(" LIKE '%").append(filtroSoc.getNombre()).append("'");
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(" s.nombre ").append(" = '").append(filtroSoc.getNombre()).append("'");
                    break;
            }
            qry = qry.append(nombreBuscado);
        }

        if (filtroSoc.getIdentificador() != null) {
            switch (filtroSoc.getCoincidenciaIdentificador()) {
                case 1:
                    identificadorBuscado = (identificadorBuscado).append(" s.nifnrf ").append(" LIKE '").append(filtroSoc.getIdentificador()).append("%'");
                    break;
                case 2:
                    identificadorBuscado = (identificadorBuscado).append(" s.nifnrf ").append(" LIKE '%").append(filtroSoc.getIdentificador()).append("%'");
                    break;
                case 3:
                    identificadorBuscado = (identificadorBuscado).append(" s.nifnrf ").append(" LIKE '%").append(filtroSoc.getIdentificador()).append("'");
                    break;
                case 4:
                    identificadorBuscado = (identificadorBuscado).append(" s.nifnrf ").append(" = '").append(filtroSoc.getIdentificador()).append("'");
                    break;
            }
            if (filtroSoc.getNombre() != null) {
                qry = qry.append(" AND ");
            }
            qry = qry.append(identificadorBuscado);
        }

        if ((filtroSoc.getPerfiles() != null) && (!filtroSoc.getPerfiles().isEmpty())) {
            StringBuilder perfiles = new StringBuilder();
            for (int ic01 = 0; ic01 < filtroSoc.getPerfiles().size(); ic01++) {
                perfiles.append("'").append(filtroSoc.getPerfiles().get(ic01)).append("'");
                if (ic01 != filtroSoc.getPerfiles().size() - 1) {
                    perfiles.append(", ");
                }
            }
            codper = codper.append(" p.codPerfil ").append(" IN ").append("(").append(perfiles).append(")");
            if (filtroSoc.getNombre() != null || filtroSoc.getIdentificador() != null) {
                qry = qry.append(" AND ");
            }
            qry = qry.append(codper);
        }

        qry.append(orderBy);
        //qry.append(poxMaxResult);

        reply = em.createQuery(qry.toString()).getResultList();

        return reply.get(0);
    }

    
    private Exception Exception(String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
