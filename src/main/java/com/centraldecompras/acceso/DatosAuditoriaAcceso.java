/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.acceso;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Miguel
 */
@Embeddable
public class DatosAuditoriaAcceso implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(DatosAuditoriaAcceso.class.getName());

    @NotNull
    @Column(name = "estadoRegistro", nullable = false, updatable = true)
    private String estadoRegistro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = true, updatable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = true)
    private Date updated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deleted", nullable = true)
    private Date deleted = null;

    @Column(name = "ultimaAccion", nullable = false)
    private String ultimaAccion;

    @Column(name = "usuarioUltimaAccion", nullable = false)
    private String usuarioUltimaAccion;

    @Column(name = "fechaPrevistaActivacion")
    private int fechaPrevistaActivacion;

    @Column(name = "fechaPrevistaDesactivacion")
    private int fechaPrevistaDesactivacion;

    public DatosAuditoriaAcceso() {
    }

    public DatosAuditoriaAcceso(String estadoRegistro, String ultimaAccion, String usuarioUltimaAccion, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion) {
        this.estadoRegistro = estadoRegistro;
        this.ultimaAccion = ultimaAccion;
        this.usuarioUltimaAccion = usuarioUltimaAccion;
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

    public String getEstadoRegistro() {
        return estadoRegistro;
    }

    public void setEstadoRegistro(String estadoRegistro) {
        this.estadoRegistro = estadoRegistro;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    /*
     @PrePersist
     protected void onCreated() {
     created = new Date();
     }
     */

    public Date getUpdated() {
        return updated;
    }
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /*
     @PreUpdate
     protected void onUpdated() {
     updated = new Date();
     }
     */

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public String getUltimaAccion() {
        return ultimaAccion;
    }

    public void setUltimaAccion(String ultimaAccion) {
        this.ultimaAccion = ultimaAccion;
    }

    public String getUsuarioUltimaAccion() {
        return usuarioUltimaAccion;
    }

    public void setUsuarioUltimaAccion(String usuarioUltimaAccion) {
        this.usuarioUltimaAccion = usuarioUltimaAccion;
    }

    public int getFechaPrevistaActivacion() {
        return fechaPrevistaActivacion;
    }

    public void setFechaPrevistaActivacion(int fechaPrevistaActivacion) {
        this.fechaPrevistaActivacion = fechaPrevistaActivacion;
    }

    public int getFechaPrevistaDesactivacion() {
        return fechaPrevistaDesactivacion;
    }

    public void setFechaPrevistaDesactivacion(int fechaPrevistaDesactivacion) {
        this.fechaPrevistaDesactivacion = fechaPrevistaDesactivacion;
    }

}
