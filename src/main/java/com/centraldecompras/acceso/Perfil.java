package com.centraldecompras.acceso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_AUT_perfiles",
        uniqueConstraints = {@UniqueConstraint(columnNames="codPerfil")} )
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Perfil.findPerfilBy_CP", query = "SELECT d FROM Perfil d WHERE trim(d.codPerfil) = trim(:codPerfil)")  
})
public class Perfil implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Perfil.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    /*
    @Id
    @NotNull
    @SequenceGenerator(name = "PERFIL_GEN", sequenceName = "PERFIL_GEN", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "PERFIL_GEN")
    @Column(name = "ID", nullable = false, updatable = false)
    private Long idPerfil;
    */
    
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idPerfil;
    
    @Version
    private int version;

    // Atributos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    @NotNull
    @Column(name = "codPerfil", length = 6)
    private String codPerfil;

    @NotNull
    @Column(name = "descPerfil", length = 45)
    private String descPerfil;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    //@JoinColumn(name = "empleadosSociedades", referencedColumnName = "empleadosSociedades", insertable = false, updatable = false)
    @OneToMany(targetEntity = UsrSocPrf.class, mappedBy = "perfil", fetch = FetchType.EAGER)
    private Collection<UsrSocPrf> prsSocPrfList = new ArrayList<UsrSocPrf>();

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoriaAcceso atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Perfil() {
    }

    public Perfil(String uuid, String codPerfil, String descPerfil, DatosAuditoriaAcceso atributosAuditoria) {
        this.idPerfil = uuid;
        this.codPerfil = codPerfil;
        this.descPerfil = descPerfil;
        this.atributosAuditoria = atributosAuditoria;
    }

    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public String getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(String codPerfil) {
        this.codPerfil = codPerfil;
    }

    public String getDescPerfil() {
        return descPerfil;
    }

    public void setDescPerfil(String descPerfil) {
        this.descPerfil = descPerfil;
    }

    public DatosAuditoriaAcceso getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoriaAcceso atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Collection<UsrSocPrf> getUsrSocPrfList() {
        return this.prsSocPrfList;
    }

    public void setPrsSocPrfList(Collection<UsrSocPrf> prsSocPrfList) {
        this.prsSocPrfList = prsSocPrfList;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *       
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + (this.idPerfil != null ? this.idPerfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Perfil other = (Perfil) obj;
        if ((this.codPerfil == null) ? (other.codPerfil != null) : !this.codPerfil.equals(other.codPerfil)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Perfil{" + "idPerfil=" + idPerfil + ", version=" + version + ", codPerfil=" + codPerfil + ", descPerfil=" + descPerfil + ", prsSocPrfList=" + prsSocPrfList + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
