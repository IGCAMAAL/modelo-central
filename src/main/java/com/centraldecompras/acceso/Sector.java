package com.centraldecompras.acceso;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_AUT_Sector")
@XmlRootElement
public class Sector implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Sector.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSector;

    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String descripcionSector;

    @Basic
    private String driverClassName;
    
    @Basic
    private String urlDB;
    
    @Basic
    private String username;
    
    @Basic
    private String password;
    
    @Basic
    private String hostDestino;
    
    @Basic
    private int puertoDestino;
    
    @Basic
    private int puertoDestinoSSL;
    
    @Basic
    private int ultimoNivel;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoriaAcceso atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Sector() {

    }

    public Sector(String idSector, String descripcionSector, String driverClassName, String urlDB, String username, String password, String hostDestino, int puertoDestino, int puertoDestinoSSL, int ultimoNivel, DatosAuditoriaAcceso atributosAuditoria) {
        this.idSector = idSector;
        this.descripcionSector = descripcionSector;
        this.driverClassName = driverClassName;
        this.urlDB = urlDB;
        this.username = username;
        this.password = password;
        this.hostDestino = hostDestino;
        this.puertoDestino = puertoDestino;
        this.puertoDestinoSSL = puertoDestinoSSL;
        this.ultimoNivel = ultimoNivel;
        this.atributosAuditoria = atributosAuditoria;
    }
       
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getIdSector() {
        return this.idSector;
    }

    public void setIdSector(String idSector) {
        this.idSector = idSector;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getDescripcionSector() {
        return this.descripcionSector;
    }

    public void setDescripcionSector(String descripcionSector) {
        this.descripcionSector = descripcionSector;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrlDB() {
        return urlDB;
    }

    public void setUrlDB(String urlDB) {
        this.urlDB = urlDB; 
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUltimoNivel() {
        return ultimoNivel;
    }

    public void setUltimoNivel(int ultimoNivel) {
        this.ultimoNivel = ultimoNivel;
    }

    public String getHostDestino() {
        return hostDestino;
    }

    public void setHostDestino(String hostDestino) {
        this.hostDestino = hostDestino;
    }

    public int getPuertoDestino() {
        return puertoDestino;
    }

    public void setPuertoDestino(int puertoDestino) {
        this.puertoDestino = puertoDestino;
    }

    public int getPuertoDestinoSSL() {
        return puertoDestinoSSL;
    }

    public void setPuertoDestinoSSL(int puertoDestinoSSL) {
        this.puertoDestinoSSL = puertoDestinoSSL;
    }


    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoriaAcceso getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoriaAcceso atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.descripcionSector);
        hash = 97 * hash + Objects.hashCode(this.driverClassName);
        hash = 97 * hash + Objects.hashCode(this.urlDB);
        hash = 97 * hash + Objects.hashCode(this.username);
        hash = 97 * hash + Objects.hashCode(this.password);
        hash = 97 * hash + Objects.hashCode(this.hostDestino);
        hash = 97 * hash + this.puertoDestino;
        hash = 97 * hash + this.puertoDestinoSSL;
        hash = 97 * hash + this.ultimoNivel;
        hash = 97 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sector other = (Sector) obj;
        if (!Objects.equals(this.descripcionSector, other.descripcionSector)) {
            return false;
        }
        if (!Objects.equals(this.driverClassName, other.driverClassName)) {
            return false;
        }
        if (!Objects.equals(this.urlDB, other.urlDB)) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.hostDestino, other.hostDestino)) {
            return false;
        }
        if (this.puertoDestino != other.puertoDestino) {
            return false;
        }
        if (this.puertoDestinoSSL != other.puertoDestinoSSL) {
            return false;
        }
        if (this.ultimoNivel != other.ultimoNivel) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sector{" + "idSector=" + idSector + ", version=" + version + ", descripcionSector=" + descripcionSector + ", driverClassName=" + driverClassName + ", urlDB=" + urlDB + ", username=" + username + ", password=" + password + ", hostDestino=" + hostDestino + ", puertoDestino=" + puertoDestino + ", puertoDestinoSSL=" + puertoDestinoSSL + ", ultimoNivel=" + ultimoNivel + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


}
