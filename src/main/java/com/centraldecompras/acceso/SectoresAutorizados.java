package com.centraldecompras.acceso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_AUT_SectoresAutorizados", 
     uniqueConstraints = {
         @UniqueConstraint(columnNames={"prsSocPrf", "sector"})
     }
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SectoresAutorizados.findAll", query = ""
            + " SELECT d "
            + "  FROM SectoresAutorizados d "),
    @NamedQuery(name = "SectoresAutorizados.findByPrsSocPrf", query = ""
            + " SELECT d "
            + "   FROM SectoresAutorizados d "
            + "  WHERE d.prsSocPrf  = :prsSocPrf "),
    @NamedQuery(name = "SectoresAutorizados.findUSP_By_Sid", query = ""
            + " SELECT d "
            + "   FROM SectoresAutorizados d "
            + "  WHERE d.sector  = :sector "),
    @NamedQuery(name = "SectoresAutorizados.findUsrSocBy_USP_Sid", query = ""
            + "  SELECT d "
            + "    FROM SectoresAutorizados d "
            + "   WHERE d.prsSocPrf = :prsSocPrf "
            + "     AND d.sector = :sector ")
})

public class SectoresAutorizados implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SectoresAutorizados.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSectoresAutorizados;

    @Version
    private int version;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    int cargaDirectaArticulos;      // 0=NO puede cargar articulos;  1=Carga con aprobacion,        2=Carga Directa (Sin aprobacion)
    int modPreciosBaja;             // 0=NO puede modificar precios; 1=Modicacion con aprobacion,   2=Modificacion directa
    @NotNull
    BigDecimal porcentajeMaxBaja;
    int modPreciosAlta;             // 0=N= puede modificar precios; 1=Modicacion con aprobacion,   2=Modificacion directa
    @NotNull
    BigDecimal porcentajeMaxAlta;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = UsrSocPrf.class)
    @JoinColumn(name = "prsSocPrf", updatable = false)
    private UsrSocPrf prsSocPrf;

    @ManyToOne(targetEntity = Sector.class)
    @JoinColumn(name = "sector", updatable = false)
    private Sector sector;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoriaAcceso atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SectoresAutorizados() {
    }

    public SectoresAutorizados(String idSectoresAutorizados, int cargaDirectaArticulos, int modPreciosBaja, BigDecimal porcentajeMaxBaja, int modPreciosAlta, BigDecimal porcentajeMaxAlta, UsrSocPrf prsSocPrf, Sector sector, DatosAuditoriaAcceso atributosAuditoria) {
        this.idSectoresAutorizados = idSectoresAutorizados;
        this.cargaDirectaArticulos = cargaDirectaArticulos;
        this.modPreciosBaja = modPreciosBaja;
        this.porcentajeMaxBaja = porcentajeMaxBaja;
        this.modPreciosAlta = modPreciosAlta;
        this.porcentajeMaxAlta = porcentajeMaxAlta;
        this.prsSocPrf = prsSocPrf;
        this.sector = sector;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public String getIdSectoresAutorizados() {
        return idSectoresAutorizados;
    }

    public void setIdSectoresAutorizados(String idSectoresAutorizados) {
        this.idSectoresAutorizados = idSectoresAutorizados;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public int getCargaDirectaArticulos() {
        return cargaDirectaArticulos;
    }

    public void setCargaDirectaArticulos(int cargaDirectaArticulos) {
        this.cargaDirectaArticulos = cargaDirectaArticulos;
    }

    public int getModPreciosBaja() {
        return modPreciosBaja;
    }

    public void setModPreciosBaja(int modPreciosBaja) {
        this.modPreciosBaja = modPreciosBaja;
    }

    public BigDecimal getPorcentajeMaxBaja() {
        return porcentajeMaxBaja;
    }

    public void setPorcentajeMaxBaja(BigDecimal porcentajeMaxBaja) {
        this.porcentajeMaxBaja = porcentajeMaxBaja;
    }

    public int getModPreciosAlta() {
        return modPreciosAlta;
    }

    public void setModPreciosAlta(int modPreciosAlta) {
        this.modPreciosAlta = modPreciosAlta;
    }

    public BigDecimal getPorcentajeMaxAlta() {
        return porcentajeMaxAlta;
    }

    public void setPorcentajeMaxAlta(BigDecimal porcentajeMaxAlta) {
        this.porcentajeMaxAlta = porcentajeMaxAlta;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public UsrSocPrf getPrsSocPrf() {
        return prsSocPrf;
    }

    public void setPrsSocPrf(UsrSocPrf prsSocPrf) {
        this.prsSocPrf = prsSocPrf;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoriaAcceso getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoriaAcceso atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.prsSocPrf);
        hash = 89 * hash + Objects.hashCode(this.sector);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SectoresAutorizados other = (SectoresAutorizados) obj;
        if (!Objects.equals(this.prsSocPrf, other.prsSocPrf)) {
            return false;
        }
        if (!Objects.equals(this.sector, other.sector)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SectoresAutorizados{" + "idSectoresAutorizados=" + idSectoresAutorizados + ", version=" + version + ", prsSocPrf=" + prsSocPrf + ", sector=" + sector + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
