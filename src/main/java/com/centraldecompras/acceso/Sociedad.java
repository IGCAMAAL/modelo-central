package com.centraldecompras.acceso;


import com.centraldecompras.acceso.UsrSoc;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_AUT_sociedades", 
     //  indexes = {@Index (columnList = "nifnrf")},
     uniqueConstraints = {@UniqueConstraint(columnNames={"nifnrf", "tipoIdentificador"})} 
) 
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sociedad.findSociedadBy_TNR_NR", query = 
            " SELECT d "
          + "   FROM Sociedad d "
          + "  WHERE trim(d.nifnrf) = trim(:nifnrf) "
          + "    AND trim(d.tipoidentificador) = trim(:tipoIdentificador)"),
    
    
    
    
    
    
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_NIP", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
                + "      AND s.nifnrf LIKE :nifnrf "
                + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_NI", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
                + "      AND s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_NP", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
                + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_IP", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
          //    + "    WHERE s.nombre LIKE :nombre "
                + "    WHERE s.nifnrf LIKE :nifnrf "
                + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),

    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_N", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_I", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
          //    + "    WHERE s.nombre LIKE :nombre "
                + "    WHERE s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_P", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
                        
          //    + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
                + "    WHERE p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_", query = 
                  "   SELECT DISTINCT s.id "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
          //    + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
        
        
        
        
    
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_NIP_COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
                + "      AND s.nifnrf LIKE :nifnrf "
                + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_NI_COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
                + "      AND s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_NP_COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
                + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_IP_COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
          //    + "    WHERE s.nombre LIKE :nombre "
                + "    WHERE s.nifnrf LIKE :nifnrf "
                + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),

    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_N_COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
                + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_I_COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
          //    + "    WHERE s.nombre LIKE :nombre "
                + "    WHERE s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter_P_COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
                + "     JOIN usp.perfil p "
                        
          //    + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
                + "    WHERE p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 ),
    @NamedQuery(name = "Sociedad.findContUsrSocPrfWith_Filter__COUNT", query = 
                  "   SELECT COUNT(DISTINCT s.id) "
                + "     FROM UsrSocPrf usp "
                + "     JOIN usp.usrSoc us "   
                + "     JOIN us.sociedad s "
          //    + "     JOIN usp.perfil p "
                        
          //    + "    WHERE s.nombre LIKE :nombre "
          //    + "      AND s.nifnrf LIKE :nifnrf "
          //    + "      AND p.codPerfil IN (:perfiles)" 
                + " ORDER BY s.nombre, s.id "
                 )
        
        
})
/*
 @org.hibernate.annotations.Table(
 appliesTo = "PUR_ENTIDAD", indexes = {
 @Index(name = "IDX_nombre", columnNames = "nombre")
 }
 )
 */
public class Sociedad implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Sociedad.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSociedad;

    @Version
    private int version;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @NotNull
    private String tipoidentificador;
    
    @NotNull
    private String nifnrf;

    @NotNull
    private String nombre;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @OneToMany(targetEntity = UsrSoc.class, mappedBy = "sociedad", fetch = FetchType.EAGER)
    private Collection<UsrSoc> prsSocList = new ArrayList<UsrSoc>();
    
    @Column(name = "fabrica", nullable = true)
    private String fabrica;

    @ManyToMany(targetEntity = Sociedad.class, fetch = FetchType.EAGER)
    private Collection<Sociedad> distribuidoras;

    @ManyToMany(targetEntity = Sociedad.class, mappedBy = "distribuidoras", fetch = FetchType.EAGER)
    private Collection<Sociedad> fabricantes;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoriaAcceso atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Sociedad() {
    }

    public Sociedad(String idSociedad, String tipoidentificador, String nifnrf, String nombre, String fabrica, DatosAuditoriaAcceso atributosAuditoria) {
        this.idSociedad = idSociedad;
        this.tipoidentificador = tipoidentificador;
        this.nifnrf = nifnrf;
        this.nombre = nombre;
        this.fabrica = fabrica;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    
    // Metodos  ID -----------------------------------------------------------------------------------------------     
    public String getIdSociedad() {
        return idSociedad;
    }

    public void setIdSociedad(String idSociedad) {
        this.idSociedad = idSociedad;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getTipoidentificador() {
        return tipoidentificador;
    }

    public void setTipoidentificador(String tipoidentificador) {
        this.tipoidentificador = tipoidentificador;
    }

    public String getNifnrf() {
        return nifnrf;
    }
    public void setNifnrf(String nifnrf) {
        this.nifnrf = nifnrf;
    }
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // Métodos relacionales  * * * * * * * * * * * * * * * * * * * * * * * * * *   
    @XmlTransient
    public Collection<UsrSoc> getPrsSocList() {
        return prsSocList;
    }

    public void setPrsSocList(Collection<UsrSoc> prsSocList) {
        this.prsSocList = prsSocList;
    }

    public String getFabrica() {
        return fabrica;
    }

    public void setFabrica(String fabrica) {
        this.fabrica = fabrica;
    }

    public Collection<Sociedad> getDistribuidoras() {
        return distribuidoras;
    }

    public void setDistribuidoras(Collection<Sociedad> distribuidoras) {
        this.distribuidoras = distribuidoras;
    }

    public Collection<Sociedad> getFabricantes() {
        return fabricantes;
    }

    public void setFabricantes(Collection<Sociedad> fabricantes) {
        this.fabricantes = fabricantes;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoriaAcceso getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoriaAcceso atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.idSociedad != null ? this.idSociedad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sociedad other = (Sociedad) obj;
        if ((this.nifnrf == null) ? (other.nifnrf != null) : !this.nifnrf.equals(other.nifnrf)) {
            return false;
        }
        if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
            return false;
        }
        return true;
    }

}
