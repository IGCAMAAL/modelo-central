package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_UnidadMedidaLang",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"idioma", "traduccionDesc"})
        })
@XmlRootElement
//@IdClass(UnidadMedidaLangPK.class)
@NamedQueries({                          
    @NamedQuery(name = "UnidadMedidaLang.findUnidadMedidaLangBy_UM", query = ""
            + " SELECT d "
            + "   FROM UnidadMedidaLang d "
            + "  WHERE d.unidadMedidaEntity = :unidadMedida "),
    
    @NamedQuery(name = "UnidadMedidaLang.findUnidadMedidaBy_UM_Lang", query = ""
            + " SELECT d "
            + "   FROM UnidadMedidaLang d "
            + "  WHERE d.unidadMedidaEntity = :unidadMedida "
            + "    AND d.idioma = :idioma "),
    
    @NamedQuery(name = "UnidadMedidaLang.findUnidadMedidaLangBy_IdFV_Lang", query = ""
            + " SELECT d "
            + "   FROM UnidadMedidaLang d "
            + "  WHERE d.unidadMedidaId = :unidadMedidaId "
            + "    AND d.idioma = :idioma "),

    @NamedQuery(name = "UnidadMedidaLang.findUnidadMedidaLangBy_Lang", query = ""
            + " SELECT d.traduccionDesc, d.unidadMedidaId "
            + "   FROM UnidadMedidaLang d "
            + "  WHERE d.idioma = :idioma "),
    
    @NamedQuery(name = "UnidadMedidaLang.deleteUnidadMedidaLangBy_idUM_Lang", query = ""
            + " DELETE "
            + "   FROM UnidadMedidaLang d "
            + "  WHERE d.unidadMedidaId = :unidadMedidaId "
            + "    AND d.idioma = :idioma ")
})
public class UnidadMedidaLang implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(UnidadMedidaLang.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String unidadMedidaId;

    @Id
    @NotNull
    private String idioma;
    
    @Version
    private int version;
    
    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private String traduccionDesc;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull    
    @ManyToOne(targetEntity=UnidadMedida.class)
    private UnidadMedida unidadMedidaEntity;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;
    
    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UnidadMedidaLang() {
    }

    public UnidadMedidaLang(String unidadMedidaId, String idioma, int version, String traduccionDesc, UnidadMedida unidadMedidaEntity, DatosAuditoria atributosAuditoria) {
        this.unidadMedidaId = unidadMedidaId;
        this.idioma = idioma;
        this.version = version;
        this.traduccionDesc = traduccionDesc;
        this.unidadMedidaEntity = unidadMedidaEntity;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public String getUnidadMedidaId() {
        return unidadMedidaId;
    }

    public void setUnidadMedidaId(String unidadMedidaId) {
        this.unidadMedidaId = unidadMedidaId;
    }


    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getTraduccionDesc() {
        return traduccionDesc;
    }

    public void setTraduccionDesc(String traduccionDesc) {
        this.traduccionDesc = traduccionDesc;
    }
    
    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UnidadMedida getUnidadMedidaEntity() {
        return unidadMedidaEntity;
    }

    public void setUnidadMedidaEntity(UnidadMedida unidadMedidaEntity) {
        this.unidadMedidaEntity = unidadMedidaEntity;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.idioma);
        hash = 13 * hash + Objects.hashCode(this.traduccionDesc);
        hash = 13 * hash + Objects.hashCode(this.unidadMedidaEntity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadMedidaLang other = (UnidadMedidaLang) obj;
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        if (!Objects.equals(this.traduccionDesc, other.traduccionDesc)) {
            return false;
        }
        if (!Objects.equals(this.unidadMedidaEntity, other.unidadMedidaEntity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnidadMedidaLang{" + "unidadMedidaId=" + unidadMedidaId + ", idioma=" + idioma + ", version=" + version + ", traduccionDesc=" + traduccionDesc + ", unidadMedidaEntity=" + unidadMedidaEntity + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


}
