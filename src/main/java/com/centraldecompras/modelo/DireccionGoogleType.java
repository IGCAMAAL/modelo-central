package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_CMO_direccionGoogleType",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"type", "direccionidk", "direcciongooglegomponenteid"})}
)

@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DireccionGoogleType.findDireccionGoogleTypeBy_TDC", query = " "
            + " SELECT d "
            + "   FROM DireccionGoogleType d "
            + "  WHERE d.type = :tipoDireccion "
            + "    AND d.direccionidk = :direccionid "
            + "    AND d.direcciongooglegomponenteid = :direcciongooglegomponenteid "
    ),

    @NamedQuery(name = "DireccionGoogleType.findDireccionesGoogleTypeBy_D", query = " "
            + " SELECT d "
            + "   FROM DireccionGoogleType d "
            + "  WHERE d.direccionidk = :direccionid "
    ),
    @NamedQuery(name = "DireccionGoogleType.findDireccionesGoogleTypeBy_DC", query = " "
            + " SELECT d "
            + "   FROM DireccionGoogleType d "
            + "  WHERE d.direcciongooglegomponenteid = :direcciongooglegomponenteid"
    ),

    @NamedQuery(name = "DireccionGoogleType.destroyTypeNoRecivedD", query = " "
            + " DELETE "
            + "   FROM DireccionGoogleType d "
            + "  WHERE d.direccionidk = :direccionid "
    ),
    @NamedQuery(name = "DireccionGoogleType.destroyTypeNoRecivedC", query = " "
            + " DELETE "
            + "   FROM DireccionGoogleType d "
            + "  WHERE d.direcciongooglegomponenteid = :direcciongooglegomponenteid"
    ),

    @NamedQuery(name = "DireccionGoogleType.destroyDireccionesGoogleTypeBy_D", query = " "
            + " DELETE "
            + "   FROM DireccionGoogleType d "
            + "  WHERE d.direccionidk = :direccionid "
    ),
    @NamedQuery(name = "DireccionGoogleType.destroyDireccionesGoogleTypeBy_DC", query = " "
            + " DELETE "
            + "   FROM DireccionGoogleType d "
            + "  WHERE d.direcciongooglegomponenteid = :direcciongooglegomponenteid "
    )
})

public class DireccionGoogleType implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(DireccionGoogleType.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idDireccionGoogleType;

    @Version
    private int version;

    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private String type;

    private String direcciongooglegomponenteid;

    private String direccionidk;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = DireccionGoogleComponente.class, fetch = FetchType.EAGER)
    private DireccionGoogleComponente direccionGoogleComponente;

    @ManyToOne(targetEntity = Direccion.class, fetch = FetchType.EAGER)
    private Direccion direccion;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public DireccionGoogleType() {

    }

    public DireccionGoogleType(String idDireccionGoogleType, String type, String direcciongooglegomponenteid, String direccionidk, DireccionGoogleComponente direccionGoogleComponente, Direccion direccion, DatosAuditoria atributosAuditoria) {
        this.idDireccionGoogleType = idDireccionGoogleType;
        this.type = type;
        this.direcciongooglegomponenteid = direcciongooglegomponenteid;
        this.direccionidk = direccionidk;
        this.direccionGoogleComponente = direccionGoogleComponente;
        this.direccion = direccion;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getIdDireccionGoogleType() {
        return idDireccionGoogleType;
    }

    public void setIdDireccionGoogleType(String idDireccionGoogleType) {
        this.idDireccionGoogleType = idDireccionGoogleType;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirecciongooglegomponenteid() {
        return direcciongooglegomponenteid;
    }

    public void setDirecciongooglegomponenteid(String direcciongooglegomponenteid) {
        this.direcciongooglegomponenteid = direcciongooglegomponenteid;
    }

    public String getDireccionidk() {
        return direccionidk;
    }

    public void setDireccionidk(String direccionidk) {
        this.direccionidk = direccionidk;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DireccionGoogleComponente getDireccionGoogleComponente() {
        return direccionGoogleComponente;
    }

    public void setDireccionGoogleComponente(DireccionGoogleComponente direccionGoogleComponente) {
        this.direccionGoogleComponente = direccionGoogleComponente;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.type);
        hash = 47 * hash + Objects.hashCode(this.direccionidk);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DireccionGoogleType other = (DireccionGoogleType) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.direcciongooglegomponenteid, other.direcciongooglegomponenteid)) {
            return false;
        }
        if (!Objects.equals(this.direccionidk, other.direccionidk)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DireccionGoogleType{" + "idDireccionGoogleType=" + idDireccionGoogleType + ", version=" + version + ", type=" + type + ", direcciongooglegomponenteid=" + direcciongooglegomponenteid + ", direccionid=" + direccionidk + ", direccionGoogleComponente=" + direccionGoogleComponente + ", direccion=" + direccion + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
