package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_NivelProductoLang",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"idioma", "traduccionDesc"})
        }
)
@XmlRootElement
//@IdClass(NivelProductoLangPK.class)
@NamedQueries({
    @NamedQuery(name = "NivelProductoLang.findNivelProductoLangBy_NP", query = ""
            + " SELECT d FROM NivelProductoLang d "
            + "  WHERE d.nivelProductoEntity = :nivelProducto"),

    @NamedQuery(name = "NivelProductoLang.findNivelProductoLangBy_IdNP_Lang", query = ""
            + " SELECT d "
            + "   FROM NivelProductoLang d "
            + "  WHERE d.nivelProductoId = :nivelProductoId "
            + "    AND d.idioma = :idioma"),

    @NamedQuery(name = "NivelProductoLang.findProductoLangKey", query = ""
            + "   SELECT distinct d.idioma"
            + "     FROM NivelProductoLang d "
            + " GROUP BY d.idioma "),
    
    @NamedQuery(name = "NivelProductoLang.deleteNivelProductoLangBy_IdNP_Lang", query = ""
            + " DELETE "
            + "   FROM NivelProductoLang d "
            + "  WHERE d.nivelProductoId = :nivelProductoId "
            + "    AND d.idioma = :idioma"),
})
public class NivelProductoLang implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(NivelProductoLang.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private int nivelProductoId;

    @Id
    @Basic
    private String idioma;

    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String traduccionDesc;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    @ManyToOne(targetEntity = NivelProducto.class)
    private NivelProducto nivelProductoEntity;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public NivelProductoLang() {
    }

    public NivelProductoLang(int nivelProductoId, String idioma, String traduccionDesc, NivelProducto nivelProductoEntity, DatosAuditoria atributosAuditoria) {
        this.nivelProductoId = nivelProductoId;
        this.idioma = idioma;
        this.traduccionDesc = traduccionDesc;
        this.nivelProductoEntity = nivelProductoEntity;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public int getNivelProductoId() {
        return nivelProductoId;
    }

    public void setNivelProductoId(int nivelProductoId) {
        this.nivelProductoId = nivelProductoId;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getTraduccionDesc() {
        return traduccionDesc;
    }

    public void setTraduccionDesc(String traduccionDesc) {
        this.traduccionDesc = traduccionDesc;
    }

    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public NivelProducto getNivelProductoEntity() {
        return nivelProductoEntity;
    }

    public void setNivelProductoEntity(NivelProducto nivelProductoEntity) {
        this.nivelProductoEntity = nivelProductoEntity;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.idioma);
        hash = 89 * hash + Objects.hashCode(this.traduccionDesc);
        hash = 89 * hash + Objects.hashCode(this.nivelProductoEntity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NivelProductoLang other = (NivelProductoLang) obj;
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        if (!Objects.equals(this.traduccionDesc, other.traduccionDesc)) {
            return false;
        }
        if (!Objects.equals(this.nivelProductoEntity, other.nivelProductoEntity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NivelProductoLang{" + "nivelProductoId=" + nivelProductoId + ", idioma=" + idioma + ", version=" + version + ", traduccionDesc=" + traduccionDesc + ", nivelProductoEntity=" + nivelProductoEntity + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
