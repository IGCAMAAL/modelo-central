package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_MAT_ProductoEtiqueta")
@XmlRootElement
@NamedQueries({                          
   @NamedQuery(name = "ProductoEtiqueta.findEtiquetasBy_idP", query = ""
           + "  SELECT d "
           + "    FROM ProductoEtiqueta d "
           + "   WHERE d.productoIdk = :productoIdk "
   ),
    
   @NamedQuery(name = "ProductoEtiqueta.findProductoEtiquetaBy_idP_idE", query = ""
           + "  SELECT d "
           + "    FROM ProductoEtiqueta d "
           + "   WHERE d.productoIdk = :productoIdk "
           + "     AND d.etiquetaIdk = :etiquetaIdk "
   ),
   
   @NamedQuery(name = "ProductoEtiqueta.deleteProductoEtiquetaBy_idP_idE", query = ""
           + "  DELETE "
           + "    FROM ProductoEtiqueta d "
           + "   WHERE d.productoIdk = :productoIdk "
           + "     AND d.etiquetaIdk = :etiquetaIdk "
   ), 
    @NamedQuery(name = "ProductoEtiqueta.findProductWithLabels", query
            = " SELECT DISTINCT(p_01.idProducto)  "
                    
            + "   FROM Producto           p_01, "                    
            + "        ProductoEtiqueta   pe "
                    
            + "  WHERE p_01.route                                   LIKE  ( SELECT p_02.route            "
            + "                                                               FROM Producto p_02         "
            + "                                                              WHERE p_02.id = :productoId "
            + "                                                           )                              "
            + "                                                           || '%'                         "
                    
            + "    AND p_01.routeType                                     =  'Z'                         "
                    
            + "    AND pe.etiqueta.idEtiqueta                         IN  ( :etiquetasId)  "                    
            + "    AND pe.atributosAuditoria.estadoRegistro           IN  ( :estados)      "                      
            + "    AND pe.etiqueta.atributosAuditoria.estadoRegistro  IN  ( :estados)      "                        
    )    
})
public class ProductoEtiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ProductoEtiqueta.class.getName());


    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id@NotNull@Column(name = "productoIdk", length = 36, nullable = false, updatable = false)
    private String productoIdk;

    @Id@NotNull@Column(name = "etiquetaIdk", length = 36, nullable = false, updatable = false)
    private String etiquetaIdk;
    
    @Version
    private int version;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *    
    @ManyToOne(targetEntity = Producto.class)
    private Producto producto;

    @ManyToOne(targetEntity = Etiqueta.class)
    private Etiqueta etiqueta;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ProductoEtiqueta() {
    }

    public ProductoEtiqueta(String productoIdk, String etiquetaIdk, Producto producto, Etiqueta etiqueta, DatosAuditoria atributosAuditoria) {
        this.productoIdk = productoIdk;
        this.etiquetaIdk = etiquetaIdk;
        this.producto = producto;
        this.etiqueta = etiqueta;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getProductoIdk() {
        return productoIdk;
    }

    public void setProductoIdk(String productoIdk) {
        this.productoIdk = productoIdk;
    }

    public String getEtiquetaIdk() {
        return etiquetaIdk;
    }

    public void setEtiquetaIdk(String etiquetaIdk) {
        this.etiquetaIdk = etiquetaIdk;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Etiqueta getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(Etiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.productoIdk);
        hash = 31 * hash + Objects.hashCode(this.etiquetaIdk);
        hash = 31 * hash + Objects.hashCode(this.producto);
        hash = 31 * hash + Objects.hashCode(this.etiqueta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoEtiqueta other = (ProductoEtiqueta) obj;
        if (!Objects.equals(this.productoIdk, other.productoIdk)) {
            return false;
        }
        if (!Objects.equals(this.etiquetaIdk, other.etiquetaIdk)) {
            return false;
        }
        if (!Objects.equals(this.producto, other.producto)) {
            return false;
        }
        if (!Objects.equals(this.etiqueta, other.etiqueta)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProductoEtiqueta{" + "productoIdk=" + productoIdk + ", etiquetaIdk=" + etiquetaIdk + ", version=" + version + ", producto=" + producto + ", etiqueta=" + etiqueta + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
