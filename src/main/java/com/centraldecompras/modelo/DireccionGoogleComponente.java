package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_CMO_direccionGoogleComponente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DireccionGoogleComponente.findDireccionGoogleComponenteBy_idD", query = " "
        + " SELECT d "
        + "   FROM DireccionGoogleComponente d "
        + "  WHERE d.direccion = :direccion " )
})
public class DireccionGoogleComponente implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(DireccionGoogleComponente.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idDireccionGoogleComponente;

    @Version
    private int version;

    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private String shortName;

    @NotNull
    private String longName;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = Direccion.class, fetch = FetchType.EAGER)
    private Direccion direccion;

    @OneToMany(targetEntity = DireccionGoogleType.class, mappedBy = "direccionGoogleComponente", fetch = FetchType.EAGER)
    private Collection<DireccionGoogleType> direccionGoogleType;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public DireccionGoogleComponente() {
    }
    
    public DireccionGoogleComponente(String idDireccionGoogleComponente, String shortName, String longName, Direccion direccion, Collection<DireccionGoogleType> direccionGoogleType, DatosAuditoria atributosAuditoria) {
        this.idDireccionGoogleComponente = idDireccionGoogleComponente;
        this.shortName = shortName;
        this.longName = longName;
        this.direccion = direccion;
        this.direccionGoogleType = direccionGoogleType;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getIdDireccionGoogleComponente() {
        return idDireccionGoogleComponente;
    }

    public void setIdDireccionGoogleComponente(String idDireccionGoogleComponente) {
        this.idDireccionGoogleComponente = idDireccionGoogleComponente;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos BAsicos  * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    // Métodos BAsicos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Collection<DireccionGoogleType> getDireccionGoogleType() {
        return direccionGoogleType;
    }

    public void setDireccionGoogleType(Collection<DireccionGoogleType> direccionGoogleType) {
        this.direccionGoogleType = direccionGoogleType;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

   // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

// Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.shortName != null ? this.shortName.hashCode() : 0);
        hash = 89 * hash + (this.longName != null ? this.longName.hashCode() : 0);
        hash = 89 * hash + (this.direccionGoogleType != null ? this.direccionGoogleType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DireccionGoogleComponente other = (DireccionGoogleComponente) obj;
        if ((this.shortName == null) ? (other.shortName != null) : !this.shortName.equals(other.shortName)) {
            return false;
        }
        if ((this.longName == null) ? (other.longName != null) : !this.longName.equals(other.longName)) {
            return false;
        }
        if (this.direccionGoogleType != other.direccionGoogleType && (this.direccionGoogleType == null || !this.direccionGoogleType.equals(other.direccionGoogleType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DireccionGoogleComponente{" + "idDireccionGoogleComponente=" + idDireccionGoogleComponente + ", version=" + version + ", shortName=" + shortName + ", longName=" + longName + ", direccion=" + direccion + ", direccionGoogleType=" + direccionGoogleType + ", atributosAuditoria=" + atributosAuditoria + '}';
    }
 
    
}
