package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_LGT_SocFab")
@XmlRootElement
public class SocFab implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SocFab.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl socDist;

    @Id
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl socFabr;

    @Version
    private int version;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    
    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SocFab() {

    }

    public SocFab(SociedadRpl socDist, SociedadRpl socFabr, DatosAuditoria atributosAuditoria) {
        this.socDist = socDist;
        this.socFabr = socFabr;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SociedadRpl getSocDist() {
        return this.socDist;
    }

    public void setSocDist(SociedadRpl socDist) {
        this.socDist = socDist;
    }

    public SociedadRpl getSocFabr() {
        return this.socFabr;
    }

    public void setSocFabr(SociedadRpl socFabr) {
        this.socFabr = socFabr;
    }    

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
 
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.socDist != null ? this.socDist.hashCode() : 0);
        hash = 97 * hash + (this.socFabr != null ? this.socFabr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SocFab other = (SocFab) obj;
        if (this.socDist != other.socDist && (this.socDist == null || !this.socDist.equals(other.socDist))) {
            return false;
        }
        if (this.socFabr != other.socFabr && (this.socFabr == null || !this.socFabr.equals(other.socFabr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SocFab{" + "socDist=" + socDist.toString() + ", socFabr=" + socFabr.toString() + ", version=" + version + ", atributosAuditoria=" + atributosAuditoria.toString() + '}';
    }

    
}
