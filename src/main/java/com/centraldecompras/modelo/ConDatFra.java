package com.centraldecompras.modelo;

import java.io.Serializable;

import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_VEN_CondicionesDatosFacturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConDatFra.findConDatFraBy_idS", query = " "
        + " SELECT d "
        + "   FROM ConDatFra d "
        + "  WHERE d.sociedad.id = :sociedadid "     
    )
})
public class ConDatFra implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ConDatFra.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idConDatFra;

    @Version
    private int version;

    private String regType; // CN=Condicion, DF=Dato Facturacion
    
    // Atributos de basicos / relacion * * * * * * * * * * * * * * * * * * * * *
    private String condDatfra;
                        
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = SociedadRpl.class, fetch = FetchType.EAGER)
    private SociedadRpl sociedad;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public ConDatFra() {
    }

    public ConDatFra(String idConDatFra, String regType, String condDatfra, SociedadRpl sociedad, DatosAuditoria atributosAuditoria) {
        this.idConDatFra = idConDatFra;
        this.regType = regType;
        this.condDatfra = condDatfra;
        this.sociedad = sociedad;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public void setIdConDatFra(String idConDatFra) {
        this.idConDatFra = idConDatFra;
    }
    
    public String getIdConDatFra() {
        return idConDatFra;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Atributos de basicos / relacion * * * * * * * * * * * * * * * * * * * * *
    public String getRegType() {
        return regType;
    }

    public void setRegType(String regType) {
        this.regType = regType;
    }

    public String getCondDatfra() {
        return condDatfra;
    }

    public void setCondDatfra(String condDatfra) {
        this.condDatfra = condDatfra;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public SociedadRpl getSociedad() {
        return this.sociedad;
    }

    public void setSociedad(SociedadRpl sociedad) {
        this.sociedad = sociedad;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.regType);
        hash = 97 * hash + Objects.hashCode(this.condDatfra);
        hash = 97 * hash + Objects.hashCode(this.sociedad);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConDatFra other = (ConDatFra) obj;
        if (!Objects.equals(this.regType, other.regType)) {
            return false;
        }
        if (!Objects.equals(this.condDatfra, other.condDatfra)) {
            return false;
        }
        if (!Objects.equals(this.sociedad, other.sociedad)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ConDatFra{" + "idConDatFra=" + idConDatFra + ", version=" + version + ", regType=" + regType + ", condDatfra=" + condDatfra + ", sociedad=" + sociedad + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
