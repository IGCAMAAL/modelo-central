package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_Etiqueta")
@XmlRootElement
public class Etiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Etiqueta.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idEtiqueta;
    
    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private String etiquetaCod;

    @NotNull
    private String etiquetaDesc;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Etiqueta() {
    }

    public Etiqueta(String idEtiqueta, String etiquetaCod, String etiquetaDesc, DatosAuditoria atributosAuditoria) {
        this.idEtiqueta = idEtiqueta;
        this.etiquetaCod = etiquetaCod;
        this.etiquetaDesc = etiquetaDesc;
        this.atributosAuditoria = atributosAuditoria;
    }

    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public String getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(String idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    public String getEtiquetaCod() {
        return etiquetaCod;
    }

    public void setEtiquetaCod(String etiquetaCod) {
        this.etiquetaCod = etiquetaCod;
    }

    public String getEtiquetaDesc() {
        return etiquetaDesc;
    }

    public void setEtiquetaDesc(String etiquetaDesc) {
        this.etiquetaDesc = etiquetaDesc;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.idEtiqueta);
        hash = 41 * hash + Objects.hashCode(this.etiquetaCod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Etiqueta other = (Etiqueta) obj;
        if (!Objects.equals(this.idEtiqueta, other.idEtiqueta)) {
            return false;
        }
        if (!Objects.equals(this.etiquetaCod, other.etiquetaCod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Etiqueta{" + "idEtiqueta=" + idEtiqueta + ", version=" + version + ", etiquetaCod=" + etiquetaCod + ", etiquetaDesc=" + etiquetaDesc + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


}
