package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_EtiquetaLang",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"idioma", "traduccionDesc"})
        })
@XmlRootElement
//@IdClass(EtiquetaLangPK.class)
@NamedQueries({
    @NamedQuery(name = "EtiquetaLang.findEtiquetaLangBy_E", query = ""
            + " SELECT d "
            + "   FROM EtiquetaLang d "
            + "  WHERE d.etiquetaEntity = :etiqueta "),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaLangBy_E_Lang", query = ""
            + "  SELECT d "
            + "    FROM EtiquetaLang d "
            + "   WHERE d.etiquetaEntity = :etiqueta "
            + "     AND d.idioma = :idioma "),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaLangBy_IdE_Lang", query = ""
            + "  SELECT d "
            + "    FROM EtiquetaLang d "
            + "   WHERE d.etiquetaId = :etiquetaId "
            + "     AND d.idioma = :idioma "),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaEmpiezaTypeAheadTodo", query = ""
            + "   SELECT d "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :traduccionDesc "),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaEmpiezaTypeAheadSelEstado", query = ""
            + "   SELECT d "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :traduccionDesc "
            + "      AND d.atributosAuditoria.estadoRegistro = :estadoRegistro"),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaContieneTypeAheadTodo", query = ""
            + "   SELECT d "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :traduccionDesc "
            + "      AND d.traduccionDesc NOT LIKE :traduccionDescExclude"),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaContieneTypeAheadSelEstado", query = ""
            + "   SELECT d "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :traduccionDesc "
            + "      AND d.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "      AND d.traduccionDesc NOT LIKE :traduccionDescExclude"),

    @NamedQuery(name = "EtiquetaLang.deleteEtiquetaLangBy_idE_Lang", query = ""
            + "  DELETE "
            + "    FROM EtiquetaLang d "
            + "   WHERE d.etiquetaId = :etiquetaId "
            + "     AND d.idioma = :idioma "),

    
    
    @NamedQuery(name = "EtiquetaLang.findEtiquetaWith_Filter_E", query = ""
            + "   SELECT d.etiquetaId "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :etiqueta "
            + " ORDER BY d.traduccionDesc "),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaWith_Filter_", query = ""
            + "   SELECT d.etiquetaId "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + " ORDER BY d.traduccionDesc "),

    
    
    @NamedQuery(name = "EtiquetaLang.findEtiquetaWith_Filter_E_COUNT", query = ""
            + "   SELECT COUNT(d.etiquetaId) "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :etiqueta "
            + " ORDER BY d.traduccionDesc "),

    @NamedQuery(name = "EtiquetaLang.findEtiquetaWith_Filter__COUNT", query = ""
            + "   SELECT COUNT(d.etiquetaId) "
            + "     FROM EtiquetaLang d "
            + "    WHERE d.idioma = :idioma "
            + " ORDER BY d.traduccionDesc ")
        
        
        
    })



public class EtiquetaLang implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(EtiquetaLang.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String etiquetaId;

    @Id
    @Basic
    private String idioma;

    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String traduccionDesc;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    @ManyToOne(targetEntity = Etiqueta.class)
    private Etiqueta etiquetaEntity;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public EtiquetaLang() {
    }

    public EtiquetaLang(String etiquetaId, String idioma, String traduccionDesc, Etiqueta etiquetaEntity, DatosAuditoria atributosAuditoria) {
        this.etiquetaId = etiquetaId;
        this.idioma = idioma;
        this.traduccionDesc = traduccionDesc;
        this.etiquetaEntity = etiquetaEntity;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public String getEtiquetaId() {
        return etiquetaId;
    }

    public void setEtiquetaId(String etiquetaId) {
        this.etiquetaId = etiquetaId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getTraduccionDesc() {
        return traduccionDesc;
    }

    public void setTraduccionDesc(String traduccionDesc) {
        this.traduccionDesc = traduccionDesc;
    }

    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Etiqueta getEtiquetaEntity() {
        return etiquetaEntity;
    }

    public void setEtiquetaEntity(Etiqueta etiquetaEntity) {
        this.etiquetaEntity = etiquetaEntity;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.idioma);
        hash = 89 * hash + Objects.hashCode(this.traduccionDesc);
        hash = 89 * hash + Objects.hashCode(this.etiquetaEntity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EtiquetaLang other = (EtiquetaLang) obj;
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        if (!Objects.equals(this.traduccionDesc, other.traduccionDesc)) {
            return false;
        }
        if (!Objects.equals(this.etiquetaEntity, other.etiquetaEntity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EtiquetaLang{" + "etiquetaId=" + etiquetaId + ", idioma=" + idioma + ", version=" + version + ", traduccionDesc=" + traduccionDesc + ", etiquetaEntity=" + etiquetaEntity + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
