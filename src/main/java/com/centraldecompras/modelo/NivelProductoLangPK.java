package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class NivelProductoLangPK implements Serializable {

    @NotNull
    private int nivelProductoId;

    @NotNull
    private String idioma;

    public NivelProductoLangPK() {
    }

    public NivelProductoLangPK(int nivelProductoId, String idioma) {
        this.nivelProductoId = nivelProductoId;
        this.idioma = idioma;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.nivelProductoId;
        hash = 97 * hash + Objects.hashCode(this.idioma);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NivelProductoLangPK other = (NivelProductoLangPK) obj;
        if (!Objects.equals(this.nivelProductoId, other.nivelProductoId)) {
            return false;
        }
        if (this.idioma != other.idioma) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NivelProductoLangPK{" + "nivelProductoId=" + nivelProductoId + ", idioma=" + idioma + '}';
    }

}
