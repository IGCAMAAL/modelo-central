package com.centraldecompras.modelo;

import com.centraldecompras.zglobal.enums.LongFields;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_CMO_TipoIva")
@XmlRootElement

public class TipoIva implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(TipoIva.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String tipoIvaId;

    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    @Column(name = "claveIva", length = 10, nullable = false, updatable = false, unique = true)
    private String claveIva;

    @Basic
    @Digits(integer = LongFields.N3, fraction = LongFields.N4)
    private BigDecimal porcentajeIva;

    @Basic
    @Digits(integer = LongFields.N3, fraction = LongFields.N4)    
    private BigDecimal porcentajeImporte;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public TipoIva() {
    }

    public TipoIva(String tipoIvaId, String claveIva, BigDecimal porcentajeIva, BigDecimal porcentajeImporte, DatosAuditoria atributosAuditoria) {
        this.tipoIvaId = tipoIvaId;
        this.claveIva = claveIva;
        this.porcentajeIva = porcentajeIva;
        this.porcentajeImporte = porcentajeImporte;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getTipoIvaId() {
        return tipoIvaId;
    }

    public void setTipoIvaId(String tipoIvaId) {
        this.tipoIvaId = tipoIvaId;
    }

    public String getClaveIva() {
        return claveIva;
    }

    public void setClaveIva(String claveIva) {
        this.claveIva = claveIva;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public BigDecimal getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(BigDecimal porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getPorcentajeImporte() {
        return porcentajeImporte;
    }

    public void setPorcentajeImporte(BigDecimal porcentajeImporte) {
        this.porcentajeImporte = porcentajeImporte;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.tipoIvaId);
        hash = 37 * hash + Objects.hashCode(this.claveIva);
        hash = 37 * hash + Objects.hashCode(this.porcentajeIva);
        hash = 37 * hash + Objects.hashCode(this.porcentajeImporte);
        hash = 37 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoIva other = (TipoIva) obj;
        if (!Objects.equals(this.tipoIvaId, other.tipoIvaId)) {
            return false;
        }
        if (!Objects.equals(this.claveIva, other.claveIva)) {
            return false;
        }
        if (!Objects.equals(this.porcentajeIva, other.porcentajeIva)) {
            return false;
        }
        if (!Objects.equals(this.porcentajeImporte, other.porcentajeImporte)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoIva{" + "tipoIvaId=" + tipoIvaId + ", claveIva=" + claveIva + ", version=" + version + ", porcentajeIva=" + porcentajeIva + ", porcentajeImporte=" + porcentajeImporte + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
