package com.centraldecompras.modelo;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.zglobal.enums.LongFields;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PUR_CompraLinea",
        indexes = {
            @Index(name = "index_CompraLinea_PedidoLin", columnList = "pedido"),
            @Index(name = "index_CompraLinea_PedidoLin", columnList = "pedidoLin"),
            @Index(name = "index_CompraLinea_CestaLin", columnList = "cesta"),
            @Index(name = "index_CompraLinea_CestaLin", columnList = "cestaLin")
        }
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidoLin.findAll", query = ""
            + "   SELECT p "
            + "     FROM CompraLinea p "
            + ""),
    
    
    @NamedQuery(name = "CompraLinea.findUltimaLineaPedido", query = ""
            + "   SELECT max(d.pedidoLin) "
            + "     FROM CompraLinea d "
            + "    WHERE d.pedido = :pedido "
            + ""),
    
    
    @NamedQuery(name = "CompraLinea.findUltimaLineaCesta", query = ""
            + "   SELECT max(d.cestaLin) "
            + "     FROM CompraLinea d "
            + "    WHERE d.cesta = :cesta "
            + ""),
    
    
    @NamedQuery(name = "CompraLinea.findCompraLineasCesta", query = ""
            + "   SELECT d "
            + "     FROM CompraLinea d "
            + "    WHERE d.cesta.id = :cestaId "
            + ""),
    
    
    @NamedQuery(name = "CompraLinea.findCompraLineasPedido", query = ""
            + "   SELECT d "
            + "     FROM CompraLinea d "
            + "    WHERE d.pedido.id = :pedidoId "
            + ""),
    
    
    @NamedQuery(name = "CompraLinea.findCompraLineas_SociedadesList", query = ""
            + "   SELECT distinct(d.articulo.sociedad.id) "
            + "     FROM CompraLinea d "
            + "    WHERE d.cesta.id = :cestaId "
            + " GROUP BY d.articulo.sociedad.id "
            + ""),
    
    
    @NamedQuery(name = "CompraLinea.findCompraLineas_idsDeUnaSociedadList", query = ""
            + "   SELECT d.compraLineaId "
            + "     FROM CompraLinea d, "
            + "          Articulo a "
            + "    WHERE d.articulo.id = a.idArticulo "
            + "      AND d.cesta = :cesta  "            
            + "      AND a.sociedad = :sociedad "
            + ""),
    
    
    @NamedQuery(name = "CompraLinea.findCompraLinea_ProvList", query = ""
            + "   SELECT DISTINCT d.pedido.proveedorSociedad.id, d.pedido.proveedorSociedad.nombre  "
            + "     FROM CompraLinea d "
            + "    WHERE d.cesta = :cesta "
            + "      AND d.atributosAuditoria.estadoRegistro IN (:estados)  "
            + " GROUP BY d.pedido.id  "                                    
            + " ORDER BY d.pedido.id "            
            + ""),

   
    @NamedQuery(name = "CompraLinea.findCompraLinea_PagWithFilter_C", query = ""
            + "   SELECT d  "
            + "     FROM CompraLinea d, "
            + "          ArticuloLangNomb h "
            + "    WHERE d.articulo.id = h.articuloIdk "            
            + "      AND h.idioma = :idioma "            
            + "      AND d.cesta = :cesta "
            + "      AND d.atributosAuditoria.estadoRegistro IN (:estados)  "
            + " ORDER BY d.articulo.sociedad.nombre,  h.traduccionNomb"            
            + ""),
    
    @NamedQuery(name = "CompraLinea.findCompraLinea_PagWithFilter_C_COUNT", query = ""
            + "   SELECT COUNT(d)  "
            + "     FROM CompraLinea d, "
            + "          ArticuloLangNomb h "
            + "    WHERE d.articulo.id = h.articuloIdk "            
            + "      AND h.idioma = :idioma "            
            + "      AND d.cesta = :cesta "
            + "      AND d.atributosAuditoria.estadoRegistro IN (:estados)  "
            + ""),  

})
public class CompraLinea implements Serializable {

    private static final long serialVersionUID = 1L;

    // Atributos ID -----------------------------------------------------------------------------------------------    
    @Id
    @NotNull
    @Column(name = "ID")
    private String compraLineaId;

    @Version
    private int version;

    // Atributos Relacion ------------------------------------------------------------------------------------------        
    @ManyToOne(targetEntity = CompraCesta.class)
    private CompraCesta cesta;

    @ManyToOne(targetEntity = Sociedad.class)
    private Sociedad sociedad;
    
    @ManyToOne(targetEntity = CompraPedido.class)
    private CompraPedido pedido;

    @ManyToOne(targetEntity = Articulo.class)
    private Articulo articulo;

    @ManyToOne(targetEntity = TipoIva.class)
    private TipoIva tipoIva;

    // Atributos Basicos-------------------------------------------------------------------------------------------- 
    @Basic(optional = false)
    private int cestaLin;

    @Basic(optional = false)
    private int pedidoLin;

    @Digits(integer = LongFields.N17, fraction = LongFields.N4)
    @Basic(optional = false)
    private BigDecimal cantidadPedida;  // Trabaja con 6 decimales

    @Digits(integer = LongFields.N17, fraction = LongFields.N4)
    @Basic(optional = false)
    private BigDecimal precioAplicado;  // Trabaja con 2 decimales

    @Digits(integer = LongFields.N17, fraction = LongFields.N4)
    @Basic(optional = false)
    private BigDecimal descuentoAplicado;  // Trabaja con 2 decimales

    @Digits(integer = LongFields.N3, fraction = LongFields.N4)
    @Basic(optional = false)
    private BigDecimal descuentoPorcAplicado;  // Trabaja con 2 decimales

    @Digits(integer = LongFields.N17, fraction = LongFields.N4)
    @Basic(optional = false)
    private BigDecimal importeLineaSinIva;  // Trabaja con 2 decimales

    @Digits(integer = LongFields.N17, fraction = LongFields.N4)
    @Basic(optional = false)
    private BigDecimal importeIva;  // Trabaja con 2 decimales

    @Digits(integer = LongFields.N17, fraction = LongFields.N4)
    @Basic(optional = false)
    private BigDecimal importeLineaConIva;  // Trabaja con 2 decimales
    
    @Column(length = LongFields.compraEstado)
    String lineaEstado;
    
    // Atributos Auditoria-------------------------------------------------------------------------------------------- 
    @Embedded
    DatosAuditoria atributosAuditoria;

    // 2 Constructores ---------------------------------------------------------------------------------------------
    public CompraLinea() {
    }

    public CompraLinea(String compraLineaId, CompraCesta cesta, CompraPedido pedido, Articulo articulo, TipoIva tipoIva, int cestaLin, int pedidoLin, BigDecimal cantidadPedida, BigDecimal precioAplicado, BigDecimal descuentoAplicado, BigDecimal descuentoPorcAplicado, BigDecimal importeLineaSinIva, BigDecimal importeIva, BigDecimal importeLineaConIva, String lineaEstado,DatosAuditoria atributosAuditoria) {
        this.compraLineaId = compraLineaId;
        this.cesta = cesta;
        this.pedido = pedido;                   
        this.articulo = articulo;              
        this.tipoIva = tipoIva;                 
        this.cestaLin = cestaLin;
        this.pedidoLin = pedidoLin;
        
        this.cantidadPedida = cantidadPedida;                   
        this.precioAplicado = precioAplicado;                   
        this.descuentoAplicado = descuentoAplicado;             
        this.descuentoPorcAplicado = descuentoPorcAplicado;     
        this.importeLineaSinIva = importeLineaSinIva;
        this.importeIva = importeIva;
        this.importeLineaConIva = importeLineaConIva;
        this.lineaEstado = lineaEstado;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Metodos  ID -----------------------------------------------------------------------------------------------     
    public String getCompraLineaId() {
        return compraLineaId;
    }

    public void setCompraLineaId(String compraLineaId) {
        this.compraLineaId = compraLineaId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Metodos  Relacion Id ---------------------------------------------------------------------------------------- 
    public CompraCesta getCesta() {
        return cesta;
    }

    public void setCesta(CompraCesta cesta) {
        this.cesta = cesta;
    }

    public CompraPedido getPedido() {
        return pedido;
    }

    public void setPedido(CompraPedido pedido) {
        this.pedido = pedido;
    }

    public int getCestaLin() {
        return cestaLin;
    }

    public void setCestaLin(int cestaLin) {
        this.cestaLin = cestaLin;
    }

    public int getPedidoLin() {
        return pedidoLin;
    }

    public void setPedidoLin(int pedidoLin) {
        this.pedidoLin = pedidoLin;
    }

    // Metodos  Relacion ----------------------------------------------------------------------------------------   
    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public TipoIva getTipoIva() {
        return tipoIva;
    }

    public void setTipoIva(TipoIva tipoIva) {
        this.tipoIva = tipoIva;
    }

    public Sociedad getSociedad() {
        return sociedad;
    }

    public void setSociedad(Sociedad sociedad) {
        this.sociedad = sociedad;
    }

    // Metodos  Basicos ----------------------------------------------------------------------------------------   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    public BigDecimal getCantidadPedida() {
        return cantidadPedida;
    }

    public void setCantidadPedida(BigDecimal cantidadPedida) {
        this.cantidadPedida = cantidadPedida;
    }

    public BigDecimal getPrecioAplicado() {
        return precioAplicado;
    }

    public void setPrecioAplicado(BigDecimal precioAplicado) {
        this.precioAplicado = precioAplicado;
    }

    public BigDecimal getDescuentoAplicado() {
        return descuentoAplicado;
    }

    public void setDescuentoAplicado(BigDecimal descuentoAplicado) {
        this.descuentoAplicado = descuentoAplicado;
    }

    public BigDecimal getDescuentoPorcAplicado() {
        return descuentoPorcAplicado;
    }

    public void setDescuentoPorcAplicado(BigDecimal descuentoPorcAplicado) {
        this.descuentoPorcAplicado = descuentoPorcAplicado;
    }

    public BigDecimal getImporteLineaSinIva() {
        return importeLineaSinIva;
    }

    public void setImporteLineaSinIva(BigDecimal importeLineaSinIva) {
        this.importeLineaSinIva = importeLineaSinIva;
    }

    public BigDecimal getImporteIva() {
        return importeIva;
    }

    public void setImporteIva(BigDecimal importeIva) {
        this.importeIva = importeIva;
    }

    public BigDecimal getImporteLineaConIva() {
        return importeLineaConIva;
    }

    public void setImporteLineaConIva(BigDecimal importeLineaConIva) {
        this.importeLineaConIva = importeLineaConIva;
    }
    
    public String getLineaEstado() {
        return lineaEstado;
    }

    public void setLineaEstado(String lineaEstado) {
        this.lineaEstado = lineaEstado;
    }
    
    // Metodos  Entity ---------------------------------------------------------------------------------------- 

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.compraLineaId);
        hash = 29 * hash + Objects.hashCode(this.cesta);
        hash = 29 * hash + Objects.hashCode(this.pedido);
        hash = 29 * hash + Objects.hashCode(this.articulo);
        hash = 29 * hash + Objects.hashCode(this.tipoIva);
        hash = 29 * hash + this.cestaLin;
        hash = 29 * hash + this.pedidoLin;
        hash = 29 * hash + Objects.hashCode(this.cantidadPedida);
        hash = 29 * hash + Objects.hashCode(this.precioAplicado);
        hash = 29 * hash + Objects.hashCode(this.descuentoAplicado);
        hash = 29 * hash + Objects.hashCode(this.descuentoPorcAplicado);
        hash = 29 * hash + Objects.hashCode(this.importeLineaSinIva);
        hash = 29 * hash + Objects.hashCode(this.importeIva);
        hash = 29 * hash + Objects.hashCode(this.importeLineaConIva);
        hash = 29 * hash + Objects.hashCode(this.lineaEstado);
        hash = 29 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraLinea other = (CompraLinea) obj;
        if (!Objects.equals(this.compraLineaId, other.compraLineaId)) {
            return false;
        }
        if (!Objects.equals(this.cesta, other.cesta)) {
            return false;
        }
        if (!Objects.equals(this.pedido, other.pedido)) {
            return false;
        }
        if (!Objects.equals(this.articulo, other.articulo)) {
            return false;
        }
        if (!Objects.equals(this.tipoIva, other.tipoIva)) {
            return false;
        }
        if (this.cestaLin != other.cestaLin) {
            return false;
        }
        if (this.pedidoLin != other.pedidoLin) {
            return false;
        }
        if (!Objects.equals(this.cantidadPedida, other.cantidadPedida)) {
            return false;
        }
        if (!Objects.equals(this.precioAplicado, other.precioAplicado)) {
            return false;
        }
        if (!Objects.equals(this.descuentoAplicado, other.descuentoAplicado)) {
            return false;
        }
        if (!Objects.equals(this.descuentoPorcAplicado, other.descuentoPorcAplicado)) {
            return false;
        }
        if (!Objects.equals(this.importeLineaSinIva, other.importeLineaSinIva)) {
            return false;
        }
        if (!Objects.equals(this.importeIva, other.importeIva)) {
            return false;
        }
        if (!Objects.equals(this.importeLineaConIva, other.importeLineaConIva)) {
            return false;
        }
        if (!Objects.equals(this.lineaEstado, other.lineaEstado)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }


}
