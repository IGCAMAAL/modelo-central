package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_RPL_SociedadRpl")
@XmlRootElement

public class SociedadRpl implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SociedadRpl.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSociedad;

    @Version
    private int version;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    // TODO quitar
    /*
     @OneToMany(targetEntity=Contacto.class,mappedBy="sociedad", fetch = FetchType.EAGER)
     private Collection<Contacto> contacto;
     */
    // TODO quitar
    /*
     @OneToMany(targetEntity=Direccion.class,mappedBy="sociedad", fetch = FetchType.EAGER)
     private Collection<Direccion> direccion;
    */
    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public SociedadRpl() {
    }

    public SociedadRpl(String idSociedad) {
        this.idSociedad = idSociedad;
    }

    // Metodos  ID -----------------------------------------------------------------------------------------------     
    public String getIdSociedad() {
        return idSociedad;
    }

    public void setIdSociedad(String idSociedad) {
        this.idSociedad = idSociedad;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos relacionales  * * * * * * * * * * * * * * * * * * * * * * * * * * 
    /*
    public Collection<Direccion> getDireccion() {
        return direccion;
    }

    public void setDireccion(Collection<Direccion> direccion) {
        this.direccion = direccion;
    }
*/
    /*
    public Collection<Contacto> getContacto() {
        return contacto;
    }

    public void setContacto(Collection<Contacto> contacto) {
        this.contacto = contacto;
    }
*/
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idSociedad);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SociedadRpl other = (SociedadRpl) obj;
        if (!Objects.equals(this.idSociedad, other.idSociedad)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SociedadRpl{" + "idSociedad=" + idSociedad + ", version=" + version + '}';
    }

}
