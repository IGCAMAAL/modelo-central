package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

public interface ImgDocService {

    void create(ImgDoc imgDoc) throws PreexistingEntityException, Exception;

    void edit(ImgDoc imgDoc) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    ImgDoc findImgDoc(String id);

    List<String[]> findImgDocBy_SU(List<String> entitiesId);
    
    byte[] findImgPic_idI(String idImgDoc);
    
    void create_C(ImgDoc imgDoc) throws PreexistingEntityException, Exception;


}
