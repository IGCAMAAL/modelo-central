/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.cmo.service.interfaces.ContactoService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContactoServiceImpl implements ContactoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ContactoServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(Contacto contacto) throws PreexistingEntityException, Exception {

        try {
            SociedadRpl sociedadRpl = contacto.getSociedad();
            if (sociedadRpl != null) {
                sociedadRpl = em.getReference(sociedadRpl.getClass(), sociedadRpl.getIdSociedad());
                contacto.setSociedad(sociedadRpl);
            }
            em.persist(contacto);
        } catch (Exception ex) {
            if (findContacto(contacto.getIdContacto()) != null) {
                throw new PreexistingEntityException("Contacto " + contacto + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(Contacto contacto) throws PreexistingEntityException, Exception {

        try {
            SociedadRpl sociedadRpl = contacto.getSociedad();
            if (sociedadRpl != null) {
                sociedadRpl = em.getReference(sociedadRpl.getClass(), sociedadRpl.getIdSociedad());
                contacto.setSociedad(sociedadRpl);
            }
            em.persist(contacto);
        } catch (Exception ex) {
            if (findContacto(contacto.getIdContacto()) != null) {
                throw new PreexistingEntityException("Contacto " + contacto + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(Contacto contacto) throws NonexistentEntityException, Exception {
        try {
            Contacto persistentContacto = em.find(Contacto.class, contacto.getIdContacto());
            SociedadRpl sociedadOld = persistentContacto.getSociedad();
            SociedadRpl sociedadNew = contacto.getSociedad();

            if (sociedadNew != null) {
                sociedadNew = em.getReference(sociedadNew.getClass(), sociedadNew.getIdSociedad());
                contacto.setSociedad(sociedadNew);
            }
            contacto = em.merge(contacto);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = contacto.getIdContacto();
                if (findContacto(id) == null) {
                    throw new NonexistentEntityException("The contacto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Contacto contacto;
        try {
            contacto = em.getReference(Contacto.class, id);
            contacto.getIdContacto();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The contacto with id " + id + " no longer exists.", enfe);
        }

        em.remove(contacto);
    }

    public List<Contacto> findContactoEntities() {
        return findContactoEntities(true, -1, -1);
    }

    public List<Contacto> findContactoEntities(int maxResults, int firstResult) {
        return findContactoEntities(false, maxResults, firstResult);
    }

    public List<Contacto> findContactoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Contacto.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Contacto findContacto(String id) {
        return em.find(Contacto.class, id);
    }

    public int getContactoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Contacto> rt = cq.from(Contacto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Contacto> findContactosBy_S(SociedadRpl sociedad) {
        List<Contacto> reply = null;
        try {
            reply = (List<Contacto>) em.createNamedQuery("Contacto.findContactosBy_S")
                    .setParameter("sociedad", sociedad)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica para la Almacen= " + sociedad.toString());
        }
        
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<Contacto> findContactosBy_P(Persona persona) {
        List<Contacto> reply = null;
        try {
            reply = (List<Contacto>) em.createNamedQuery("Contacto.findContactosBy_A")
                    .setParameter("persona", persona)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica para la Almacen= " + persona.toString());            
        }
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<Contacto> findContactosBy_A(Almacen almacen) {
        List<Contacto> reply = null;
        try {
            reply = (List<Contacto>) em.createNamedQuery("Contacto.findContactosBy_A")
                    .setParameter("almacen", almacen)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println(".............No se ha encontrado ninguan Zona Geografica para la Almacen= " + almacen.toString());
        }
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

}
