/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class DireccionServiceImpl implements DireccionService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DireccionServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(Direccion direccion) throws PreexistingEntityException, Exception {
        try {
            em.persist(direccion);
        } catch (Exception ex) {
            if (findDireccion(direccion.getIdDireccion()) != null) {
                throw new PreexistingEntityException("Direccion " + direccion + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(Direccion direccion) throws PreexistingEntityException, Exception {
        try {
            em.persist(direccion);
        } catch (Exception ex) {
            if (findDireccion(direccion.getIdDireccion()) != null) {
                throw new PreexistingEntityException("Direccion " + direccion + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(Direccion direccion) throws NonexistentEntityException, Exception {
        try {
            direccion = em.merge(direccion);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = direccion.getIdDireccion();
                if (findDireccion(id) == null) {
                    throw new NonexistentEntityException("The direccion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Direccion direccion;
        try {
            direccion = em.getReference(Direccion.class, id);
            direccion.getIdDireccion();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The direccion with id " + id + " no longer exists.", enfe);
        }
        em.remove(direccion);
    }

    public List<Direccion> findDireccionEntities() {
        return findDireccionEntities(true, -1, -1);
    }

    public List<Direccion> findDireccionEntities(int maxResults, int firstResult) {
        return findDireccionEntities(false, maxResults, firstResult);
    }

    public List<Direccion> findDireccionEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Direccion.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Direccion findDireccion(String id) {
        return em.find(Direccion.class, id);
    }

    public int getDireccionCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Direccion> rt = cq.from(Direccion.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Direccion> findDireccionesBy_S(SociedadRpl sociedad) {
        List<Direccion> reply = null;
        try {
            reply = (List<Direccion>) em.createNamedQuery("Direccion.findDireccionesBy_S")
                    .setParameter("sociedad", sociedad)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica para la Sociedad= " + sociedad);
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<Direccion> findDireccionesBy_P(Persona persona) {
        List<Direccion> reply = null;
        try {
            reply = (List<Direccion>) em.createNamedQuery("Direccion.findDireccionesBy_P")
                    .setParameter("persona", persona)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica para la Persona= " + persona);
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<Direccion> findDireccionesBy_A(Almacen almacen) {
        List<Direccion> reply = null;
        try {
            reply = (List<Direccion>) em.createNamedQuery("Direccion.findDireccionesBy_A")
                    .setParameter("almacen", almacen)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica para la Almacen= " + almacen);
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }
}
