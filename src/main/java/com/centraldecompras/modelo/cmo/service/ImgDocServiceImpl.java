package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgDocService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ImgDocServiceImpl implements ImgDocService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImgDocServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(ImgDoc imgDoc) throws PreexistingEntityException, Exception {
        try {
            em.persist(imgDoc);
        } catch (Exception ex) {
            if (findImgDoc(imgDoc.getIdImgDoc()) != null) {
                throw new PreexistingEntityException("ImgDoc " + imgDoc + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ImgDoc imgDoc) throws NonexistentEntityException, Exception {
        try {
            imgDoc = em.merge(imgDoc);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = imgDoc.getIdImgDoc();
                if (findImgDoc(id) == null) {
                    throw new NonexistentEntityException("The imgDoc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }
    
    public void destroy(String id) throws NonexistentEntityException {
        ImgDoc imgDoc;
        try {
            imgDoc = em.getReference(ImgDoc.class, id);
            imgDoc.getIdImgDoc();
            em.remove(imgDoc);
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The imgDoc with id " + id + " no longer exists.", enfe);
        }
    }

    public List<ImgDoc> findImgDocEntities() {
        return findImgDocEntities(true, -1, -1);
    }

    public List<ImgDoc> findImgDocEntities(int maxResults, int firstResult) {
        return findImgDocEntities(false, maxResults, firstResult);
    }

    private List<ImgDoc> findImgDocEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ImgDoc.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public ImgDoc findImgDoc(String id) {
        return em.find(ImgDoc.class, id);
    }

    public int getImgDocCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ImgDoc> rt = cq.from(ImgDoc.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<String[]> findImgDocBy_SU(List<String> entitiesId) {
        List<Object[]> outSql = null;
        List<String[]> reply = new ArrayList();
        try {
            outSql = (List<Object[]>) em.createNamedQuery("ImgDoc.findImgDocBy_SU")
                    .setParameter("entitiesId", entitiesId)
                    .getResultList();
        } catch (Exception ex) {
            // Nothing to do 
        }
           
        for(Object[] inListArrO: outSql){
            String[] outListArrS = new String[inListArrO.length];
            for(int ic1=0; ic1<inListArrO.length; ic1++){
                outListArrS[ic1] = (String)inListArrO[ic1];
            }
            reply.add(outListArrS);
        } 
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }
    
    public byte[] findImgPic_idI(String idImgDoc) {
        byte[] reply = null;
        try {
            reply = (byte[]) em.createNamedQuery("ImgDoc.findImgPic_idI")
                    .setParameter("idImgDoc", idImgDoc)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing to do 
        }
        
        return reply;
    }  
    
    @Transactional
    public void create_C(ImgDoc imgDoc) throws PreexistingEntityException, Exception {
        try {
            em.persist(imgDoc);
        } catch (Exception ex) {
            if (findImgDoc(imgDoc.getIdImgDoc()) != null) {
                throw new PreexistingEntityException("ImgDoc " + imgDoc + " already exists.", ex);
            }
            throw ex;
        }
    }

}
