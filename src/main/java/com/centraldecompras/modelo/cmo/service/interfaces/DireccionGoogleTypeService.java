/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.modelo.DireccionGoogleType;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface DireccionGoogleTypeService {

    DireccionGoogleType findDireccionGoogleType(String id);

    DireccionGoogleType findDireccionGoogleTypeBy_TDC(String tipoDireccion, String direccionid, String direcciongooglegomponenteid);

    int destroyTypeNoRecived(List<String> tiposDireccion, String direccionid, String direcciongooglegomponenteid);

    List<DireccionGoogleType> findDireccionesGoogleTypeBy_DDC(String direccionid, String direcciongooglegomponenteid);

    void destroy(String id) throws NonexistentEntityException;

    int destroyDireccionesGoogleTypeBy_DDC_B(String direccionid, String direcciongooglegomponenteid);
    
    void create(DireccionGoogleType direccionGoogleType) throws PreexistingEntityException, Exception;
    
    void create_C(DireccionGoogleType direccionGoogleType) throws PreexistingEntityException, Exception;
}
