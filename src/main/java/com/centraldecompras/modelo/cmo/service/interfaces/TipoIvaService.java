/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.TipoIva;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;

/**
 *
 * @author ciberado
 */
public interface TipoIvaService {

    TipoIva findTipoIva(String id);

    void create(TipoIva tipoIva) throws PreexistingEntityException, Exception;

    void edit(TipoIva tipoIva) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;



}
