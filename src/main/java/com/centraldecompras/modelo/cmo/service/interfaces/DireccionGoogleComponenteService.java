/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.DireccionGoogleComponente;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;


/**
 *
 * @author ciberado
 */
public interface DireccionGoogleComponenteService{

    DireccionGoogleComponente findDireccionGoogleComponente(String id);

    List<DireccionGoogleComponente> findDireccionGoogleComponenteBy_idD(Direccion direccion);
    
    void create(DireccionGoogleComponente direccionGoogleComponente)  throws PreexistingEntityException, Exception ;
    
    void edit(DireccionGoogleComponente direccionGoogleComponente) throws NonexistentEntityException, Exception;
    
    void destroy(String id) throws NonexistentEntityException;
    
    void create_C(DireccionGoogleComponente direccionGoogleComponente)  throws PreexistingEntityException, Exception ;
   
}
