package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.modelo.Contadores;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;

public interface ContadoresService  {

    Contadores create(Contadores contador) throws PreexistingEntityException, Exception ;

    Contadores edit(Contadores contador) throws NonexistentEntityException, Exception ;

    Contadores findContadores(int anio, String tipoDoc);
    
    Integer incrementaContador(Integer anioDocumento, String tipoDocumento) throws Exception ;

    
    
}
