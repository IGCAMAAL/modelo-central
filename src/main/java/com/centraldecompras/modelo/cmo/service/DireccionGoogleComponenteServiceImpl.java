/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.DireccionGoogleComponente;
import com.centraldecompras.modelo.DireccionGoogleType;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleComponenteService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class DireccionGoogleComponenteServiceImpl implements DireccionGoogleComponenteService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DireccionGoogleComponenteServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(DireccionGoogleComponente direccionGoogleComponente) throws PreexistingEntityException, Exception {
        if (direccionGoogleComponente.getDireccionGoogleType() == null) {
            direccionGoogleComponente.setDireccionGoogleType(new ArrayList<DireccionGoogleType>());
        }
        try {
            Collection<DireccionGoogleType> attachedDireccionGoogleType = new ArrayList<DireccionGoogleType>();
            for (DireccionGoogleType direccionGoogleTypeDireccionGoogleTypeToAttach : direccionGoogleComponente.getDireccionGoogleType()) {
                direccionGoogleTypeDireccionGoogleTypeToAttach = em.getReference(direccionGoogleTypeDireccionGoogleTypeToAttach.getClass(), direccionGoogleTypeDireccionGoogleTypeToAttach.getIdDireccionGoogleType());
                attachedDireccionGoogleType.add(direccionGoogleTypeDireccionGoogleTypeToAttach);
            }
            direccionGoogleComponente.setDireccionGoogleType(attachedDireccionGoogleType);
            em.persist(direccionGoogleComponente);
            for (DireccionGoogleType direccionGoogleTypeDireccionGoogleType : direccionGoogleComponente.getDireccionGoogleType()) {
                DireccionGoogleComponente oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType = direccionGoogleTypeDireccionGoogleType.getDireccionGoogleComponente();
                direccionGoogleTypeDireccionGoogleType.setDireccionGoogleComponente(direccionGoogleComponente);
                direccionGoogleTypeDireccionGoogleType = em.merge(direccionGoogleTypeDireccionGoogleType);
                if (oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType != null) {
                    oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType.getDireccionGoogleType().remove(direccionGoogleTypeDireccionGoogleType);
                    oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType = em.merge(oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType);
                }
            }
        } catch (Exception ex) {
            if (findDireccionGoogleComponente(direccionGoogleComponente.getIdDireccionGoogleComponente()) != null) {
                throw new PreexistingEntityException("DireccionGoogleComponente " + direccionGoogleComponente + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(DireccionGoogleComponente direccionGoogleComponente) throws PreexistingEntityException, Exception {
        if (direccionGoogleComponente.getDireccionGoogleType() == null) {
            direccionGoogleComponente.setDireccionGoogleType(new ArrayList<DireccionGoogleType>());
        }
        try {
            Collection<DireccionGoogleType> attachedDireccionGoogleType = new ArrayList<DireccionGoogleType>();
            for (DireccionGoogleType direccionGoogleTypeDireccionGoogleTypeToAttach : direccionGoogleComponente.getDireccionGoogleType()) {
                direccionGoogleTypeDireccionGoogleTypeToAttach = em.getReference(direccionGoogleTypeDireccionGoogleTypeToAttach.getClass(), direccionGoogleTypeDireccionGoogleTypeToAttach.getIdDireccionGoogleType());
                attachedDireccionGoogleType.add(direccionGoogleTypeDireccionGoogleTypeToAttach);
            }
            direccionGoogleComponente.setDireccionGoogleType(attachedDireccionGoogleType);
            em.persist(direccionGoogleComponente);
            for (DireccionGoogleType direccionGoogleTypeDireccionGoogleType : direccionGoogleComponente.getDireccionGoogleType()) {
                DireccionGoogleComponente oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType = direccionGoogleTypeDireccionGoogleType.getDireccionGoogleComponente();
                direccionGoogleTypeDireccionGoogleType.setDireccionGoogleComponente(direccionGoogleComponente);
                direccionGoogleTypeDireccionGoogleType = em.merge(direccionGoogleTypeDireccionGoogleType);
                if (oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType != null) {
                    oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType.getDireccionGoogleType().remove(direccionGoogleTypeDireccionGoogleType);
                    oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType = em.merge(oldDireccionGoogleComponenteOfDireccionGoogleTypeDireccionGoogleType);
                }
            }
        } catch (Exception ex) {
            if (findDireccionGoogleComponente(direccionGoogleComponente.getIdDireccionGoogleComponente()) != null) {
                throw new PreexistingEntityException("DireccionGoogleComponente " + direccionGoogleComponente + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(DireccionGoogleComponente direccionGoogleComponente) throws NonexistentEntityException, Exception {
        try {
            DireccionGoogleComponente persistentDireccionGoogleComponente = em.find(DireccionGoogleComponente.class, direccionGoogleComponente.getIdDireccionGoogleComponente());
            Collection<DireccionGoogleType> direccionGoogleTypeOld = persistentDireccionGoogleComponente.getDireccionGoogleType();
            Collection<DireccionGoogleType> direccionGoogleTypeNew = direccionGoogleComponente.getDireccionGoogleType();
            Collection<DireccionGoogleType> attachedDireccionGoogleTypeNew = new ArrayList<DireccionGoogleType>();
            for (DireccionGoogleType direccionGoogleTypeNewDireccionGoogleTypeToAttach : direccionGoogleTypeNew) {
                direccionGoogleTypeNewDireccionGoogleTypeToAttach = em.getReference(direccionGoogleTypeNewDireccionGoogleTypeToAttach.getClass(), direccionGoogleTypeNewDireccionGoogleTypeToAttach.getIdDireccionGoogleType());
                attachedDireccionGoogleTypeNew.add(direccionGoogleTypeNewDireccionGoogleTypeToAttach);
            }
            direccionGoogleTypeNew = attachedDireccionGoogleTypeNew;
            direccionGoogleComponente.setDireccionGoogleType(direccionGoogleTypeNew);
            direccionGoogleComponente = em.merge(direccionGoogleComponente);
            for (DireccionGoogleType direccionGoogleTypeOldDireccionGoogleType : direccionGoogleTypeOld) {
                if (!direccionGoogleTypeNew.contains(direccionGoogleTypeOldDireccionGoogleType)) {
                    direccionGoogleTypeOldDireccionGoogleType.setDireccionGoogleComponente(null);
                    direccionGoogleTypeOldDireccionGoogleType = em.merge(direccionGoogleTypeOldDireccionGoogleType);
                }
            }
            for (DireccionGoogleType direccionGoogleTypeNewDireccionGoogleType : direccionGoogleTypeNew) {
                if (!direccionGoogleTypeOld.contains(direccionGoogleTypeNewDireccionGoogleType)) {
                    DireccionGoogleComponente oldDireccionGoogleComponenteOfDireccionGoogleTypeNewDireccionGoogleType = direccionGoogleTypeNewDireccionGoogleType.getDireccionGoogleComponente();
                    direccionGoogleTypeNewDireccionGoogleType.setDireccionGoogleComponente(direccionGoogleComponente);
                    direccionGoogleTypeNewDireccionGoogleType = em.merge(direccionGoogleTypeNewDireccionGoogleType);
                    if (oldDireccionGoogleComponenteOfDireccionGoogleTypeNewDireccionGoogleType != null && !oldDireccionGoogleComponenteOfDireccionGoogleTypeNewDireccionGoogleType.equals(direccionGoogleComponente)) {
                        oldDireccionGoogleComponenteOfDireccionGoogleTypeNewDireccionGoogleType.getDireccionGoogleType().remove(direccionGoogleTypeNewDireccionGoogleType);
                        oldDireccionGoogleComponenteOfDireccionGoogleTypeNewDireccionGoogleType = em.merge(oldDireccionGoogleComponenteOfDireccionGoogleTypeNewDireccionGoogleType);
                    }
                }
            }
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = direccionGoogleComponente.getIdDireccionGoogleComponente();
                if (findDireccionGoogleComponente(id) == null) {
                    throw new NonexistentEntityException("The direccionGoogleComponente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        DireccionGoogleComponente direccionGoogleComponente;
        try {
            direccionGoogleComponente = em.getReference(DireccionGoogleComponente.class, id);
            direccionGoogleComponente.getIdDireccionGoogleComponente();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The direccionGoogleComponente with id " + id + " no longer exists.", enfe);
        }
        Collection<DireccionGoogleType> direccionGoogleType = direccionGoogleComponente.getDireccionGoogleType();
        for (DireccionGoogleType direccionGoogleTypeDireccionGoogleType : direccionGoogleType) {
            direccionGoogleTypeDireccionGoogleType.setDireccionGoogleComponente(null);
            direccionGoogleTypeDireccionGoogleType = em.merge(direccionGoogleTypeDireccionGoogleType);
        }
        em.remove(direccionGoogleComponente);
    }

    public List<DireccionGoogleComponente> findDireccionGoogleComponenteEntities() {
        return findDireccionGoogleComponenteEntities(true, -1, -1);
    }

    public List<DireccionGoogleComponente> findDireccionGoogleComponenteEntities(int maxResults, int firstResult) {
        return findDireccionGoogleComponenteEntities(false, maxResults, firstResult);
    }

    private List<DireccionGoogleComponente> findDireccionGoogleComponenteEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(DireccionGoogleComponente.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public DireccionGoogleComponente findDireccionGoogleComponente(String id) {
        return em.find(DireccionGoogleComponente.class, id);
    }

    public int getDireccionGoogleComponenteCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<DireccionGoogleComponente> rt = cq.from(DireccionGoogleComponente.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<DireccionGoogleComponente> findDireccionGoogleComponenteBy_idD(Direccion direccion) {
        List<DireccionGoogleComponente> reply = null;
        try {
            reply = (List<DireccionGoogleComponente>) em.createNamedQuery("DireccionGoogleComponente.findDireccionGoogleComponenteBy_idD")
                    .setParameter("direccion", direccion)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Direccion para la direccion= " + direccion);
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

}
