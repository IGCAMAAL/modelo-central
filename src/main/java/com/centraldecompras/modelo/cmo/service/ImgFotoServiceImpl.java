package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.ImgFoto;
import com.centraldecompras.modelo.cmo.service.interfaces.ImgFotoService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ImgFotoServiceImpl implements ImgFotoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ImgFotoServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(ImgFoto imgFoto) throws PreexistingEntityException, Exception {
        try {
            em.persist(imgFoto);
        } catch (Exception ex) {
            if (findImgFoto(imgFoto.getIdImgDoc()) != null) {
                throw new PreexistingEntityException("ImgFoto " + imgFoto + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ImgFoto imgFoto) throws NonexistentEntityException, Exception {
        try {
            imgFoto = em.merge(imgFoto);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = imgFoto.getIdImgDoc();
                if (findImgFoto(id) == null) {
                    throw new NonexistentEntityException("The ImgFoto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }
    
    public void destroy(String id) throws NonexistentEntityException {
        ImgFoto imgFoto;
        try {
            imgFoto = em.getReference(ImgFoto.class, id);
            imgFoto.getIdImgDoc();
            em.remove(imgFoto);
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The imgDoc with id " + id + " no longer exists.", enfe);
        }
    }

    public List<ImgFoto> findImgFotoEntities() {
        return findImgFotoEntities(true, -1, -1);
    }

    public List<ImgFoto> findImgFotoEntities(int maxResults, int firstResult) {
        return findImgFotoEntities(false, maxResults, firstResult);
    }

    private List<ImgFoto> findImgFotoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ImgFoto.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public ImgFoto findImgFoto(String id) {
        return em.find(ImgFoto.class, id);
    }

    public int getImgFotoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ImgDoc> rt = cq.from(ImgFoto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<String[]> findImgFotoBy_SU(List<String> entitiesId) {
        List<Object[]> outSql = null;
        List<String[]> reply = new ArrayList();
        try {
            outSql = (List<Object[]>) em.createNamedQuery("ImgFoto.findImgFotoBy_SU")
                    .setParameter("entitiesId", entitiesId)
                    .getResultList();
        } catch (Exception ex) {
            // Nothing to do 
        }
           
        for(Object[] inListArrO: outSql){
            String[] outListArrS = new String[inListArrO.length];
            for(int ic1=0; ic1<inListArrO.length; ic1++){
                outListArrS[ic1] = (String)inListArrO[ic1];
            }
            reply.add(outListArrS);
        } 
        
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }
    
    public byte[] findImgPic_idI(String idImgDoc) {
        byte[] reply = null;
        try {
            reply = (byte[]) em.createNamedQuery("ImgFoto.findImgPic_idI")
                    .setParameter("idImgDoc", idImgDoc)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing to do 
        }
        
        return reply;
    }  
    
    @Transactional
    public void create_C(ImgFoto imgFoto) throws PreexistingEntityException, Exception {
        try {
            em.persist(imgFoto);
        } catch (Exception ex) {
            if (findImgFoto(imgFoto.getIdImgDoc()) != null) {
                throw new PreexistingEntityException("ImgFoto " + imgFoto + " already exists.", ex);
            }
            throw ex;
        }
    }

}
