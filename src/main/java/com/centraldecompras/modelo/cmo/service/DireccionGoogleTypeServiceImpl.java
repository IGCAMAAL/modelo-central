/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.modelo.DireccionGoogleType;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleTypeService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class DireccionGoogleTypeServiceImpl implements DireccionGoogleTypeService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DireccionGoogleTypeServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;
    
    @Transactional
    public void create_C(DireccionGoogleType direccionGoogleType) throws PreexistingEntityException, Exception {
        try {
            em.persist(direccionGoogleType);
        } catch (Exception ex) {
            if (findDireccionGoogleType(direccionGoogleType.getIdDireccionGoogleType()) != null) {
                throw new PreexistingEntityException("DireccionGoogleType " + direccionGoogleType + " already exists.", ex);
            }
            throw ex;
        }
    }
    
    public void create(DireccionGoogleType direccionGoogleType) throws PreexistingEntityException, Exception {
        try {
            em.persist(direccionGoogleType);
        } catch (Exception ex) {
            if (findDireccionGoogleType(direccionGoogleType.getIdDireccionGoogleType()) != null) {
                throw new PreexistingEntityException("DireccionGoogleType " + direccionGoogleType + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(DireccionGoogleType direccionGoogleType) throws NonexistentEntityException, Exception {
        try {
            direccionGoogleType = em.merge(direccionGoogleType);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = direccionGoogleType.getIdDireccionGoogleType();
                if (findDireccionGoogleType(id) == null) {
                    throw new NonexistentEntityException("The direccionGoogleType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        DireccionGoogleType direccionGoogleType;
        try {
            direccionGoogleType = em.getReference(DireccionGoogleType.class, id);
            direccionGoogleType.getIdDireccionGoogleType();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The direccionGoogleType with id " + id + " no longer exists.", enfe);
        }
        em.remove(direccionGoogleType);
    }

    public List<DireccionGoogleType> findDireccionGoogleTypeEntities() {
        return findDireccionGoogleTypeEntities(true, -1, -1);
    }

    public List<DireccionGoogleType> findDireccionGoogleTypeEntities(int maxResults, int firstResult) {
        return findDireccionGoogleTypeEntities(false, maxResults, firstResult);
    }

    private List<DireccionGoogleType> findDireccionGoogleTypeEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(DireccionGoogleType.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public DireccionGoogleType findDireccionGoogleType(String id) {
        return em.find(DireccionGoogleType.class, id);
    }

    public int getDireccionGoogleTypeCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<DireccionGoogleType> rt = cq.from(DireccionGoogleType.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public DireccionGoogleType findDireccionGoogleTypeBy_TDC(String tipoDireccion, String direccionid, String direcciongooglegomponenteid) {
        DireccionGoogleType direccionGoogleType = null;
        try {
            direccionGoogleType = (DireccionGoogleType) em.createNamedQuery("DireccionGoogleType.findDireccionGoogleTypeBy_TDC")
                    .setParameter("type", tipoDireccion)
                    .setParameter("direccionid", direccionid)
                    .setParameter("direcciongooglegomponenteid", direcciongooglegomponenteid)
                    .getSingleResult();
        } catch (Exception ex) {
                        log.warn(ex + ".............No se ha encontrado tipoDireccion =" + tipoDireccion);
        }
        return direccionGoogleType;
    }

    public List<DireccionGoogleType> findDireccionesGoogleTypeBy_DDC(String direccionid, String direcciongooglegomponenteid) {
        List<DireccionGoogleType> reply = null;
        if (direccionid != null) {
            try {
                reply = em.createNamedQuery("DireccionGoogleType.findDireccionesGoogleTypeBy_D")
                        .setParameter("direccionid", direccionid)
                        .getResultList();
            } catch (Exception ex) {
                            log.warn(ex + ".............No se han encontrado tipos de Direccion para " + direccionid);
            }
        } else if (direcciongooglegomponenteid != null) {
            try {
                reply = em.createNamedQuery("DireccionGoogleType.findDireccionesGoogleTypeBy_DC")
                        .setParameter("direcciongooglegomponenteid", direcciongooglegomponenteid)
                        .getResultList();
            } catch (Exception ex) {
                            log.warn(ex + ".............No se ha encontrado tipos de Direccion para " + direcciongooglegomponenteid);
            }  
        }
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }
    
    public int destroyTypeNoRecived(List<String> tiposDireccion, String direccionid, String direcciongooglegomponenteid) {
        int borrados = 0;

        if (direccionid != null) {
            try {
                borrados = em.createNamedQuery("DireccionGoogleType.destroyTypeNoRecivedD")
                        .setParameter("direccionid", direccionid)
                        .executeUpdate();
            } catch (Exception ex) {
                            log.warn(ex + ".............No se ha encontrado type con direccionid= " + direccionid);
            }
        } else if (direcciongooglegomponenteid != null) {
            try {
                borrados = em.createNamedQuery("DireccionGoogleType.destroyTypeNoRecivedC")
                        .setParameter("direcciongooglegomponenteid", direcciongooglegomponenteid)
                        .executeUpdate();
            } catch (Exception ex) {
                            log.warn(ex + ".............No se ha encontrado type con direcciongooglegomponenteid=" + direcciongooglegomponenteid);
            }
        }

        return borrados;
    }

    public int destroyDireccionesGoogleTypeBy_DDC_B(String direccionid, String direcciongooglegomponenteid) {
        int reply = 0;
        if (direccionid != null) {
            try {
                reply = em.createNamedQuery("DireccionGoogleType.destroyDireccionesGoogleTypeBy_D")
                        .setParameter("direccionid", direccionid)
                        .executeUpdate();
            } catch (Exception ex) {
                            log.warn(ex + ".............No se han encontrado tipos de Direccion para: " + direccionid);
            }
        } 
        if (direcciongooglegomponenteid != null) {
            try {
                reply = em.createNamedQuery("DireccionGoogleType.destroyDireccionesGoogleTypeBy_DC")
                        .setParameter("direcciongooglegomponenteid", direcciongooglegomponenteid)
                        .executeUpdate();
            } catch (Exception ex) {
                log.warn(ex + ".............No se han encontrado tipos de componente para: " + direcciongooglegomponenteid);
            }
        } 
        return reply;
    }
}
