/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.modelo.Contadores;
import com.centraldecompras.modelo.cmo.service.interfaces.ContadoresService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.UtilsMethods;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContadoresServiceImpl implements ContadoresService {

    @PersistenceContext
    private EntityManager em;

    public Contadores create(Contadores contador) throws PreexistingEntityException, Exception {
        Contadores reply = null;

        int anio = contador.getAnio();
        String tipDoc = contador.getTipoDoc();

        try {
            em.persist(contador);
            reply = findContadores(anio, tipDoc);
        } catch (Exception ex) {
            if (findContadores(anio, tipDoc) != null) {
                throw new PreexistingEntityException("The contador with Anio " + anio + " y tipDoc: " + tipDoc + " already exists.", ex);
            }
            throw ex;
        }

        return reply;
    }

    public Contadores edit(Contadores contador) throws NonexistentEntityException, Exception {
        Contadores reply = null;

        int anio = contador.getAnio();
        String tipDoc = contador.getTipoDoc();

        try {
            reply = em.merge(contador);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findContadores(anio, tipDoc) == null) {
                    throw new NonexistentEntityException("The contador with Anio " + anio + " y tipDoc: " + tipDoc + " no longer exists.");
                }
            }
            throw ex;
        }

        return reply;
    }

    @Override
    public Contadores findContadores(int anio, String tipoDoc) {
        Contadores reply = null;

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Contadores> q = builder.createQuery(Contadores.class);
        Root<Contadores> r = q.from(Contadores.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(r.get("anio"), anio));
        predicates.add(builder.equal(r.get("tipoDoc"), tipoDoc));

        q.where(predicates.toArray(new Predicate[]{}));
        Query query = em.createQuery(q);

        reply = (Contadores) query.getSingleResult();

        return reply;
    }

    @Transactional
    public Integer incrementaContador(Integer anioDocumento, String tipoDocumento) throws Exception {
        Integer reply = 0;
        
        Contadores contadorDoc = null;
        try {
            contadorDoc = findContadores(anioDocumento, tipoDocumento);
        } catch (Exception e) {
            contadorDoc = create(new Contadores(anioDocumento, tipoDocumento, 0));
        }

        if (contadorDoc == null) {
            Object[] valores = {ElementEnum.KApp.NUM_PDD.getKApp(), anioDocumento};
            String mensaje = MessageFormat.format("ERROR: No se ha podido crear el contador de {0} para el año {1}", valores);
            throw new Exception(MessageFormat.format(mensaje, new Throwable()));
        }

        reply = contadorDoc.getNumeroDoc() + 1;
        contadorDoc.setNumeroDoc(reply);
        edit(contadorDoc);

        return reply;
    }
}
