/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ContactoService {

    Contacto findContacto(String id);

    List<Contacto> findContactosBy_S(SociedadRpl sociedad);
    
    List<Contacto> findContactosBy_P(Persona persona);
    
    List<Contacto> findContactosBy_A(Almacen almacen);

    void create(Contacto contacto) throws PreexistingEntityException, Exception;

    void edit(Contacto contacto) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    void create_C(Contacto contacto) throws PreexistingEntityException, Exception;

    List<Contacto> findContactoEntities(boolean all, int maxResults, int firstResult);

}
