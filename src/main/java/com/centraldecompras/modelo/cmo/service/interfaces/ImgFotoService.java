package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.modelo.ImgDoc;
import com.centraldecompras.modelo.ImgFoto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

public interface ImgFotoService {

    void create(ImgFoto imgFoto) throws PreexistingEntityException, Exception;

    void edit(ImgFoto imgFoto) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    ImgFoto findImgFoto(String id);

    List<String[]> findImgFotoBy_SU(List<String> entitiesId);
    
    byte[] findImgPic_idI(String idImgDoc);
    
    void create_C(ImgFoto imgFoto) throws PreexistingEntityException, Exception;


}
