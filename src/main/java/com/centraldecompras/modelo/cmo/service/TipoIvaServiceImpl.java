package com.centraldecompras.modelo.cmo.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.TipoIva;
import com.centraldecompras.modelo.cmo.service.interfaces.TipoIvaService;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class TipoIvaServiceImpl implements TipoIvaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TipoIvaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;


    public void create(TipoIva tipoIva) throws PreexistingEntityException, Exception {

        try {
            em.persist(tipoIva);
        } catch (Exception ex) {
            if (findTipoIva(tipoIva.getTipoIvaId()) != null) {
                throw new PreexistingEntityException("Contacto " + tipoIva + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(TipoIva tipoIva) throws NonexistentEntityException, Exception {
        try {
            tipoIva = em.merge(tipoIva);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = tipoIva.getTipoIvaId();
                if (findTipoIva(id) == null) {
                    throw new NonexistentEntityException("The contacto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        TipoIva tipoIva;
        try {
            tipoIva = em.getReference(TipoIva.class, id); 
            tipoIva.getTipoIvaId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The contacto with id " + id + " no longer exists.", enfe);
        }

        em.remove(tipoIva);
    }

    public List<TipoIva> findTipoIvaEntities() {
        return findTipoIvaEntities(true, -1, -1);
    }

    public List<TipoIva> findTipoIvaEntities(int maxResults, int firstResult) {
        return findTipoIvaEntities(false, maxResults, firstResult);
    }

    public List<TipoIva> findTipoIvaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(TipoIva.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public TipoIva findTipoIva(String id) {
        return em.find(TipoIva.class, id);
    }

    public int getTipoIvaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<TipoIva> rt = cq.from(TipoIva.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
