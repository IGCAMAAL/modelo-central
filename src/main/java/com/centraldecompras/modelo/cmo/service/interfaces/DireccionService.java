/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.cmo.service.interfaces;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface DireccionService {

    Direccion findDireccion(String id);

    List<Direccion> findDireccionesBy_S(SociedadRpl sociedad);

    List<Direccion> findDireccionesBy_P(Persona persona);

    List<Direccion> findDireccionesBy_A(Almacen almacen);

    void create(Direccion direccion) throws PreexistingEntityException, Exception;

    void edit(Direccion direccion) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    void create_C(Direccion direccion) throws PreexistingEntityException, Exception;

    List<Direccion> findDireccionEntities(boolean all, int maxResults, int firstResult);
}
