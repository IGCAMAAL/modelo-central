package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PUR_CompraPedidoPieLin")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = " CompraPedidoPieLin.findAll", query = ""
            + " SELECT p "
            + "   FROM CompraPedidoPieLin p"),
    
    @NamedQuery(name = " CompraPedidoPieLin.findByIdPedido", query = ""
            + " SELECT p "
            + "   FROM CompraPedidoPieLin p "
            + "  WHERE p.pedido.id = :compraPedidoIdk"),
    
    @NamedQuery(name = " CompraPedidoPieLin.findUltimaLinCompraPedidoPieLin", query = ""
            + " SELECT max(d.compraPedidoPieLin) "
            + "   FROM CompraPedidoPieLin d "
            + "  WHERE d.pedido.id = :compraPedidoIdk"),
    
    @NamedQuery(name = "CompraPedidoPieLin.findCompraPedidoPieLin", query = ""
            + " SELECT d "
            + "   FROM CompraPedidoPieLin d "
            + "  WHERE d.pedido.id = :compraPedidoIdk "
            + "    AND d.compraPedidoPieLin = :compraPedidoPieLin ")
})

public class CompraPedidoPieLin implements Serializable {

    private static final long serialVersionUID = 1L;

    // Atributos ID -----------------------------------------------------------------------------------------------    
    @Id
    @NotNull
    @ManyToOne(targetEntity = CompraPedido.class)
    private CompraPedido pedido;

    @Id
    @NotNull
    @Column(name = "compraPedidoPieLin")
    private int compraPedidoPieLin;

    @Version
    private int version;

    // Atributos Basicos--------------------------------------------------------------------------------------------   
    @Embedded
    DatosAuditoria atributosAuditoria;

    // 2 Constructores ---------------------------------------------------------------------------------------------
    public CompraPedidoPieLin() {
    }

    public CompraPedidoPieLin(String compraPedidoIdk, int compraPedidoPieLin, CompraPedido pedido, DatosAuditoria atributosAuditoria) {
        this.pedido = pedido;
        this.compraPedidoPieLin = compraPedidoPieLin;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Metodos  ID -----------------------------------------------------------------------------------------------
    public CompraPedido getPedido() {
        return pedido;
    }

    public void setPedido(CompraPedido pedido) {
        this.pedido = pedido;
    }
    
    public int getCompraPedidoPieLin() {
        return compraPedidoPieLin;
    }

    public void setCompraPedidoPieLin(int compraPedidoPieLin) {
        this.compraPedidoPieLin = compraPedidoPieLin;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Metodos  Relacion ----------------------------------------------------------------------------------------   


    // Metodos  Basicos ----------------------------------------------------------------------------------------   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Metodos  Entity ---------------------------------------------------------------------------------------- 

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.pedido);
        hash = 13 * hash + this.compraPedidoPieLin;
        hash = 13 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraPedidoPieLin other = (CompraPedidoPieLin) obj;
        if (!Objects.equals(this.pedido, other.pedido)) {
            return false;
        }
        if (this.compraPedidoPieLin != other.compraPedidoPieLin) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }


}
