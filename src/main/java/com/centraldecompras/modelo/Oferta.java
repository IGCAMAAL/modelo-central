package com.centraldecompras.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_VEN_Oferta")
@XmlRootElement
@NamedQueries({                          
   @NamedQuery(name = "Oferta.findOfertasBy_idS_Activas", query = ""
           + "    SELECT o "
           + "      FROM Oferta o, "
           + "           Articulo a "
           + "     WHERE o.articulo.id = a.id "
           + "       AND a.sociedad.id = :sociedadId "
           + "       AND o.estadoAprobacion IN (:estadosAprobacion) "
           + "       AND :fechaActual "
           + "   BETWEEN o.atributosAuditoria.fechaPrevistaActivacion  AND ( CASE WHEN o.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN 99999999 "
           + "                                                                    ELSE o.atributosAuditoria.fechaPrevistaDesactivacion "
           + "                                                               END)"),

   @NamedQuery(name = "Oferta.findOfertasBy_idS_ALL", query = ""
           + "    SELECT o "
           + "      FROM Oferta o, "
           + "           Articulo a "
           + "     WHERE o.articulo.id = a.id "
           + "       AND a.sociedad.id = :sociedadId ")
})
public class Oferta implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Oferta.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idOferta;

    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private BigDecimal uniMinVtaOferta;

    @NotNull
    private BigDecimal precioOfertaUniMed;
    
    private BigDecimal costeDiario;
    
    private String descripcionOferta;
    
    private String estadoAprobacion;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = Articulo.class)
    private Articulo articulo;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Oferta() {
    }

    public Oferta(String idOferta, BigDecimal uniMinVtaOferta, BigDecimal precioOfertaUniMed, BigDecimal costeDiario, String descripcionOferta, Articulo articulo, DatosAuditoria atributosAuditoria) {
        this.idOferta = idOferta;
        this.uniMinVtaOferta = uniMinVtaOferta;
        this.precioOfertaUniMed = precioOfertaUniMed;
        this.costeDiario = costeDiario;
        this.descripcionOferta = descripcionOferta;
        this.articulo = articulo;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(String idOferta) {
        this.idOferta = idOferta;
    }

    public BigDecimal getUniMinVtaOferta() {
        return uniMinVtaOferta;
    }

    public void setUniMinVtaOferta(BigDecimal uniMinVtaOferta) {
        this.uniMinVtaOferta = uniMinVtaOferta;
    }

    public BigDecimal getPrecioOfertaUniMed() {
        return precioOfertaUniMed;
    }

    public void setPrecioOfertaUniMed(BigDecimal precioOfertaUniMed) {
        this.precioOfertaUniMed = precioOfertaUniMed;
    }

    public BigDecimal getCosteDiario() {
        return costeDiario;
    }

    public void setCosteDiario(BigDecimal costeDiario) {
        this.costeDiario = costeDiario;
    }

    public String getDescripcionOferta() {
        return descripcionOferta;
    }

    public void setDescripcionOferta(String descripcionOferta) {
        this.descripcionOferta = descripcionOferta;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Articulo getArticulo() {
        return articulo;
    }
    
    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getEstadoAprobacion() {
        return estadoAprobacion;
    }

    public void setEstadoAprobacion(String estadoAprobacion) {
        this.estadoAprobacion = estadoAprobacion;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.idOferta);
        hash = 31 * hash + Objects.hashCode(this.uniMinVtaOferta);
        hash = 31 * hash + Objects.hashCode(this.precioOfertaUniMed);
        hash = 31 * hash + Objects.hashCode(this.costeDiario);
        hash = 31 * hash + Objects.hashCode(this.descripcionOferta);
        hash = 31 * hash + Objects.hashCode(this.estadoAprobacion);
        hash = 31 * hash + Objects.hashCode(this.articulo);
        hash = 31 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Oferta other = (Oferta) obj;
        if (!Objects.equals(this.idOferta, other.idOferta)) {
            return false;
        }
        if (!Objects.equals(this.uniMinVtaOferta, other.uniMinVtaOferta)) {
            return false;
        }
        if (!Objects.equals(this.precioOfertaUniMed, other.precioOfertaUniMed)) {
            return false;
        }
        if (!Objects.equals(this.costeDiario, other.costeDiario)) {
            return false;
        }
        if (!Objects.equals(this.descripcionOferta, other.descripcionOferta)) {
            return false;
        }
        if (!Objects.equals(this.estadoAprobacion, other.estadoAprobacion)) {
            return false;
        }
        if (!Objects.equals(this.articulo, other.articulo)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Oferta{" + "idOferta=" + idOferta + ", version=" + version + ", uniMinVtaOferta=" + uniMinVtaOferta + ", precioOfertaUniMed=" + precioOfertaUniMed + ", costeDiario=" + costeDiario + ", descripcionOferta=" + descripcionOferta + ", estadoAprobacion=" + estadoAprobacion + ", articulo=" + articulo + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


    
}
