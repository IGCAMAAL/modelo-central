package com.centraldecompras.modelo;

import java.io.Serializable;
import java.math.BigDecimal;

import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_VEN_DiasEntrega")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DiasEntrega.findDiasEntregaBy_idS", query = ""
        + " SELECT d "
        + "   FROM DiasEntrega d "
        + "  WHERE d.sociedad.id = :sociedadid " ),
    @NamedQuery(name = "DiasEntrega.findDiasEntregaBy_S_Z", query = ""
        + " SELECT d "
        + "   FROM DiasEntrega d "
        + "  WHERE d.sociedad = :sociedad " 
        + "    AND d.zonaGeografica = :zonaGeografica " ),
    @NamedQuery(name = "DiasEntrega.destroyBy_S_Z", query = ""
        + " DELETE "
        + "   FROM DiasEntrega d "
        + "  WHERE d.sociedad = :sociedad " 
        + "    AND d.zonaGeografica = :zonaGeografica " )
})
public class DiasEntrega implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(DiasEntrega.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @ManyToOne(targetEntity = SociedadRpl.class, fetch = FetchType.EAGER)
    private SociedadRpl sociedad;
    
    @Id
    @NotNull
    @ManyToOne(targetEntity=ZonaGeografica.class, fetch = FetchType.EAGER)
    private ZonaGeografica zonaGeografica;
    
    @Version
    private int version;

    // Atributos de basicos / relacion * * * * * * * * * * * * * * * * * * * * *
    private Boolean lunes;
    private Boolean martes;
    private Boolean miercoles;
    private Boolean jueves;
    private Boolean viernes;
    private Boolean sabado;
    private Boolean domingo;
    
    private BigDecimal pedidoMinimo;
    private BigDecimal pedidoMinimoLibreTrans;
    private BigDecimal precioTransporte;
    
    private int diasDemora;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public DiasEntrega() {
    }

    public DiasEntrega(SociedadRpl sociedad, Boolean lunes, Boolean martes, Boolean miercoles, Boolean jueves, Boolean viernes, Boolean sabado, Boolean domingo, BigDecimal pedidoMinimo, BigDecimal pedidoMinimoLibreTrans, BigDecimal precioTransporte, int diasDemora, ZonaGeografica zonaGeografica, DatosAuditoria atributosAuditoria) {
        this.sociedad = sociedad;
        this.lunes = lunes;
        this.martes = martes;
        this.miercoles = miercoles;
        this.jueves = jueves;
        this.viernes = viernes;
        this.sabado = sabado;
        this.domingo = domingo;
        this.pedidoMinimo = pedidoMinimo;
        this.pedidoMinimoLibreTrans = pedidoMinimoLibreTrans;
        this.precioTransporte = precioTransporte;
        this.diasDemora = diasDemora;
        this.zonaGeografica = zonaGeografica;
        this.atributosAuditoria = atributosAuditoria;
    }


    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SociedadRpl getSociedad() {
        return this.sociedad;
    }

    public void setSociedad(SociedadRpl sociedad) {
        this.sociedad = sociedad;
    }

    public ZonaGeografica getZonaGeografica() {
        return zonaGeografica;
    }

    public void setZonaGeografica(ZonaGeografica zonaGeografica) {
        this.zonaGeografica = zonaGeografica;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * *

    public Boolean getLunes() {
        return lunes;
    }

    public void setLunes(Boolean lunes) {
        this.lunes = lunes;
    }

    public Boolean getMartes() {
        return martes;
    }

    public void setMartes(Boolean martes) {
        this.martes = martes;
    }

    public Boolean getMiercoles() {
        return miercoles;
    }

    public void setMiercoles(Boolean miercoles) {
        this.miercoles = miercoles;
    }

    public Boolean getJueves() {
        return jueves;
    }

    public void setJueves(Boolean jueves) {
        this.jueves = jueves;
    }

    public Boolean getViernes() {
        return viernes;
    }

    public void setViernes(Boolean viernes) {
        this.viernes = viernes;
    }

    public Boolean getSabado() {
        return sabado;
    }

    public void setSabado(Boolean sabado) {
        this.sabado = sabado;
    }

    public Boolean getDomingo() {
        return domingo;
    }

    public void setDomingo(Boolean domingo) {
        this.domingo = domingo;
    }

    public BigDecimal getPedidoMinimo() {
        return pedidoMinimo;
    }

    public void setPedidoMinimo(BigDecimal pedidoMinimo) {
        this.pedidoMinimo = pedidoMinimo;
    }

    public BigDecimal getPedidoMinimoLibreTrans() {
        return pedidoMinimoLibreTrans;
    }

    public void setPedidoMinimoLibreTrans(BigDecimal pedidoMinimoLibreTrans) {
        this.pedidoMinimoLibreTrans = pedidoMinimoLibreTrans;
    }

    public BigDecimal getPrecioTransporte() {
        return precioTransporte;
    }

    public void setPrecioTransporte(BigDecimal precioTransporte) {
        this.precioTransporte = precioTransporte;
    }

    public int getDiasDemora() {
        return diasDemora;
    }

    public void setDiasDemora(int diasDemora) {
        this.diasDemora = diasDemora;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.sociedad);
        hash = 73 * hash + Objects.hashCode(this.zonaGeografica);
        hash = 73 * hash + Objects.hashCode(this.lunes);
        hash = 73 * hash + Objects.hashCode(this.martes);
        hash = 73 * hash + Objects.hashCode(this.miercoles);
        hash = 73 * hash + Objects.hashCode(this.jueves);
        hash = 73 * hash + Objects.hashCode(this.viernes);
        hash = 73 * hash + Objects.hashCode(this.sabado);
        hash = 73 * hash + Objects.hashCode(this.domingo);
        hash = 73 * hash + Objects.hashCode(this.pedidoMinimo);
        hash = 73 * hash + Objects.hashCode(this.pedidoMinimoLibreTrans);
        hash = 73 * hash + Objects.hashCode(this.precioTransporte);
        hash = 73 * hash + this.diasDemora;
        hash = 73 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DiasEntrega other = (DiasEntrega) obj;
        if (!Objects.equals(this.sociedad, other.sociedad)) {
            return false;
        }
        if (!Objects.equals(this.zonaGeografica, other.zonaGeografica)) {
            return false;
        }
        if (!Objects.equals(this.lunes, other.lunes)) {
            return false;
        }
        if (!Objects.equals(this.martes, other.martes)) {
            return false;
        }
        if (!Objects.equals(this.miercoles, other.miercoles)) {
            return false;
        }
        if (!Objects.equals(this.jueves, other.jueves)) {
            return false;
        }
        if (!Objects.equals(this.viernes, other.viernes)) {
            return false;
        }
        if (!Objects.equals(this.sabado, other.sabado)) {
            return false;
        }
        if (!Objects.equals(this.domingo, other.domingo)) {
            return false;
        }
        if (!Objects.equals(this.pedidoMinimo, other.pedidoMinimo)) {
            return false;
        }
        if (!Objects.equals(this.pedidoMinimoLibreTrans, other.pedidoMinimoLibreTrans)) {
            return false;
        }
        if (!Objects.equals(this.precioTransporte, other.precioTransporte)) {
            return false;
        }
        if (this.diasDemora != other.diasDemora) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DiasEntrega{" + "sociedad=" + sociedad + ", zonaGeografica=" + zonaGeografica + ", version=" + version + ", lunes=" + lunes + ", martes=" + martes + ", miercoles=" + miercoles + ", jueves=" + jueves + ", viernes=" + viernes + ", sabado=" + sabado + ", domingo=" + domingo + ", pedidoMinimo=" + pedidoMinimo + ", pedidoMinimoLibreTrans=" + pedidoMinimoLibreTrans + ", precioTransporte=" + precioTransporte + ", diasDemora=" + diasDemora + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
