package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_PRM_FormatoVta")
@XmlRootElement
public class FormatoVta implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(FormatoVtaLang.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idFormatoVta;
    
    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private String tipoEnvase;

    @NotNull
    private String descEnvase;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    // @ManyToOne
    // @JoinColumn(name="department_id")
    // private FormatoVtaLang department;
        
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public FormatoVta() {
    }

    public FormatoVta(String idFormatoVta, int version, String tipoEnvase, String descEnvase, DatosAuditoria atributosAuditoria) {
        this.idFormatoVta = idFormatoVta;
        this.version = version;
        this.tipoEnvase = tipoEnvase;
        this.descEnvase = descEnvase;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
    public String getIdFormatoVta() {
        return this.idFormatoVta;
    }

    public void setIdFormatoVta(String idFormatoVta) {
        this.idFormatoVta = idFormatoVta;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
    public String getDescEnvase() {
        return this.descEnvase;
    }

    public void setDescEnvase(String descEnvase) {
        this.descEnvase = descEnvase;
    }

    public String getTipoEnvase() {
        return this.tipoEnvase;
    }

    public void setTipoEnvase(String tipoEnvase) {
        this.tipoEnvase = tipoEnvase;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormatoVta other = (FormatoVta) obj;
        if ((this.idFormatoVta == null) ? (other.idFormatoVta != null) : !this.idFormatoVta.equals(other.idFormatoVta)) {
            return false;
        }
        if ((this.tipoEnvase == null) ? (other.tipoEnvase != null) : !this.tipoEnvase.equals(other.tipoEnvase)) {
            return false;
        }
        if ((this.descEnvase == null) ? (other.descEnvase != null) : !this.descEnvase.equals(other.descEnvase)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.idFormatoVta != null ? this.idFormatoVta.hashCode() : 0);
        hash = 41 * hash + (this.tipoEnvase != null ? this.tipoEnvase.hashCode() : 0);
        hash = 41 * hash + (this.descEnvase != null ? this.descEnvase.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "FormatoVta{" + "idFormatoVta=" + idFormatoVta + ", version=" + version + ", tipoEnvase=" + tipoEnvase + ", descEnvase=" + descEnvase + ", atributosAuditoria=" + atributosAuditoria + '}';
    }



}
