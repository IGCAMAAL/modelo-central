/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface SocZonGeoService {

    void create(SocZonGeo socZonGeo) throws PreexistingEntityException, Exception;

    void edit(SocZonGeo socZonGeo) throws NonexistentEntityException, Exception;
    
    void destroy(String id) throws NonexistentEntityException;
    
    SocZonGeo findSocZonGeo(String id);
    
    SocZonGeo findSocZonGeoBy_idZidS(String zonaGeograficaid, String sociedadid);
    
    List<SocZonGeo> findSocZonGeoBy_idS(String sociedadid);
    
    List<SocZonGeo> findSocZonGeoEntities(boolean all, int maxResults, int firstResult);

    void create_C(SocZonGeo socZonGeo) throws PreexistingEntityException, Exception;
    
    List<SocZonGeo> findsocZonasGeograficasOrderBy_idS_N();
    
    SocZonGeo findSocZonGeoBy_idS_idZ_Estado(String sociedadId, String zonaGeograficaId, String estado);

}
