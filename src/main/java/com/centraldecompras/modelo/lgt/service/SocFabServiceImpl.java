/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocFab;
import com.centraldecompras.modelo.lgt.service.interfaces.SocFabService;
import com.centraldecompras.modelo.SociedadRpl;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class SocFabServiceImpl implements SocFabService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocFabServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(SocFab socFab) throws PreexistingEntityException, Exception {
        try {
            em.persist(socFab);
        } catch (Exception ex) {
            if (findSocFab(socFab.getSocDist()) != null) {
                throw new PreexistingEntityException("SocFab " + socFab + " already exists.", ex);
            }
            throw ex;
        } 
    }
    
    public void create(SocFab socFab) throws PreexistingEntityException, Exception {
        try {
            em.persist(socFab);
        } catch (Exception ex) {
            if (findSocFab(socFab.getSocDist()) != null) {
                throw new PreexistingEntityException("SocFab " + socFab + " already exists.", ex);
            }
            throw ex;
        } 
    }

    public void edit(SocFab socFab) throws NonexistentEntityException, Exception {
        try {
            socFab = em.merge(socFab);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                SociedadRpl id = socFab.getSocDist();
                if (findSocFab(id) == null) {
                    throw new NonexistentEntityException("The socFab with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(String id) throws NonexistentEntityException {
            SocFab socFab;
            try {
                socFab = em.getReference(SocFab.class, id);
                socFab.getSocDist();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The socFab with id " + id + " no longer exists.", enfe);
            }
            em.remove(socFab);
    }

    public void destroy(SociedadRpl id) throws NonexistentEntityException {
        destroy(id.getIdSociedad());
    }

    public List<SocFab> findSocFabEntities() {
        return findSocFabEntities(true, -1, -1);
    }

    public List<SocFab> findSocFabEntities(int maxResults, int firstResult) {
        return findSocFabEntities(false, maxResults, firstResult);
    }

    private List<SocFab> findSocFabEntities(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SocFab.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
    }

    public SocFab findSocFab(String id) {
            return em.find(SocFab.class, id);
    }

    public SocFab findSocFab(SociedadRpl id) {
        return findSocFab(id.getIdSociedad());
    }

    public int getSocFabCount() {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SocFab> rt = cq.from(SocFab.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
    }

}
