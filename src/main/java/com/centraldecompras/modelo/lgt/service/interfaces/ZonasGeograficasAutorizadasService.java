/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.ZonasGeograficasAutorizadas;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ZonasGeograficasAutorizadasService{

    ZonasGeograficasAutorizadas findZonasGeograficasAutorizadas(String id);

    void create(ZonasGeograficasAutorizadas zonasGeograficasAutorizadas)  throws PreexistingEntityException, Exception;
    void create_C(ZonasGeograficasAutorizadas zonasGeograficasAutorizadas) throws PreexistingEntityException, Exception;
    List<ZonasGeograficasAutorizadas> findZonasGeograficasAutorizadasEntities(boolean all, int maxResults, int firstResult);
}
