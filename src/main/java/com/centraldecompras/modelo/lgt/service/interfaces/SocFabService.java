/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocFab;

/**
 *
 * @author ciberado
 */
public interface SocFabService{

    SocFab findSocFab(String id);

    void create(SocFab socFab)  throws PreexistingEntityException, Exception;
    void create_C(SocFab socFab) throws PreexistingEntityException, Exception;
}
