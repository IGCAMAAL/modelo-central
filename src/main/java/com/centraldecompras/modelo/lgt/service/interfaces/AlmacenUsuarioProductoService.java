/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.Map;

public interface AlmacenUsuarioProductoService {

    void create(AlmacenUsuarioProducto almacenUsuarioProducto) throws PreexistingEntityException, Exception;

    void edit(AlmacenUsuarioProducto almacenUsuarioProducto) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoEntities();

    List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoEntities(int maxResults, int firstResult);

    int getAlmacenUsuarioProductoCount();

    AlmacenUsuarioProducto findAlmacenUsuarioProductoBy_AUP(Almacen almacen, Persona usuario, Producto producto);

    List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoBy_AU(String tipoReg, Almacen almacen, Persona usuario, List<String> estadoRegistro);

    List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoBy_A(String tipoReg, Almacen almacen, List<String> estadoRegistro);

    public AlmacenUsuarioProducto findAlmacenUsuarioProductoBy_idA_idU_idP_Estado(String productoId, String almacenId, String ususarioId, String estado);

    List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoBY_P(Producto producto);

}
