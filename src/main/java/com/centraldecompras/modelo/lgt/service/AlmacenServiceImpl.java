package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AlmacenServiceImpl implements AlmacenService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AlmacenServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(Almacen almacen) throws PreexistingEntityException, Exception {
        try {
            em.persist(almacen);
        } catch (Exception ex) {
            if (findAlmacen(almacen.getIdAlmacen()) != null) {
                throw new PreexistingEntityException("Almacen " + almacen + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(Almacen almacen) throws PreexistingEntityException, Exception {
        try {
            em.persist(almacen);
        } catch (Exception ex) {
            if (findAlmacen(almacen.getIdAlmacen()) != null) {
                throw new PreexistingEntityException("Almacen " + almacen + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(Almacen almacen) throws NonexistentEntityException, Exception {
        try {
            almacen = em.merge(almacen);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = almacen.getIdAlmacen();
                if (findAlmacen(id) == null) {
                    throw new NonexistentEntityException("The almacen with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Almacen almacen;
        try {
            almacen = em.getReference(Almacen.class, id);
            almacen.getIdAlmacen();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The almacen with id " + id + " no longer exists.", enfe);
        }
        em.remove(almacen);
    }

    public List<Almacen> findAlmacenEntities() {
        return findAlmacenEntities(true, -1, -1);
    }

    public List<Almacen> findAlmacenEntities(int maxResults, int firstResult) {
        return findAlmacenEntities(false, maxResults, firstResult);
    }

    private List<Almacen> findAlmacenEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Almacen.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Almacen findAlmacen(String id) {
        return em.find(Almacen.class, id);
    }

    public int getAlmacenCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Almacen> rt = cq.from(Almacen.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Almacen> findAlmacenesBy_idS(String sociedadIdk) {
        List<Almacen> reply = null;
        try {
            reply = (List<Almacen>) em.createNamedQuery("Almacen.findAlmacenesBy_idS")
                    .setParameter("sociedadIdk", sociedadIdk)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado almacenes para la sociedad:  " + sociedadIdk);
            // ex.printStackTrace();
        }
        if (reply == null) {
            reply = new ArrayList();
        }
        return reply;
    }
}
