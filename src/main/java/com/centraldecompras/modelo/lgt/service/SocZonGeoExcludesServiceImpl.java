/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.modelo.SocProductoExcl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocZonGeoExcl;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoExcludesService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel
 */
@Service
public class SocZonGeoExcludesServiceImpl implements SocZonGeoExcludesService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocZonGeoExcludesServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(SocZonGeoExcl socZonGeoExcl) throws PreexistingEntityException, Exception {
        try {
            em.persist(socZonGeoExcl); 
        } catch (Exception ex) {
            if (findSocZonExclGeo(socZonGeoExcl.getId()) != null) {
                throw new PreexistingEntityException("socZonGeoExcl " + socZonGeoExcl + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(SocZonGeoExcl socZonGeoExcl) throws NonexistentEntityException, Exception { 
        try {
            socZonGeoExcl = em.merge(socZonGeoExcl);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = socZonGeoExcl.getId();
                if (findSocZonExclGeo(id) == null) {
                    throw new NonexistentEntityException("The socZonGeo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        SocZonGeoExcl socZonGeoExcl;
        try {
            socZonGeoExcl = em.getReference(SocZonGeoExcl.class, id);
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The socZonGeoExcl with id " + id + " no longer exists.", enfe);
        }
        em.remove(socZonGeoExcl);
    }

    public List<SocZonGeoExcl> findSocZonGeoExclEntities() {
        return SocZonGeoExcludesServiceImpl.this.findSocZonGeoExclEntities(true, -1, -1);
    }

    public List<SocZonGeoExcl> findSocZonGeoExclEntities(int maxResults, int firstResult) {
        return SocZonGeoExcludesServiceImpl.this.findSocZonGeoExclEntities(false, maxResults, firstResult);
    }

    public List<SocZonGeoExcl> findSocZonGeoExclEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SocZonGeoExcl.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public SocZonGeoExcl findSocZonExclGeo(String id) {
        return em.find(SocZonGeoExcl.class, id);
    }

    public SocZonGeoExcl findSocZonGeoExcl(SociedadRpl id) {
        return findSocZonExclGeo(id.getIdSociedad());
    }

    public int getSocZonGeoExclCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SocZonGeoExcl> rt = cq.from(SocZonGeoExcl.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public SocZonGeoExcl findSocZonGeoExcludesBy_idZ_r(String sociedadId, String route) {
        List<SocZonGeoExcl> reply = null;
        try {
            reply = (List<SocZonGeoExcl>) em.createNamedQuery("SocZonGeoExcl.findSocZonGeoExcludesBy_idZ_r")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("route", route)
                    .getResultList();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        } 
        if(reply.size()>0){
            return reply.get(0); 
        } else {
            return null;
        }
    }

    public int destroySocZonGeoExcludesBy_idZ_r(String sociedadId, String route) throws NonexistentEntityException {
        int reply = 0;
        try {
            reply = (int) em.createNamedQuery("SocZonGeoExcl.destroySocZonGeoExcludesBy_idZ_r")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("route", route)
                    .executeUpdate();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return reply;
    }

}
