package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuarioProductoService;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class AlmacenUsuarioProductoServiceImpl implements AlmacenUsuarioProductoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AlmacenUsuarioProductoServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(AlmacenUsuarioProducto almacenUsuarioProducto) throws PreexistingEntityException, Exception {
        try {
            em.persist(almacenUsuarioProducto);
        } catch (Exception ex) {
            if (findAlmacenUsuarioProductoBy_AUP(almacenUsuarioProducto.getAlmacen(), almacenUsuarioProducto.getUsuario(), almacenUsuarioProducto.getProducto()) != null) {
                throw new PreexistingEntityException("almacenUsuarioProducto " + almacenUsuarioProducto.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(AlmacenUsuarioProducto almacenUsuarioProducto) throws NonexistentEntityException, Exception {
        try {
            almacenUsuarioProducto = em.merge(almacenUsuarioProducto);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findAlmacenUsuarioProductoBy_AUP(almacenUsuarioProducto.getAlmacen(), almacenUsuarioProducto.getUsuario(), almacenUsuarioProducto.getProducto()) == null) {
                    throw new PreexistingEntityException("almacenUsuarioProducto " + almacenUsuarioProducto.toString() + " no longer exists.", ex);
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        // TODO
    }

    public List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoEntities() {
        return findAlmacenUsuarioProductoEntities(true, -1, -1);
    }

    public List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoEntities(int maxResults, int firstResult) {
        return findAlmacenUsuarioProductoEntities(false, maxResults, firstResult);
    }

    private List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(AlmacenUsuarioProducto.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getAlmacenUsuarioProductoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<AlmacenUsuarioProducto> rt = cq.from(AlmacenUsuarioProducto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public AlmacenUsuarioProducto findAlmacenUsuarioProductoBy_AUP(Almacen almacen, Persona usuario, Producto producto) {
        AlmacenUsuarioProducto reply = null;
        try {
            reply = (AlmacenUsuarioProducto) em.createNamedQuery("AlmacenUsuarioProducto.findAlmacenUsuarioProductoBy_AUP")
                    .setParameter("almacen", almacen)
                    .setParameter("usuario", usuario)
                    .setParameter("producto", producto)
                    .getSingleResult();
        } catch (Exception ex) {
            throw ex;
        }
        return reply;
    }

    public List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoBy_AU(String tipoReg, Almacen almacen, Persona usuario, List<String> estadoRegistro) {
        List<AlmacenUsuarioProducto> reply = null;
        try {
            reply = (List<AlmacenUsuarioProducto>) em.createNamedQuery("AlmacenUsuarioProducto.findAlmacenUsuarioProductoBy_AU")
                    .setParameter("tipoReg", tipoReg)
                    .setParameter("almacen", almacen)
                    .setParameter("usuario", usuario)
                    .setParameter("estadoRegistro", estadoRegistro)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex.getMessage() + ".............No se ha encontrado una lista de prodcutos paar la combinación Almacen / Persona  dada:  " + almacen.getNombreAlmacen() + " / " + usuario.getUserName());
        }
        return reply;
    }

    public List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoBy_A(String tipoReg, Almacen almacen, List<String> estadoRegistro) {
        List<AlmacenUsuarioProducto> reply = null;
        try {
            reply = (List<AlmacenUsuarioProducto>) em.createNamedQuery("AlmacenUsuarioProducto.findAlmacenUsuarioProductoBy_A")
                    .setParameter("tipoReg", tipoReg)
                    .setParameter("almacen", almacen)
                    .setParameter("estadoRegistro", estadoRegistro)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex.getMessage() + ".............No se ha encontrado una lista de " + tipoReg + " para el Almacen :  " + almacen.getNombreAlmacen());
        }
        return reply;
    }

    public AlmacenUsuarioProducto findAlmacenUsuarioProductoBy_idA_idU_idP_Estado(String almacenId, String ususarioId, String productoId, String estado) {
        AlmacenUsuarioProducto almacenUsuarioProducto = null;
        try {
            almacenUsuarioProducto = (AlmacenUsuarioProducto) em.createNamedQuery("AlmacenUsuarioProducto.AlmacenUsuarioProductoBy_idA_idU_idP_Estado")
                    .setParameter("almacenId", almacenId)
                    .setParameter("ususarioId", ususarioId)
                    .setParameter("productoId", productoId)
                    .setParameter("estado", estado)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la relacion Almacen / Usuaeio / Producto = " + productoId + " - " + almacenId + " - " + ususarioId + " - " + estado);
        }
        return almacenUsuarioProducto;
    }

    public List<AlmacenUsuarioProducto> findAlmacenUsuarioProductoBY_P(Producto producto) {
        List<AlmacenUsuarioProducto> almacenUsuarioProducto = null;
        try {
            almacenUsuarioProducto = (List<AlmacenUsuarioProducto>) em.createNamedQuery("AlmacenUsuarioProducto.findAlmacenUsuarioProductoBY_P")
                    .setParameter("producto", producto)
                    .getResultList();
        } catch (Exception ex) {
            log.info(ex + ".............No se han encontrado Productos = " + producto.getIdProducto());
        }
        return almacenUsuarioProducto;
    }



}
