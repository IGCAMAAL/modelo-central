/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.ZonasGeograficasAutorizadas;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonasGeograficasAutorizadasService;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class ZonasGeograficasAutorizadasServiceImpl implements ZonasGeograficasAutorizadasService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ZonasGeograficasAutorizadasServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(ZonasGeograficasAutorizadas zonasGeograficasAutorizadas) {
            em.persist(zonasGeograficasAutorizadas);
    }

    public void create(ZonasGeograficasAutorizadas zonasGeograficasAutorizadas) {
            em.persist(zonasGeograficasAutorizadas);
    }

    public void edit(ZonasGeograficasAutorizadas zonasGeograficasAutorizadas) throws NonexistentEntityException, Exception {
        try {
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = zonasGeograficasAutorizadas.getIdZonasGeograficasDeAccion();
                if (findZonasGeograficasAutorizadas(id) == null) {
                    throw new NonexistentEntityException("The zonasGeograficasAutorizadas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(String id) throws NonexistentEntityException {
            ZonasGeograficasAutorizadas zonasGeograficasAutorizadas;
            try {
                zonasGeograficasAutorizadas = em.getReference(ZonasGeograficasAutorizadas.class, id);
                zonasGeograficasAutorizadas.getIdZonasGeograficasDeAccion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The zonasGeograficasAutorizadas with id " + id + " no longer exists.", enfe);
            }
            em.remove(zonasGeograficasAutorizadas);
    }

    public List<ZonasGeograficasAutorizadas> findZonasGeograficasAutorizadasEntities() {
        return findZonasGeograficasAutorizadasEntities(true, -1, -1);
    }

    public List<ZonasGeograficasAutorizadas> findZonasGeograficasAutorizadasEntities(int maxResults, int firstResult) {
        return findZonasGeograficasAutorizadasEntities(false, maxResults, firstResult);
    }

    public List<ZonasGeograficasAutorizadas> findZonasGeograficasAutorizadasEntities(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ZonasGeograficasAutorizadas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
    }

    public ZonasGeograficasAutorizadas findZonasGeograficasAutorizadas(String id) {
            return em.find(ZonasGeograficasAutorizadas.class, id);
    }

    public int getZonasGeograficasAutorizadasCount() {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ZonasGeograficasAutorizadas> rt = cq.from(ZonasGeograficasAutorizadas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
    }

}
