/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.CompraLinea;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocZonGeoExcl;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.modelo.UnidadMedidaLang;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class ZonaGeograficaServiceImpl implements ZonaGeograficaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ZonaGeograficaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(ZonaGeografica zonaGeografica) {
        em.persist(zonaGeografica);
    }

    @Transactional
    public void create(ZonaGeografica zonaGeografica) {
        em.persist(zonaGeografica);
    }

    @Transactional
    public void edit(ZonaGeografica zonaGeografica) throws NonexistentEntityException, Exception {
        try {
            zonaGeografica = em.merge(zonaGeografica);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = zonaGeografica.getIdZonaGeografica();
                if (findZonaGeografica(id) == null) {
                    throw new NonexistentEntityException("The zonaGeografica with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    @Transactional
    public void destroy(String id) throws NonexistentEntityException {
        ZonaGeografica zonaGeografica;
        try {
            zonaGeografica = em.getReference(ZonaGeografica.class, id);
            zonaGeografica.getIdZonaGeografica();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The zonaGeografica with id " + id + " no longer exists.", enfe);
        }
        em.remove(zonaGeografica);
    }

    public List<ZonaGeografica> findZonaGeograficaEntities() {
        return findZonaGeograficaEntities(true, -1, -1);
    }

    public List<ZonaGeografica> findZonaGeograficaEntities(int maxResults, int firstResult) {
        return findZonaGeograficaEntities(false, maxResults, firstResult);
    }

    public List<ZonaGeografica> findZonaGeograficaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ZonaGeografica.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public ZonaGeografica findZonaGeografica(String id) {
        return em.find(ZonaGeografica.class, id);
    }

    public int getZonaGeograficaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ZonaGeografica> rt = cq.from(ZonaGeografica.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<ZonaGeografica> findZonasGeograficasBy_ParentId(ZonaGeografica zonaGeografica) {
        return em.createNamedQuery("ZonaGeografica.findZonasGeograficasBy_ParentId")
                .setParameter("padreJerarquia", zonaGeografica)
                .getResultList();
    }

    @Override
    public List<ZonaGeografica> findZonaGeograficaTypeAhead(int tipo, String cadena, String estado, int cantidadInt) {
        Query query = null;

        StringBuffer descripcionSql = new StringBuffer();
        StringBuffer descripcionExcludeSql = new StringBuffer();

        if (tipo == 1) {
            // Empieza
            descripcionSql.append(cadena).append("%");
            if (estado.equals(KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("ZonaGeografica.findZonaGeograficaEmpiezaTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("ZonaGeografica.findZonaGeograficaEmpiezaTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setMaxResults(cantidadInt);
            }

        } else {
            // Contiene
            descripcionSql.append("%").append(cadena).append("%");
            descripcionExcludeSql.append(cadena).append("%");

            if (estado.equals(KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("ZonaGeografica.findZonaGeograficaContieneTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("ZonaGeografica.findZonaGeograficaContieneTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setMaxResults(cantidadInt);
            }
        }

        return query.getResultList();
    }

    public List<ZonaGeografica> findZonasGeograficasOrderBy_N() {
        Query query = null;
        query = em.createNamedQuery("ZonaGeografica.findZonasGeograficasOrderBy_N");
        return query.getResultList();
    }

    @Transactional
    public void persistencia(List<Object> createdObjets, List<Object> updatedObjets) throws Exception {
        log.info("....................................................................mbm............CONTROL");

        for(Object createdObjet : createdObjets){
            if(createdObjet instanceof ZonaGeografica) {
                create((ZonaGeografica)createdObjet);
            }
        }
        for(Object updatedObjet : updatedObjets){
            if(updatedObjet instanceof ZonaGeografica) {
                edit((ZonaGeografica)updatedObjet);
            }
        }        
    }    
    
    public List<ZonaGeografica> findZonasGeograficasBy_idZ(String zonaGeograficaId) {
        List<ZonaGeografica> zonasGeograficas = new ArrayList(); 
        try {
            zonasGeograficas = (List<ZonaGeografica>) em.createNamedQuery("ZonaGeografica.findZonasGeograficasBy_idZ")
                    .setParameter("zonaGeograficaId", zonaGeograficaId)
                    .getResultList();
        } catch (Exception ex) {
            log.info(ex + ".............No se han encontrado Zonas = " + zonaGeograficaId);
        }
        return zonasGeograficas; 
    }     
   
    public List<ZonaGeografica> findZonasGeograficasRouteBy_idZ(String zonaGeograficaId) {
        List<ZonaGeografica> reply = new ArrayList();
        
        CriteriaBuilder cb = em.getCriteriaBuilder();

        // Qry de la tabla principal
        CriteriaQuery<ZonaGeografica> qry = cb.createQuery(ZonaGeografica.class);
        Root<ZonaGeografica> root = qry.from(ZonaGeografica.class);        
        qry.select(root); 
         
        // SubQry de la tabla de join
        Subquery<SocZonGeoExcl> subqry = qry.subquery(SocZonGeoExcl.class);
        Root<SocZonGeoExcl> subroot = subqry.from(SocZonGeoExcl.class);
        //Join<ZonaGeografica, SocZonGeoExcl> join = root.join("idZonaGeografica");        
        subqry.select(subroot); 

        Predicate correlatePredicate = cb.like (subroot.get("route"), "%"+zonaGeograficaId+"%");
        subqry.where(correlatePredicate);

        List<Predicate> p = new ArrayList<>(); 
        // Parametros de la tabla Principal                
        p.add(cb.like (root.get("route"), "%"+zonaGeograficaId+"%"));        
        p.add(cb.equal(root.get("atributosAuditoria").get("estadoRegistro"), KApp.ACTIVO.getKApp()));
        // Parametros de la subquery
        p.add(cb.not(cb.exists(subqry)));
        
        qry.where(p.toArray(new Predicate[]{}));
        
        // Ordenacion de la tabla principal
        List<Order> orderList = new ArrayList();        
        //orderList.add(cb.desc(root.get("route")));
        orderList.add(cb.desc(root.get("nivelInt")));        
        qry.orderBy(orderList);
        
        Query query = em.createQuery(qry);
        
        reply = (List<ZonaGeografica>) query.getResultList();        
        
        return reply; 
        
    }
    
}
