package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.AlmacenUsuProdExcl;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenUsuProdExcludesService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class AlmacenUsuProdExcludesServiceImpl implements AlmacenUsuProdExcludesService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AlmacenUsuProdExcludesServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(AlmacenUsuProdExcl almacenUsuarioProductoExcludes) throws PreexistingEntityException, Exception {
        try {
            em.persist(almacenUsuarioProductoExcludes);
        } catch (Exception ex) {
            if (findAlmacenUsuarioProductoExcludesBy_idA_idU_r(almacenUsuarioProductoExcludes.getAlmacenId(), almacenUsuarioProductoExcludes.getUsuarioId(), almacenUsuarioProductoExcludes.getRoute()) != null) {
                throw new PreexistingEntityException("almacenUsuarioProducto " + almacenUsuarioProductoExcludes.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(AlmacenUsuProdExcl almacenUsuarioProductoExcludes) throws NonexistentEntityException, Exception {
        try {
            almacenUsuarioProductoExcludes = em.merge(almacenUsuarioProductoExcludes);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findAlmacenUsuarioProductoExcludesBy_idA_idU_r(almacenUsuarioProductoExcludes.getAlmacenId(), almacenUsuarioProductoExcludes.getUsuarioId(), almacenUsuarioProductoExcludes.getRoute()) == null) {
                    throw new PreexistingEntityException("almacenUsuarioProducto " + almacenUsuarioProductoExcludes.toString() + " no longer exists.", ex);
                }
            }
            throw ex;
        }
    }

    public List<AlmacenUsuProdExcl> findSocProductoExcludesEntities() {
        return findAlmacenUsuarioProductoExcludesEntities(true, -1, -1);
    }

    public List<AlmacenUsuProdExcl> findSocProductoExcludesEntities(int maxResults, int firstResult) {
        return findAlmacenUsuarioProductoExcludesEntities(false, maxResults, firstResult);
    }

    private List<AlmacenUsuProdExcl> findAlmacenUsuarioProductoExcludesEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(AlmacenUsuProdExcl.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getSocProductoExcludesCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<AlmacenUsuarioProducto> rt = cq.from(AlmacenUsuarioProducto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public AlmacenUsuProdExcl findAlmacenUsuarioProductoExcludesBy_idA_idU_r(String almacenId, String usuarioId, String route) {
        AlmacenUsuProdExcl reply = null;
        try {
            reply = (AlmacenUsuProdExcl) em.createNamedQuery("AlmacenUsuarioProductoExcludes.findAlmacenUsuarioProductoExcludesBy_idA_idU_r")
                    .setParameter("almacenId", almacenId)
                    .setParameter("usuarioId", usuarioId)
                    .setParameter("route", route)
                    .getSingleResult();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return reply;
    }

    public int destroyAlmacenUsuarioProductoExcludesBy_idA_idU_r(String almacenId, String usuarioId, String route) throws NonexistentEntityException {
        int reply = 0;
        try {
            reply = (int) em.createNamedQuery("AlmacenUsuarioProductoExcludes.destroyAlmacenUsuarioProductoExcludesBy_idA_idU_r")
                    .setParameter("almacenId", almacenId)
                    .setParameter("usuarioId", usuarioId)
                    .setParameter("route", route)
                    .executeUpdate();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return reply;
    }

}
