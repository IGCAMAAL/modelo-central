/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocZonGeoExcl;
import com.centraldecompras.modelo.SociedadRpl;
import java.util.List;

/**
 *
 * @author Miguel
 */

public interface SocZonGeoExcludesService {

    public void create(SocZonGeoExcl socZonGeoExcl) throws PreexistingEntityException, Exception ;

    public void edit(SocZonGeoExcl socZonGeoExcl) throws NonexistentEntityException, Exception ;

    public void destroy(String id) throws NonexistentEntityException ;

    public List<SocZonGeoExcl> findSocZonGeoExclEntities() ;

    public List<SocZonGeoExcl> findSocZonGeoExclEntities(int maxResults, int firstResult) ;

    public List<SocZonGeoExcl> findSocZonGeoExclEntities(boolean all, int maxResults, int firstResult) ;

    public SocZonGeoExcl findSocZonExclGeo(String id) ;

    public SocZonGeoExcl findSocZonGeoExcl(SociedadRpl id) ;

    public int getSocZonGeoExclCount() ;
    
    public SocZonGeoExcl findSocZonGeoExcludesBy_idZ_r(String sociedadId, String route) ;

    public int destroySocZonGeoExcludesBy_idZ_r(String sociedadId, String route) throws NonexistentEntityException ;
    



}
