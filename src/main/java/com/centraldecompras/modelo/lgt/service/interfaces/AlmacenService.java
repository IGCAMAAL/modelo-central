/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface AlmacenService {

    Almacen findAlmacen(String id);

    void create(Almacen almacen) throws PreexistingEntityException, Exception;

    void edit(Almacen almacen) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;
    
    void create_C(Almacen almacen) throws PreexistingEntityException, Exception;

    List<Almacen> findAlmacenesBy_idS(String sociedadIdk);
 }
