/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.modelo.Producto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.ZonaGeografica;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ZonaGeograficaService {

    ZonaGeografica findZonaGeografica(String id);

    void create(ZonaGeografica zonaGeografica) throws PreexistingEntityException, Exception;

    void create_C(ZonaGeografica zonaGeografica);

    void edit(ZonaGeografica zonaGeografica) throws NonexistentEntityException, Exception;

    List<ZonaGeografica> findZonaGeograficaEntities();
    
    List<ZonaGeografica> findZonasGeograficasBy_ParentId(ZonaGeografica zonaGeografica);

    List<ZonaGeografica> findZonaGeograficaTypeAhead(int tipo, String cadena, String estado, int cantidadInt);

    List<ZonaGeografica> findZonaGeograficaEntities(boolean all, int maxResults, int firstResult);
    
    List<ZonaGeografica> findZonasGeograficasOrderBy_N();
    
    public void persistencia(List<Object> createdObjets, List<Object> updatedObjets) throws Exception ;
    
    List<ZonaGeografica> findZonasGeograficasBy_idZ(String zonaGeograficaId);
    
    List<ZonaGeografica> findZonasGeograficasRouteBy_idZ(String zonaGeograficaId);
  
    
}
