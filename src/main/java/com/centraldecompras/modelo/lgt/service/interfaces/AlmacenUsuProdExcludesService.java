package com.centraldecompras.modelo.lgt.service.interfaces;

import com.centraldecompras.modelo.AlmacenUsuProdExcl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;


public interface AlmacenUsuProdExcludesService  {

    public void create(AlmacenUsuProdExcl almacenUsuarioProductoExcludes) throws PreexistingEntityException, Exception ;

    public void edit(AlmacenUsuProdExcl almacenUsuarioProductoExcludes) throws NonexistentEntityException, Exception ;

    public List<AlmacenUsuProdExcl> findSocProductoExcludesEntities();

    public List<AlmacenUsuProdExcl> findSocProductoExcludesEntities(int maxResults, int firstResult);

    public int getSocProductoExcludesCount();

    public AlmacenUsuProdExcl findAlmacenUsuarioProductoExcludesBy_idA_idU_r(String almacenId, String usuarioId, String route);

    public int destroyAlmacenUsuarioProductoExcludesBy_idA_idU_r(String almacenId, String usuarioId, String route) throws NonexistentEntityException ;       

}
