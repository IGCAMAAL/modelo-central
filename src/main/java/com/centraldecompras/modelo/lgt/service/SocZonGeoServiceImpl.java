/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.lgt.service;

import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoService;
import com.centraldecompras.modelo.SociedadRpl;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class SocZonGeoServiceImpl implements SocZonGeoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocZonGeoServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(SocZonGeo socZonGeo) throws PreexistingEntityException, Exception {
        try {
            em.persist(socZonGeo);
        } catch (Exception ex) {
            if (findSocZonGeo(socZonGeo.getSociedad()) != null) {
                throw new PreexistingEntityException("SocZonGeo " + socZonGeo + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(SocZonGeo socZonGeo) throws PreexistingEntityException, Exception {
        try {
            em.persist(socZonGeo);
        } catch (Exception ex) {
            if (findSocZonGeo(socZonGeo.getSociedad()) != null) {
                throw new PreexistingEntityException("SocZonGeo " + socZonGeo + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(SocZonGeo socZonGeo) throws NonexistentEntityException, Exception {
        try {
            socZonGeo = em.merge(socZonGeo);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                SociedadRpl id = socZonGeo.getSociedad();
                if (findSocZonGeo(id) == null) {
                    throw new NonexistentEntityException("The socZonGeo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        SocZonGeo socZonGeo;
        try {
            socZonGeo = em.getReference(SocZonGeo.class, id);
            socZonGeo.getSociedad();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The socZonGeo with id " + id + " no longer exists.", enfe);
        }
        em.remove(socZonGeo);
    }

    public List<SocZonGeo> findSocZonGeoEntities() {
        return findSocZonGeoEntities(true, -1, -1);
    }

    public List<SocZonGeo> findSocZonGeoEntities(int maxResults, int firstResult) {
        return findSocZonGeoEntities(false, maxResults, firstResult);
    }

    public List<SocZonGeo> findSocZonGeoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SocZonGeo.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public SocZonGeo findSocZonGeo(String id) {
        return em.find(SocZonGeo.class, id);
    }

    public SocZonGeo findSocZonGeo(SociedadRpl id) {
        return findSocZonGeo(id.getIdSociedad());
    }

    public int getSocZonGeoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SocZonGeo> rt = cq.from(SocZonGeo.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public SocZonGeo findSocZonGeoBy_idZidS(String zonaGeograficaid, String sociedadid) {
        SocZonGeo socZonGeo = null;
        try {
            socZonGeo = (SocZonGeo) em.createNamedQuery("SocZonGeo.findSocZonGeoBy_idZidS")
                    .setParameter("zonaGeograficaid", zonaGeograficaid)
                    .setParameter("sociedadid", sociedadid)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la relacion Zona Geografica / Sociedad= " + zonaGeograficaid + " / " + sociedadid);
        }
        return socZonGeo;
    }

    public List<SocZonGeo> findSocZonGeoBy_idS(String sociedadid) {
        List<SocZonGeo> reply = null;
        try {
            reply = (List<SocZonGeo>) em.createNamedQuery("SocZonGeo.findSocZonGeoBy_idS")
                    .setParameter("sociedadid", sociedadid)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica para la Sociedad= " + sociedadid);
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<SocZonGeo> findsocZonasGeograficasOrderBy_idS_N() {
        List<SocZonGeo> reply = null;
        try {
            reply = (List<SocZonGeo>) em.createNamedQuery("SocZonGeo.findsocZonasGeograficasOrderBy_idS_N")
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica ");
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public SocZonGeo findSocZonGeoBy_idS_idZ_Estado(String sociedadId, String zonaGeograficaId, String estado) {
        SocZonGeo socZonGeo = null;
        try { 
            socZonGeo = (SocZonGeo) em.createNamedQuery("SocZonGeo.findSocZonGeoBy_idS_idZ_Estado") 
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("zonaGeograficaId", zonaGeograficaId)
                    .setParameter("estado", estado)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la relacion sociedadId / zona  = " + sociedadId + " - " + zonaGeograficaId + " - " + estado);
        }
        return socZonGeo;
    }
}
