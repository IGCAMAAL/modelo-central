package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class EtiquetaLangPK implements Serializable {

    @NotNull
    private String etiquetaId;

    @NotNull
    private String idioma;

    public EtiquetaLangPK() {
    }

    public EtiquetaLangPK(String etiquetaId, String idioma) {
        this.etiquetaId = etiquetaId;
        this.idioma = idioma;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.etiquetaId);
        hash = 29 * hash + Objects.hashCode(this.idioma);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EtiquetaLangPK other = (EtiquetaLangPK) obj;
        if (!Objects.equals(this.etiquetaId, other.etiquetaId)) {
            return false;
        }
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return "UnidadMedidaLangPK{" + "etiquetaId=" + etiquetaId + ", idioma=" + idioma + '}';
    }




}
