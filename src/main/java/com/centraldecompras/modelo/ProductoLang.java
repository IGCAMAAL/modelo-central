package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_MAT_ProductoLang",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"ID", "idioma", "traduccionDesc"})
        }
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoLang.findProductoLangBy_P", query = ""
        + " SELECT d "
        + "   FROM ProductoLang d "
        + "  WHERE d.productoEntity = :producto" ),
    
    @NamedQuery(name = "ProductoLang.findProductoLangBy_P_Lang", query = ""
        + " SELECT d "
        + "   FROM ProductoLang d "
        + "  WHERE d.productoEntity = :producto "
        + "    AND d.idioma = :idioma" ),
    
    @NamedQuery(name = "ProductoLang.findProductoLangBy_IdP_Lang", query = ""
            + " SELECT d "
            + "   FROM ProductoLang d "
            + "  WHERE d.productoId = :productoId "
            + "    AND d.idioma = :idioma" ),
    
    @NamedQuery(name = "ProductoLang.findProductoLangBy_IdP", query = ""
            + " SELECT d "
            + "   FROM ProductoLang d "
            + "  WHERE d.productoId = :productoId"),
    
    @NamedQuery(name = "ProductoLang.deleteProductoLangBy_IdP_Lang", query = ""
            + " DELETE "
            + "   FROM ProductoLang d "
            + "  WHERE d.productoId = :productoId "
            + "    AND d.idioma = :idioma" )    
        
})                                   

public class ProductoLang implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ProductoLang.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id@Basic@Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String productoId;

    @Id@Basic
    private String idioma;
    
    @Version
    private int version;
    
    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String traduccionDesc;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity=Producto.class)
    private Producto productoEntity;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;
    
    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ProductoLang() {
    }

    public ProductoLang(String productoId, String idioma, String traduccionDesc, Producto productoEntity, DatosAuditoria atributosAuditoria) {
        this.productoId = productoId;
        this.idioma = idioma;
        this.traduccionDesc = traduccionDesc;
        this.productoEntity = productoEntity;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getProductoId() {
        return productoId;
    }

    public void setProductoId(String productoId) {
        this.productoId = productoId;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getTraduccionDesc() {
        return traduccionDesc;
    }

    public void setTraduccionDesc(String traduccionDesc) {
        this.traduccionDesc = traduccionDesc;
    }
    
    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Producto getProductoEntity() {
        return productoEntity;
    }

    public void setProductoEntity(Producto productoEntity) {
        this.productoEntity = productoEntity;
    }
    
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.idioma);
        hash = 41 * hash + Objects.hashCode(this.traduccionDesc);
        hash = 41 * hash + Objects.hashCode(this.productoEntity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoLang other = (ProductoLang) obj;
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        if (!Objects.equals(this.traduccionDesc, other.traduccionDesc)) {
            return false;
        }
        if (!Objects.equals(this.productoEntity, other.productoEntity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProductoLang{" + "productoId=" + productoId + ", idioma=" + idioma + ", version=" + version + ", traduccionDesc=" + traduccionDesc + ", productoEntity=" + productoEntity + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
