package com.centraldecompras.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CC_CMO_Contadores")
public class Contadores implements Serializable {

    @Id
    @Column(updatable = false, insertable = true, nullable = false, length = 4, scale = 4, precision = 0)
    private int anio; 

    @Id
    @Column(updatable = false, insertable = true, nullable = false, length = 3, scale = 0, precision = 0)
    private String tipoDoc; 
    
    @Column(insertable = true, nullable = false, length = 10, scale = 10, precision = 0)
    private int numeroDoc;

    public Contadores() {

    }

    public Contadores(int anio, String tipoDoc, int numeroDoc) {
        this.anio = anio;
        this.tipoDoc = tipoDoc;
        this.numeroDoc = numeroDoc;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }
    
    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public int getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(int numeroDoc) {
        this.numeroDoc = numeroDoc;
    }


}
