package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_MAT_SocProductoExcl",
        indexes = {
            @Index(name = "index_SocProductoExcl", columnList = "sociedadId"),
        //           ,@Index(name = "index_AlmacenUsuProdExcl", columnList = "route")    // El tamáño del atributo no soporta ser index
        }
//        ,uniqueConstraints = {@UniqueConstraint(columnNames = {"almacenId", "usuarioId", "route"})}  // El tamáño del atributo no soporta ser uniqueConstraints
)
@XmlRootElement

@NamedQueries({
    @NamedQuery(name = "SocProductoExcl.findSocProductoExcludesBy_idS_r", query = ""
            + "  SELECT d "
            + "    FROM SocProductoExcl d "
            + "   WHERE d.sociedadId = :sociedadId "
            + "     AND d.route      = :route "
    ),

    @NamedQuery(name = "SocProductoExcl.destroySocProductoExcludesBy_idS_r", query = ""
            + "  DELETE  "
            + "    FROM SocProductoExcl d "
            + "   WHERE d.sociedadId = :sociedadId "
            + "     AND d.route      = :route "
    )
})

public class SocProductoExcl implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SocProductoExcl.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic(optional = false)
    @Column(length = 36, nullable = false, updatable = false)
    private String id;

    @Basic(optional = false)
    @Column(length = 36, nullable = false, updatable = false)
    private String sociedadId;

    @Basic(optional = false)
    @Column(length = 361, nullable = false, updatable = false)    // 1 + (longId+1) x 10; Para longId=35;  1 + (36x10) = 361  
    private String route;

    @Version
    private int version;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic(optional = false)
    private String productoId;

    @Basic
    private int nivelInt;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SocProductoExcl() {

    }

    public SocProductoExcl(SocProductoExcl socProductoExcl) {
        this.id = socProductoExcl.getId();
        this.sociedadId = socProductoExcl.getSociedadId();
        this.route = socProductoExcl.getRoute();
        this.productoId = socProductoExcl.getProductoId();
        this.nivelInt = socProductoExcl.getNivelInt();
    }

    public SocProductoExcl(String id, String sociedadId, String route, String productoId, int nivelInt) {
        this.id = id;
        this.sociedadId = sociedadId;
        this.route = route;
        this.productoId = productoId;
        this.nivelInt = nivelInt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSociedadId() {
        return sociedadId;
    }

    public void setSociedadId(String sociedadId) {
        this.sociedadId = sociedadId;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    // MÃ©todos Version * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // MÃ©todos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getProductoId() {
        return productoId;
    }

    public void setProductoId(String productoId) {
        this.productoId = productoId;
    }

    public int getNivelInt() {
        return nivelInt;
    }

    public void setNivelInt(int nivelInt) {
        this.nivelInt = nivelInt;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.sociedadId);
        hash = 97 * hash + Objects.hashCode(this.route);
        hash = 97 * hash + Objects.hashCode(this.productoId);
        hash = 97 * hash + this.nivelInt;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SocProductoExcl other = (SocProductoExcl) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.sociedadId, other.sociedadId)) {
            return false;
        }
        if (!Objects.equals(this.route, other.route)) {
            return false;
        }
        if (!Objects.equals(this.productoId, other.productoId)) {
            return false;
        }
        if (this.nivelInt != other.nivelInt) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SocProductoExcl{" + "id=" + id + ", sociedadId=" + sociedadId + ", route=" + route + ", version=" + version + ", productoId=" + productoId + ", nivelInt=" + nivelInt + '}';
    }


}
