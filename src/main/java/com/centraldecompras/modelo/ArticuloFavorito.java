package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_MAT_ArticuloFavorito")
@XmlRootElement
@NamedQueries({                          
   
   @NamedQuery(name = "ArticuloFavorito.findArticuloFavoritoBy_idS_idA_idU", query = ""
           + "  SELECT d "
           + "    FROM ArticuloFavorito d "
           + "   WHERE d.sociedadIdk = :sociedadIdk "
           + "     AND d.articuloIdk = :articuloIdk "
           + "     AND d.usuarioIdk = :usuarioIdk "),
   
   @NamedQuery(name = "ArticuloFavorito.deleteArticuloFavoritoBy_idS_idA_idU", query = ""
           + "  DELETE "
           + "    FROM ArticuloFavorito d "
           + "   WHERE d.sociedadIdk = :sociedadIdk "
           + "     AND d.articuloIdk = :articuloIdk "
           + "     AND d.usuarioIdk = :usuarioIdk "),
})
public class ArticuloFavorito implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ArticuloFavorito.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "sociedadIdk", length = 36, nullable = false, updatable = false)
    private String sociedadIdk;
    
    @Id
    @NotNull
    @Column(name = "articuloIdk", length = 36, nullable = false, updatable = false)
    private String articuloIdk;

    @Id
    @NotNull
    @Column(name = "usuarioIdk", length = 36, nullable = false, updatable = false)
    private String usuarioIdk;
    
    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    private boolean favorito;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ArticuloFavorito() {
    }

    public ArticuloFavorito(String sociedadIdk, String articuloIdk, String usuarioIdk, boolean favorito, DatosAuditoria atributosAuditoria) { 
        this.sociedadIdk = sociedadIdk;
        this.articuloIdk = articuloIdk;
        this.usuarioIdk = usuarioIdk;
        this.favorito = favorito;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getSociedadIdk() {
        return sociedadIdk;
    }

    public void setSociedadIdk(String sociedadIdk) {
        this.sociedadIdk = sociedadIdk;
    }

    public String getArticuloIdk() {
        return articuloIdk;
    }

    public void setArticuloIdk(String articuloIdk) {
        this.articuloIdk = articuloIdk;
    }

    public String getUsuarioIdk() {
        return usuarioIdk;
    }

    public void setUsuarioIdk(String usuarioIdk) {
        this.usuarioIdk = usuarioIdk;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    public boolean isFavorito() {
        return favorito;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.sociedadIdk);
        hash = 31 * hash + Objects.hashCode(this.articuloIdk);
        hash = 31 * hash + Objects.hashCode(this.usuarioIdk);
        hash = 31 * hash + (this.favorito ? 1 : 0);
        hash = 31 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloFavorito other = (ArticuloFavorito) obj;
        if (!Objects.equals(this.sociedadIdk, other.sociedadIdk)) {
            return false;
        }
        if (!Objects.equals(this.articuloIdk, other.articuloIdk)) {
            return false;
        }
        if (!Objects.equals(this.usuarioIdk, other.usuarioIdk)) {
            return false;
        }
        if (this.favorito != other.favorito) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ArticuloAsociadoUsuario{" + "sociedadIdk=" + sociedadIdk + ", articuloIdk=" + articuloIdk + ", usuarioIdk=" + usuarioIdk + ", version=" + version + ", favorito=" + favorito + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
