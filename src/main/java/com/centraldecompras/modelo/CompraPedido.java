package com.centraldecompras.modelo;

import com.centraldecompras.acceso.Sociedad;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_PUR_CompraPedido")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompraPedido.findAll", query = ""
            + "SELECT p "
            + "FROM CompraPedido p"),
    @NamedQuery(name = "CompraPedido.findByIdPedido", query = ""
            + "SELECT p "
            + "FROM CompraPedido p "
            + "WHERE p.compraPedidoId = :compraPedidoId")}
)
public class CompraPedido implements Serializable {

    private static final long serialVersionUID = 1L;

    // Atributos ID -----------------------------------------------------------------------------------------------    
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String compraPedidoId;

    @Version
    private int version;

    // Atributos Relacion ------------------------------------------------------------------------------------------   
    @ManyToOne(targetEntity = CompraCesta.class)
    private CompraCesta compraCesta;
    
    @ManyToOne(targetEntity = Sociedad.class)
    private Sociedad compradorSociedad;
    @ManyToOne(targetEntity = Persona.class)
    private Persona compradorPersona;
    @ManyToOne(targetEntity = Almacen.class)
    private Almacen compradorAlmacen;
            
    @ManyToOne(targetEntity = Sociedad.class)
    private Sociedad proveedorSociedad;

    // Atributos Basicos--------------------------------------------------------------------------------------------
    long pedidoNumero;
    int pedidoFechaEnvio;
    
    String proveedorPersonaContactoId;
    String proveedorDireccion;
    String proveedorTelefono;
    String proveedorFax;
    String proveedorEmail;
    
    String pedidoEstado;
    boolean pedidoFavorito;
    int pedidoFechaEntrega;
    String pedidoMoneda;
    
    @Embedded
    DatosAuditoria atributosAuditoria;

    // 2 Constructores ---------------------------------------------------------------------------------------------
    public CompraPedido() {
    }

    public CompraPedido(String compraPedidoId, CompraCesta compraCesta, Sociedad compradorSociedad, Persona compradorPersona, Almacen compradorAlmacen, Sociedad proveedorSociedad, long pedidoNumero, int pedidoFechaEnvio, String proveedorPersonaContactoId, String proveedorDireccion, String proveedorTelefono, String proveedorFax, String proveedorEmail, String pedidoEstado, boolean pedidoFavorito, int pedidoFechaEntrega, String pedidoMoneda, DatosAuditoria atributosAuditoria) {
        this.compraPedidoId = compraPedidoId;
        this.compraCesta = compraCesta;
        this.compradorSociedad = compradorSociedad;
        this.compradorPersona = compradorPersona;
        this.compradorAlmacen = compradorAlmacen;
        this.proveedorSociedad = proveedorSociedad;
        this.pedidoNumero = pedidoNumero;
        this.pedidoFechaEnvio = pedidoFechaEnvio;
        this.proveedorPersonaContactoId = proveedorPersonaContactoId;
        this.proveedorDireccion = proveedorDireccion;
        this.proveedorTelefono = proveedorTelefono;
        this.proveedorFax = proveedorFax;
        this.proveedorEmail = proveedorEmail;
        this.pedidoEstado = pedidoEstado;
        this.pedidoFavorito = pedidoFavorito;
        this.pedidoFechaEntrega = pedidoFechaEntrega;
        this.pedidoMoneda = pedidoMoneda;
        this.atributosAuditoria = atributosAuditoria;
    }


    // Metodos  ID -----------------------------------------------------------------------------------------------     
    public String getCompraPedidoId() {
        return compraPedidoId;
    }

    public void setCompraPedidoId(String compraPedidoId) {
        this.compraPedidoId = compraPedidoId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
        // Metodos  Relacion ----------------------------------------------------------------------------------------
    public Sociedad getCompradorSociedad() {
        return compradorSociedad;
    }

    public void setCompradorSociedad(Sociedad compradorSociedad) {
        this.compradorSociedad = compradorSociedad;
    }

    public Persona getCompradorPersona() {
        return compradorPersona;
    }

    public void setCompradorPersona(Persona compradorPersona) {
        this.compradorPersona = compradorPersona;
    }

    public Almacen getCompradorAlmacen() {
        return compradorAlmacen;
    }

    public void setCompradorAlmacen(Almacen compradorAlmacen) {
        this.compradorAlmacen = compradorAlmacen;
    }

    public Sociedad getProveedorSociedad() {
        return proveedorSociedad;
    }


    public void setProveedorSociedad(Sociedad proveedorSociedad) {
        this.proveedorSociedad = proveedorSociedad;
    }

    public CompraCesta getCompraCesta() {
        return compraCesta;
    }

    public void setCompraCesta(CompraCesta compraCesta) {
        this.compraCesta = compraCesta;
    }

    
    // Metodos  Basicos ----------------------------------------------------------------------------------------

    public long getPedidoNumero() {
        return pedidoNumero;
    }

    public void setPedidoNumero(long pedidoNumero) {
        this.pedidoNumero = pedidoNumero;
    }

    public int getPedidoFechaEnvio() {
        return pedidoFechaEnvio;
    }

    public void setPedidoFechaEnvio(int pedidoFechaEnvio) {
        this.pedidoFechaEnvio = pedidoFechaEnvio;
    }

    public String getProveedorPersonaContactoId() {
        return proveedorPersonaContactoId;
    }

    public void setProveedorPersonaContactoId(String proveedorPersonaContactoId) {
        this.proveedorPersonaContactoId = proveedorPersonaContactoId;
    }

    public String getProveedorDireccion() {
        return proveedorDireccion;
    }

    public void setProveedorDireccion(String proveedorDireccion) {
        this.proveedorDireccion = proveedorDireccion;
    }

    public String getProveedorTelefono() {
        return proveedorTelefono;
    }

    public void setProveedorTelefono(String proveedorTelefono) {
        this.proveedorTelefono = proveedorTelefono;
    }

    public String getProveedorFax() {
        return proveedorFax;
    }

    public void setProveedorFax(String proveedorFax) {
        this.proveedorFax = proveedorFax;
    }

    public String getProveedorEmail() {
        return proveedorEmail;
    }

    public void setProveedorEmail(String proveedorEmail) {
        this.proveedorEmail = proveedorEmail;
    }

    public String getPedidoEstado() {
        return pedidoEstado;
    }

    public void setPedidoEstado(String pedidoEstado) {
        this.pedidoEstado = pedidoEstado;
    }

    public boolean isPedidoFavorito() {
        return pedidoFavorito;
    }

    public void setPedidoFavorito(boolean pedidoFavorito) {
        this.pedidoFavorito = pedidoFavorito;
    }

    public int getPedidoFechaEntrega() {
        return pedidoFechaEntrega;
    }

    public void setPedidoFechaEntrega(int pedidoFechaEntrega) {
        this.pedidoFechaEntrega = pedidoFechaEntrega;
    }

    public String getPedidoMoneda() {
        return pedidoMoneda;
    }

    public void setPedidoMoneda(String pedidoMoneda) {
        this.pedidoMoneda = pedidoMoneda;
    }
    
    
    // Metodos  Embebidos ----------------------------------------------------------------------------------------
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Metodos  Entity ----------------------------------------------------------------------------------------

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.compraPedidoId);
        hash = 71 * hash + this.version;
        hash = 71 * hash + Objects.hashCode(this.compraCesta);
        hash = 71 * hash + Objects.hashCode(this.compradorSociedad);
        hash = 71 * hash + Objects.hashCode(this.compradorPersona);
        hash = 71 * hash + Objects.hashCode(this.compradorAlmacen);
        hash = 71 * hash + Objects.hashCode(this.proveedorSociedad);
        hash = 71 * hash + (int) (this.pedidoNumero ^ (this.pedidoNumero >>> 32));
        hash = 71 * hash + this.pedidoFechaEnvio;
        hash = 71 * hash + Objects.hashCode(this.proveedorPersonaContactoId);
        hash = 71 * hash + Objects.hashCode(this.proveedorDireccion);
        hash = 71 * hash + Objects.hashCode(this.proveedorTelefono);
        hash = 71 * hash + Objects.hashCode(this.proveedorFax);
        hash = 71 * hash + Objects.hashCode(this.proveedorEmail);
        hash = 71 * hash + Objects.hashCode(this.pedidoEstado);
        hash = 71 * hash + (this.pedidoFavorito ? 1 : 0);
        hash = 71 * hash + this.pedidoFechaEntrega;
        hash = 71 * hash + Objects.hashCode(this.pedidoMoneda);
        hash = 71 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraPedido other = (CompraPedido) obj;
        if (!Objects.equals(this.compraPedidoId, other.compraPedidoId)) {
            return false;
        }
        if (!Objects.equals(this.compraCesta, other.compraCesta)) {
            return false;
        }
        if (!Objects.equals(this.compradorSociedad, other.compradorSociedad)) {
            return false;
        }
        if (!Objects.equals(this.compradorPersona, other.compradorPersona)) {
            return false;
        }
        if (!Objects.equals(this.compradorAlmacen, other.compradorAlmacen)) {
            return false;
        }
        if (!Objects.equals(this.proveedorSociedad, other.proveedorSociedad)) {
            return false;
        }
        if (this.pedidoNumero != other.pedidoNumero) {
            return false;
        }
        if (this.pedidoFechaEnvio != other.pedidoFechaEnvio) {
            return false;
        }
        if (!Objects.equals(this.proveedorPersonaContactoId, other.proveedorPersonaContactoId)) {
            return false;
        }
        if (!Objects.equals(this.proveedorDireccion, other.proveedorDireccion)) {
            return false;
        }
        if (!Objects.equals(this.proveedorTelefono, other.proveedorTelefono)) {
            return false;
        }
        if (!Objects.equals(this.proveedorFax, other.proveedorFax)) {
            return false;
        }
        if (!Objects.equals(this.proveedorEmail, other.proveedorEmail)) {
            return false;
        }
        if (!Objects.equals(this.pedidoEstado, other.pedidoEstado)) {
            return false;
        }
        if (this.pedidoFavorito != other.pedidoFavorito) {
            return false;
        }
        if (this.pedidoFechaEntrega != other.pedidoFechaEntrega) {
            return false;
        }
        if (!Objects.equals(this.pedidoMoneda, other.pedidoMoneda)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CompraPedido{" + "compraPedidoId=" + compraPedidoId + ", version=" + version + ", compraCesta=" + compraCesta + ", compradorSociedad=" + compradorSociedad + ", compradorPersona=" + compradorPersona + ", compradorAlmacen=" + compradorAlmacen + ", proveedorSociedad=" + proveedorSociedad + ", pedidoNumero=" + pedidoNumero + ", pedidoFechaEnvio=" + pedidoFechaEnvio + ", proveedorPersonaContactoId=" + proveedorPersonaContactoId + ", proveedorDireccion=" + proveedorDireccion + ", proveedorTelefono=" + proveedorTelefono + ", proveedorFax=" + proveedorFax + ", proveedorEmail=" + proveedorEmail + ", pedidoEstado=" + pedidoEstado + ", pedidoFavorito=" + pedidoFavorito + ", pedidoFechaEntrega=" + pedidoFechaEntrega + ", pedidoMoneda=" + pedidoMoneda + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
