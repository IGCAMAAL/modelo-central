package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_MAT_SocProductoAvatar")
@XmlRootElement
@NamedQueries({                           
    @NamedQuery(name = "SocProductoAvatar.findSocProductoAvatarBy_idS_idP", query = " "
            + " SELECT d "
            + "   FROM SocProductoAvatar d "
            + "  WHERE d.sociedadId = :sociedadId "
            + "    AND d.productoId = :productoId"
    ),
    
    @NamedQuery(name = "SocProductoAvatar.deleteSocProductoAvatarBy_idS_idP", query = " "
            + " DELETE "
            + "   FROM SocProductoAvatar d "
            + "  WHERE d.sociedadId = :sociedadId "
            + "    AND d.productoId = :productoId"
    ),

    @NamedQuery(name = "SocProductoAvatar.findSocProductoAvatarEmpiezaTypeAheadTodo",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion, "
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProductoAvatar s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoId = p.idProducto "
            + "   AND s.productoId = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND s.sociedadId = :sociedadIdk  "
    ),

    @NamedQuery(name = "SocProductoAvatar.findSocProductoAvatarEmpiezaTypeAheadSelEstado",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProductoAvatar s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoId = p.idProducto "
            + "   AND s.productoId = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND p.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND s.sociedadId = :sociedadIdk  "
    ),

    @NamedQuery(name = "SocProductoAvatar.findSocProductoAvatarContieneTypeAheadTodo",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProductoAvatar s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoId = p.idProducto "
            + "   AND s.productoId = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND d.traduccionDesc NOT LIKE :descipcionExclude"
            + "   AND s.sociedadId = :sociedadIdk  "
    ),

    @NamedQuery(name = "SocProductoAvatar.findSocProductoAvatarContieneTypeAheadSelEstado",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProductoAvatar s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoId = p.idProducto "
            + "   AND s.productoId = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND p.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND d.traduccionDesc NOT LIKE :descipcionExclude  "
            + "   AND s.sociedadId = :sociedadIdk  "
    )
})
public class SocProductoAvatar implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SocProductoAvatar.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "sociedadId", length = 36)
    private String sociedadId;

    @Id
    @NotNull
    @Column(name = "productoId", length = 36)
    private String productoId;

    @Version
    private int version;
    
    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Basic
    private int nivelInt;
    
    private Boolean asignada;
    
    @Basic
    @Column(name = "idPadreJerarquia", nullable = false, updatable = true)
    private String idPadreJerarquia;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      

    public SocProductoAvatar() {
    }

    public SocProductoAvatar(String sociedadId, String productoId, int version, int nivelInt, Boolean asignada, String idPadreJerarquia) {
        this.sociedadId = sociedadId;
        this.productoId = productoId;
        this.version = version;
        this.nivelInt = nivelInt;
        this.asignada = asignada;
        this.idPadreJerarquia = idPadreJerarquia;
    }
    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getSociedadId() {
        return sociedadId;
    }

    public void setSociedadId(String sociedadId) {
        this.sociedadId = sociedadId;
    }

    public String getProductoId() {
        return productoId;
    }

    public void setProductoId(String productoId) {
        this.productoId = productoId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public int getNivelInt() {
        return nivelInt;
    }

    public void setNivelInt(int nivelInt) {
        this.nivelInt = nivelInt;
    }

    public Boolean getAsignada() {
        return asignada;
    }

    public void setAsignada(Boolean asignada) {
        this.asignada = asignada;
    }

    public String getIdPadreJerarquia() {
        return idPadreJerarquia;
    }

    public void setIdPadreJerarquia(String idPadreJerarquia) {
        this.idPadreJerarquia = idPadreJerarquia;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.sociedadId);
        hash = 97 * hash + Objects.hashCode(this.productoId);
        hash = 97 * hash + this.nivelInt;
        hash = 97 * hash + Objects.hashCode(this.asignada);
        hash = 97 * hash + Objects.hashCode(this.idPadreJerarquia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SocProductoAvatar other = (SocProductoAvatar) obj;
        if (!Objects.equals(this.sociedadId, other.sociedadId)) {
            return false;
        }
        if (!Objects.equals(this.productoId, other.productoId)) {
            return false;
        }
        if (this.nivelInt != other.nivelInt) {
            return false;
        }
        if (!Objects.equals(this.asignada, other.asignada)) {
            return false;
        }
        if (!Objects.equals(this.idPadreJerarquia, other.idPadreJerarquia)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SocProductoAvatar{" + "sociedadId=" + sociedadId + ", productoId=" + productoId + ", version=" + version + ", nivelInt=" + nivelInt + ", asignada=" + asignada + ", idPadreJerarquia=" + idPadreJerarquia + '}';
    }


}
