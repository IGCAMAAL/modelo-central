package com.centraldecompras.modelo;

import com.centraldecompras.acceso.Sociedad;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_MAT_Articulos")
@XmlRootElement

@NamedQueries({                          
   @NamedQuery(name = "Articulo.findArticuloBy_idP_idS", query = ""
           + "    SELECT a "
           + "      FROM Articulo a, "
           + "           ArticuloProducto p "
           + "     WHERE a.idArticulo = p.articuloIdk "
           + "       AND p.productoIdk = :productoIdk "
           + "       AND a.sociedad.id = :sociedadId "
   ),
    
   @NamedQuery(name = "Articulo.findArticuloSel_PA", query = ""
           + "    SELECT distinct(a.sociedad.id) "
           + "      FROM Articulo a "
           + "     WHERE a.estadoAprobacion IN (:estadoAprobacion)"
           + "  GROUP BY a.sociedad "
           + "  ORDER BY a.sociedad "
   ),
    
   @NamedQuery(name = "Articulo.findArticuloSel_PA_R", query = ""
           + "    SELECT a.sociedad.id, a.estadoAprobacion, count(a.estadoAprobacion) "
           + "      FROM Articulo a "
           + "     WHERE a.estadoAprobacion IN (:estadoAprobacion) "
           + "  GROUP BY a.sociedad, a.estadoAprobacion "
           + "  ORDER BY a.sociedad, a.estadoAprobacion "
   ),
   
   @NamedQuery(name = "Articulo.findContArticulosPASoc", query = ""
           + "    SELECT count(a) "
           + "      FROM Articulo a "
           + "     WHERE a.estadoAprobacion IN (:estadoAprobacion) "
           + "       AND a.sociedad.id = :sociedadId "
   ),
   
   @NamedQuery(name = "Articulo.findPagArticulosPASoc", query = ""
           + "    SELECT a "
           + "      FROM Articulo a, "
           + "           ArticuloLangNomb b "
           + "     WHERE a.idArticulo = b.articuloIdk "
           + "       AND a.estadoAprobacion IN (:estadoAprobacion) "
           + "       AND b.idioma = :idioma "
           + "       AND a.sociedad.id = :sociedadId "
           + "  ORDER BY b.traduccionNomb "
   )
})


public class Articulo implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Articulo.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idArticulo;

    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private BigDecimal uniMinVta;

    @NotNull
    private BigDecimal precioUniMed;

    @NotNull
    private BigDecimal precioUniMedNuevo;
    
    private String Moneda;

    @NotNull
    private BigDecimal unidadFormatoVta;

    private String descrArticulo;

    private String nombreArticulo;

    private String codiParaProveedor;

    private String codigoEAN;
    
    private String estadoAprobacion;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = FormatoVta.class)
    private FormatoVta formatoVta;

    @ManyToOne(targetEntity = Sociedad.class)
    private Sociedad sociedad;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Articulo() {
    }

    public Articulo(String idArticulo, BigDecimal uniMinVta, BigDecimal precioUniMed, BigDecimal precioUniMedNuevo, String Moneda, BigDecimal unidadFormatoVta, String descrArticulo, String nombreArticulo, 
            String codiParaProveedor, String codigoEAN, String estadoAprobacion, FormatoVta formatoVta, Sociedad sociedad, DatosAuditoria atributosAuditoria) {
        this.idArticulo = idArticulo;
        this.uniMinVta = uniMinVta;
        this.precioUniMed = precioUniMed;
        this.precioUniMedNuevo = precioUniMedNuevo;
        this.Moneda = Moneda;
        this.unidadFormatoVta = unidadFormatoVta;
        this.descrArticulo = descrArticulo;
        this.nombreArticulo = nombreArticulo;
        this.codiParaProveedor = codiParaProveedor;
        this.codigoEAN = codigoEAN;
        this.estadoAprobacion = estadoAprobacion;
        this.formatoVta = formatoVta;
        this.sociedad = sociedad;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getIdArticulo() {
        return this.idArticulo;
    }

    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public BigDecimal getUniMinVta() {
        return this.uniMinVta;
    }

    public void setUniMinVta(BigDecimal uniMinVta) {
        this.uniMinVta = uniMinVta;
    }

    public BigDecimal getPrecioUniMed() {
        return this.precioUniMed;
    }

    public void setPrecioUniMed(BigDecimal precioUniMed) {
        this.precioUniMed = precioUniMed;
    }

    public BigDecimal getPrecioUniMedNuevo() {
        return precioUniMedNuevo;
    }

    public void setPrecioUniMedNuevo(BigDecimal precioUniMedNuevo) {
        this.precioUniMedNuevo = precioUniMedNuevo;
    }

    public String getMoneda() {
        return Moneda;
    }

    public void setMoneda(String Moneda) {
        this.Moneda = Moneda;
    }

    public String getDescrArticulo() {
        return descrArticulo;
    }

    public void setDescrArticulo(String descrArticulo) {
        this.descrArticulo = descrArticulo;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public String getCodiParaProveedor() {
        return codiParaProveedor;
    }

    public void setCodiParaProveedor(String codiParaProveedor) {
        this.codiParaProveedor = codiParaProveedor;
    }

    public String getCodigoEAN() {
        return codigoEAN;
    }

    public void setCodigoEAN(String codigoEAN) {
        this.codigoEAN = codigoEAN;
    }

    public String getEstadoAprobacion() {
        return estadoAprobacion;
    }

    public void setEstadoAprobacion(String estadoAprobacion) {
        this.estadoAprobacion = estadoAprobacion;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public FormatoVta getFormatoVta() {
        return this.formatoVta;
    }

    public void setFormatoVta(FormatoVta formatoVta) {
        this.formatoVta = formatoVta;
    }

    public BigDecimal getUnidadFormatoVta() {
        return this.unidadFormatoVta;
    }

    public void setUnidadFormatoVta(BigDecimal unidadFormatoVta) {
        this.unidadFormatoVta = unidadFormatoVta;
    }

    public Sociedad getSociedad() {
        return sociedad;
    }

    public void setSociedad(Sociedad sociedad) {
        this.sociedad = sociedad;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    



    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.idArticulo);
        hash = 83 * hash + Objects.hashCode(this.uniMinVta);
        hash = 83 * hash + Objects.hashCode(this.precioUniMed);
        hash = 83 * hash + Objects.hashCode(this.precioUniMedNuevo);
        hash = 83 * hash + Objects.hashCode(this.Moneda);
        hash = 83 * hash + Objects.hashCode(this.unidadFormatoVta);
        hash = 83 * hash + Objects.hashCode(this.descrArticulo);
        hash = 83 * hash + Objects.hashCode(this.nombreArticulo);
        hash = 83 * hash + Objects.hashCode(this.codiParaProveedor);
        hash = 83 * hash + Objects.hashCode(this.codigoEAN);
        hash = 83 * hash + Objects.hashCode(this.estadoAprobacion);
        hash = 83 * hash + Objects.hashCode(this.formatoVta);
        hash = 83 * hash + Objects.hashCode(this.sociedad);
        hash = 83 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Articulo other = (Articulo) obj;
        if (!Objects.equals(this.idArticulo, other.idArticulo)) {
            return false;
        }
        if (!Objects.equals(this.uniMinVta, other.uniMinVta)) {
            return false;
        }
        if (!Objects.equals(this.precioUniMed, other.precioUniMed)) {
            return false;
        }
        if (!Objects.equals(this.precioUniMedNuevo, other.precioUniMedNuevo)) {
            return false;
        }
        if (!Objects.equals(this.Moneda, other.Moneda)) {
            return false;
        }
        if (!Objects.equals(this.unidadFormatoVta, other.unidadFormatoVta)) {
            return false;
        }
        if (!Objects.equals(this.descrArticulo, other.descrArticulo)) {
            return false;
        }
        if (!Objects.equals(this.nombreArticulo, other.nombreArticulo)) {
            return false;
        }
        if (!Objects.equals(this.codiParaProveedor, other.codiParaProveedor)) {
            return false;
        }
        if (!Objects.equals(this.codigoEAN, other.codigoEAN)) {
            return false;
        }
        if (!Objects.equals(this.estadoAprobacion, other.estadoAprobacion)) {
            return false;
        }
        if (!Objects.equals(this.formatoVta, other.formatoVta)) {
            return false;
        }
        if (!Objects.equals(this.sociedad, other.sociedad)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Articulo{" + "idArticulo=" + idArticulo + ", version=" + version + ", uniMinVta=" + uniMinVta + ", precioUniMed=" + precioUniMed + ", precioUniMedNuevo=" + precioUniMedNuevo + ", Moneda=" + Moneda + ", unidadFormatoVta=" + unidadFormatoVta + ", descrArticulo=" + descrArticulo + ", nombreArticulo=" + nombreArticulo + ", codiParaProveedor=" + codiParaProveedor + ", codigoEAN=" + codigoEAN + ", estadoAprobacion=" + estadoAprobacion + ", formatoVta=" + formatoVta + ", sociedad=" + sociedad + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

    


}
