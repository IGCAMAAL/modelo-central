package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_UnidadMedida")
@XmlRootElement
public class UnidadMedida implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(UnidadMedida.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idUnidadMedida;
    
    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private String unidadMedidaCod;

    @NotNull
    private String unidadMedidaDesc;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UnidadMedida() {
    }

    public UnidadMedida(String idUnidadMedida, int version, String unidadMedidaCod, String unidadMedidaDesc, DatosAuditoria atributosAuditoria) {
        this.idUnidadMedida = idUnidadMedida;
        this.version = version;
        this.unidadMedidaCod = unidadMedidaCod;
        this.unidadMedidaDesc = unidadMedidaDesc;
        this.atributosAuditoria = atributosAuditoria;
    }
        
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public String getIdUnidadMedida() {
        return this.idUnidadMedida;
    }

    public void setIdUnidadMedida(String idUnidadMedida) {
        this.idUnidadMedida = idUnidadMedida;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getUnidadMedidaCod() {
        return this.unidadMedidaCod;
    }

    public void setUnidadMedidaCod(String unidadMedidaCod) {
        this.unidadMedidaCod = unidadMedidaCod;
    }

    public String getunidadMedidaDesc() {
        return this.unidadMedidaDesc;
    }

    public void setUnidadMedidaDesc(String unidadMedidaDesc) {
        this.unidadMedidaDesc = unidadMedidaDesc;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.idUnidadMedida != null ? this.idUnidadMedida.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadMedida other = (UnidadMedida) obj;
        if ((this.unidadMedidaCod == null) ? (other.unidadMedidaCod != null) : !this.unidadMedidaCod.equals(other.unidadMedidaCod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnidadMedida{" + "idUnidadMedida=" + idUnidadMedida + ", version=" + version + ", unidadMedida=" + unidadMedidaCod + ", descUnidadMedida=" + unidadMedidaDesc + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
