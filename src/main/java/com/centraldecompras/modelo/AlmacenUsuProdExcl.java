package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_LGT_AlmacenUsuProdExcl",
        indexes = {
            @Index(name = "index_AlmacenUsuProdExcl", columnList = "almacenId")
            ,@Index(name = "index_AlmacenUsuProdExcl", columnList = "usuarioId")
 //           ,@Index(name = "index_AlmacenUsuProdExcl", columnList = "route")    // El tamáño del atributo no soporta ser index
        } 
        
 //        ,uniqueConstraints = {@UniqueConstraint(columnNames = {"almacenId", "usuarioId", "route"})}  // El tamáño del atributo no soporta ser uniqueConstraints
)
@XmlRootElement
@NamedQueries({                          
  
   @NamedQuery(name = "AlmacenUsuarioProductoExcludes.findAlmacenUsuarioProductoExcludesBy_idA_idU_r", query = ""
           + "  SELECT d "
           + "    FROM AlmacenUsuProdExcl d "
           + "   WHERE d.almacenId = :almacenId "                      
           + "     AND d.usuarioId = :usuarioId "
            + "    AND d.route     = :route "           
   ),
   
   @NamedQuery(name = "AlmacenUsuarioProductoExcludes.destroyAlmacenUsuarioProductoExcludesBy_idA_idU_r", query = ""
           + "  DELETE  "
           + "    FROM AlmacenUsuProdExcl d "
           + "   WHERE d.almacenId = :almacenId "                      
           + "     AND d.usuarioId = :usuarioId "
            + "    AND d.route     = :route "  
   )

})



public class AlmacenUsuProdExcl implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(AlmacenUsuProdExcl.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic(optional = false)
    @Column(length = 36, nullable = false, updatable = false)
    private String id;
    
    @Basic(optional = false)
    @Column(length = 36, nullable = false, updatable = false)
    private String almacenId;

    
    @Basic(optional = false)
    @Column(length = 36, nullable = false, updatable = false)    
    private String usuarioId;

    @Basic(optional = false)
    @Column(length = 361, nullable = false, updatable = false)    // 1 + (longId+1) x 10; Para longId=35;  1 + (36x10) = 361  
    private String route;

    @Version
    private int version;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic(optional = false)
    private String productoId;
    
    @Basic
    private int nivelInt;
    
    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public AlmacenUsuProdExcl() {

    }
    public AlmacenUsuProdExcl(AlmacenUsuProdExcl almacenUsuProdExcl) {
        this.almacenId = almacenUsuProdExcl.getAlmacenId();
        this.usuarioId = almacenUsuProdExcl.getUsuarioId();
        this.route = almacenUsuProdExcl.getRoute();
        this.productoId = almacenUsuProdExcl.getProductoId();
        this.nivelInt = almacenUsuProdExcl.getNivelInt();
    }
    public AlmacenUsuProdExcl(String almacenId, String usuarioId, String route, String productoId, int nivelInt) {
        this.almacenId = almacenId;
        this.usuarioId = usuarioId;
        this.route = route;
        this.productoId = productoId;
        this.nivelInt = nivelInt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    
    public String getAlmacenId() {
        return almacenId;
    }

    public void setAlmacenId(String almacenId) {
        this.almacenId = almacenId;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }


    // MÃ©todos Version * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // MÃ©todos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    public String getProductoId() {
        return productoId;
    }

    public void setProductoId(String productoId) {
        this.productoId = productoId;
    }

    public int getNivelInt() {
        return nivelInt;
    }

    public void setNivelInt(int nivelInt) {
        this.nivelInt = nivelInt;
    }



    // MÃ©todos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 



    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.almacenId);
        hash = 83 * hash + Objects.hashCode(this.usuarioId);
        hash = 83 * hash + Objects.hashCode(this.route);
        hash = 83 * hash + Objects.hashCode(this.productoId);
        hash = 83 * hash + this.nivelInt;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlmacenUsuProdExcl other = (AlmacenUsuProdExcl) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.almacenId, other.almacenId)) {
            return false;
        }
        if (!Objects.equals(this.usuarioId, other.usuarioId)) {
            return false;
        }
        if (!Objects.equals(this.route, other.route)) {
            return false;
        }
        if (!Objects.equals(this.productoId, other.productoId)) {
            return false;
        }
        if (this.nivelInt != other.nivelInt) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlmacenUsuProdExcl{" + "id=" + id + ", almacenId=" + almacenId + ", usuarioId=" + usuarioId + ", route=" + route + ", version=" + version + ", productoId=" + productoId + ", nivelInt=" + nivelInt + '}';
    }


}
