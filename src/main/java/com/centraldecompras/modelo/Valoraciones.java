package com.centraldecompras.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_VEN_Valoraciones")
@XmlRootElement
@NamedQueries({         
    @NamedQuery(name = "Valoraciones.findValoracionesBy_idS", query = 
            " SELECT d "
          + "   FROM Valoraciones d "
          + "  WHERE d.sociedadOpinada.id = :sociedadOpinadaid ")   
})
public class Valoraciones implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Valoraciones.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idValoracion;

    @Version
    private int version;
    
    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * 
    @NotNull
    private BigDecimal valServicio;
    @NotNull
    private BigDecimal valEntrega;
    @NotNull
    private BigDecimal valDisponibilidad;
    @NotNull
    private BigDecimal valRelCalidadPrecio;
    @NotNull
    private BigDecimal valAtencion;
    
    private String aprobacion;
    // Atributos Id de relacion * * * * * * * * * * * * * * * * * * * * * * * 

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * 
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl sociedadOpinante; 
    
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl sociedadOpinada;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public Valoraciones() {
    }

    public Valoraciones(String idValoracion, BigDecimal valServicio, BigDecimal valEntrega, BigDecimal valDisponibilidad, BigDecimal valRelCalidadPrecio, BigDecimal valAtencion, String aprobacion, SociedadRpl sociedadOpinante, SociedadRpl sociedadOpinada, DatosAuditoria atributosAuditoria) {
        this.idValoracion = idValoracion;
        this.valServicio = valServicio;
        this.valEntrega = valEntrega;
        this.valDisponibilidad = valDisponibilidad;
        this.valRelCalidadPrecio = valRelCalidadPrecio;
        this.valAtencion = valAtencion;
        this.aprobacion = aprobacion;
        this.sociedadOpinante = sociedadOpinante;
        this.sociedadOpinada = sociedadOpinada;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getIdValoracion() {
        return idValoracion;
    }

    public void setIdValoracion(String idValoracion) {
        this.idValoracion = idValoracion;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public BigDecimal getValServicio() {
        return valServicio;
    }

    public void setValServicio(BigDecimal valServicio) {
        this.valServicio = valServicio;
    }

    public BigDecimal getValEntrega() {
        return valEntrega;
    }

    public void setValEntrega(BigDecimal valEntrega) {
        this.valEntrega = valEntrega;
    }

    public BigDecimal getValDisponibilidad() {
        return valDisponibilidad;
    }

    public void setValDisponibilidad(BigDecimal valDisponibilidad) {
        this.valDisponibilidad = valDisponibilidad;
    }

    public BigDecimal getValRelCalidadPrecio() {
        return valRelCalidadPrecio;
    }

    public void setValRelCalidadPrecio(BigDecimal valRelCalidadPrecio) {
        this.valRelCalidadPrecio = valRelCalidadPrecio;
    }

    public BigDecimal getValAtencion() {
        return valAtencion;
    }

    public void setValAtencion(BigDecimal valAtencion) {
        this.valAtencion = valAtencion;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    // Métodos Relaciones id * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public SociedadRpl getSociedadOpinante() {
        return sociedadOpinante;
    }

    public void setSociedadOpinante(SociedadRpl sociedadOpinante) {
        this.sociedadOpinante = sociedadOpinante;
    }

    public SociedadRpl getSociedadOpinada() {
        return sociedadOpinada;
    }

    public void setSociedadOpinada(SociedadRpl sociedadOpinada) {
        this.sociedadOpinada = sociedadOpinada;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.valServicio);
        hash = 97 * hash + Objects.hashCode(this.valEntrega);
        hash = 97 * hash + Objects.hashCode(this.valDisponibilidad);
        hash = 97 * hash + Objects.hashCode(this.valRelCalidadPrecio);
        hash = 97 * hash + Objects.hashCode(this.valAtencion);
        hash = 97 * hash + Objects.hashCode(this.sociedadOpinante);
        hash = 97 * hash + Objects.hashCode(this.sociedadOpinada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Valoraciones other = (Valoraciones) obj;
        if (!Objects.equals(this.valServicio, other.valServicio)) {
            return false;
        }
        if (!Objects.equals(this.valEntrega, other.valEntrega)) {
            return false;
        }
        if (!Objects.equals(this.valDisponibilidad, other.valDisponibilidad)) {
            return false;
        }
        if (!Objects.equals(this.valRelCalidadPrecio, other.valRelCalidadPrecio)) {
            return false;
        }
        if (!Objects.equals(this.valAtencion, other.valAtencion)) {
            return false;
        }
        if (!Objects.equals(this.sociedadOpinante, other.sociedadOpinante)) {
            return false;
        }
        if (!Objects.equals(this.sociedadOpinada, other.sociedadOpinada)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Valoraciones{" + "idValoracion=" + idValoracion + ", version=" + version + ", valServicio=" + valServicio + ", valEntrega=" + valEntrega + ", valDisponibilidad=" + valDisponibilidad + ", valRelCalidadPrecio=" + valRelCalidadPrecio + ", valAtencion=" + valAtencion + ", aprobacion=" + aprobacion + ", sociedadOpinante=" + sociedadOpinante + ", sociedadOpinada=" + sociedadOpinada + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


}
