/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadFormatoVta;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadFormatoVtaService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class UnidadFormatoVtaServiceImpl implements UnidadFormatoVtaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UnidadFormatoVtaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(UnidadFormatoVta unidadFormatoVta) throws PreexistingEntityException, Exception {
        try {
        em.persist(unidadFormatoVta);
        } catch (Exception ex) {
            if (findUnidadFormatoVta(unidadFormatoVta.getIdUnidadFormatoVta()) != null) {
                throw new PreexistingEntityException("UnidadFormatoVta " + unidadFormatoVta + " already exists.", ex);
    }
            throw ex;
        } 
    }
    
    @Transactional
    public void create(UnidadFormatoVta unidadFormatoVta) throws PreexistingEntityException, Exception {
        try {
        em.persist(unidadFormatoVta);
        } catch (Exception ex) {
            if (findUnidadFormatoVta(unidadFormatoVta.getIdUnidadFormatoVta()) != null) {
                throw new PreexistingEntityException("UnidadFormatoVta " + unidadFormatoVta + " already exists.", ex);
    }
            throw ex;
        } 
    }

    @Transactional
    public void edit(UnidadFormatoVta unidadFormatoVta) throws NonexistentEntityException, Exception {
        try {
                unidadFormatoVta = em.merge(unidadFormatoVta);;
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = unidadFormatoVta.getIdUnidadFormatoVta();
                if (findUnidadFormatoVta(id) == null) {
                    throw new NonexistentEntityException("The unidadFormatoVta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    @Transactional
    public void destroy(String id) throws NonexistentEntityException {
        UnidadFormatoVta unidadFormatoVta;
        try {
            unidadFormatoVta = em.getReference(UnidadFormatoVta.class, id);
            unidadFormatoVta.getIdUnidadFormatoVta();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The unidadFormatoVta with id " + id + " no longer exists.", enfe);
        }
        em.remove(unidadFormatoVta);
    }

    @Transactional
    public List<UnidadFormatoVta> findUnidadFormatoVtaEntities() {
        return findUnidadFormatoVtaEntities(true, -1, -1);
    }

    @Transactional
    public List<UnidadFormatoVta> findUnidadFormatoVtaEntities(int maxResults, int firstResult) {
        return findUnidadFormatoVtaEntities(false, maxResults, firstResult);
    }

    @Transactional
    private List<UnidadFormatoVta> findUnidadFormatoVtaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UnidadFormatoVta.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    @Transactional
    public UnidadFormatoVta findUnidadFormatoVta(String id) {
        return em.find(UnidadFormatoVta.class, id);
    }

    @Transactional
    public int getUnidadFormatoVtaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UnidadFormatoVta> rt = cq.from(UnidadFormatoVta.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
