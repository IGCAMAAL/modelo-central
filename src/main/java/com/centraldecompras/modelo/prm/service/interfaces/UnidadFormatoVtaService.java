/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.UnidadFormatoVta;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.FormatoVta;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface UnidadFormatoVtaService {

    UnidadFormatoVta findUnidadFormatoVta(String id);

    void create(UnidadFormatoVta unidadFormatoVta) throws PreexistingEntityException, Exception;

    void create_C(UnidadFormatoVta unidadFormatoVta) throws PreexistingEntityException, Exception;

    void edit(UnidadFormatoVta unidadFormatoVta) throws NonexistentEntityException, Exception;

    List<UnidadFormatoVta> findUnidadFormatoVtaEntities();

}
