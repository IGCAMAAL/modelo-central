
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface EtiquetaService {

    Etiqueta findEtiqueta(String id);

    void create(Etiqueta etiqueta) throws PreexistingEntityException, Exception; 

    void edit(Etiqueta etiqueta) throws NonexistentEntityException, Exception;
     
    List<Etiqueta> findEtiquetaEntities();

    void destroy(String id) throws NonexistentEntityException;
}
