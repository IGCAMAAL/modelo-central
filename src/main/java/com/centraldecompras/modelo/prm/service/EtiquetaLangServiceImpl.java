/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.acceso.service.SociedadServiceImpl;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadMedidaLang;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaLangService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class EtiquetaLangServiceImpl implements EtiquetaLangService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EtiquetaLangServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(EtiquetaLang etiquetaLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(etiquetaLang);
        } catch (Exception ex) {
            if (findEtiquetaLang(etiquetaLang.getEtiquetaEntity().getIdEtiqueta()) != null) {
                throw new PreexistingEntityException("EtiquetaLang " + etiquetaLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(EtiquetaLang etiquetaLang) throws NonexistentEntityException, Exception {
        try {
            etiquetaLang = em.merge(etiquetaLang);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = etiquetaLang.getEtiquetaEntity().getIdEtiqueta();
                if (findEtiquetaLang(id) == null) {
                    throw new NonexistentEntityException("The unidadMedidaLang with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EtiquetaLang etiquetaLang;
        try {
            etiquetaLang = em.getReference(EtiquetaLang.class, id);
            etiquetaLang.getEtiquetaEntity().getIdEtiqueta();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The etiquetaLang with id " + id + " no longer exists.", enfe);
        }
        em.remove(etiquetaLang);
    }

    public List<EtiquetaLang> findEtiquetaLangEntities() {
        return findEtiquetaLangEntities(true, -1, -1);
    }

    public List<EtiquetaLang> findEtiquetaLangEntities(int maxResults, int firstResult) {
        return findEtiquetaLangEntities(false, maxResults, firstResult);
    }

    private List<EtiquetaLang> findEtiquetaLangEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UnidadMedidaLang.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public EtiquetaLang findEtiquetaLang(String id) {
        return em.find(EtiquetaLang.class, id);
    }

    public int getEtiquetaLangCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UnidadMedidaLang> rt = cq.from(UnidadMedidaLang.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<EtiquetaLang> findEtiquetaLangBy_E(Etiqueta etiqueta) {
        List<EtiquetaLang> reply = null;
        reply = (List<EtiquetaLang>) em.createNamedQuery("EtiquetaLang.findEtiquetaLangBy_E")
                .setParameter("etiqueta", etiqueta)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList();
        }
        return reply;

    }

    public List<EtiquetaLang> findEtiquetaLangBy_E_Lang(EtiquetaLang etiquetaLang) {

        List<EtiquetaLang> reply = null;
        reply = (List<EtiquetaLang>) em.createNamedQuery("EtiquetaLang.findEtiquetaLangBy_E_Lang")
                .setParameter("etiqueta", etiquetaLang.getEtiquetaEntity())
                .setParameter("idioma", etiquetaLang.getIdioma())
                .getResultList();
        if (reply == null) {
            reply = new ArrayList();
        }
        return reply;
    }

    public EtiquetaLang findEtiquetaLangBy_IdE_Lang(String etiquetaId, String idioma) {
        EtiquetaLang reply = null;
        try {
            reply = (EtiquetaLang) em.createNamedQuery("EtiquetaLang.findEtiquetaLangBy_IdE_Lang")
                    .setParameter("etiquetaId", etiquetaId)
                    .setParameter("idioma", idioma)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing TODO
        }
        return reply;

    }

    public DynaBean findEtiqueta_Pag_WithFilter(ParametroFiltro parametroFiltro) {
        DynaBean reply = null;
        List<String> idEtiquetas = null;

        StringBuilder nombreBuscado = new StringBuilder();
        int ultimaPagina = 0;

        if (parametroFiltro.getEtiqueta().isEmpty() || parametroFiltro.getEtiqueta().equals("")) {
            parametroFiltro.setEtiqueta(null);
        }

        Long resultadosmax = findEtiqueta_Con_WithFilter(parametroFiltro);

        int leidos = resultadosmax.intValue();
        ultimaPagina = (leidos + parametroFiltro.getNumeroRegistrosPagina() - 1) / parametroFiltro.getNumeroRegistrosPagina();

        if (parametroFiltro.getPaginaActual() > ultimaPagina) {
            parametroFiltro.setPaginaActual(ultimaPagina);
        }

        if (parametroFiltro.getEtiqueta() != null) {
            switch (parametroFiltro.getCoincidenciaEtiqueta()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta());
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta());
                    break;
            }
        }

        try {

            if (parametroFiltro.getEtiqueta() != null) {
                idEtiquetas = (List<String>) em.createNamedQuery("EtiquetaLang.findEtiquetaWith_Filter_E")
                        .setParameter("etiqueta", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .setFirstResult((parametroFiltro.getPaginaActual() - 1) * parametroFiltro.getNumeroRegistrosPagina())
                        .setMaxResults(parametroFiltro.getNumeroRegistrosPagina())
                        .getResultList();
            }

            if (parametroFiltro.getEtiqueta() == null) {
                idEtiquetas = (List<String>) em.createNamedQuery("EtiquetaLang.findEtiquetaWith_Filter_")
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .setFirstResult((parametroFiltro.getPaginaActual() - 1) * parametroFiltro.getNumeroRegistrosPagina())
                        .setMaxResults(parametroFiltro.getNumeroRegistrosPagina())
                        .getResultList();
            }

        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Etiquetas con patron=" + parametroFiltro.getEtiqueta() + ex.getMessage());
        }

        DynaProperty[] props = new DynaProperty[]{
            new DynaProperty("totalPaginas", Integer.TYPE),
            new DynaProperty("totalRegistros", Integer.TYPE),
            new DynaProperty("paginaActual", Integer.TYPE),
            new DynaProperty("idEtiquetas", List.class),};
        BasicDynaClass dynaClass = new BasicDynaClass("datosEtiqueta", null, props);

        try {
            reply = dynaClass.newInstance();
            reply.set("totalPaginas", ultimaPagina);
            reply.set("totalRegistros", leidos);
            reply.set("paginaActual", parametroFiltro.getPaginaActual());
            reply.set("idEtiquetas", idEtiquetas);
        } catch (IllegalAccessException | InstantiationException ex) {
            try {
                throw Exception(ex.getMessage());
            } catch (java.lang.Exception ex1) {
                Logger.getLogger(SociedadServiceImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

        return reply;
    }

    private Exception Exception(String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Long findEtiqueta_Con_WithFilter(ParametroFiltro parametroFiltro) {
        Long reply = null;

        StringBuilder nombreBuscado = new StringBuilder();

        if (parametroFiltro.getEtiqueta() != null) {
            switch (parametroFiltro.getCoincidenciaEtiqueta()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta());
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta());
                    break;
            }
        }

        try {

            if (parametroFiltro.getEtiqueta() != null) {
                reply = (Long) em.createNamedQuery("EtiquetaLang.findEtiquetaWith_Filter_E_COUNT")
                        .setParameter("etiqueta", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .getSingleResult();
            }

            if (parametroFiltro.getEtiqueta() == null) {
                reply = (Long) em.createNamedQuery("EtiquetaLang.findEtiquetaWith_Filter__COUNT")
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .getSingleResult();
            }

        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Etiquetas con patron=" + parametroFiltro.getEtiqueta() + ex.getMessage());
        }

        return reply;
    }

    public Long findEtiqueta_Con_WithFilter_VIEJO(ParametroFiltro parametroFiltro) {
        List<Long> reply = null;

        StringBuilder nombreBuscado = new StringBuilder();
        StringBuilder identificadorBuscado = new StringBuilder();
        StringBuilder codper = new StringBuilder();

        StringBuilder qry = new StringBuilder(
                "   SELECT COUNT(d.etiquetaId) "
                + "     FROM EtiquetaLang d "
                + "    WHERE ");

        if (parametroFiltro.getEtiqueta() != null) {
            switch (parametroFiltro.getCoincidenciaEtiqueta()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(" d.traduccionDesc ").append(" LIKE '").append(parametroFiltro.getEtiqueta()).append("%'");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append(" d.traduccionDesc ").append(" LIKE '%").append(parametroFiltro.getEtiqueta()).append("%'");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append(" d.traduccionDesc ").append(" LIKE '%").append(parametroFiltro.getEtiqueta()).append("'");
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(" d.traduccionDesc ").append(" = '").append(parametroFiltro.getEtiqueta()).append("'");
                    break;
            }
            qry = qry.append(nombreBuscado);
        }

        if (parametroFiltro.getIdioma() != null) {
            if (parametroFiltro.getEtiqueta() != null) {
                qry = qry.append(" AND ");
            }
            identificadorBuscado = (identificadorBuscado).append(" d.idioma ").append(" = '").append(parametroFiltro.getIdioma()).append("'");
            qry = qry.append(identificadorBuscado);
        }

        reply = em.createQuery(qry.toString()).getResultList();

        return reply.get(0);
    }

    public List<EtiquetaLang> findEtiquetaLangTypeAhead(int tipo, String cadena, String estado, int cantidadInt, String idioma) {
        List<EtiquetaLang> reply = new ArrayList();
        Query query = null;

        StringBuffer descripcionSql = new StringBuffer();
        StringBuffer descripcionExcludeSql = new StringBuffer();

        if (tipo == 1) {
            // Empieza
            descripcionSql.append(cadena).append("%");
            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("EtiquetaLang.findEtiquetaEmpiezaTypeAheadTodo")
                        .setParameter("idioma", idioma)
                        .setParameter("traduccionDesc", descripcionSql.toString())
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("EtiquetaLang.findEtiquetaEmpiezaTypeAheadSelEstado")
                        .setParameter("idioma", idioma)
                        .setParameter("traduccionDesc", descripcionSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setMaxResults(cantidadInt);
            }

        } else {
            // Contiene
            descripcionSql.append("%").append(cadena).append("%");
            descripcionExcludeSql.append(cadena).append("%");

            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("EtiquetaLang.findEtiquetaContieneTypeAheadTodo")
                        .setParameter("idioma", idioma)
                        .setParameter("traduccionDesc", descripcionSql.toString())
                        .setParameter("traduccionDescExclude", descripcionExcludeSql.toString())
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("EtiquetaLang.findEtiquetaContieneTypeAheadSelEstado")
                        .setParameter("idioma", idioma)
                        .setParameter("traduccionDesc", descripcionSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setParameter("traduccionDescExclude", descripcionExcludeSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setMaxResults(cantidadInt);
            }
        }

        reply = query.getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public int deleteEtiquetaLangBy_idE_Lang(String etiquetaId, String idioma) {
        return em.createNamedQuery("EtiquetaLang.deleteEtiquetaLangBy_idE_Lang")
                .setParameter("etiquetaId", etiquetaId)
                .setParameter("idioma", idioma)
                .executeUpdate();
    }
}
