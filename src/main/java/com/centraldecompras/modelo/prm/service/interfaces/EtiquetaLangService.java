package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.modelo.prm.service.ParametroFiltro;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import org.apache.commons.beanutils.DynaBean;

/**
 *
 * @author ciberado
 */
public interface EtiquetaLangService {

    EtiquetaLang findEtiquetaLang(String id);

    void create(EtiquetaLang etiquetaLang) throws PreexistingEntityException, Exception;

    void edit(EtiquetaLang etiquetaLang) throws NonexistentEntityException, Exception;

    List<EtiquetaLang> findEtiquetaLangEntities();

    List<EtiquetaLang> findEtiquetaLangBy_E(Etiqueta etiqueta);

    List<EtiquetaLang> findEtiquetaLangBy_E_Lang(EtiquetaLang etiquetaLang);

    EtiquetaLang findEtiquetaLangBy_IdE_Lang(String etiquetaLangId, String lang);

    DynaBean findEtiqueta_Pag_WithFilter(ParametroFiltro parametroFiltro);

    List<EtiquetaLang> findEtiquetaLangTypeAhead(int tipo, String cadena, String estado, int cantidadInt, String idioma);
    
    int deleteEtiquetaLangBy_idE_Lang(String etiquetaId, String idioma);

}
