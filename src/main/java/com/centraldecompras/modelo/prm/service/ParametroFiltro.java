package com.centraldecompras.modelo.prm.service;

public class ParametroFiltro {
     String etiqueta;
     String idioma;

     int coincidenciaEtiqueta;         //1=Empieza por, 2=Contiene, 3=Finaliza con, 4=Es igual a
    
     int  numeroRegistrosPagina;
     int paginaActual; 

    public ParametroFiltro() {
    }
    
    public ParametroFiltro(String etiqueta, String idioma, int coincidenciaNombre, int coincidenciaEtiqueta, int numeroRegistrosPagina, int paginaActual) {
        this.etiqueta = etiqueta;
        this.idioma = idioma;

        this.coincidenciaEtiqueta = coincidenciaEtiqueta;

        this.numeroRegistrosPagina = numeroRegistrosPagina;
        this.paginaActual = paginaActual;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getCoincidenciaEtiqueta() {
        return coincidenciaEtiqueta;
    }

    public void setCoincidenciaEtiqueta(int coincidenciaEtiqueta) {
        this.coincidenciaEtiqueta = coincidenciaEtiqueta;
    }

    public int getNumeroRegistrosPagina() {
        return numeroRegistrosPagina;
    }

    public void setNumeroRegistrosPagina(int numeroRegistrosPagina) {
        this.numeroRegistrosPagina = numeroRegistrosPagina;
    }

    public int getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(int paginaActual) {
        this.paginaActual = paginaActual;
    }
    
    
}
