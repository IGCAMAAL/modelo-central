/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

public interface UnidadMedidaService {

    UnidadMedida findUnidadMedida(String id);

    void create(UnidadMedida unidadMedida) throws PreexistingEntityException, Exception;

    void create_C(UnidadMedida unidadMedida) throws PreexistingEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    void edit(UnidadMedida unidadMedida) throws NonexistentEntityException, Exception;

    List<UnidadMedida> findUnidadMedidaEntities();

}
