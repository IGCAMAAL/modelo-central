package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaService;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class FormatoVtaServiceImpl implements FormatoVtaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FormatoVtaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(FormatoVta formatoVta) throws PreexistingEntityException, Exception {
        try {
            em.persist(formatoVta);
        } catch (Exception ex) {
            if (findFormatoVta(formatoVta.getIdFormatoVta()) != null) {
                throw new PreexistingEntityException("FormatoVta " + formatoVta + " already exists.", ex);
            }
            throw ex;
        }
    }
    
    public void create(FormatoVta formatoVta) throws PreexistingEntityException, Exception {
        try {
            em.persist(formatoVta);
        } catch (Exception ex) {
            if (findFormatoVta(formatoVta.getIdFormatoVta()) != null) {
                throw new PreexistingEntityException("FormatoVta " + formatoVta + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(FormatoVta formatoVta) throws NonexistentEntityException, Exception {
        try {
              formatoVta = em.merge(formatoVta);
         } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = formatoVta.getIdFormatoVta() ;
                if(findFormatoVta(id) == null){
                    throw new NonexistentEntityException("The formatoVta with id " + formatoVta.getTipoEnvase() + " no longer exists.");
            }}
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        FormatoVta formatoVta;
        try {
            formatoVta = em.getReference(FormatoVta.class, id);
            formatoVta.getIdFormatoVta();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The formatoVta with id " + id + " no longer exists.", enfe);
        }
        em.remove(formatoVta);
    }

    public List<FormatoVta> findFormatoVtaEntities() {
        return findFormatoVtaEntities(true, -1, -1);
    }

    public List<FormatoVta> findFormatoVtaEntities(int maxResults, int firstResult) {
        return findFormatoVtaEntities(false, maxResults, firstResult);
    }

    private List<FormatoVta> findFormatoVtaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(FormatoVta.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public FormatoVta findFormatoVta(String id) {
        return em.find(FormatoVta.class, id);
    }

    public int getFormatoVtaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<FormatoVta> rt = cq.from(FormatoVta.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
