package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NivelProductoServiceImpl implements NivelProductoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NivelProductoServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(NivelProducto nivelProducto) {
        em.persist(nivelProducto);
    } 
    
    public void create(NivelProducto nivelProducto) {
        em.persist(nivelProducto);
    } 

    public void edit(NivelProducto nivelProducto) throws NonexistentEntityException, Exception {
        try {
            nivelProducto = em.merge(nivelProducto);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = nivelProducto.getIdNivelProducto();
                if (findNivelProducto(id) == null) {
                    throw new NonexistentEntityException("The generoJerarquia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        NivelProducto nivelProducto;
        try {
            nivelProducto = em.getReference(NivelProducto.class, id);
            nivelProducto.getIdNivelProducto();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The generoJerarquia with id " + id + " no longer exists.", enfe);
        }
        em.remove(nivelProducto);
    }

    public List<NivelProducto> findNivelProductoEntities() {
        return findNivelProductoEntities(true, -1, -1);
    }

    public List<NivelProducto> findNivelProductoEntities(int maxResults, int firstResult) {
        return findNivelProductoEntities(false, maxResults, firstResult);
    }

    private List<NivelProducto> findNivelProductoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(NivelProducto.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public NivelProducto findNivelProducto(int id) {
        return em.find(NivelProducto.class, id);
    }

    public int getNivelProductoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<NivelProducto> rt = cq.from(NivelProducto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
   
}
