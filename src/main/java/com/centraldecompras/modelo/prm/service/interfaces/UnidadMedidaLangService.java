/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.UnidadMedidaLang;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface UnidadMedidaLangService {

    UnidadMedidaLang findUnidadMedidaLang(String id);

    void create(UnidadMedidaLang unidadMedidaLang) throws PreexistingEntityException, Exception;

    void create_C(UnidadMedidaLang unidadMedidaLang) throws PreexistingEntityException, Exception;

    void edit(UnidadMedidaLang unidadMedidaLang) throws NonexistentEntityException, Exception;

    int deleteUnidadMedidaLangBy_idUM_Lang(String unidadMedidaId, String idioma);

    List<UnidadMedidaLang> findUnidadMedidaLangEntities();

    List<UnidadMedidaLang> findUnidadMedidaLangBy_UM(UnidadMedida unidadMedida);

    List<UnidadMedidaLang> findUnidadMedidaBy_UM_Lang(UnidadMedidaLang unidadMedidaLang);

    UnidadMedidaLang findUnidadMedidaLangBy_IdFV_Lang(String unidadMedidaId, String lang);

    List<String[]> findUnidadMedidaLangBy_Lang(String idioma);
}
