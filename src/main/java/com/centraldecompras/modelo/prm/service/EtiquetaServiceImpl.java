package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.EtiquetaService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class EtiquetaServiceImpl implements EtiquetaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(EtiquetaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(Etiqueta etiqueta) throws PreexistingEntityException, Exception {
        try {
            em.persist(etiqueta);
        } catch (Exception ex) {
            if (findEtiqueta(etiqueta.getIdEtiqueta()) != null) {
                throw new PreexistingEntityException("Etiqueta " + etiqueta + " already exists.", ex);
    }
            throw ex;
        } 
    }
     
    public void edit(Etiqueta etiqueta) throws NonexistentEntityException, Exception { 
        try {
                etiqueta = em.merge(etiqueta);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = etiqueta.getIdEtiqueta();
                if (findEtiqueta(id) == null) {
                    throw new NonexistentEntityException("The etiqueta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }
    
    public void destroy(String id) throws NonexistentEntityException {
            Etiqueta etiqueta;
            try { 
                etiqueta = em.getReference(Etiqueta.class, id);
                etiqueta.getIdEtiqueta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The etiqueta with id " + id + " no longer exists.", enfe);
            }
            em.remove(etiqueta);
    }
    
    public List<Etiqueta> findEtiquetaEntities() {
        return findEtiquetaEntities(true, -1, -1);
    }
    
    public List<Etiqueta> findEtiquetaEntities(int maxResults, int firstResult) {
        return findEtiquetaEntities(false, maxResults, firstResult);
    }
    
    private List<Etiqueta> findEtiquetaEntities(boolean all, int maxResults, int firstResult) {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Etiqueta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
    }
    
    public Etiqueta findEtiqueta(String id) {
            return em.find(Etiqueta.class, id);
    }
    
    public int getUnidadMedidaCount() {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UnidadMedida> rt = cq.from(UnidadMedida.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
    }
    
}
