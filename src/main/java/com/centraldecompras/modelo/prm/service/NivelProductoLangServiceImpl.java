/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.modelo.ArticuloLangDesc;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.NivelProductoLangPK;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class NivelProductoLangServiceImpl implements NivelProductoLangService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NivelProductoLangServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(NivelProductoLang nivelProductoLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(nivelProductoLang);
        } catch (Exception ex) {
            NivelProductoLangPK nivelProductoLangPK = new NivelProductoLangPK(nivelProductoLang.getNivelProductoId(), nivelProductoLang.getIdioma());
            //if (findNivelProductoLang(nivelProductoLang.getNivelProductoEntity().getIdNivelProducto()) != null) {
            if (findNivelProductoLang(nivelProductoLangPK) != null) {
                throw new PreexistingEntityException("nivelProductoLang " + nivelProductoLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(NivelProductoLang nivelProductoLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(nivelProductoLang);
        } catch (Exception ex) {
            NivelProductoLangPK nivelProductoLangPK = new NivelProductoLangPK(nivelProductoLang.getNivelProductoId(), nivelProductoLang.getIdioma());
            //if (findNivelProductoLang(nivelProductoLang.getNivelProductoEntity().getIdNivelProducto()) != null) {
            if (findNivelProductoLang(nivelProductoLangPK) != null) {
                throw new PreexistingEntityException("nivelProductoLang " + nivelProductoLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(NivelProductoLang nivelProductoLang) throws NonexistentEntityException, Exception {
        try {
            nivelProductoLang = em.merge(nivelProductoLang);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                // int id = nivelProductoLang.getNivelProductoEntity().getIdNivelProducto();
                //  if (findNivelProductoLang(id) == null) {  throw new NonexistentEntityException("The generoJerarquiaLang with id " + id + " no longer exists."); } 
                NivelProductoLangPK nivelProductoLangPK = new NivelProductoLangPK(nivelProductoLang.getNivelProductoId(), nivelProductoLang.getIdioma());
                if (findNivelProductoLang(nivelProductoLangPK) == null) {
                    throw new NonexistentEntityException("The NivelProductoLang with id " + nivelProductoLangPK.toString() + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        NivelProductoLang nivelProductoLang;
        try {
            nivelProductoLang = em.getReference(NivelProductoLang.class, id);
            nivelProductoLang.getNivelProductoId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The generoJerarquiaLang with id " + id + " no longer exists.", enfe);
        }
        em.remove(nivelProductoLang);
    }

    public List<NivelProductoLang> findNivelProductoLangEntities() {
        return findNivelProductoLangEntities(true, -1, -1);
    }

    public List<NivelProductoLang> findNivelProductoLangEntities(int maxResults, int firstResult) {
        return findNivelProductoLangEntities(false, maxResults, firstResult);
    }

    private List<NivelProductoLang> findNivelProductoLangEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(NivelProductoLang.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public NivelProductoLang findNivelProductoLang(NivelProductoLangPK nivelProductoLangPK) {
        return em.find(NivelProductoLang.class, nivelProductoLangPK);
    }

    public int getNivelProductoLangCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<NivelProductoLang> rt = cq.from(NivelProductoLang.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<NivelProductoLang> findNivelProductoLangBy_NP(NivelProducto nivelProducto) {
        List<NivelProductoLang> reply = null;
        reply = (List<NivelProductoLang>) em.createNamedQuery("NivelProductoLang.findNivelProductoLangBy_NP")
                .setParameter("nivelProducto", nivelProducto)
                .getResultList();
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

    public NivelProductoLang findNivelProductoLangBy_IdNP_Lang(int nivelProductoId, String lang) {
        NivelProductoLang reply = null;
        try {
            reply = (NivelProductoLang) em.createNamedQuery("NivelProductoLang.findNivelProductoLangBy_IdNP_Lang")
                    .setParameter("nivelProductoId", nivelProductoId)
                    .setParameter("idioma", lang)
                    .getSingleResult();
        } catch (Exception e) {
            // Nothing TODO
        }
        return reply;
    }

    public List<String> findProductoLangKey() {
        List<String> reply = null;
        reply =(List<String>) em
                .createNamedQuery("NivelProductoLang.findProductoLangKey")
                .getResultList();
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

    public int deleteNivelProductoLangBy_IdNP_Lang(int nivelProductoId, String lang) {
        return (int) em.createNamedQuery("NivelProductoLang.deleteNivelProductoLangBy_IdNP_Lang")
                .setParameter("nivelProductoId", nivelProductoId)
                .setParameter("idioma", lang)
                .getSingleResult();
    }
}
