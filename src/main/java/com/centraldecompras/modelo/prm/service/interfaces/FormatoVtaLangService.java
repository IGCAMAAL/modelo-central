/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.FormatoVtaLang;
import com.centraldecompras.modelo.prm.service.ParametroFiltro;
import java.util.List;
import org.apache.commons.beanutils.DynaBean;

/**
 *
 * @author ciberado
 */
public interface FormatoVtaLangService {

    FormatoVtaLang findProductoLang(String id);

    void create(FormatoVtaLang formatoVtaLang) throws PreexistingEntityException, Exception;

    void create_C(FormatoVtaLang formatoVtaLang) throws PreexistingEntityException, Exception;

    void edit(FormatoVtaLang formatoVtaLang) throws NonexistentEntityException, Exception;

    List<FormatoVtaLang> findFormatoVtaLangBy_FV(FormatoVta formatoVta);

    List<FormatoVtaLang> findFormatoVtaLangBy_FV_Lang(FormatoVtaLang formatoVtaLang);

    FormatoVtaLang findFormatoVtaLangBy_IdFV_Lang(String formatoVtaId, String lang);

    int deleteFormatoVtaLangBy_idFV_Lang(String formatoVtaId, String idioma);
    
    DynaBean findFormatoVta_Pag_WithFilter(ParametroFiltro parametroFiltro);
}
