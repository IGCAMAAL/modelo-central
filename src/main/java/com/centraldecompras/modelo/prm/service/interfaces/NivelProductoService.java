/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.NivelProducto;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface NivelProductoService {

    NivelProducto findNivelProducto(int id);

    void create(NivelProducto NivelProducto) throws PreexistingEntityException, Exception;

    void create_C(NivelProducto nivelProducto);

    List<NivelProducto> findNivelProductoEntities();
}
