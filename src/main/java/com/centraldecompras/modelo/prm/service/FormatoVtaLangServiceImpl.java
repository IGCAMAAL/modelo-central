/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.acceso.service.SociedadServiceImpl;
import com.centraldecompras.modelo.ArticuloLangDesc;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.EtiquetaLang;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.FormatoVtaLang;
import com.centraldecompras.modelo.UnidadMedidaLang;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaLangService;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FormatoVtaLangServiceImpl implements FormatoVtaLangService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(FormatoVtaLangServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(FormatoVtaLang formatoVtaLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(formatoVtaLang);
        } catch (Exception ex) {
            if (findProductoLang(formatoVtaLang.getFormatoVtaEntity().getIdFormatoVta()) != null) {
                throw new PreexistingEntityException("FormatoVtaLang " + formatoVtaLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(FormatoVtaLang formatoVtaLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(formatoVtaLang);
        } catch (Exception ex) {
            if (findProductoLang(formatoVtaLang.getFormatoVtaEntity().getIdFormatoVta()) != null) {
                throw new PreexistingEntityException("FormatoVtaLang " + formatoVtaLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(FormatoVtaLang formatoVtaLang) throws NonexistentEntityException, Exception {
        try {
            formatoVtaLang = em.merge(formatoVtaLang);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = formatoVtaLang.getFormatoVtaEntity().getIdFormatoVta();
                if (findProductoLang(id) == null) {
                    throw new NonexistentEntityException("The formatoVtaLang with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        FormatoVtaLang formatoVtaLang;
        try {
            formatoVtaLang = em.getReference(FormatoVtaLang.class, id);
            formatoVtaLang.getFormatoVtaEntity();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The formatoVtaLang with id " + id + " no longer exists.", enfe);
        }
        em.remove(formatoVtaLang);
    }

    public List<FormatoVtaLang> findFormatoVtaLangEntities() {
        return findFormatoVtaLangEntities(true, -1, -1);
    }

    public List<FormatoVtaLang> findFormatoVtaLangEntities(int maxResults, int firstResult) {
        return findFormatoVtaLangEntities(false, maxResults, firstResult);
    }

    private List<FormatoVtaLang> findFormatoVtaLangEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(FormatoVtaLang.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public FormatoVtaLang findProductoLang(String id) {
        return em.find(FormatoVtaLang.class, id);
    }

    public int getFormatoVtaLangCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<FormatoVtaLang> rt = cq.from(FormatoVtaLang.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<FormatoVtaLang> findFormatoVtaLangBy_FV(FormatoVta formatoVta) {
        List<FormatoVtaLang> reply = null;
        reply = (List<FormatoVtaLang>) em.createNamedQuery("FormatoVtaLang.findFormatoVtaLangBy_FV")
                .setParameter("formatoVta", formatoVta)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<FormatoVtaLang> findFormatoVtaLangBy_FV_Lang(FormatoVtaLang formatoVtaLang) {
        List<FormatoVtaLang> reply = null;
        reply = (List<FormatoVtaLang>) em.createNamedQuery("FormatoVtaLang.findFormatoVtaLangBy_FV_Lang")
                .setParameter("formatoVta", formatoVtaLang.getFormatoVtaEntity())
                .setParameter("idioma", formatoVtaLang.getIdioma())
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public FormatoVtaLang findFormatoVtaLangBy_IdFV_Lang(String formatoVtaId, String lang) {
        FormatoVtaLang reply = null;
        try {
            reply = (FormatoVtaLang) em.createNamedQuery("FormatoVtaLang.findFormatoVtaLangBy_IdFV_Lang")
                    .setParameter("formatoVtaId", formatoVtaId)
                    .setParameter("idioma", lang)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing TODO
        }
        return reply;
    }

    public int deleteFormatoVtaLangBy_idFV_Lang(String formatoVtaId, String idioma) {
        return em.createNamedQuery("FormatoVtaLang.deleteFormatoVtaLangBy_idFV_Lang")
                .setParameter("formatoVtaId", formatoVtaId)
                .setParameter("idioma", idioma)
                .executeUpdate();
    }

    public DynaBean findFormatoVta_Pag_WithFilter(ParametroFiltro parametroFiltro) {
        DynaBean reply = null;
        List<String> idFormatoVta = null;

        StringBuilder nombreBuscado = new StringBuilder();
        int ultimaPagina = 0;

        if (parametroFiltro.getEtiqueta().isEmpty() || parametroFiltro.getEtiqueta().equals("")) {
            parametroFiltro.setEtiqueta(null);
        }

        Long resultadosmax = findFormatoVta_Con_WithFilter(parametroFiltro);

        int leidos = resultadosmax.intValue();
        ultimaPagina = leidos / parametroFiltro.getNumeroRegistrosPagina();
        int residuo = leidos % parametroFiltro.getNumeroRegistrosPagina();
        if (residuo != 0) {
            ultimaPagina++;
        }
        if (ultimaPagina == 0) {
            ultimaPagina++;
        }

        if (parametroFiltro.getPaginaActual() > ultimaPagina) {
            parametroFiltro.setPaginaActual(ultimaPagina);
        }

        if (parametroFiltro.getEtiqueta() != null) {
            switch (parametroFiltro.getCoincidenciaEtiqueta()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta());
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta());
                    break;
            }
        }

        try {

            if (parametroFiltro.getEtiqueta() != null) {
                idFormatoVta = (List<String>) em.createNamedQuery("FormatoVtaLang.findFormatoVtaWith_Filter_E")
                        .setParameter("etiqueta", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .setFirstResult((parametroFiltro.getPaginaActual() - 1) * parametroFiltro.getNumeroRegistrosPagina())
                        .setMaxResults(parametroFiltro.getNumeroRegistrosPagina())
                        .getResultList();
            }

            if (parametroFiltro.getEtiqueta() == null) {
                idFormatoVta = (List<String>) em.createNamedQuery("FormatoVtaLang.findFormatoVtaWith_Filter_")
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .setFirstResult((parametroFiltro.getPaginaActual() - 1) * parametroFiltro.getNumeroRegistrosPagina())
                        .setMaxResults(parametroFiltro.getNumeroRegistrosPagina())
                        .getResultList();
            }

        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado formatos de veta con patron=" + parametroFiltro.getEtiqueta() + ex.getMessage());
        }

        DynaProperty[] props = new DynaProperty[]{
            new DynaProperty("totalPaginas", Integer.TYPE),
            new DynaProperty("totalRegistros", Integer.TYPE),
            new DynaProperty("paginaActual", Integer.TYPE),
            new DynaProperty("idFormatoVta", List.class),};
        BasicDynaClass dynaClass = new BasicDynaClass("datosFormatoVta", null, props);

        try {
            reply = dynaClass.newInstance();
            reply.set("totalPaginas", ultimaPagina);
            reply.set("totalRegistros", leidos);
            reply.set("paginaActual", parametroFiltro.getPaginaActual());
            reply.set("idFormatoVta", idFormatoVta);
        } catch (IllegalAccessException | InstantiationException ex) {
            try {
                throw ex;
            } catch (java.lang.Exception ex1) {
                log.warn(ex + ".............Error al busar formatos de vta con patron=" + parametroFiltro.getEtiqueta() + ex.getMessage());
            }
        }

        return reply;
    }

    private Long findFormatoVta_Con_WithFilter(ParametroFiltro parametroFiltro) {
        Long reply = null;

        StringBuilder nombreBuscado = new StringBuilder();

        if (parametroFiltro.getEtiqueta() != null) {
            switch (parametroFiltro.getCoincidenciaEtiqueta()) {
                case 1:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 2:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta()).append("%");
                    break;
                case 3:
                    nombreBuscado = nombreBuscado.append("%").append(parametroFiltro.getEtiqueta());
                    break;
                case 4:
                    nombreBuscado = nombreBuscado.append(parametroFiltro.getEtiqueta());
                    break;
            }
        }

        try {

            if (parametroFiltro.getEtiqueta() != null) {
                reply = (Long) em.createNamedQuery("FormatoVtaLang.findFormatoVtaWith_Filter_E_COUNT")
                        .setParameter("etiqueta", (nombreBuscado == null) ? null : nombreBuscado.toString())
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .getSingleResult();
            }

            if (parametroFiltro.getEtiqueta() == null) {
                reply = (Long) em.createNamedQuery("FormatoVtaLang.findFormatoVtaWith_Filter__COUNT")
                        .setParameter("idioma", parametroFiltro.getIdioma())
                        .getSingleResult();
            }

        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Formatos de vta con patron=" + parametroFiltro.getEtiqueta() + ex.getMessage());
        }

        return reply;
    }

}
