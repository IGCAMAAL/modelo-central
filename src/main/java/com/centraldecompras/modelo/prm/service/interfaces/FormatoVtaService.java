package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

public interface FormatoVtaService {

    FormatoVta findFormatoVta(String id);

    void create(FormatoVta formatoVta) throws PreexistingEntityException, Exception;

    void create_C(FormatoVta formatoVta) throws PreexistingEntityException, Exception;

    void edit(FormatoVta formatoVta) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;
    
    List<FormatoVta> findFormatoVtaEntities();
}
