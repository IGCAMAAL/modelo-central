/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadFormatoVta;
import com.centraldecompras.modelo.UnidadFormatoVtaLang;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface UnidadFormatoVtaLangService {

    UnidadFormatoVtaLang findUnidadFormatoVtaLang(String id);

    void create(UnidadFormatoVtaLang unidadFormatoVtaLang) throws PreexistingEntityException, Exception;

    void create_C(UnidadFormatoVtaLang unidadFormatoVtaLang) throws PreexistingEntityException, Exception;

    void edit(UnidadFormatoVtaLang unidadFormatoVtaLang) throws NonexistentEntityException, Exception;

    List<UnidadFormatoVtaLang> findUnidadFormatoVtaLangById_UFV(UnidadFormatoVta unidadFormatoVta);

    List<UnidadFormatoVtaLang> findUnidadFormatoVtaLangBy_UFV_Lang(UnidadFormatoVtaLang unidadFormatoVtaLang);

    UnidadFormatoVtaLang findUnidadFormatoVtaLangBy_IdUFV_Lang(String unidadFormatoVtaId, String lang);
}
