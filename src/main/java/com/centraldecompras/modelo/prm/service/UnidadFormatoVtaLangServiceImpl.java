/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadFormatoVta;
import com.centraldecompras.modelo.UnidadFormatoVtaLang;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadFormatoVtaLangService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class UnidadFormatoVtaLangServiceImpl implements UnidadFormatoVtaLangService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UnidadFormatoVtaLangServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(UnidadFormatoVtaLang unidadFormatoVtaLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(unidadFormatoVtaLang);
        } catch (Exception ex) {
            if (findUnidadFormatoVtaLang(unidadFormatoVtaLang.getUnidadFormatoVtaEntity()) != null) {
                throw new PreexistingEntityException("UnidadFormatoVtaLang " + unidadFormatoVtaLang + " already exists.", ex);
            }
            throw ex;
        }
    }
    
    @Transactional
    public void create(UnidadFormatoVtaLang unidadFormatoVtaLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(unidadFormatoVtaLang);
        } catch (Exception ex) {
            if (findUnidadFormatoVtaLang(unidadFormatoVtaLang.getUnidadFormatoVtaEntity()) != null) {
                throw new PreexistingEntityException("UnidadFormatoVtaLang " + unidadFormatoVtaLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    @Transactional
    public void edit(UnidadFormatoVtaLang unidadFormatoVtaLang) throws NonexistentEntityException, Exception {
        try {
                unidadFormatoVtaLang = em.merge(unidadFormatoVtaLang);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = unidadFormatoVtaLang.getUnidadFormatoVtaEntity().getIdUnidadFormatoVta();
                if (findUnidadFormatoVtaLang(id) == null) {
                    throw new NonexistentEntityException("The unidadFormatoVtaLang with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    @Transactional
    public void destroy(String id) throws NonexistentEntityException {
        UnidadFormatoVtaLang unidadFormatoVtaLang;
        try {
            unidadFormatoVtaLang = em.getReference(UnidadFormatoVtaLang.class, id);
            unidadFormatoVtaLang.getUnidadFormatoVtaEntity();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The unidadFormatoVtaLang with id " + id + " no longer exists.", enfe);
        }
        em.remove(unidadFormatoVtaLang);
    }

    @Transactional
    public List<UnidadFormatoVtaLang> findUnidadFormatoVtaLangEntities() {
        return findUnidadFormatoVtaLangEntities(true, -1, -1);
    }

    @Transactional
    public List<UnidadFormatoVtaLang> findUnidadFormatoVtaLangEntities(int maxResults, int firstResult) {
        return findUnidadFormatoVtaLangEntities(false, maxResults, firstResult);
    }

    @Transactional
    private List<UnidadFormatoVtaLang> findUnidadFormatoVtaLangEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UnidadFormatoVtaLang.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    @Transactional
    public UnidadFormatoVtaLang findUnidadFormatoVtaLang(String id) {
        return em.find(UnidadFormatoVtaLang.class, id);
    }
        
    @Transactional
    public UnidadFormatoVtaLang findUnidadFormatoVtaLang(UnidadFormatoVta id) {
        return em.find(UnidadFormatoVtaLang.class, id);
    }
    
    @Transactional
    public int getUnidadFormatoVtaLangCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UnidadFormatoVtaLang> rt = cq.from(UnidadFormatoVtaLang.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    @Transactional
    public List<UnidadFormatoVtaLang> findUnidadFormatoVtaLangById_UFV(UnidadFormatoVta unidadFormatoVta) {
        return em.createNamedQuery("UnidadFormatoVtaLang.findUnidadFormatoVtaLangById_UFV")
                .setParameter("unidadFormatoVta", unidadFormatoVta)
                .getResultList();
    }
    
    public List<UnidadFormatoVtaLang> findUnidadFormatoVtaLangBy_UFV_Lang(UnidadFormatoVtaLang unidadFormatoVtaLang) {
        return em.createNamedQuery("UnidadFormatoVtaLang.findUnidadFormatoVtaLangBy_UFV_Lang")
                .setParameter("unidadFormatoVta", unidadFormatoVtaLang.getUnidadFormatoVtaEntity())
                .setParameter("idioma", unidadFormatoVtaLang.getIdioma())
                .getResultList();
    }  


    public UnidadFormatoVtaLang findUnidadFormatoVtaLangBy_IdUFV_Lang(String unidadFormatoVtaId, String lang){
        return (UnidadFormatoVtaLang) em.createNamedQuery("UnidadFormatoVtaLang.findUnidadFormatoVtaLangBy_IdUFV_Lang")
                .setParameter("unidadFormatoVtaId", unidadFormatoVtaId)
                .setParameter("idioma", lang)
                .getSingleResult();
    }
}
