package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UnidadMedidaServiceImpl implements UnidadMedidaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UnidadMedidaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(UnidadMedida unidadMedida) throws PreexistingEntityException, Exception {
        try {
            em.persist(unidadMedida);
        } catch (Exception ex) {
            if (findUnidadMedida(unidadMedida.getIdUnidadMedida()) != null) {
                throw new PreexistingEntityException("UnidadMedida " + unidadMedida + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(UnidadMedida unidadMedida) throws PreexistingEntityException, Exception {
        try {
            em.persist(unidadMedida);
        } catch (Exception ex) {
            if (findUnidadMedida(unidadMedida.getIdUnidadMedida()) != null) {
                throw new PreexistingEntityException("UnidadMedida " + unidadMedida + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(UnidadMedida unidadMedida) throws NonexistentEntityException, Exception {
        try {
            unidadMedida = em.merge(unidadMedida);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = unidadMedida.getIdUnidadMedida();
                if (findUnidadMedida(id) == null) {
                    throw new NonexistentEntityException("The unidadMedida with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        UnidadMedida unidadMedida;
        try {
            unidadMedida = em.getReference(UnidadMedida.class, id);
            unidadMedida.getIdUnidadMedida();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The unidadMedida with id " + id + " no longer exists.", enfe);
        }
        em.remove(unidadMedida);
    }

    public List<UnidadMedida> findUnidadMedidaEntities() {
        return findUnidadMedidaEntities(true, -1, -1);
    }

    public List<UnidadMedida> findUnidadMedidaEntities(int maxResults, int firstResult) {
        return findUnidadMedidaEntities(false, maxResults, firstResult);
    }

    private List<UnidadMedida> findUnidadMedidaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UnidadMedida.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public UnidadMedida findUnidadMedida(String id) {
        return em.find(UnidadMedida.class, id);
    }

    public int getUnidadMedidaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UnidadMedida> rt = cq.from(UnidadMedida.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
