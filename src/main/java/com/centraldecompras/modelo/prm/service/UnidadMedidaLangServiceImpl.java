package com.centraldecompras.modelo.prm.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.UnidadMedidaLang;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaLangService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UnidadMedidaLangServiceImpl implements UnidadMedidaLangService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UnidadMedidaLangServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(UnidadMedidaLang unidadMedidaLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(unidadMedidaLang);
        } catch (Exception ex) {
            if (findUnidadMedidaLang(unidadMedidaLang.getUnidadMedidaEntity().getIdUnidadMedida()) != null) {
                throw new PreexistingEntityException("UnidadMedidaLang " + unidadMedidaLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(UnidadMedidaLang unidadMedidaLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(unidadMedidaLang);
        } catch (Exception ex) {
            if (findUnidadMedidaLang(unidadMedidaLang.getUnidadMedidaEntity().getIdUnidadMedida()) != null) {
                throw new PreexistingEntityException("UnidadMedidaLang " + unidadMedidaLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(UnidadMedidaLang unidadMedidaLang) throws NonexistentEntityException, Exception {
        try {
            unidadMedidaLang = em.merge(unidadMedidaLang);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = unidadMedidaLang.getUnidadMedidaEntity().getIdUnidadMedida();
                if (findUnidadMedidaLang(id) == null) {
                    throw new NonexistentEntityException("The unidadMedidaLang with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        UnidadMedidaLang unidadMedidaLang;
        try {
            unidadMedidaLang = em.getReference(UnidadMedidaLang.class, id);
            unidadMedidaLang.getUnidadMedidaEntity().getIdUnidadMedida();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The unidadMedidaLang with id " + id + " no longer exists.", enfe);
        }
        em.remove(unidadMedidaLang);
    }

    public List<UnidadMedidaLang> findUnidadMedidaLangEntities() {
        return findUnidadMedidaLangEntities(true, -1, -1);
    }

    public List<UnidadMedidaLang> findUnidadMedidaLangEntities(int maxResults, int firstResult) {
        return findUnidadMedidaLangEntities(false, maxResults, firstResult);
    }

    private List<UnidadMedidaLang> findUnidadMedidaLangEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(UnidadMedidaLang.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public UnidadMedidaLang findUnidadMedidaLang(String id) {
        return em.find(UnidadMedidaLang.class, id);
    }

    public int getUnidadMedidaLangCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<UnidadMedidaLang> rt = cq.from(UnidadMedidaLang.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<UnidadMedidaLang> findUnidadMedidaLangBy_UM(UnidadMedida unidadMedida) {

        List<UnidadMedidaLang> reply = new ArrayList<>();
        reply = (List<UnidadMedidaLang>) em.createNamedQuery("UnidadMedidaLang.findUnidadMedidaLangBy_UM")
                .setParameter("unidadMedida", unidadMedida)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<UnidadMedidaLang> findUnidadMedidaBy_UM_Lang(UnidadMedidaLang unidadMedidaLang) {
        List<UnidadMedidaLang> reply = new ArrayList<>();
        reply = (List<UnidadMedidaLang>) em.createNamedQuery("UnidadMedidaLang.findUnidadMedidaBy_UM_Lang")
                .setParameter("unidadMedida", unidadMedidaLang.getUnidadMedidaEntity())
                .setParameter("idioma", unidadMedidaLang.getIdioma())
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public UnidadMedidaLang findUnidadMedidaLangBy_IdFV_Lang(String unidadMedidaId, String lang) {
        UnidadMedidaLang reply = null;
        try {
            reply = (UnidadMedidaLang) em.createNamedQuery("UnidadMedidaLang.findUnidadMedidaLangBy_IdFV_Lang")
                    .setParameter("unidadMedidaId", unidadMedidaId)
                    .setParameter("idioma", lang)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing TODO
        }
        return reply;

    }

    public List<String[]> findUnidadMedidaLangBy_Lang(String idioma) {
        List<String[]> reply = new ArrayList<>();

        reply = (List<String[]>) em.createNamedQuery("UnidadMedidaLang.findUnidadMedidaLangBy_Lang")
                .setParameter("idioma", idioma)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public int deleteUnidadMedidaLangBy_idUM_Lang(String unidadMedidaId, String idioma) {
        return em.createNamedQuery("UnidadMedidaLang.deleteUnidadMedidaLangBy_idUM_Lang")
                .setParameter("unidadMedidaId", unidadMedidaId)
                .setParameter("idioma", idioma)
                .executeUpdate();
    }
}
