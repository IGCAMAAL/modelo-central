/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.prm.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.NivelProductoLangPK;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface NivelProductoLangService {

    NivelProductoLang findNivelProductoLang(NivelProductoLangPK nivelProductoLangPK);

    void create(NivelProductoLang nivelProductoLang) throws PreexistingEntityException, Exception;

    void create_C(NivelProductoLang nivelProductoLang) throws PreexistingEntityException, Exception;

    void edit(NivelProductoLang nivelProductoLang) throws NonexistentEntityException, Exception;

    List<NivelProductoLang> findNivelProductoLangBy_NP(NivelProducto nivelProducto);

    NivelProductoLang findNivelProductoLangBy_IdNP_Lang(int nivelProductoId, String lang);
    
    List<String> findProductoLangKey();
    
    int deleteNivelProductoLangBy_IdNP_Lang(int nivelProductoId, String lang);
    
}
