package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_RPL_PerfilRpl")
@XmlRootElement

public class PerfilRpl implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(PerfilRpl.class.getName());

    
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idPerfil;
    
    @Version
    private int version;


    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public PerfilRpl() {
    }

    public PerfilRpl(String uuid) {
        this.idPerfil = uuid;
    }

    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    


    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *       

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.idPerfil);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerfilRpl other = (PerfilRpl) obj;
        if (!Objects.equals(this.idPerfil, other.idPerfil)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PerfilRpl{" + "idPerfil=" + idPerfil + ", version=" + version + '}';
    }

}
