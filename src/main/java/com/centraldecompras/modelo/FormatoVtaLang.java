package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_FormatoVtaLang",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"idioma", "traduccionDesc"})
        })
@XmlRootElement
//@IdClass(FormatoVtaLangPK.class)
@NamedQueries({
    @NamedQuery(name = "FormatoVtaLang.findFormatoVtaLangBy_FV", query = ""
            + " SELECT d "
            + "   FROM FormatoVtaLang d "
            + "  WHERE d.formatoVtaEntity = :formatoVta" ),
    @NamedQuery(name = "FormatoVtaLang.findFormatoVtaLangBy_FV_Lang", query = ""
            + " SELECT d "
            + "   FROM FormatoVtaLang d "
            + "  WHERE d.formatoVtaEntity = :formatoVta "
            + "    AND d.idioma = :idioma" ), 
    @NamedQuery(name = "FormatoVtaLang.findFormatoVtaLangBy_IdFV_Lang",query = ""
            + " SELECT d "
            + "   FROM FormatoVtaLang d "
            + "  WHERE d.formatoVtaId = :formatoVtaId "
            + "    AND d.idioma = :idioma" ),
    @NamedQuery(name = "FormatoVtaLang.deleteFormatoVtaLangBy_idFV_Lang",query = ""
            + " DELETE "
            + "   FROM FormatoVtaLang d "
            + "  WHERE d.formatoVtaId = :formatoVtaId "
            + "    AND d.idioma = :idioma" ) ,
    
    
    
    @NamedQuery(name = "FormatoVtaLang.findFormatoVtaWith_Filter_E", query = ""
            + "   SELECT d.formatoVtaId "
            + "     FROM FormatoVtaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :etiqueta "
            + " ORDER BY d.traduccionDesc "),

    @NamedQuery(name = "FormatoVtaLang.findFormatoVtaWith_Filter_", query = ""
            + "   SELECT d.formatoVtaId "
            + "     FROM FormatoVtaLang d "
            + "    WHERE d.idioma = :idioma "
            + " ORDER BY d.traduccionDesc "),

    
    
    @NamedQuery(name = "FormatoVtaLang.findFormatoVtaWith_Filter_E_COUNT", query = ""
            + "   SELECT COUNT(d.formatoVtaId) "
            + "     FROM FormatoVtaLang d "
            + "    WHERE d.idioma = :idioma "
            + "      AND d.traduccionDesc LIKE :etiqueta "
            + " ORDER BY d.traduccionDesc "),

    @NamedQuery(name = "FormatoVtaLang.findFormatoVtaWith_Filter__COUNT", query = ""
            + "   SELECT COUNT(d.formatoVtaId) "
            + "     FROM FormatoVtaLang d "
            + "    WHERE d.idioma = :idioma "
            + " ORDER BY d.traduccionDesc ")
        
            
})
public class FormatoVtaLang implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(FormatoVtaLang.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String formatoVtaId;

    @Id    
    @Basic
    private String idioma;
    
    @Version
    private int version;
    
    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String traduccionDesc;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    @ManyToOne(targetEntity=FormatoVta.class)
    private FormatoVta formatoVtaEntity;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;
    
    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public FormatoVtaLang() {
    }

    public FormatoVtaLang(String formatoVtaId, String idioma, int version, String traduccionDesc, FormatoVta formatoVtaEntity, DatosAuditoria atributosAuditoria) {
        this.formatoVtaId = formatoVtaId;
        this.idioma = idioma;
        this.version = version;
        this.traduccionDesc = traduccionDesc;
        this.formatoVtaEntity = formatoVtaEntity;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public String getFormatoVtaId() {
        return formatoVtaId;
    }

    public void setFormatoVtaId(String formatoVtaId) {
        this.formatoVtaId = formatoVtaId;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getTraduccionDesc() {
        return traduccionDesc;
    }

    public void setTraduccionDesc(String traduccionDesc) {
        this.traduccionDesc = traduccionDesc;
    }
    
    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public FormatoVta getFormatoVtaEntity() {
        return formatoVtaEntity;
    }

    public void setFormatoVtaEntity(FormatoVta formatoVtaEntity) {
        this.formatoVtaEntity = formatoVtaEntity;
    }
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.idioma);
        hash = 97 * hash + Objects.hashCode(this.traduccionDesc);
        hash = 97 * hash + Objects.hashCode(this.formatoVtaEntity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormatoVtaLang other = (FormatoVtaLang) obj;
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        if (!Objects.equals(this.traduccionDesc, other.traduccionDesc)) {
            return false;
        }
        if (!Objects.equals(this.formatoVtaEntity, other.formatoVtaEntity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FormatoVtaLang{" + "formatoVtaId=" + formatoVtaId + ", idioma=" + idioma + ", version=" + version + ", traduccionDesc=" + traduccionDesc + ", formatoVtaEntity=" + formatoVtaEntity + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


    
    
}
