package com.centraldecompras.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_VEN_Comentarios")
@XmlRootElement
@NamedQueries({         
    @NamedQuery(name = "Comentarios.findComentariosBy_idS", query = 
            " SELECT d "
          + "   FROM Comentarios d "
          + "  WHERE d.sociedadOpinada.id = :sociedadOpinadaid ")   
})
public class Comentarios implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Comentarios.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idComentario;

    @Version
    private int version;
    
    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * 
    private String opinion;

    private String aprobacion;
    
    @NotNull
    private BigDecimal relCalidadPrecio;
    
    @NotNull
    private BigDecimal valoracion;
    
    // Atributos Id de relacion * * * * * * * * * * * * * * * * * * * * * * * 

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * 
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl sociedadOpinante; 
    
    @ManyToOne(targetEntity = Articulo.class)
    private Articulo articuloOpinado; 
    
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl sociedadOpinada;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public Comentarios() {
    }

    public Comentarios(String idComentario, String opinion, String aprobacion, BigDecimal relCalidadPrecio, BigDecimal valoracion, 
            /* String sociedadOpinanteIdk, String articuloOpinadoIdk, String sociedadOpinadaIdk, */
            SociedadRpl sociedadOpinante, Articulo articuloOpinado, SociedadRpl sociedadOpinada, DatosAuditoria atributosAuditoria) {
        this.idComentario = idComentario;
        this.opinion = opinion;
        this.aprobacion = aprobacion;
        this.relCalidadPrecio = relCalidadPrecio;
        this.valoracion = valoracion;

        this.sociedadOpinante = sociedadOpinante;
        this.articuloOpinado = articuloOpinado;
        this.sociedadOpinada = sociedadOpinada;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(String idComentario) {
        this.idComentario = idComentario;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    public BigDecimal getRelCalidadPrecio() {
        return relCalidadPrecio;
    }

    public void setRelCalidadPrecio(BigDecimal relCalidadPrecio) {
        this.relCalidadPrecio = relCalidadPrecio;
    }

    public BigDecimal getValoracion() {
        return valoracion;
    }

    public void setValoracion(BigDecimal valoracion) {
        this.valoracion = valoracion;
    }

    // Métodos Relaciones id * * * * * * * * * * * * * * * * * * * * * * * * * * *

    
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public SociedadRpl getSociedadOpinante() {
        return sociedadOpinante;
    }

    public void setSociedadOpinante(SociedadRpl sociedadOpinante) {
        this.sociedadOpinante = sociedadOpinante;
    }

    public Articulo getArticuloOpinado() {
        return articuloOpinado;
    }

    public void setArticuloOpinado(Articulo articuloOpinado) {
        this.articuloOpinado = articuloOpinado;
    }

    public SociedadRpl getSociedadOpinada() {
        return sociedadOpinada;
    }

    public void setSociedadOpinada(SociedadRpl sociedadOpinada) {
        this.sociedadOpinada = sociedadOpinada;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.opinion);
        hash = 43 * hash + Objects.hashCode(this.relCalidadPrecio);
        hash = 43 * hash + Objects.hashCode(this.valoracion);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comentarios other = (Comentarios) obj;
        if (!Objects.equals(this.opinion, other.opinion)) {
            return false;
        }
        if (!Objects.equals(this.relCalidadPrecio, other.relCalidadPrecio)) {
            return false;
        }
        if (!Objects.equals(this.valoracion, other.valoracion)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Opiniones{" + "idOpinion=" + idComentario + ", version=" + version + ", opinion=" + opinion + ", aprobacion=" + aprobacion + ", relCalidadPrecio=" + relCalidadPrecio + ", valoracion=" + valoracion + ", sociedadOpinanteIdk=" + 

                ", SociedadOpinante=" + sociedadOpinante + ", articuloOpinado=" + articuloOpinado + ", sociedadOpinada=" + sociedadOpinada + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


}
