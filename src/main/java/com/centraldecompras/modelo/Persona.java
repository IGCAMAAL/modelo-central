package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_RPL_personas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = ""
            + "SELECT d "
            + "FROM Persona d"),
    
    @NamedQuery(name = "Persona.findPersonasByIdWeb", query = ""
            + "SELECT d "
            + "FROM Persona d "
            + "WHERE d.userName = :userName"),
    
    @NamedQuery(name = "Persona.findPersonaByIdWeb", query = ""
            + "SELECT d "
            + "FROM Persona d "
            + "WHERE d.userName = :userName")        
})
public class Persona implements  Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Persona.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idPersona;

    @Version
    private int version;
    
    // Atributos Autenticacion* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Column(length = 64, nullable = false, unique = false)
    private String userName;

    // Atributos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    @Column(name = "nombre", length = 25)
    private String nombre;


    @Column(name = "apellido1", length = 25)
    private String apellido1;


    @Column(name = "apellido2", length = 25)
    private String apellido2;


    @Column(name = "cifpasport", length = 15)
    private String cifpasport;


    @Column(name = "fechaNacimiento")
    private int fechaNacimiento;
    
    @Column(length = 128, nullable = true, unique = false)
    private String email;
     
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @OneToMany(targetEntity=Contacto.class,mappedBy="persona")
    private Collection<Contacto> contacto;
    
    @OneToMany(targetEntity=Direccion.class,mappedBy="persona")
    private Collection<Direccion> direccion;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Persona() {
    }

    public Persona(String idPersona, String userName, String nombre, String apellido1, String apellido2, String cifpasport, int fechaNacimiento, String email, DatosAuditoria atributosAuditoria) {
        this.idPersona = idPersona;
        this.userName = userName;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.cifpasport = cifpasport;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public String getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(String idPersona) {
        this.idPersona = idPersona;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos autenticacion * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getCifpasport() {
        return cifpasport;
    }

    public void setCifpasport(String cifpasport) {
        this.cifpasport = cifpasport;
    }

    public int getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(int fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    public Collection<Contacto> getContacto() {
        return contacto;
    }

    public void setContacto(Collection<Contacto> contacto) {
        this.contacto = contacto;
    }

    public Collection<Direccion> getDireccion() {
        return direccion;
    }

    public void setDireccion(Collection<Direccion> direccion) {
        this.direccion = direccion;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.apellido1);
        hash = 53 * hash + Objects.hashCode(this.cifpasport);
        hash = 53 * hash + this.fechaNacimiento;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.apellido1, other.apellido1)) {
            return false;
        }
        if (!Objects.equals(this.cifpasport, other.cifpasport)) {
            return false;
        }
        if (this.fechaNacimiento != other.fechaNacimiento) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Persona{" + "idPersona=" + idPersona + ", version=" + version + ", userName=" + userName + ", nombre=" + nombre + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", cifpasport=" + cifpasport + ", fechaNacimiento=" + fechaNacimiento + ", email=" + email + ", contacto=" + contacto + ", direccion=" + direccion + ", atributosAuditoria=" + atributosAuditoria + '}';
    }



}
