package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_CMO_ImgDoc",
        indexes = {
            @Index(name = "index_ImgDoc_uuidReferen", columnList = "uuidReferen", unique = false)})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImgDoc.findImgDocBy_SU", query = ""
            + " SELECT u.tipo ,u.idImgDoc, u.extension, u.descripcion, u.nomOriginal, u.uuidReferen "
            + "   FROM ImgDoc as u "
            + "  WHERE u.uuidReferen IN (:entitiesId)"),

    @NamedQuery(name = "ImgDoc.findImgPic_idI", query = ""
            + " SELECT u.picture  "
            + "   FROM ImgDoc as u "
            + "  WHERE u.idImgDoc = :idImgDoc")
})

public class ImgDoc implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ImgDoc.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idImgDoc;

    @Version
    private int version;

    // Atributos de basicos / relacion * * * * * * * * * * * * * * * * * * * * *
    private String uuidReferen;    // uuid relacionado, pero no se establece relacion referencial

    /*  LG=Logo, FU=Fotografia usuario, FP=Fotografia Producto, DC=Documento */
    private String tipo;

    private String nomOriginal; /* Nombre original del fichero*/

    private String descripcion; /* Informacion adicional escrita por usuario */

    private String extension;   /* Extension del fichero */

    @Lob
    @Column(length = 1048576)
    private byte[] picture;

    private String replyString;
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public ImgDoc() {
    }

    public ImgDoc(String idImgDoc, String uuidReferen, String tipo, String nomOriginal, String descripcion, String extension, byte[] picture) {
        this.idImgDoc = idImgDoc;
        this.uuidReferen = uuidReferen;
        this.tipo = tipo;
        this.nomOriginal = nomOriginal;
        this.descripcion = descripcion;
        this.extension = extension;
        this.picture = picture;
    }

    public ImgDoc(ImgFoto imgFoto) {
        this.atributosAuditoria = imgFoto.getAtributosAuditoria();
        this.idImgDoc = imgFoto.getIdImgDoc();
        this.uuidReferen = imgFoto.getUuidReferen();
        this.tipo = imgFoto.getTipo();
        this.nomOriginal = imgFoto.getNomOriginal();
        this.descripcion = imgFoto.getDescripcion();
        this.extension = imgFoto.getExtension();
        this.picture = imgFoto.getPicture();
    }
    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public String getIdImgDoc() {
        return idImgDoc;
    }

    public void setIdImgDoc(String idImgDoc) {
        this.idImgDoc = idImgDoc;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos  * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    public String getUuidReferen() {
        return uuidReferen;
    }

    public void setUuidReferen(String uuidReferen) {
        this.uuidReferen = uuidReferen;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getNomOriginal() {
        return nomOriginal;
    }

    public void setNomOriginal(String nomOriginal) {
        this.nomOriginal = nomOriginal;
    }

    public String getReplyString() {
        return replyString;
    }

    public void setReplyString(String replyString) {
        this.replyString = replyString;
    }

    // Métodos Relacion  * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    // Métodos Auditoria  * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.uuidReferen);
        hash = 37 * hash + Objects.hashCode(this.nomOriginal);
        hash = 37 * hash + Objects.hashCode(this.extension);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ImgDoc other = (ImgDoc) obj;
        if (!Objects.equals(this.uuidReferen, other.uuidReferen)) {
            return false;
        }
        if (!Objects.equals(this.nomOriginal, other.nomOriginal)) {
            return false;
        }
        if (!Objects.equals(this.extension, other.extension)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ImgDoc{" + "idImgDoc=" + idImgDoc + ", version=" + version + ", uuidReferen=" + uuidReferen + ", tipo=" + tipo + ", nomOriginal=" + nomOriginal + ", descripcion=" + descripcion + ", extension=" + extension + ", picture=" + picture + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
