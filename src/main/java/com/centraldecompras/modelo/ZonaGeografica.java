package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_LGT_ZonaGeografica", 
       indexes = {@Index(columnList = "route"), @Index(columnList = "nivelInt")
})

@XmlRootElement
@NamedQueries({                          
   @NamedQuery(name = "ZonaGeografica.findZonasGeograficasBy_ParentId", query = "SELECT d FROM ZonaGeografica d WHERE d.padreJerarquia = :padreJerarquia"),
    
   @NamedQuery(name = "ZonaGeografica.findZonaGeograficaEmpiezaTypeAheadTodo", query = ""
           + " SELECT d "
           + "   FROM ZonaGeografica d "
           + "  WHERE d.descripcion LIKE :descripcion" ),
   
   @NamedQuery(name = "ZonaGeografica.findZonaGeograficaEmpiezaTypeAheadSelEstado", query = ""
           + " SELECT d "
           + "   FROM ZonaGeografica d "
           + "  WHERE d.descripcion LIKE :descripcion "
           + "    AND d.atributosAuditoria.estadoRegistro = :estadoRegistro" ),
   
   @NamedQuery(name = "ZonaGeografica.findZonaGeograficaContieneTypeAheadTodo", query = ""
           + " SELECT d "
           + "   FROM ZonaGeografica d "
           + "  WHERE d.descripcion LIKE :descripcion "
           + "    AND d.descripcion NOT LIKE :descipcionExclude" ),
   
   @NamedQuery(name = "ZonaGeografica.findZonaGeograficaContieneTypeAheadSelEstado", query = ""
           + " SELECT d "
           + "   FROM ZonaGeografica d "
           + "  WHERE d.descripcion LIKE :descripcion "
           + "    AND d.atributosAuditoria.estadoRegistro = :estadoRegistro "
           + "    AND d.descripcion NOT LIKE :descipcionExclude" ),
        
   @NamedQuery(name = "ZonaGeografica.findZonasGeograficasOrderBy_N", query = ""
           + "   SELECT d "
           + "     FROM ZonaGeografica d "
           + " ORDER BY d.nivelInt, d.idPadreJerarquia " ),
   
       @NamedQuery(name = "ZonaGeografica.findZonasGeograficasBy_idZ", query = ""
            + "  SELECT z "
            + "    FROM ZonaGeografica z "
            + "   WHERE z.route          LIKE '%.' || :zonaGeograficaId || '.%' ")   
})
public class ZonaGeografica implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ZonaGeografica.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idZonaGeografica;

    @Version
    private int version;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private int nivelInt;
    
    @NotNull
    @Column(name = "etiqueta", nullable = false, updatable = true)
    private String etiqueta;
    
    @NotNull
    @Column(name = "descripcion", nullable = false, updatable = true)
    private String descripcion;
    
    @NotNull
    @Column(name = "idPadreJerarquia", nullable = false, updatable = true)
    private String idPadreJerarquia;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity=ZonaGeografica.class)
    private ZonaGeografica padreJerarquia;
    
    @Basic
    @Column(length = 361)    // 1 + (longId+1) x 10; Para longId=35;  1 + (36x10) = 361  
    private String route;

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public ZonaGeografica() {
    }

    public ZonaGeografica(String idZonaGeografica, int nivelInt, String etiqueta, String descripcion, String idPadreJerarquia, ZonaGeografica padreJerarquia, DatosAuditoria atributosAuditoria) {
        this.idZonaGeografica = idZonaGeografica;
        this.nivelInt = nivelInt;
        this.etiqueta = etiqueta;
        this.descripcion = descripcion;
        this.idPadreJerarquia = idPadreJerarquia;
        this.padreJerarquia = padreJerarquia;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getIdZonaGeografica() {
        return this.idZonaGeografica;
    }

    public void setIdZonaGeografica(String idZonaGeografica) {
        this.idZonaGeografica = idZonaGeografica;
    }
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    public int getNivelInt() {
        return nivelInt;
    }

    public void setNivelInt(int nivelInt) {
        this.nivelInt = nivelInt;
    }
    
    public String getEtiqueta() {    
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdPadreJerarquia() {
        return idPadreJerarquia;
    }

    public void setIdPadreJerarquia(String idPadreJerarquia) {
        this.idPadreJerarquia = idPadreJerarquia;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ZonaGeografica getPadreJerarquia() {    
        return padreJerarquia;
    }

    public void setPadreJerarquia(ZonaGeografica padreJerarquia) {
        this.padreJerarquia = padreJerarquia;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
 
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ZonaGeografica other = (ZonaGeografica) obj;
        if ((this.etiqueta == null) ? (other.etiqueta != null) : !this.etiqueta.equals(other.etiqueta)) {
            return false;
        }
        if ((this.descripcion == null) ? (other.descripcion != null) : !this.descripcion.equals(other.descripcion)) {
            return false;
        }
        return true;
    }
/*
    @Override
    public String toString() {
        return "ZonaGeografica{" + "idZonaGeografica=" + idZonaGeografica + ", version=" + version + ", etiqueta=" + etiqueta + ", descripcion=" + descripcion + ", padreJerarquia=" + padreJerarquia.toString() + ", atributosAuditoria=" + atributosAuditoria.toString() + '}';
    }
*/
    
}
