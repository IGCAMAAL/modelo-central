package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class UnidadFormatoVtaLangPK implements Serializable {

    @NotNull
    private String unidadFormatoVtaId;

    @NotNull
    private String idioma;

    public UnidadFormatoVtaLangPK() {
    }

    public UnidadFormatoVtaLangPK(String unidadFormatoVtaId, String idioma) {
        this.unidadFormatoVtaId = unidadFormatoVtaId;
        this.idioma = idioma;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.unidadFormatoVtaId);
        hash = 29 * hash + Objects.hashCode(this.idioma);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadFormatoVtaLangPK other = (UnidadFormatoVtaLangPK) obj;
        if (!Objects.equals(this.unidadFormatoVtaId, other.unidadFormatoVtaId)) {
            return false;
        }
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnidadFormatoVtaLangPK{" + "unidadFormatoVtaId=" + unidadFormatoVtaId + ", idioma=" + idioma + '}';
    }

}
