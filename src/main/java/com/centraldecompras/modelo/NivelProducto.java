package com.centraldecompras.modelo;

import com.centraldecompras.modelo.DatosAuditoria;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_NivelProducto")
@XmlRootElement
public class NivelProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(NivelProducto.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private int idNivelProducto;

    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String descripcionNivelProducto;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity=SectorRpl.class)
    private SectorRpl sector;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public NivelProducto() {

    }

    public NivelProducto(int idNivelProducto, String descripcionNivelProducto, SectorRpl sector, DatosAuditoria atributosAuditoria) {
        this.idNivelProducto = idNivelProducto;
        this.descripcionNivelProducto = descripcionNivelProducto;
        this.sector = sector;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public int getIdNivelProducto() {
        return idNivelProducto;
    }

    public void setIdNivelProducto(int idNivelProducto) {
        this.idNivelProducto = idNivelProducto;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getDescripcionNivelProducto() {
        return descripcionNivelProducto;
    }

    public void setDescripcionNivelProducto(String descripcionNivelProducto) {
        this.descripcionNivelProducto = descripcionNivelProducto;
    }

    
    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public SectorRpl getSector() {
        return sector;
    }

    public void setSector(SectorRpl sector) {
        this.sector = sector;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.descripcionNivelProducto);
        hash = 43 * hash + Objects.hashCode(this.sector);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NivelProducto other = (NivelProducto) obj;
        if (!Objects.equals(this.descripcionNivelProducto, other.descripcionNivelProducto)) {
            return false;
        }
        if (!Objects.equals(this.sector, other.sector)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "NivelProducto{" + "idNivelProducto=" + idNivelProducto + ", version=" + version + ", descripcionNivelProducto=" + descripcionNivelProducto + ", sector=" + sector + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
