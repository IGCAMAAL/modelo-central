package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_LGT_SocZonGeoAvatar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SocZonGeoAvatar.findSocZonGeoAvatarBy_idS_idZ", query = " "
            + " SELECT aup "
            + "   FROM SocZonGeoAvatar aup "
            + "  WHERE aup.sociedadid  = :sociedadid "
            + "    AND aup.zonaGeograficaid  = :zonaGeograficaid "
    ),

    @NamedQuery(name = "SocZonGeoAvatar.deleteSocZonGeoAvatarBy_idS_idZ", query = " "
            + " DELETE "
            + "   FROM SocZonGeoAvatar aup "
            + "  WHERE aup.sociedadid = :sociedadid "
            + "    AND aup.zonaGeograficaid = :zonaGeograficaid "
    ),

})
public class SocZonGeoAvatar implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SocZonGeoAvatar.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic(optional = false)
    @Column(length = 36)
    private String sociedadid;

    @Id
    @Basic(optional = false)
    @Column(length = 36)
    private String zonaGeograficaid;

    @Version
    private int version;

    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * 
    private Boolean asignada;

    @Basic
    @Column(length = 36)
    private int nivelInt;
    
    @Basic
    @Column(name = "idPadreJerarquia", nullable = false, updatable = true, length = 36)
    private String idPadreJerarquia;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SocZonGeoAvatar() {
    }

    public SocZonGeoAvatar(String sociedadid, String zonaGeograficaid, Boolean asignada, int nivelInt, String idPadreJerarquia) {
        this.sociedadid = sociedadid;
        this.zonaGeograficaid = zonaGeograficaid;
        this.asignada = asignada;
        this.nivelInt = nivelInt;
        this.idPadreJerarquia = idPadreJerarquia;
    }

    public String getSociedadid() {
        return sociedadid;
    }

    public void setSociedadid(String sociedadid) {
        this.sociedadid = sociedadid;
    }

    public String getZonaGeograficaid() {
        return zonaGeograficaid;
    }

    public void setZonaGeograficaid(String zonaGeograficaid) {
        this.zonaGeograficaid = zonaGeograficaid;
    }
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public int getNivelInt() {
        return nivelInt;
    }

    public void setNivelInt(int nivelInt) {
        this.nivelInt = nivelInt;
    }

    public Boolean getAsignada() {
        return asignada;
    }

    public void setAsignada(Boolean asignada) {
        this.asignada = asignada;
    }

    public String getIdPadreJerarquia() {
        return idPadreJerarquia;
    }

    public void setIdPadreJerarquia(String idPadreJerarquia) {
        this.idPadreJerarquia = idPadreJerarquia;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.sociedadid);
        hash = 53 * hash + Objects.hashCode(this.zonaGeograficaid);
        hash = 53 * hash + Objects.hashCode(this.asignada);
        hash = 53 * hash + this.nivelInt;
        hash = 53 * hash + Objects.hashCode(this.idPadreJerarquia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SocZonGeoAvatar other = (SocZonGeoAvatar) obj;
        if (!Objects.equals(this.sociedadid, other.sociedadid)) {
            return false;
        }
        if (!Objects.equals(this.zonaGeograficaid, other.zonaGeograficaid)) {
            return false;
        }
        if (!Objects.equals(this.asignada, other.asignada)) {
            return false;
        }
        if (this.nivelInt != other.nivelInt) {
            return false;
        }
        if (!Objects.equals(this.idPadreJerarquia, other.idPadreJerarquia)) {
            return false;
        }
        return true;
    }



}
