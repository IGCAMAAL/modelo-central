package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_LGT_ZonasGeograficasAutorizadas")
@XmlRootElement
public class ZonasGeograficasAutorizadas implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ZonasGeograficasAutorizadas.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idZonasGeograficasDeAccion;

    @Version
    private int version;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = Persona.class)
    private Persona persona;

    @ManyToOne(targetEntity = ZonaGeografica.class)
    private ZonaGeografica zonaGeografica;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public ZonasGeograficasAutorizadas() {
    }

    public ZonasGeograficasAutorizadas(String idZonasGeograficasDeAccion, Persona persona, ZonaGeografica zonaGeografica, DatosAuditoria atributosAuditoria) {
        this.idZonasGeograficasDeAccion = idZonasGeograficasDeAccion;
        this.persona = persona;
        this.zonaGeografica = zonaGeografica;
        this.atributosAuditoria = atributosAuditoria;
    }

    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public String getIdZonasGeograficasDeAccion() {
        return this.idZonasGeograficasDeAccion;
    }

    public void setIdZonasGeograficasDeAccion(String idZonasGeograficasDeAccion) {
        this.idZonasGeograficasDeAccion = idZonasGeograficasDeAccion;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public ZonaGeografica getZonaGeografica() {
        return this.zonaGeografica;
    }

    public void setZonaGeografica(ZonaGeografica zonaGeografica) {
        this.zonaGeografica = zonaGeografica;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
}
