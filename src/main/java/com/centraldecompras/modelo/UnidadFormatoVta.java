package com.centraldecompras.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_UnidadFormatoVta")
@XmlRootElement
public class UnidadFormatoVta implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(UnidadFormatoVta.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idUnidadFormatoVta;
    
    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private BigDecimal tipoCapacEnvase;

    @NotNull
    private String descCapacEnvase;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UnidadFormatoVta() {
    }

    public UnidadFormatoVta(String idUnidadFormatoVta, int version, BigDecimal tipoCapacEnvase, String descCapacEnvase, DatosAuditoria atributosAuditoria) {
        this.idUnidadFormatoVta = idUnidadFormatoVta;
        this.version = version;
        this.tipoCapacEnvase = tipoCapacEnvase;
        this.descCapacEnvase = descCapacEnvase;
        this.atributosAuditoria = atributosAuditoria;
    }
        
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public String getIdUnidadFormatoVta() {
        return this.idUnidadFormatoVta;
    }

    public void setIdUnidadFormatoVta(String idUnidadFormatoVta) {
        this.idUnidadFormatoVta = idUnidadFormatoVta;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public String getDescCapacEnvase() {
        return this.descCapacEnvase;
    }

    public void setDescCapacEnvase(String descCapacEnvase) {
        this.descCapacEnvase = descCapacEnvase;
    }

    public BigDecimal getTipoCapacEnvase() {
        return this.tipoCapacEnvase;
    }

    public void setTipoCapacEnvase(BigDecimal tipoCapacEnvase) {
        this.tipoCapacEnvase = tipoCapacEnvase;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + (this.idUnidadFormatoVta != null ? this.idUnidadFormatoVta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadFormatoVta other = (UnidadFormatoVta) obj;
        if ((this.tipoCapacEnvase == null) ? (other.tipoCapacEnvase != null) : !this.tipoCapacEnvase.equals(other.tipoCapacEnvase)) {
            return false;
        }
        if ((this.descCapacEnvase == null) ? (other.descCapacEnvase != null) : !this.descCapacEnvase.equals(other.descCapacEnvase)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnidadFormatoVta{" + "idUnidadFormatoVta=" + idUnidadFormatoVta + ", version=" + version + ", tipoCapacEnvase=" + tipoCapacEnvase + ", descCapacEnvase=" + descCapacEnvase + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
