package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_PRM_UnidadFormatoVtaLang",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"idioma", "traduccionDesc"})
        })
@XmlRootElement
//@IdClass(UnidadFormatoVtaLangPK.class)
@NamedQueries({
    @NamedQuery(name = "UnidadFormatoVtaLang.findUnidadFormatoVtaLangById_UFV", query = ""
            + " SELECT d "
            + "   FROM UnidadFormatoVtaLang d "
            + "  WHERE d.unidadFormatoVtaEntity = :unidadFormatoVta" ),
    
    @NamedQuery(name = "UnidadFormatoVtaLang.findUnidadFormatoVtaLangBy_UFV_Lang", query = ""
            + " SELECT d "
            + "   FROM UnidadFormatoVtaLang d "
            + "  WHERE d.unidadFormatoVtaEntity = :unidadFormatoVta "
            + "    AND d.idioma = :idioma" ),
    
    @NamedQuery(name = "UnidadFormatoVtaLang.findUnidadFormatoVtaLangBy_IdUFV_Lang", query = ""
            + " SELECT d "
            + "   FROM UnidadFormatoVtaLang d "
            + "  WHERE d.unidadFormatoVtaId = :unidadFormatoVtaId "
            + "    AND d.idioma = :idioma" ),
})
public class UnidadFormatoVtaLang implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(UnidadFormatoVtaLang.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String unidadFormatoVtaId;
    
    @Id
    @Basic
    private String idioma;
    
    @Version
    private int version;
    
    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String traduccionDesc;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    @ManyToOne(targetEntity=UnidadFormatoVta.class)
    private UnidadFormatoVta unidadFormatoVtaEntity;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;
    
    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UnidadFormatoVtaLang() {
    }

    public UnidadFormatoVtaLang(String unidadFormatoVtaId, String idioma, int version, String traduccionDesc, UnidadFormatoVta unidadFormatoVtaEntity, DatosAuditoria atributosAuditoria) {
        this.unidadFormatoVtaId = unidadFormatoVtaId;
        this.idioma = idioma;
        this.version = version;
        this.traduccionDesc = traduccionDesc;
        this.unidadFormatoVtaEntity = unidadFormatoVtaEntity;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public String getUnidadFormatoVtaId() {
        return unidadFormatoVtaId;
    }

    public void setUnidadFormatoVtaId(String unidadFormatoVtaId) {
        this.unidadFormatoVtaId = unidadFormatoVtaId;
    }
    
    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getTraduccionDesc() {
        return traduccionDesc;
    }

    public void setTraduccionDesc(String traduccionDesc) {
        this.traduccionDesc = traduccionDesc;
    }
    
    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public UnidadFormatoVta getUnidadFormatoVtaEntity() {
        return unidadFormatoVtaEntity;
    }

    public void setUnidadFormatoVtaEntity(UnidadFormatoVta unidadFormatoVtaEntity) {
        this.unidadFormatoVtaEntity = unidadFormatoVtaEntity;
    }

    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idioma);
        hash = 97 * hash + Objects.hashCode(this.traduccionDesc);
        hash = 97 * hash + Objects.hashCode(this.unidadFormatoVtaEntity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadFormatoVtaLang other = (UnidadFormatoVtaLang) obj;
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        if (!Objects.equals(this.traduccionDesc, other.traduccionDesc)) {
            return false;
        }
        if (!Objects.equals(this.unidadFormatoVtaEntity, other.unidadFormatoVtaEntity)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnidadFormatoVtaLang{" + "unidadFormatoVtaId=" + unidadFormatoVtaId + ", idioma=" + idioma + ", version=" + version + ", traduccionDesc=" + traduccionDesc + ", unidadFormatoVtaEntity=" + unidadFormatoVtaEntity + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

    
}
