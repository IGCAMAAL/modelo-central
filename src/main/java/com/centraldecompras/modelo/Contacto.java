package com.centraldecompras.modelo;

import java.io.Serializable;

import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_CMO_contacto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contacto.findContactosBy_S", query = " "
        + " SELECT d "
        + "   FROM Contacto d "
        + "  WHERE d.sociedad = :sociedad "     
    ),
    @NamedQuery(name = "Contacto.findContactosBy_P", query = " "
        + " SELECT d "
        + "   FROM Contacto d "
        + "  WHERE d.persona = :persona "     
    ),
    @NamedQuery(name = "Contacto.findContactosBy_A", query = " "
        + " SELECT d "
        + "   FROM Contacto d "
        + "  WHERE d.almacen = :almacen "     
    )
})
public class Contacto implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Contacto.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idContacto;

    @Version
    private int version;

    // Atributos de basicos / relacion * * * * * * * * * * * * * * * * * * * * *
    private String tipo;
    
    private String valor;
    
    private String descripcion;
                      
    private String nombre;
    
    private String cargo;
    
    private Boolean visibleProve;
    
    private Boolean visibleAsoc;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = Persona.class, fetch = FetchType.EAGER)
    private Persona persona;

    @ManyToOne(targetEntity = SociedadRpl.class, fetch = FetchType.EAGER)
    private SociedadRpl sociedad;

    @ManyToOne(targetEntity=Almacen.class, fetch = FetchType.EAGER)
    private Almacen almacen;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Contacto() {
    }

    public Contacto(String idContacto, String tipo, String valor, String descripcion, String nombre, String cargo, Boolean visibleProve, Boolean visibleAsoc, Persona persona, SociedadRpl sociedad, Almacen almacen, DatosAuditoria atributosAuditoria) {
        this.idContacto = idContacto;
        this.tipo = tipo;
        this.valor = valor;
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.cargo = cargo;
        this.visibleProve = visibleProve;
        this.visibleAsoc = visibleAsoc;
        this.persona = persona;
        this.sociedad = sociedad;
        this.almacen = almacen;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public String getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(String idContacto) {
        this.idContacto = idContacto;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Atributos de basicos / relacion * * * * * * * * * * * * * * * * * * * * *
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getVisibleProve() {
        return visibleProve;
    }

    public void setVisibleProve(Boolean visibleProve) {
        this.visibleProve = visibleProve;
    }

    public Boolean getVisibleAsoc() {
        return visibleAsoc;
    }

    public void setVisibleAsoc(Boolean visibleAsoc) {
        this.visibleAsoc = visibleAsoc;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *

    public SociedadRpl getSociedad() {
        return this.sociedad;
    }

    public void setSociedad(SociedadRpl sociedad) {
        this.sociedad = sociedad;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }


    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.tipo);
        hash = 97 * hash + Objects.hashCode(this.valor);
        hash = 97 * hash + Objects.hashCode(this.descripcion);
        hash = 97 * hash + Objects.hashCode(this.nombre);
        hash = 97 * hash + Objects.hashCode(this.cargo);
        hash = 97 * hash + Objects.hashCode(this.visibleProve);
        hash = 97 * hash + Objects.hashCode(this.visibleAsoc);
        hash = 97 * hash + Objects.hashCode(this.persona);
        hash = 97 * hash + Objects.hashCode(this.sociedad);
        hash = 97 * hash + Objects.hashCode(this.almacen);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contacto other = (Contacto) obj;
        if (!Objects.equals(this.tipo, other.tipo)) {
            return false;
        }
        if (!Objects.equals(this.valor, other.valor)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.cargo, other.cargo)) {
            return false;
        }
        if (!Objects.equals(this.visibleProve, other.visibleProve)) {
            return false;
        }
        if (!Objects.equals(this.visibleAsoc, other.visibleAsoc)) {
            return false;
        }
        if (!Objects.equals(this.persona, other.persona)) {
            return false;
        }
        if (!Objects.equals(this.sociedad, other.sociedad)) {
            return false;
        }
        if (!Objects.equals(this.almacen, other.almacen)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Contacto{" + "idContacto=" + idContacto + ", version=" + version + ", tipo=" + tipo + ", valor=" + valor + ", descripcion=" + descripcion + ", nombre=" + nombre + ", cargo=" + cargo + ", visibleProve=" + visibleProve + ", visibleAsoc=" + visibleAsoc + ", persona=" + persona + ", sociedad=" + sociedad + ", almacen=" + almacen + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

    

}
