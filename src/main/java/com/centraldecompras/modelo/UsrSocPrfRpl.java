package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_RPL_UsrSocPrfRpl")
@XmlRootElement

public class UsrSocPrfRpl implements Serializable {

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idUsrSocPrf;

    @Version
    private int version;

    // Atributos de Sesion,  proceso o estado registro * * * * * * * * * * * * *
    @NotNull
    @Column(name = "estadoSesion", nullable = false, updatable = true)
    private Boolean estadoSesion;


    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public UsrSocPrfRpl() {
    }

    public UsrSocPrfRpl(String idUsrSocPrf, Boolean estadoSesion) {
        this.idUsrSocPrf = idUsrSocPrf;
        this.estadoSesion = estadoSesion;
    }
      
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    
    public String getIdUsrSocPrf() {
        return idUsrSocPrf;
    }

    public void setIdUsrSocPrf(String idUsrSocPrf) {
        this.idUsrSocPrf = idUsrSocPrf;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Metodos de Sesion o proceso * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Boolean isEstadoSesion() {
        return estadoSesion;
    }

    public void setEstadoSesion(Boolean estadoSesion) {
        this.estadoSesion = estadoSesion;
    }


    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.idUsrSocPrf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsrSocPrfRpl other = (UsrSocPrfRpl) obj;
        if (!Objects.equals(this.idUsrSocPrf, other.idUsrSocPrf)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UsrSocPrfRpl{" + "idUsrSocPrf=" + idUsrSocPrf + ", version=" + version + ", estadoSesion=" + estadoSesion + '}';
    }

}
