package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_RPL_SectoresAutorizadosRpl")
@XmlRootElement

public class SectoresAutorizadosRpl implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SectoresAutorizadosRpl.class.getName());

    
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSectoresAutorizadosRpl;
     
    @Version
    private int version;


    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public SectoresAutorizadosRpl() {
    }

    public SectoresAutorizadosRpl(String uuid) {
        this.idSectoresAutorizadosRpl = uuid;
    }

    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
    public String getIdPerfil() {
        return idSectoresAutorizadosRpl;
    }

    public void setIdPerfil(String idPerfil) {
        this.idSectoresAutorizadosRpl = idPerfil;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    


    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *       

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.idSectoresAutorizadosRpl);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SectoresAutorizadosRpl other = (SectoresAutorizadosRpl) obj;
        if (!Objects.equals(this.idSectoresAutorizadosRpl, other.idSectoresAutorizadosRpl)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PerfilRpl{" + "idPerfil=" + idSectoresAutorizadosRpl + ", version=" + version + '}';
    }

}
