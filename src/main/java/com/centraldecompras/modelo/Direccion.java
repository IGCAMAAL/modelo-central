package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_CMO_direccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Direccion.findDireccionesBy_S", query = " "
        +  " SELECT d "
        + "   FROM Direccion d "
        + "  WHERE d.sociedad = :sociedad "     
    ),
    @NamedQuery(name = "Direccion.findDireccionesBy_P", query = " "
        +  " SELECT d "
        + "   FROM Direccion d "
        + "  WHERE d.persona = :persona "     
    ),
    @NamedQuery(name = "Direccion.findDireccionesBy_A", query = " "
        + " SELECT d "
        + "   FROM Direccion d "
        + "  WHERE d.almacen = :almacen "     
    )
})
public  class Direccion implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Direccion.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idDireccion;

    @Version
    private int version;
    
  // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * *
    @NotNull
    private String tipoDireccion;

    @NotNull
    private String direccionGoogleFormateada;
    
    @NotNull
    private double latitud;

    @NotNull
    private double longitud;
    
    private Boolean visibleProve;
    
    private Boolean visibleAsoc;
    
    private String descripcion;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity=Persona.class)
    private Persona persona;

    @ManyToOne(targetEntity=SociedadRpl.class)
    private SociedadRpl sociedad;

    @ManyToOne(targetEntity=Almacen.class)
    private Almacen almacen;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;
    
    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Direccion(){

    }

    public Direccion(String idDireccion, String tipoDireccion, String direccionGoogleFormateada, double latitud, double longitud, Boolean visibleProve, Boolean visibleAsoc, String descripcion, Persona persona, SociedadRpl sociedad, Almacen almacen, DatosAuditoria atributosAuditoria) {
        this.idDireccion = idDireccion;
        this.tipoDireccion = tipoDireccion;
        this.direccionGoogleFormateada = direccionGoogleFormateada;
        this.latitud = latitud;
        this.longitud = longitud;
        this.visibleProve = visibleProve;
        this.visibleAsoc = visibleAsoc;
        this.descripcion = descripcion;
        this.persona = persona;
        this.sociedad = sociedad;
        this.almacen = almacen;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public String getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(String idDireccion) {
        this.idDireccion = idDireccion;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getTipoDireccion() {
        return tipoDireccion;
    }

    public void setTipoDireccion(String tipoDireccion) {
        this.tipoDireccion = tipoDireccion;
    }

    public String getDireccionGoogleFormateada() {
        return direccionGoogleFormateada;
    }

    public void setDireccionGoogleFormateada(String direccionGoogleFormateada) {
        this.direccionGoogleFormateada = direccionGoogleFormateada;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public Boolean getVisibleProve() {
        return visibleProve;
    }

    public void setVisibleProve(Boolean visibleProve) {
        this.visibleProve = visibleProve;
    }

    public Boolean getVisibleAsoc() {
        return visibleAsoc;
    }

    public void setVisibleAsoc(Boolean visibleAsoc) {
        this.visibleAsoc = visibleAsoc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    // Métodos Basicos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Persona getPersona() {    
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public SociedadRpl getSociedad() {
        return sociedad;
    }

    public void setSociedad(SociedadRpl sociedad) {
        this.sociedad = sociedad;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.tipoDireccion);
        hash = 43 * hash + Objects.hashCode(this.direccionGoogleFormateada);
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.latitud) ^ (Double.doubleToLongBits(this.latitud) >>> 32));
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.longitud) ^ (Double.doubleToLongBits(this.longitud) >>> 32));
        hash = 43 * hash + Objects.hashCode(this.visibleProve);
        hash = 43 * hash + Objects.hashCode(this.visibleAsoc);
        hash = 43 * hash + Objects.hashCode(this.descripcion);
        hash = 43 * hash + Objects.hashCode(this.persona);
        hash = 43 * hash + Objects.hashCode(this.sociedad);
        hash = 43 * hash + Objects.hashCode(this.almacen);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Direccion other = (Direccion) obj;
        if (!Objects.equals(this.tipoDireccion, other.tipoDireccion)) {
            return false;
        }
        if (!Objects.equals(this.direccionGoogleFormateada, other.direccionGoogleFormateada)) {
            return false;
        }
        if (Double.doubleToLongBits(this.latitud) != Double.doubleToLongBits(other.latitud)) {
            return false;
        }
        if (Double.doubleToLongBits(this.longitud) != Double.doubleToLongBits(other.longitud)) {
            return false;
        }
        if (!Objects.equals(this.visibleProve, other.visibleProve)) {
            return false;
        }
        if (!Objects.equals(this.visibleAsoc, other.visibleAsoc)) {
            return false;
        }
        if (!Objects.equals(this.descripcion, other.descripcion)) {
            return false;
        }
        if (!Objects.equals(this.persona, other.persona)) {
            return false;
        }
        if (!Objects.equals(this.sociedad, other.sociedad)) {
            return false;
        }
        if (!Objects.equals(this.almacen, other.almacen)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Direccion{" + "idDireccion=" + idDireccion + ", version=" + version + ", tipoDireccion=" + tipoDireccion + ", direccionGoogleFormateada=" + direccionGoogleFormateada + ", latitud=" + latitud + ", longitud=" + longitud + ", visibleProve=" + visibleProve + ", visibleAsoc=" + visibleAsoc + ", descripcion=" + descripcion + ", persona=" + persona + ", sociedad=" + sociedad + ", almacen=" + almacen + ", atributosAuditoria=" + atributosAuditoria + '}';
    }


}

