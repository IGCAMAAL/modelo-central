package com.centraldecompras.modelo.pur.service.interfaces;

import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ciberado
 */
public interface CompraCestaService {

    void create_C(CompraCesta compraCesta) throws PreexistingEntityException, Exception;

    void create(CompraCesta compraCesta) throws PreexistingEntityException, Exception;

    void edit(CompraCesta compraCesta) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    List<CompraCesta> findCompraCestaEntities();

    List<CompraCesta> findCompraCestaEntities(int maxResults, int firstResult);

    List<CompraCesta> findCompraCestaEntities(boolean all, int maxResults, int firstResult);

    CompraCesta findCompraCesta(String id);

    CompraCesta findCompraCesta(String idCesta, String cestaEstado);

    int getCestaCount();

    Map<String, Object> findCompraCesta_PagWithFilter_dSUE(Map<String, Object> parametroFiltro);

    Map<String, Object> findArticulosForSelect_Pag_WithFilter(Map<String, Object> parametrosFiltro);

    List<Object[]> findProductoTypeAhead(int tipo, Map<String, Object> parametrosFiltro);

    List<Object[]> findArticuloTypeAhead(int tipo, Map<String, Object> parametrosFiltro);

    List<String> findProductWithLabels(Map<String, Object> parametrosFiltro);

}
