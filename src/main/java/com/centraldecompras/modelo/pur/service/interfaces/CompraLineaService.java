/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.pur.service.interfaces;

import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.CompraLinea;
import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ciberado
 */
public interface CompraLineaService {
    /*
     void create_C(CompraLinea compraLinea) throws PreexistingEntityException, Exception ;
     */

    void create(CompraLinea compraLinea) throws PreexistingEntityException, Exception;

    void edit(CompraLinea compraLinea) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    List<CompraLinea> findCompraLineaEntities();

    List<CompraLinea> findCompraLineaEntities(int maxResults, int firstResult);

    List<CompraLinea> findCompraLineaEntities(boolean all, int maxResults, int firstResult);

    CompraLinea findCompraLinea(String id);

    int getPedidoLinCount();

    Long findUltimaLineaPedido(String idPedido);

    List<CompraLinea> findCompraLineasCesta(String cestaId);

    List<CompraLinea> findCompraLineasPedido(String pedidoId);

    List<String> findCompraLineas_SociedadesProveedorasList(String cestaId);

    List<String> findCompraLineas_idsDeUnaSociedadList(CompraPedido compraPedido);
    
    List<String[]> findCompraLinea_ProvList(CompraCesta cesta, List<String> estados);
    
    Map<String, Object> findCompraLinea_PagWithFilter_C(Map<String, Object> parametrosFiltro);
    
    List<CompraLinea> findCompraLineasPedido(CompraPedido compraPedido, List<String> estados);

}
