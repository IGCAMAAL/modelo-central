package com.centraldecompras.modelo.pur.service;

import com.centraldecompras.zglobal.exceptions.IllegalOrphanException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.modelo.CompraPedidoCabLin;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoCabLinService;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompraPedidoCabLinServiceImpl implements CompraPedidoCabLinService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CompraPedidoCabLinServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(CompraPedidoCabLin compraPedidoCabLin) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraPedidoCabLin);
        } catch (Exception ex) {
            if (findCompraPedidoCabLin(compraPedidoCabLin.getPedido().getCompraPedidoId(), compraPedidoCabLin.getCompraPedidoCabLin()) != null) {
                throw new PreexistingEntityException("Cabecera Pedido " + compraPedidoCabLin.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(CompraPedidoCabLin compraPedidoCabLin) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraPedidoCabLin);
        } catch (Exception ex) {
            if (findCompraPedidoCabLin(compraPedidoCabLin.getPedido().getCompraPedidoId(), compraPedidoCabLin.getCompraPedidoCabLin()) != null) {
                throw new PreexistingEntityException("Cabecera Pedido " + compraPedidoCabLin.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(CompraPedidoCabLin compraPedidoCabLin) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            compraPedidoCabLin = em.merge(compraPedidoCabLin);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findCompraPedidoCabLin(compraPedidoCabLin.getPedido().getCompraPedidoId(), compraPedidoCabLin.getCompraPedidoCabLin()) == null) {
                    throw new NonexistentEntityException("Cabecera Pedido " + compraPedidoCabLin + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        // Nothing TODO
    }

    public List<CompraPedidoCabLin> findCompraPedidoCabLinEntities() {
        return findCompraPedidoCabLinEntities(true, -1, -1);
    }

    public List<CompraPedidoCabLin> findCompraPedidoCabLinEntities(int maxResults, int firstResult) {
        return findCompraPedidoCabLinEntities(false, maxResults, firstResult);
    }

    public List<CompraPedidoCabLin> findCompraPedidoCabLinEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(CompraPedidoCabLin.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getCompraPedidoCabLinCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<CompraPedido> rt = cq.from(CompraPedidoCabLin.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Long findUltimaLinCompraPedidoCabLin(String compraPedidoIdk) {
        Long reply = null;
        try {
            reply = (Long) em.createNamedQuery("CompraPedidoCabLin.findUltimaLinCompraPedidoCabLin")
                    .setParameter("compraPedidoIdk", compraPedidoIdk)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing TODO
        }
        return reply;
    }

    public CompraPedidoCabLin findCompraPedidoCabLin(String compraPedidoIdk, int compraPedidoCabLin) {
        CompraPedidoCabLin reply = null;
        try {
            reply = (CompraPedidoCabLin) em.createNamedQuery("CompraPedidoCabLin.findCompraPedidoCabLin")
                    .setParameter("compraPedidoIdk", compraPedidoIdk)
                    .setParameter("compraPedidoCabLin", compraPedidoCabLin)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing TODO
        }
        return reply;
    }
}
