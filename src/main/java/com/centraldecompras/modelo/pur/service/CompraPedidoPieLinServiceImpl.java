/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.pur.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.CompraPedidoPieLin;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoPieLinService;
import com.centraldecompras.zglobal.exceptions.IllegalOrphanException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class CompraPedidoPieLinServiceImpl implements CompraPedidoPieLinService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CompraPedidoPieLinServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(CompraPedidoPieLin compraPedidoPieLin) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraPedidoPieLin);
        } catch (Exception ex) {
            if (findCompraPedidoPieLin(compraPedidoPieLin.getPedido().getCompraPedidoId(), compraPedidoPieLin.getCompraPedidoPieLin()) != null) {
                throw new PreexistingEntityException("Cabecera Pedido " + compraPedidoPieLin.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(CompraPedidoPieLin compraPedidoPieLin) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraPedidoPieLin);
        } catch (Exception ex) {
            if (findCompraPedidoPieLin(compraPedidoPieLin.getPedido().getCompraPedidoId(), compraPedidoPieLin.getCompraPedidoPieLin()) != null) {
                throw new PreexistingEntityException("Cabecera Pedido " + compraPedidoPieLin.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(CompraPedidoPieLin compraPedidoPieLin) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            compraPedidoPieLin = em.merge(compraPedidoPieLin);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findCompraPedidoPieLin(compraPedidoPieLin.getPedido().getCompraPedidoId(), compraPedidoPieLin.getCompraPedidoPieLin()) == null) {
                    throw new NonexistentEntityException("Cabecera Pedido " + compraPedidoPieLin + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        // Nothing TODO
    }

    public List<CompraPedidoPieLin> findCompraPedidoPieLinEntities() {
        return findCompraPedidoPieLinEntities(true, -1, -1);
    }

    public List<CompraPedidoPieLin> findCompraPedidoPieLinEntities(int maxResults, int firstResult) {
        return findCompraPedidoPieLinEntities(false, maxResults, firstResult);
    }

    public List<CompraPedidoPieLin> findCompraPedidoPieLinEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(CompraPedidoPieLin.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public CompraPedidoPieLin findCompraPedidoPieLin(String id) {
        return em.find(CompraPedidoPieLin.class, id);
    }

    public int getPedidoPieCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<CompraPedidoPieLin> rt = cq.from(CompraPedidoPieLin.class
        );
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public Long findUltimaLinCompraPedidoPieLin(String compraPedidoIdk) {
        Long reply = null;
        try {
            reply = (Long) em.createNamedQuery("CompraPedidoPieLin.findUltimaLinCompraPedidoPieLin")
                .setParameter("compraPedidoIdk", compraPedidoIdk)
                .getSingleResult();
        } catch (Exception ex) {
            // Nothing TODO
        }
        return reply;
    }
    
    public CompraPedidoPieLin findCompraPedidoPieLin(String compraPedidoIdk, int compraPedidoCabLin) {
        CompraPedidoPieLin reply = null;
        try {
            reply = (CompraPedidoPieLin) em.createNamedQuery("CompraPedidoPieLin.findCompraPedidoPieLin")
                .setParameter("compraPedidoIdk", compraPedidoIdk)
                .setParameter("compraPedidoCabLin", compraPedidoCabLin)
                .getSingleResult();
        } catch (Exception ex) {
            // Nothing TODO
        }
        return reply;
    }
}
