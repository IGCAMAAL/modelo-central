/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.pur.service.interfaces;

import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.zglobal.exceptions.IllegalOrphanException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface CompraPedidoService {

    void create_C(CompraPedido compraPedido) throws PreexistingEntityException, Exception;

    void create(CompraPedido compraPedido) throws PreexistingEntityException, Exception;

    void edit(CompraPedido compraPedido) throws IllegalOrphanException, NonexistentEntityException, Exception;

    void destroy(String id) throws IllegalOrphanException, NonexistentEntityException;

    List<CompraPedido> findCompraPedidoEntities();

    List<CompraPedido> findCompraPedidoEntities(int maxResults, int firstResult);

    List<CompraPedido> findCompraPedidoEntities(boolean all, int maxResults, int firstResult);

    CompraPedido findCompraPedido(String id);

    int getPedidoCabCount();

}
