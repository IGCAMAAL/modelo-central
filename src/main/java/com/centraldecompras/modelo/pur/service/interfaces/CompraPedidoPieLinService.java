/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.pur.service.interfaces;

import com.centraldecompras.modelo.CompraPedidoPieLin;
import com.centraldecompras.zglobal.exceptions.IllegalOrphanException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface CompraPedidoPieLinService {

    void create_C(CompraPedidoPieLin compraPedidoPieLin) throws PreexistingEntityException, Exception;

    void create(CompraPedidoPieLin compraPedidoPieLin) throws PreexistingEntityException, Exception;

    void edit(CompraPedidoPieLin compraPedidoPieLin) throws IllegalOrphanException, NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    List<CompraPedidoPieLin> findCompraPedidoPieLinEntities();

    List<CompraPedidoPieLin> findCompraPedidoPieLinEntities(int maxResults, int firstResult);

    List<CompraPedidoPieLin> findCompraPedidoPieLinEntities(boolean all, int maxResults, int firstResult);

    CompraPedidoPieLin findCompraPedidoPieLin(String id);

    int getPedidoPieCount();

    Long findUltimaLinCompraPedidoPieLin(String compraPedidoIdk);

    CompraPedidoPieLin findCompraPedidoPieLin(String compraPedidoIdk, int compraPedidoCabLin);
}
