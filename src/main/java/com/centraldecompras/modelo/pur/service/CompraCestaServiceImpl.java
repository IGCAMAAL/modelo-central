package com.centraldecompras.modelo.pur.service;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.modelo.Contadores;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.SocZonGeoExcl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.pur.service.interfaces.CompraCestaService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class CompraCestaServiceImpl implements CompraCestaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CompraCestaServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(CompraCesta compraCesta) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraCesta);
        } catch (Exception ex) {
            if (findCompraCesta(compraCesta.getIdCesta()) != null) {
                throw new PreexistingEntityException("compraCesta " + compraCesta + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(CompraCesta compraCesta) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraCesta);
        } catch (Exception ex) {
            if (findCompraCesta(compraCesta.getIdCesta()) != null) {
                throw new PreexistingEntityException("compraCesta " + compraCesta + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(CompraCesta compraCesta) throws NonexistentEntityException, Exception {
        try {
            compraCesta = em.merge(compraCesta);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = compraCesta.getIdCesta();
                if (findCompraCesta(id) == null) {
                    throw new NonexistentEntityException("The compraCesta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        CompraCesta compraCesta;
        try {
            compraCesta = em.getReference(CompraCesta.class, id);
            compraCesta.getIdCesta();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The CompraCesta with id " + id + " no longer exists.", enfe);
        }
        em.remove(compraCesta);
    }

    public List<CompraCesta> findCompraCestaEntities() {
        return findCompraCestaEntities(true, -1, -1);
    }

    public List<CompraCesta> findCompraCestaEntities(int maxResults, int firstResult) {
        return findCompraCestaEntities(false, maxResults, firstResult);
    }

    public List<CompraCesta> findCompraCestaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(CompraCesta.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public CompraCesta findCompraCesta(String idCesta) {
        return findCompraCesta(idCesta, null);
    }

    @Override
    public CompraCesta findCompraCesta(String idCesta, String cestaEstado) {
        CompraCesta reply = null;

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<CompraCesta> q = builder.createQuery(CompraCesta.class);
        Root<CompraCesta> r = q.from(CompraCesta.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(r.get("idCesta"), idCesta));
        if (cestaEstado != null) {
            predicates.add(builder.equal(r.get("cestaEstado"), cestaEstado));
        }
        q.where(predicates.toArray(new Predicate[]{}));
        Query query = em.createQuery(q);

        reply = (CompraCesta) query.getSingleResult();

        return reply;
    }

    public int getCestaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<CompraCesta> rt = cq.from(CompraCesta.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Map<String, Object> findCompraCesta_PagWithFilter_dSUE(Map<String, Object> parametrosFiltro) {
        Map<String, Object> reply = new HashMap<String, Object>();

        try {
            Integer numeroRegistrosPagina = (Integer) parametrosFiltro.get("numeroRegistrosPagina");
            Integer paginaActual = (Integer) parametrosFiltro.get("paginaActual");

            Long resultadosmax = findCompraCesta_CountWithFilter_dSUE(parametrosFiltro);
            Integer leidos = resultadosmax.intValue();

            Integer ultimaPagina = leidos / numeroRegistrosPagina;
            Integer resto = leidos % numeroRegistrosPagina;

            if (resto != 0) {
                ultimaPagina++;
            }

            ultimaPagina = (ultimaPagina == 0) ? 1 : ultimaPagina;

            if (paginaActual > ultimaPagina) {
                paginaActual = ultimaPagina;
            }

            /*
             List<CompraCesta> cestas = (List<CompraCesta>) em.createNamedQuery("CompraCesta.findCompraCesta_CountWithFilter_dSUE")
             .setParameter("createdIni", ((Calendar) parametrosFiltro.get("createdIni")).getTime())
             .setParameter("createdFin", ((Calendar) parametrosFiltro.get("createdFin")).getTime())
             .setParameter("sociedad", (Sociedad) parametrosFiltro.get("sociedadEntity"))
             .setParameter("persona", (Persona) parametrosFiltro.get("usuarioEntity"))
             .setParameter("cestaEstado", (List) parametrosFiltro.get("cestaEstado"))
             .setFirstResult((paginaActual - 1) * numeroRegistrosPagina)
             .setMaxResults(numeroRegistrosPagina)
             .getResultList();
             */
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<CompraCesta> qry = cb.createQuery(CompraCesta.class);
            Root<CompraCesta> root = qry.from(CompraCesta.class);
            qry.select(root);

            List<Predicate> whereParam = new ArrayList<>();
            whereParam.add(cb.between(root.get("atributosAuditoria").get("created"), ((Calendar) parametrosFiltro.get("createdIni")).getTime(), ((Calendar) parametrosFiltro.get("createdFin")).getTime()));
            whereParam.add(cb.equal(root.get("sociedad"), (Sociedad) parametrosFiltro.get("sociedadEntity")));
            whereParam.add(cb.equal(root.get("persona"), (Persona) parametrosFiltro.get("usuarioEntity")));
            whereParam.add(cb.in(root.get("cestaEstado")).value((List) parametrosFiltro.get("cestaEstado")));
            qry.where(whereParam.toArray(new Predicate[]{}));
            
            List<Order> orderList = new ArrayList();
            orderList.add(cb.desc(root.get("atributosAuditoria").get("created")));
            orderList.add(cb.desc(root.get("cestaDescripcion")));
            qry.orderBy(orderList);

            List<CompraCesta> cestas = em.createQuery(qry)
                    .setFirstResult((paginaActual - 1) * numeroRegistrosPagina)
                    .setMaxResults(numeroRegistrosPagina)
                    .getResultList();

            //return cestas; 
            reply.put("totalPaginas", ultimaPagina);
            reply.put("totalRegistros", leidos);
            reply.put("paginaActual", paginaActual);
            reply.put("cestas", (cestas == null) ? new ArrayList() : cestas);

        } catch (Exception ex) {
            log.warn(ex);
            throw ex;
        }

        return reply;
    }

    private Long findCompraCesta_CountWithFilter_dSUE(Map<String, Object> parametrosFiltro) {
        Long reply = null;

        try {
            /*
             reply = (Long) em.createNamedQuery("CompraCesta.findCompraCestaWithFilter_dSUE_COUNT")
             .setParameter("createdIni", ((Calendar) parametrosFiltro.get("createdIni")).getTime())
             .setParameter("createdFin", ((Calendar) parametrosFiltro.get("createdFin")).getTime())
             .setParameter("sociedad", (Sociedad) parametrosFiltro.get("sociedadEntity"))
             .setParameter("persona", (Persona) parametrosFiltro.get("usuarioEntity"))
             .setParameter("cestaEstado", (List) parametrosFiltro.get("cestaEstado"))
             .getSingleResult();
             */
            
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> qry = cb.createQuery(Long.class);
            Root<CompraCesta> root = qry.from(CompraCesta.class);
            CriteriaQuery<Long> select = qry.select(cb.count(root));  
            
            List<Predicate> whereParam = new ArrayList<>();
            whereParam.add(cb.between(root.get("atributosAuditoria").get("created"), ((Calendar) parametrosFiltro.get("createdIni")).getTime(), ((Calendar) parametrosFiltro.get("createdFin")).getTime()));
            whereParam.add(cb.equal(root.get("sociedad"), (Sociedad) parametrosFiltro.get("sociedadEntity")));
            whereParam.add(cb.equal(root.get("persona"), (Persona) parametrosFiltro.get("usuarioEntity")));
            whereParam.add(cb.in(root.get("cestaEstado")).value((List) parametrosFiltro.get("cestaEstado")));
            select.where(whereParam.toArray(new Predicate[]{}));
            
            List<Order> orderList = new ArrayList();
            orderList.add(cb.desc(root.get("atributosAuditoria").get("created")));
            orderList.add(cb.desc(root.get("cestaDescripcion")));
            select.orderBy(orderList);

            TypedQuery<Long> typedQuery = em.createQuery(select);
            //typedQuery.setFirstResult(startIndex);
            //typedQuery.setMaxResults(pageSize);

            reply = typedQuery.getSingleResult();

        } catch (Exception ex) {
            log.warn(ex);
            throw ex;
        }

        return reply;
    }
    
    

    private Long findArticulosForSelect_Coun_WithFilter(Map<String, Object> parametrosFiltro) {
        Long reply = 0L;
        try {
            Boolean count = Boolean.TRUE;
            Boolean typeAheadArt = Boolean.FALSE;

            Boolean etiquetas = (parametrosFiltro.containsKey("productosConEtiqueta")) ? Boolean.TRUE : Boolean.FALSE;
            Boolean estados = (parametrosFiltro.containsKey("estados")) ? Boolean.TRUE : Boolean.FALSE;

            reply = (Long) findArticulosFor_Sel_TAhead_Qry_WithFilterDin(parametrosFiltro, count, etiquetas, estados, typeAheadArt, 0).getSingleResult();

        } catch (Exception ex) {
            log.info(ex.getMessage());
            throw (ex);
        }
        return reply;
    }

    public Map<String, Object> findArticulosForSelect_Pag_WithFilter(Map<String, Object> parametrosFiltro) {
        Map<String, Object> reply = new HashMap<String, Object>();

        try {

            Integer numeroRegistrosPagina = (Integer) parametrosFiltro.get("numeroRegistrosPagina");
            Integer paginaActual = (Integer) parametrosFiltro.get("paginaActual");

            StringBuffer cadenaArtSql = new StringBuffer();
            cadenaArtSql.append("%").append((String) parametrosFiltro.get("cadenaArt")).append("%");
            parametrosFiltro.put("cadenaArtSql", cadenaArtSql.toString());

            Long resultadosmax = findArticulosForSelect_Coun_WithFilter(parametrosFiltro);
            Integer leidos = resultadosmax.intValue();

            Integer ultimaPagina = leidos / numeroRegistrosPagina;
            Integer resto = leidos % numeroRegistrosPagina;

            if (resto != 0) {
                ultimaPagina++;
            }
            if (ultimaPagina == 0) {
                ultimaPagina = 1;
            }
            if (paginaActual > ultimaPagina) {
                paginaActual = ultimaPagina;
            }

            parametrosFiltro.replace("paginaActual", paginaActual);
            parametrosFiltro.replace("numeroRegistrosPagina", numeroRegistrosPagina);

            Boolean count = Boolean.FALSE;
            Boolean typeAheadArt = Boolean.FALSE;

            Boolean etiquetas = (parametrosFiltro.containsKey("productosConEtiqueta")) ? Boolean.TRUE : Boolean.FALSE;
            Boolean estados = (parametrosFiltro.containsKey("estados")) ? Boolean.TRUE : Boolean.FALSE;

            List<Object[]> articulos_aup = findArticulosFor_Sel_TAhead_Qry_WithFilterDin(parametrosFiltro, count, etiquetas, estados, typeAheadArt, 0).getResultList();

            reply.put("totalPaginas", ultimaPagina);
            reply.put("totalRegistros", leidos);
            reply.put("paginaActual", paginaActual);
            reply.put("articulos_aup", (articulos_aup == null) ? new ArrayList() : articulos_aup);

        } catch (Exception ex) {
            log.info(ex.getMessage());
            throw (ex);
        }

        return reply;

    }

    public List<Object[]> findArticuloTypeAhead(int tipo, Map<String, Object> parametrosFiltro) {
        List<Object[]> reply = new ArrayList();
        try {
            StringBuffer cadenaArtSql = new StringBuffer();
            StringBuffer cadenaExcludeSql = new StringBuffer();

            Boolean count = Boolean.FALSE;
            Boolean typeAheadArt = Boolean.TRUE;
            Boolean etiquetas = (parametrosFiltro.containsKey("productosConEtiqueta")) ? Boolean.TRUE : Boolean.FALSE;
            Boolean estados = (parametrosFiltro.containsKey("estados")) ? Boolean.TRUE : Boolean.FALSE;

            if (tipo == 1) {  /* ------- Empieza ------------ */

                cadenaArtSql.append((String) parametrosFiltro.get("cadenaArt")).append("%");
            } else {  /* ------- Contiene ------------ */

                cadenaArtSql.append("%").append((String) parametrosFiltro.get("cadenaArt")).append("%");
                cadenaExcludeSql.append((String) parametrosFiltro.get("cadenaArt")).append("%");
                parametrosFiltro.put("cadenaExcludeSql", cadenaExcludeSql.toString());
            }
            parametrosFiltro.put("cadenaArtSql", cadenaArtSql.toString());

            reply = (List<Object[]>) findArticulosFor_Sel_TAhead_Qry_WithFilterDin(parametrosFiltro, count, etiquetas, estados, typeAheadArt, tipo).getResultList();

            if (reply == null) {
                reply = new ArrayList<>();
            }

        } catch (Exception ex) {
            log.warn(ex);
            throw (ex);
        }
        return reply;
    }

    private Query findArticulosFor_Sel_TAhead_Qry_WithFilterDin(Map<String, Object> parametrosFiltro, Boolean count, Boolean etiquetas, Boolean estados, Boolean typeAheadArt, int tipo) {

        StringBuffer qrySbf = new StringBuffer();
        Integer paginaActual = 0;
        Integer numeroRegistrosPagina = 0;

        if (count) {
            qrySbf.append(" SELECT COUNT(DISTINCT ap) ");
        } else {
            qrySbf.append(" SELECT DISTINCT(ap), al ");
            paginaActual = (Integer) parametrosFiltro.get("paginaActual");
            numeroRegistrosPagina = (Integer) parametrosFiltro.get("numeroRegistrosPagina");
        }

        qrySbf.append("  FROM ArticuloProducto            ap, "
                + "           SocProducto                 sp, "
                + "           AlmacenUsuarioProducto     aup, "
                + "           ArticuloLangNomb            al  "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "      WHERE  ap.producto.route                               LIKE        '%' || :productoId  || '.%'                                       "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                                
                + "        AND  ap.producto.route                               LIKE          sp.producto.route || '%'                                        " // Productos asignados a sociedad      
                + "        AND  ap.articulo.sociedad.idSociedad                 =             sp.sociedad.idSociedad                                          " // Productos asignados a sociedad                      
                + "        AND  ap.producto.route                               NOT IN      (  SELECT spe.route                                               " // que no estén excluidos en la jerarquia de asignación de productos.
                + "                                                                              FROM SocProductoExcl spe                                     " //
                + "                                                                             WHERE spe.sociedadId    =  ap.articulo.sociedad.idSociedad    " //                
                + "                                                                         )                                                                 " //          
                //
                + "        AND  sp.asignada                                     =           true                                                              " // Relacion sociedad / producto asignada       
                + "        AND  sp.atributosAuditoria.estadoRegistro            IN         (:estados)                                                         " // Relacion almacen / sociedad / producto activa                                        
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND  ap.producto.route                               LIKE         aup.producto.route || '%'                           " // Productos asignados al almacen / usuario                                       
                + "        AND  ap.producto.route                               NOT IN      ( SELECT aupe.route                                  " // que no estén excluidos en la jerarquia de asignación de almacen / usuario / producto.
                + "                                                                             FROM AlmacenUsuProdExcl aupe                     " //       
                + "                                                                            WHERE aupe.almacenId   = :almacenId       " // Almacen de la relacion almacen / usuario / producto
                + "                                                                              AND aupe.usuarioId   = :usuarioId       " // Usuario de la relacion almacen / usuario / prodocto                      
                + "                                                                         )                                                    " //                
                //
                + "        AND aup.almacen.idAlmacen                            =           :almacenId                                           " // Almacen de la relacion almacen / usuario / producto
                + "        AND aup.usuario.idPersona                            =           :usuarioId                                           " // Usuario de la relacion almacen / usuario / prodocto                      
                + "        AND aup.asignada                                     =           true                                                 " // Relacion almacen / sociedad / producto asignada                                      
                + "        AND aup.atributosAuditoria.estadoRegistro            IN         (:estados)                                            " // Relacion almacen / sociedad / producto activa                                        
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND   al.articuloIdk                                 =         ap.articulo.idArticulo                                 "
                + "        AND   al.idioma                                      =         :idioma                                                "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND   :hoy                                           BETWEEN             aup.producto.atributosAuditoria.fechaPrevistaActivacion                       "
                + "                                                                 AND CASE  WHEN  aup.producto.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999 "
                + "                                                                           ELSE  aup.producto.atributosAuditoria.fechaPrevistaDesactivacion                    "
                + "                                                                     END                                                                                       "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND  ap.articulo.sociedad.idSociedad                 IN          ( SELECT sz.sociedad.idSociedad                                                                     " // que el proveedor sirva 
                + "                                                                             FROM SocZonGeo sz                                                                               " // en la zona donde está 
                + "                                                                            WHERE sz.zonaGeografica.idZonaGeografica   IN (:zonasPadreAlmacen)                               " // ubicado del almacen del cliente     
                + "                                                                              AND sz.asignada                           = true                                               " //
                + "                                                                              AND sz.atributosAuditoria.estadoRegistro IN (:estados)                                         "
                + "                                                                              AND sz.sociedad.idSociedad           NOT IN ( SELECT sze.sociedadId                                                            " // Que no estén excluidos 
                + "                                                                                                                              FROM SocZonGeoExcl sze                                                         " // en la jerarquia de asignación 
                + "                                                                                                                             WHERE  sze.route        LIKE '%' || sz.zonaGeografica.idZonaGeografica || '.%'  " // de zonas a sociedad proveedora.               
                + "                                                                                                                           )                                                                                 "
                + "                                                                          )                                                                                                                                  "
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND   :hoy                                           BETWEEN             ap.producto.atributosAuditoria.fechaPrevistaActivacion                       "
                + "                                                                 AND CASE  WHEN  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999 "
                + "                                                                           ELSE  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion                    "
                + "                                                                     END                                                                                      "
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND   :hoy                                           BETWEEN             ap.articulo.atributosAuditoria.fechaPrevistaActivacion                       "
                + "                                                                 AND CASE  WHEN  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN 99999999  "
                + "                                                                           ELSE  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion                    "
                + "                                                                 END                                                                                          "
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND   :hoy                                           BETWEEN             ap.atributosAuditoria.fechaPrevistaActivacion                                "
                + "                                                                 AND CASE  WHEN  ap.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN 99999999           "
                + "                                                                           ELSE  ap.atributosAuditoria.fechaPrevistaDesactivacion                             "
                + "                                                                 END                                                                                          "
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + ""
                + "        AND  al.traduccionNomb                               LIKE :cadenaArtSql    "
                + "");

        qrySbf.append("QccQ_etiquetas_QccQ");

        qrySbf.append(" "
                + "        AND  ap.producto.atributosAuditoria.estadoRegistro   IN          (:estados)                                                                           " // Productos activs                                                        
                + "        AND  ap.articulo.atributosAuditoria.estadoRegistro   IN          (:estados)                                                                           " // Articulos activos                                
                + "        AND  ap.atributosAuditoria.estadoRegistro            IN          (:estados)                                                                           " // Relacion almacen / sociedad / producto activa                       
                + ""
                + " ORDER BY al.traduccionNomb "
                + "");

        // Como se ha quitado el typeAhead de artivulos
        // se eliminan estos dos filtros.
        //qrySbf.append("QccQ_traduccionNombArticulo_QccQ");
        //qrySbf.append("QccQ_excludeNombArticulo_QccQ");
        // -----------------------------------------------------------------------------------------------------------------------------------------------        
        String qryString = qrySbf.toString();

        if (etiquetas) {
            qryString = qryString.replaceAll("QccQ_etiquetas_QccQ", "    AND   ap.producto.idProducto                          IN       (:productosConEtiqueta)      ");
        } else {
            qryString = qryString.replaceAll("QccQ_etiquetas_QccQ", "");
        }

        //if (typeAheadArt) {
        //    qryString = qryString.replaceAll("QccQ_traduccionNombArticulo_QccQ", "    AND  al.traduccionNomb                  LIKE      :cadenaArtSql                ");
        //    if (tipo == 2) {
        //        qryString = qryString.replaceAll("QccQ_excludeNombArticulo_QccQ", "       AND  al.traduccionNomb              NOT LIKE  :cadenaExcludeSql            ");
        //    } else {
        //        qryString = qryString.replaceAll("QccQ_excludeNombArticulo_QccQ", "");
        //    }
        //} else {
        //    qryString = qryString.replaceAll("QccQ_traduccionNombArticulo_QccQ", "");
        //    qryString = qryString.replaceAll("QccQ_excludeNombArticulo_QccQ", "");
        //}
        // -----------------------------------------------------------------------------------------------------------------------------------------------
        Query query = em.createQuery(qryString);
        query.setParameter("idioma", (String) parametrosFiltro.get("idioma"));
        query.setParameter("estados", (List<String>) parametrosFiltro.get("estados"));
        query.setParameter("hoy", (Integer) parametrosFiltro.get("hoy"));
        query.setParameter("almacenId", (String) parametrosFiltro.get("almacenId"));
        query.setParameter("usuarioId", (String) parametrosFiltro.get("usuarioId"));
        query.setParameter("productoId", (String) parametrosFiltro.get("productoId"));
        query.setParameter("zonasPadreAlmacen", ((List<String>) parametrosFiltro.get("zonasPadreAlmacen")));
        query.setParameter("cadenaArtSql", (String) parametrosFiltro.get("cadenaArtSql"));

        if (etiquetas) {
            query.setParameter("productosConEtiqueta", (List<String>) parametrosFiltro.get("productosConEtiqueta"));
        }

        //if (typeAheadArt) {
        //    query.setParameter("cadenaArtSql", (String) parametrosFiltro.get("cadenaArtSql"));
        //    if (tipo == 2) {
        //        query.setParameter("cadenaExcludeSql", (String) parametrosFiltro.get("cadenaArtSql"));
        //    }
        //    query.setMaxResults((Integer) parametrosFiltro.get("cantidad"));
        //}
        if (!count && !typeAheadArt) {
            query.setFirstResult((paginaActual - 1) * numeroRegistrosPagina);
            query.setMaxResults(numeroRegistrosPagina);
        }

        log.info("........... AhoraVAlaQRY .............................. AhoraVAlaQRY .............................. AhoraVAlaQRY .............................. AhoraVAlaQRY ...................");
        return query;
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------- 
    // Esta entrada ya no debe funcionar para el typeAhead de productos. 
    // Se va a transformar para un filtro de articulos para la carga de la cesta
    // No hacer nada hasta que esté decidico    
    public List<Object[]> findProductoTypeAhead(int tipo, Map<String, Object> parametrosFiltro) {
        List<Object[]> reply = new ArrayList();
        try {
            StringBuffer traduccionNombProdSql = new StringBuffer();
            StringBuffer traduccionExcludeSql = new StringBuffer();

            Boolean etiquetas = (parametrosFiltro.containsKey("productosConEtiqueta")) ? Boolean.TRUE : Boolean.FALSE;
            Boolean estados = (parametrosFiltro.containsKey("estados")) ? Boolean.TRUE : Boolean.FALSE;

            if (tipo == 1) {
                traduccionNombProdSql.append((String) parametrosFiltro.get("descripcionProd")).append("%"); /* ------- Empieza ------------ */

            } else {
                traduccionNombProdSql.append("%").append((String) parametrosFiltro.get("descripcionProd")).append("%"); /* ------- Contiene ------------ */

                traduccionExcludeSql.append((String) parametrosFiltro.get("descripcionProd")).append("%");
                parametrosFiltro.put("traduccionExcludeSql", traduccionExcludeSql.toString());
            }

            parametrosFiltro.replace("descripcionProd", traduccionNombProdSql.toString());

            reply = (List<Object[]>) findProductosFor_TAhead_Qry_WithFilterDin(parametrosFiltro, etiquetas, estados, tipo).getResultList();
            if (reply == null) {
                reply = new ArrayList<>();
            }

        } catch (Exception ex) {
            log.warn(ex);
            throw (ex);
        }
        return reply;
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------- 
    // Esta entrada ya no debe funcionar para el typeAhead de productos. 
    // Se va a transformar para un filtro de articulos para la carga de la cesta
    // No hacer nada hasta que esté decidico        
    private Query findProductosFor_TAhead_Qry_WithFilterDin(Map<String, Object> parametrosFiltro, Boolean etiquetas, Boolean estados, int tipo) {

        StringBuffer qrySbf = new StringBuffer();

        qrySbf.append(" SELECT  DISTINCT(p), pl                   "
                + "       FROM  Producto                   p, "
                + "             SocProducto               sp, "
                + "             AlmacenUsuarioProducto   aup, "
                + "             ProductoLang              pl  "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                + "      WHERE  p.route                                         LIKE        '%' || sp.producto.idProducto || '.%'                           " // Productos asignados al proveedor
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND  pl.productoId                                   =           p.idProducto                                                    " // Que coincide con el patrón de descripcion buscado
                + "        AND  pl.idioma                                       =           :idioma                                                         " //              
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND sp.sociedad.idSociedad                           IN          ( SELECT sz.sociedad.idSociedad                                                                                         " // que el proveedor sirva en la zona donde está ubicado del almacen del cliente                      
                + "                                                                             FROM SocZonGeo sz                                                                                                   " //

                + "                                                                            WHERE sz.zonaGeografica.idZonaGeografica   IN (:zonasPadreAlmacen)                                                   " //     
                + "                                                                              AND sz.asignada                           = true                                                                   " //
                + "                                                                              AND sz.atributosAuditoria.estadoRegistro IN (:estados)                                                             " //    

                + "                                                                              AND sz.sociedad.idSociedad           NOT IN (  SELECT sze.sociedadId                                               " // Que no estén excluidos 
                + "                                                                                                                               FROM SocZonGeoExcl sze                                            " // en la jerarquia de asignación 
                + "                                                                                                                              WHERE sze.route LIKE '%' || sz.zonaGeografica.id || '.%'           " // de zonas a sociedad proveedora.               
                + "                                                                                                                           )                                                                     " //                  

                + "                                                                              AND   :hoy   BETWEEN            sz.zonaGeografica.atributosAuditoria.fechaPrevistaActivacion                       " // Produtos que estén vigentes en las fecha de gestion
                + "                                                                                              AND CASE WHEN  sz.zonaGeografica.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999  " //     
                + "                                                                                                       ELSE  sz.zonaGeografica.atributosAuditoria.fechaPrevistaDesactivacion                     " //
                + "                                                                                                  END                                                                                            " //                
                + "                                                                         )                                                                                                                       " //                 
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -                
                + "        AND sp.asignada                                      =           true                                                                                " // Relacion almacen / sociedad / producto asignada                                                     
                + "        AND sp.producto.atributosAuditoria.estadoRegistro    IN          (:estados)                                                                          " // Relacion sociedad / producto activa                                        
                + "        AND   :hoy                                           BETWEEN            sp.producto.atributosAuditoria.fechaPrevistaActivacion                       " // Relacion Sociedad/roducto que esten vigentes en las fecha de gestion
                + "                                                                 AND CASE WHEN  sp.producto.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999 " //
                + "                                                                          ELSE  sp.producto.atributosAuditoria.fechaPrevistaDesactivacion                    " //
                + "                                                                     END                                                                                     " //                
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND   p.route                                        LIKE        '%' || aup.producto.idProducto || '.%'                                              " // Productos asignados al almacen / comprador
                + "        AND aup.almacen.id                                   =           :almacenId                                                                          " // Almacen de la relacion almacen / usuario / producto
                + "        AND aup.usuario.id                                   =           :usuarioId                                                                          " // Usuario de la relacion almacen / usuario / prodocto      
                //                
                + "        AND aup.asignada                                     =           true                                                                                " // Relacion almacen / sociedad / producto asignada                     
                + "        AND aup.producto.atributosAuditoria.estadoRegistro   IN          (:estados)                                                                          " // Relacion almacen / sociedad / producto activa                                        
                + "        AND    :hoy                                          BETWEEN           aup.producto.atributosAuditoria.fechaPrevistaActivacion                       " // Relacion Sociedad/roducto que esten vigentes en las fecha de gestion
                + "                                                                 AND CASE WHEN aup.producto.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999 " //
                + "                                                                          ELSE aup.producto.atributosAuditoria.fechaPrevistaDesactivacion                    " //
                + "                                                                     END                                                                                     "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND  p.route                                         NOT IN      (  SELECT spe.route                                             " // que no estén excluidos en la jerarquia de asignación de productos.
                + "                                                                              FROM SocProductoExcl spe                                   " //
                + "                                                                             WHERE spe.route LIKE p.idProducto || '.%'                   " //

                + "                                                                         )                                                               " //        
                + "        AND   p.route                                        NOT IN      ( SELECT aupe.route                                             " // que no estén excluidos en la jerarquia de asignación de almacen / usuario / producto.
                + "                                                                             FROM AlmacenUsuProdExcl aupe                                " //
                + "                                                                            WHERE aupe.almacenId   = :almacenId                          " // Almacen de la relacion almacen / usuario / producto
                + "                                                                              AND aupe.usuarioId   = :usuarioId                          " // Usuario de la relacion almacen / usuario / prodocto                                      
                //                + "                                                                            WHERE aupe.route LIKE p.idProducto || '.%'                   " //
                + "                                                                         )                                                               " //
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                                
                + "        AND  p.atributosAuditoria.estadoRegistro             IN          (:estados)                                                      " // Pproductos activos                 
                + "        AND   :hoy                                           BETWEEN            p.atributosAuditoria.fechaPrevistaActivacion                                 " // Produtos que estén vigentes en las fecha de gestion
                + "                                                                 AND CASE WHEN  p.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999           " //     
                + "                                                                          ELSE  p.atributosAuditoria.fechaPrevistaDesactivacion                              " //
                + "                                                                     END                                                                                     " //

        );

        qrySbf.append("QccQ_traduccionNombProdSql_QccQ");
        qrySbf.append("QccQ_traduccionExcludeSql_QccQ");
        qrySbf.append("QccQ_etiquetas_QccQ");

        qrySbf.append(""
                + "   ORDER BY  pl.traduccionDesc     ");

        // -----------------------------------------------------------------------------------------------------------------------------------------------        
        //
        String qryString = qrySbf.toString();

        if (etiquetas) {
            qryString = qryString.replaceAll("QccQ_etiquetas_QccQ", "               AND   sp.producto.idProducto                      IN        (:productosConEtiqueta)    ");
        } else {
            qryString = qryString.replaceAll("QccQ_etiquetas_QccQ", "");
        }

        qryString = qryString.replaceAll("QccQ_traduccionNombProdSql_QccQ", "       AND  pl.traduccionDesc                            LIKE      :traduccionNombProdSql     ");
        if (tipo == 2) {
            qryString = qryString.replaceAll("QccQ_traduccionExcludeSql_QccQ", "    AND  pl.traduccionDesc                            NOT LIKE  :traduccionExcludeSql      ");
        } else {
            qryString = qryString.replaceAll("QccQ_traduccionExcludeSql_QccQ", "");
        }

        // -----------------------------------------------------------------------------------------------------------------------------------------------
        //
        Query query = em.createQuery(qryString);
        query.setParameter("idioma", (String) parametrosFiltro.get("idioma"));
        query.setParameter("estados", (List<String>) parametrosFiltro.get("estados"));
        query.setParameter("hoy", (Integer) parametrosFiltro.get("hoy"));
        query.setParameter("usuarioId", (String) parametrosFiltro.get("usuarioId"));
        query.setMaxResults((Integer) parametrosFiltro.get("cantidad"));
        query.setParameter("almacenId", (String) parametrosFiltro.get("almacenId"));
        query.setParameter("traduccionNombProdSql", "%" + ((String) parametrosFiltro.get("descripcionProd")) + "%");
        query.setParameter("zonasPadreAlmacen", ((List<String>) parametrosFiltro.get("zonasPadreAlmacen")));

        if (tipo == 2) {
            query.setParameter("traduccionExcludeSql", (String) parametrosFiltro.get("descripcionProd"));
        }
        if (etiquetas) {
            query.setParameter("productosConEtiqueta", (List<String>) parametrosFiltro.get("productosConEtiqueta"));
        }

        // -----------------------------------------------------------------------------------------------------------------------------------------------        
        //
        return query;
    }

    public List<String> findProductWithLabels(Map<String, Object> parametrosFiltro) {
        List<String> reply = null;

        try {
            if (parametrosFiltro.containsKey("etiquetasId")) {
                List<String> etiquetasId = (List<String>) parametrosFiltro.get("etiquetasId");

                if (etiquetasId != null && etiquetasId.size() > 0) {
                    reply = (List<String>) em.createNamedQuery("ProductoEtiqueta.findProductWithLabels")
                            .setParameter("productoId", (String) parametrosFiltro.get("productoId"))
                            .setParameter("etiquetasId", etiquetasId)
                            .setParameter("estados", (List<String>) parametrosFiltro.get("estados"))
                            .getResultList();
                }
            }
        } catch (Exception ex) {
            log.warn(ex);
            throw (ex);
        }

        return reply;
    }

}
