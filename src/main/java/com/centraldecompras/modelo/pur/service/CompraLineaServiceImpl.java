/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.pur.service;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.CompraCesta;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.CompraLinea;
import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.modelo.pur.service.interfaces.CompraLineaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompraLineaServiceImpl implements CompraLineaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CompraLineaServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private CompraPedidoService compraPedidoService;

    public void create(CompraLinea compraLinea) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraLinea);
        } catch (Exception ex) {
            if (findCompraLinea(compraLinea.getCompraLineaId()) != null) {
                throw new PreexistingEntityException("compraLinea " + compraLinea + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(CompraLinea compraLinea) throws NonexistentEntityException, Exception {
        try {
            compraLinea = em.merge(compraLinea);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = compraLinea.getCompraLineaId();
                if (findCompraLinea(id) == null) {
                    throw new NonexistentEntityException("The compraLinea with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        CompraLinea compraLinea;
        try {
            compraLinea = em.getReference(CompraLinea.class, id);
            compraLinea.getCompraLineaId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The CompraLinea with id " + id + " no longer exists.", enfe);
        }
        em.remove(compraLinea);
    }

    public List<CompraLinea> findCompraLineaEntities() {
        return findCompraLineaEntities(true, -1, -1);
    }

    public List<CompraLinea> findCompraLineaEntities(int maxResults, int firstResult) {
        return findCompraLineaEntities(false, maxResults, firstResult);
    }

    public List<CompraLinea> findCompraLineaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(CompraLinea.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public CompraLinea findCompraLinea(String id) {
        return em.find(CompraLinea.class, id);
    }

    public int getPedidoLinCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<CompraLinea> rt = cq.from(CompraLinea.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Long findUltimaLineaPedido(String idPedido) {
        Long reply = (Long) em.createNamedQuery("CompraLinea.findUltimaLineaPedido")
                .setParameter("idPedido", idPedido)
                .getSingleResult();
        if (reply == null) {
            return 0L;
        } else {
            return reply;
        }
    }

    public List<CompraLinea> findCompraLineasCesta(String cestaId) {
        List<CompraLinea> reply = null;

        try {
            reply = (List<CompraLinea>) em.createNamedQuery("CompraLinea.findCompraLineasCesta")
                    .setParameter("cestaId", cestaId)
                    .getResultList();
        } catch (EntityNotFoundException ex) {
            // nothing TOTO
        }
        if (reply == null) {
            reply = new ArrayList();
        }

        return reply;
    }

    public List<CompraLinea> findCompraLineasPedido(String pedidoId) {
        List<CompraLinea> reply = null;

        try {
            reply = (List<CompraLinea>) em.createNamedQuery("CompraLinea.findCompraLineasPedido")
                    .setParameter("pedidoId", pedidoId)
                    .getResultList();
        } catch (EntityNotFoundException ex) {
            // nothing TOTO
        }
        if (reply == null) {
            reply = new ArrayList();
        }

        return reply;
    }

    public List<String> findCompraLineas_SociedadesProveedorasList(String cestaId) {
        List<String> reply = null;

        try {
            reply = (List<String>) em.createNamedQuery("CompraLinea.findCompraLineas_SociedadesList")
                    .setParameter("cestaId", cestaId)
                    .getResultList();
        } catch (EntityNotFoundException ex) {
            // nothing TOTO
        }
        if (reply == null) {
            reply = new ArrayList();
        }

        return reply;
    }

    public List<String> findCompraLineas_idsDeUnaSociedadList(CompraPedido compraPedido) {
        List<String> reply = null;

        try {
            reply = (List<String>) em.createNamedQuery("CompraLinea.findCompraLineas_idsDeUnaSociedadList")
                    .setParameter("sociedad", compraPedido.getProveedorSociedad())
                    .setParameter("cesta", compraPedido.getCompraCesta())
                    .getResultList();
        } catch (EntityNotFoundException ex) {
            // nothing TOTO
        }

        if (reply == null) {
            reply = new ArrayList<>();
        }

        return reply;

    }

    public List<String[]> findCompraLinea_ProvList(CompraCesta cesta, List<String> estados) {
        List<String[]> reply = null;

        try {
            reply = (List<String[]>) em.createNamedQuery("CompraLinea.findCompraLinea_ProvList")
                    .setParameter("cesta", cesta)
                    .setParameter("estados", estados)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado lineas cesta para la cesta:  " + cesta.getCestaDescripcion());
        }

        return reply;
    }

    public Map<String, Object> findCompraLinea_PagWithFilter_C(Map<String, Object> parametrosFiltro) {
        Map<String, Object> reply = new HashMap<String, Object>();

        try {

            Integer numeroRegistrosPagina = (Integer) parametrosFiltro.get("numeroRegistrosPagina");
            Integer paginaActual = (Integer) parametrosFiltro.get("paginaActual");

            Long resultadosmax = findCompraLinea_PagWithFilter_C_COUNT(parametrosFiltro);
            Integer leidos = resultadosmax.intValue();

            Integer ultimaPagina = leidos / numeroRegistrosPagina;
            Integer resto = leidos % numeroRegistrosPagina;

            ultimaPagina = (resto != 0) ? ultimaPagina++ : ultimaPagina;
            ultimaPagina = (ultimaPagina == 0) ? 1 : ultimaPagina;
            paginaActual = (paginaActual > ultimaPagina) ? ultimaPagina : paginaActual;

            List<CompraLinea> compraLineas = (List<CompraLinea>) em.createNamedQuery("CompraLinea.findCompraLinea_PagWithFilter_C")
                    .setParameter("cesta", (CompraCesta) parametrosFiltro.get("compraCestaEntity"))
                    .setParameter("idioma", (String) parametrosFiltro.get("idioma"))
                    .setParameter("estados", (List<String>) parametrosFiltro.get("estados"))
                    .setFirstResult((paginaActual - 1) * numeroRegistrosPagina)
                    .setMaxResults(numeroRegistrosPagina)
                    .getResultList();

            reply.put("totalPaginas", ultimaPagina);
            reply.put("totalRegistros", leidos);
            reply.put("paginaActual", paginaActual);
            reply.put("compraLineas", (compraLineas == null) ? new ArrayList() : compraLineas);

        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado lineas para la cesta dada:  " + (String) parametrosFiltro.get("cestaId"));
        }

        return reply;
    }

    private Long findCompraLinea_PagWithFilter_C_COUNT(Map<String, Object> parametrosFiltro) {
        Long reply = null;

        try {
            reply = (Long) em.createNamedQuery("CompraLinea.findCompraLinea_PagWithFilter_C_COUNT")
                    .setParameter("cesta", (CompraCesta) parametrosFiltro.get("compraCestaEntity"))
                    .setParameter("idioma", (String) parametrosFiltro.get("idioma"))
                    .setParameter("estados", (List<String>) parametrosFiltro.get("estados"))
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado lineas para la cesta  dada:  " + (String) parametrosFiltro.get("cestaId"));
        }

        return reply;
    }

    public Map<String, Object> findCompraLinea_PagWithFilter_C_OLD(Map<String, Object> parametrosFiltro) {
        Map<String, Object> reply = new HashMap<String, Object>();
        List<CompraLinea> compraLineas = null;

        Integer numeroRegistrosPagina = 0;
        Integer paginaActual = 0;
        Integer ultimaPagina = 0;
        Integer resto = 0;
        Integer leidos = 0;

        try {
            numeroRegistrosPagina = (Integer) parametrosFiltro.get("numeroRegistrosPagina");
            paginaActual = (Integer) parametrosFiltro.get("paginaActual");

            Long resultadosmax = findCompraLinea_PagWithFilter_C_COUNT(parametrosFiltro);
            leidos = resultadosmax.intValue();

            ultimaPagina = leidos / numeroRegistrosPagina;
            resto = leidos % numeroRegistrosPagina;
            if (resto != 0) {
                ultimaPagina++;
            }
            if (ultimaPagina == 0) {
                ultimaPagina = 1;
            }
            if (paginaActual > ultimaPagina) {
                paginaActual = ultimaPagina;
            }

            compraLineas = (List<CompraLinea>) em.createNamedQuery("CompraLinea.findCompraLinea_PagWithFilter_C")
                    .setParameter("cesta", (CompraCesta) parametrosFiltro.get("compraCestaEntity"))
                    .setParameter("idioma", (String) parametrosFiltro.get("idioma"))
                    .setParameter("estados", (List<String>) parametrosFiltro.get("estados"))
                    .setFirstResult((paginaActual - 1) * numeroRegistrosPagina)
                    .setMaxResults(numeroRegistrosPagina)
                    .getResultList();

            /*
             Con parametros posicionales
             Query query = session.createQuery("from Student where studentId = ? and studentName=?");
             query
             .setInteger(0, 1)
             .setString(1, "Jack");
             */
            reply.put("totalPaginas", ultimaPagina);
            reply.put("totalRegistros", leidos);
            reply.put("paginaActual", paginaActual);
            reply.put("compraLineas", (compraLineas == null) ? new ArrayList() : compraLineas);

        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado lineas para la cesta dada:  " + (String) parametrosFiltro.get("cestaId"));
        }

        return reply;
    }

    private Long findCompraLinea_PagWithFilter_C_COUNT_OLD(Map<String, Object> parametrosFiltro) {
        Long reply = null;

        try {
            reply = (Long) em.createNamedQuery("CompraLinea.findCompraLinea_PagWithFilter_C_COUNT")
                    .setParameter("cesta", (CompraCesta) parametrosFiltro.get("compraCestaEntity"))
                    .setParameter("idioma", (String) parametrosFiltro.get("idioma"))
                    .setParameter("estados", (List<String>) parametrosFiltro.get("estados"))
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado lineas para la cesta  dada:  " + (String) parametrosFiltro.get("cestaId"));
        }

        return reply;
    }

    public List<CompraLinea> findCompraLineasPedido(CompraPedido compraPedido, List<String> estados) {

        List<CompraLinea> reply = null;

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<CompraLinea> q = criteriaBuilder.createQuery(CompraLinea.class);
        Root<CompraLinea> r = q.from(CompraLinea.class);

        Join<CompraLinea, Sociedad> join = r.join("articulo");
        r.fetch("articulo");

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(r.get("cesta"), compraPedido.getCompraCesta()));
        predicates.add(criteriaBuilder.equal(r.get("sociedad"), compraPedido.getProveedorSociedad()));
        if (estados != null) {
            predicates.add(criteriaBuilder.in(r.get("lineaEstado")).value(estados));
        }
        q.where(predicates.toArray(new Predicate[]{}));
        Query query = em.createQuery(q);

        reply = (List<CompraLinea>) query.getResultList();

        return reply;

    }
}
