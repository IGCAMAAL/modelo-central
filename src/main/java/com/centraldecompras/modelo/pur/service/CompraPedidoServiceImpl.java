package com.centraldecompras.modelo.pur.service;

import com.centraldecompras.zglobal.exceptions.IllegalOrphanException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.CompraPedido;
import com.centraldecompras.modelo.pur.service.interfaces.CompraLineaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoService;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompraPedidoServiceImpl implements CompraPedidoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CompraPedidoServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private CompraLineaService compraLineaService;

    @Transactional
    public void create_C(CompraPedido compraPedido) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraPedido);
        } catch (Exception ex) {
            if (findCompraPedido(compraPedido.getCompraPedidoId()) != null) {
                throw new PreexistingEntityException("Pedido " + compraPedido.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    @Override
    public void create(CompraPedido compraPedido) throws PreexistingEntityException, Exception {
        try {
            em.persist(compraPedido);
        } catch (Exception ex) {
            if (findCompraPedido(compraPedido.getCompraPedidoId()) != null) {
                throw new PreexistingEntityException("Pedido " + compraPedido.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    @Override
    public void edit(CompraPedido compraPedido) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            compraPedido = em.merge(compraPedido);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = compraPedido.getCompraPedidoId();
                if (findCompraPedido(id) == null) {
                    throw new NonexistentEntityException("The compraCesta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    @Override
    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        CompraPedido compraPedido;
        try {
            compraPedido = em.getReference(CompraPedido.class, id);
            compraPedido.getCompraPedidoId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The CompraPedido with id " + id + " no longer exists.", enfe);
        }
        em.remove(compraPedido);
    }

    @Override
    public List<CompraPedido> findCompraPedidoEntities() {
        return findCompraPedidoEntities(true, -1, -1);
    }

    @Override
    public List<CompraPedido> findCompraPedidoEntities(int maxResults, int firstResult) {
        return findCompraPedidoEntities(false, maxResults, firstResult);
    }

    public List<CompraPedido> findCompraPedidoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(CompraPedido.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public CompraPedido findCompraPedido(String id) {
        return em.find(CompraPedido.class, id);
    }

    public int getPedidoCabCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<CompraPedido> rt = cq.from(CompraPedido.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }


}
