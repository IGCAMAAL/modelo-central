/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.pur.service.interfaces;

import com.centraldecompras.modelo.CompraPedidoCabLin;
import com.centraldecompras.zglobal.exceptions.IllegalOrphanException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;


/**
 *
 * @author ciberado
 */
public interface CompraPedidoCabLinService {

    void create_C(CompraPedidoCabLin compraPedidoCabLin) throws PreexistingEntityException, Exception;

    void create(CompraPedidoCabLin compraPedidoCabLin) throws PreexistingEntityException, Exception;

    void edit(CompraPedidoCabLin compraPedidoCabLin) throws IllegalOrphanException, NonexistentEntityException, Exception;

    void destroy(String id) throws IllegalOrphanException, NonexistentEntityException;

    List<CompraPedidoCabLin> findCompraPedidoCabLinEntities();

    List<CompraPedidoCabLin> findCompraPedidoCabLinEntities(int maxResults, int firstResult);

    List<CompraPedidoCabLin> findCompraPedidoCabLinEntities(boolean all, int maxResults, int firstResult);

    int getCompraPedidoCabLinCount();

    Long findUltimaLinCompraPedidoCabLin(String compraPedidoIdk);

    CompraPedidoCabLin findCompraPedidoCabLin(String compraPedidoIdk, int compraPedidoCabLin);
}
