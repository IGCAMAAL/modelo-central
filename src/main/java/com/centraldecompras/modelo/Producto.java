package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_MAT_Producto")
@XmlRootElement
@NamedQueries({                          
   @NamedQuery(name = "Producto.findProductosBy_ParentId", 
           query = "SELECT d FROM Producto d WHERE d.padreJerarquia = :padreJerarquia"),
    
   @NamedQuery(name = "Producto.findProductoEmpiezaTypeAheadTodo", 
           query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
                   + "      p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion, "
                   + "      p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt IN (:nivelesInt) "
                   + "  AND d.idioma = :idioma "
                   + "  AND d.traduccionDesc LIKE :descripcion "
                   + ""),
   
   @NamedQuery(name = "Producto.findProductoEmpiezaTypeAheadSelEstado", 
           query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
                   + "      p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
                   + "      p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt IN (:nivelesInt) "
                   + "  AND p.atributosAuditoria.estadoRegistro = :estadoRegistro "
                   + "  AND d.idioma = :idioma "
                   + "  AND d.traduccionDesc LIKE :descripcion "
                   + ""),
   
   @NamedQuery(name = "Producto.findProductoContieneTypeAheadTodo", 
           query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
                   + "      p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
                   + "      p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt IN (:nivelesInt) "
                   + "  AND d.idioma = :idioma "
                   + "  AND d.traduccionDesc LIKE :descripcion "
                   + "  AND d.traduccionDesc NOT LIKE :descipcionExclude"
                   + ""),
   
   @NamedQuery(name = "Producto.findProductoContieneTypeAheadSelEstado", 
           query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
                   + "      p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
                   + "      p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt IN (:nivelesInt) "
                   + "  AND p.atributosAuditoria.estadoRegistro = :estadoRegistro "
                   + "  AND d.idioma = :idioma "
                   + "  AND d.traduccionDesc LIKE :descripcion "
                   + "  AND d.traduccionDesc NOT LIKE :descipcionExclude  "
                   + ""),
   @NamedQuery(name = "Producto.findProductosBy_n_i_d_t", 
           query = " SELECT p.idProducto, p.nivelInt "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt = :niveleInt "
                   + "  AND p.padreJerarquia.idProducto = :idPadreJerarquia "
                   + "  AND d.idioma = :idioma "
                   + "  AND d.traduccionDesc LIKE :traduccionDesc "
                   + ""),
   @NamedQuery(name = "Producto.findProductosBy_n_i_d", 
           query = " SELECT p.idProducto, p.nivelInt "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt = :niveleInt "
                   + "  AND p.padreJerarquia.idProducto = :idPadreJerarquia "
                   + "  AND d.idioma = :idioma "
                   + ""),
   @NamedQuery(name = "Producto.findProductosBy_n_i_d_e", 
           query = " SELECT p.idProducto, p.nivelInt "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt = :niveleInt "
                   + "  AND p.padreJerarquia.idProducto = :idPadreJerarquia "
                   + "  AND p.atributosAuditoria.estadoRegistro = :estado "                   
                   + "  AND d.idioma = :idioma "
                   + ""),
   @NamedQuery(name = "Producto.findProductoBy_n_d_t", 
           query = " SELECT p "
                   + " FROM Producto p, "
                   + "      ProductoLang d "
                   + "WHERE p.idProducto = d.productoId "
                   + "  AND p.nivelInt = :niveleInt "
                   + "  AND d.idioma = :idioma "
                   + "  AND d.traduccionDesc = :traduccionDesc "
                   + ""),
   
       @NamedQuery(name = "Producto.findProductosBy_idP", query = ""
            + "  SELECT p "
            + "    FROM Producto p "
            + "   WHERE p.route          LIKE '%.' || :productoId || '.%' "                        
            + "     AND p.routeType         = 'Z'                         ")
        
})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Producto.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idProducto;

    @Version
    private int version;

    @Basic
    private int nivelInt;
    
    @Basic
    @Column(name = "idPadreJerarquia", nullable = false, updatable = true)
    private String idPadreJerarquia;
 
    @Basic
    @Column(name = "idNivelProducto", nullable = false, updatable = true)
    private int idNivelProducto;
    
    private String descripcion;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *    
    @ManyToOne(targetEntity=NivelProducto.class)
    private NivelProducto nivelProducto;

    @ManyToOne(targetEntity=Producto.class)
    private Producto padreJerarquia;

    @ManyToOne(targetEntity=SectorRpl.class)
    private SectorRpl sector;

    @ManyToOne(targetEntity=UnidadMedida.class)
    private UnidadMedida unidadMedida;

    @Basic
    @Column(length = 1)
    private String routeType;
    
    @Basic
    @Column(length = 361)    // 1 + (longId+1) x 10; Para longId=35;  1 + (36x10) = 361  
    private String route;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Producto() {
    }

    public Producto(String idProducto, int nivelInt, String idPadreJerarquia, int idNivelProducto, String descripcion, NivelProducto nivelProducto, Producto padreJerarquia, SectorRpl sector, UnidadMedida unidadMedida, String routeType, String route, DatosAuditoria atributosAuditoria) {
        this.idProducto = idProducto;
        this.nivelInt = nivelInt;
        this.idPadreJerarquia = idPadreJerarquia;
        this.idNivelProducto = idNivelProducto;
        this.descripcion = descripcion;
        this.nivelProducto = nivelProducto;
        this.padreJerarquia = padreJerarquia;
        this.sector = sector;
        this.unidadMedida = unidadMedida;
        this.routeType = routeType;
        this.route = route;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getNivelInt() {
        return nivelInt;
    }

    public void setNivelInt(int nivelInt) {
        this.nivelInt = nivelInt;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getIdPadreJerarquia() {
        return idPadreJerarquia;
    }

    public void setIdPadreJerarquia(String idPadreJerarquia) {
        this.idPadreJerarquia = idPadreJerarquia;
    }

    public int getIdNivelProducto() {
        return idNivelProducto;
    }

    public void setIdNivelProducto(int idNivelProducto) {
        this.idNivelProducto = idNivelProducto;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Producto getPadreJerarquia() {
        return padreJerarquia;
    }

    public void setPadreJerarquia(Producto padreJerarquia) {
        this.padreJerarquia = padreJerarquia;
    }

    public NivelProducto getNivelProducto() {
        return nivelProducto;
    }

    public void setNivelProducto(NivelProducto nivelProducto) {
        this.nivelProducto = nivelProducto;
    }


    public SectorRpl getSector() {
        return sector;
    }

    public void setSector(SectorRpl sector) {
        this.sector = sector;
    }

    public UnidadMedida getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(UnidadMedida unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.nivelProducto);
        hash = 79 * hash + Objects.hashCode(this.padreJerarquia);
        hash = 79 * hash + Objects.hashCode(this.sector);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (!Objects.equals(this.nivelProducto, other.nivelProducto)) {
            return false;
        }
        if (!Objects.equals(this.padreJerarquia, other.padreJerarquia)) {
            return false;
        }
        if (!Objects.equals(this.sector, other.sector)) {
            return false;
        }
        return true;
    }


}
