package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Embedded;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_MAT_ArticuloProducto", 
     //  indexes = {@Index (columnList = "nifnrf")},
     uniqueConstraints = {@UniqueConstraint(columnNames={"productoIdk", "articuloIdk"})} )
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArticuloProducto.findArticuloProductoBy_A_P", query = 
            " SELECT d "
          + "   FROM ArticuloProducto d "
          + "  WHERE d.productoIdk = :productoId "
          + "    AND d.articuloIdk = :articuloId "),
    
    @NamedQuery(name = "ArticuloProducto.findProductosBy_A", query = 
            " SELECT d.producto "
          + "   FROM   ArticuloProducto d "
          + "  WHERE d.articuloIdk = :articuloId "),
    
    @NamedQuery(name = "ArticuloProducto.deleteArticuloProductoBy_A_P", query =  
            " DELETE "
          + "   FROM ArticuloProducto d "
          + "  WHERE d.productoIdk = :productoId "
          + "    AND d.articuloIdk = :articuloId"),
        
    @NamedQuery(name = "ArticuloProducto.findArticuloProductoEmpiezaTypeAheadTodo", query = 
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro,          s.atributosAuditoria.ultimaAccion, "
            + "        s.atributosAuditoria.fechaPrevistaActivacion, s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProducto p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoIdk "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "     
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND p.sociedadIdk = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ),

    @NamedQuery(name = "ArticuloProducto.findArticuloProductoEmpiezaTypeAheadSelEstado", query = 
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro,          s.atributosAuditoria.ultimaAccion ,"
            + "        s.atributosAuditoria.fechaPrevistaActivacion, s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProducto p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoIdk "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "   
            + "    AND s.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND p.sociedadIdk = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ),

    @NamedQuery(name = "ArticuloProducto.findArticuloProductoContieneTypeAheadTodo", query = 
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro,          s.atributosAuditoria.ultimaAccion ,"
            + "        s.atributosAuditoria.fechaPrevistaActivacion, s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProducto p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoIdk "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "   
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND d.traduccionNomb NOT LIKE :descipcionExclude"
            + "    AND p.sociedadIdk = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ),

    @NamedQuery(name = "ArticuloProducto.findArticuloProductoContieneTypeAheadSelEstado", query =
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro, s.atributosAuditoria.ultimaAccion ,"
            + "        s.atributosAuditoria.fechaPrevistaActivacion ,s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProducto p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoIdk "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "   
            + "    AND s.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND d.traduccionNomb NOT LIKE :descipcionExclude  "
            + "    AND p.sociedadIdk = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ),
        
        
    @NamedQuery(name = "ArticuloProducto.findArticuloProductoAvatarEmpiezaTypeAheadTodo", query = 
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro,          s.atributosAuditoria.ultimaAccion, "
            + "        s.atributosAuditoria.fechaPrevistaActivacion, s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProductoAvatar p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoId "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "   
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND p.sociedadId = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ),

    @NamedQuery(name = "ArticuloProducto.findArticuloProductoAvatarEmpiezaTypeAheadSelEstado", query = 
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro,          s.atributosAuditoria.ultimaAccion ,"
            + "        s.atributosAuditoria.fechaPrevistaActivacion, s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProductoAvatar p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoId "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "   
            + "    AND s.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND p.sociedadId = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ),

    @NamedQuery(name = "ArticuloProducto.findArticuloProductoAvatarContieneTypeAheadTodo", query = 
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro,          s.atributosAuditoria.ultimaAccion ,"
            + "        s.atributosAuditoria.fechaPrevistaActivacion, s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProductoAvatar p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoId "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "   
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND d.traduccionNomb NOT LIKE :descipcionExclude"
            + "    AND p.sociedadId = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ),

    @NamedQuery(name = "ArticuloProducto.findArticuloProductoAvatarContieneTypeAheadSelEstado", query =
              " SELECT s.articuloIdk, "
            + "        s.atributosAuditoria.estadoRegistro, s.atributosAuditoria.ultimaAccion ,"
            + "        s.atributosAuditoria.fechaPrevistaActivacion ,s.atributosAuditoria.fechaPrevistaDesactivacion, "
            + "        s.productoIdk "        
            + "   FROM ArticuloProducto s, "
            + "        Articulo a, "
            + "        SocProductoAvatar p, "
            + "        ArticuloLangNomb d "
            + "  WHERE s.productoIdk = p.productoId "
            + "    AND s.articuloIdk = d.articuloIdk "
            + "    AND s.articuloIdk = a.id "   
            + "    AND s.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "    AND d.idioma = :idioma "
            + "    AND d.traduccionNomb LIKE :descripcion "
            + "    AND d.traduccionNomb NOT LIKE :descipcionExclude  "
            + "    AND p.sociedadId = :sociedadIdk  "
            + "    AND a.sociedad.id = :sociedadIdk  "
    ) 
    
})
public class ArticuloProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ArticuloProducto.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    private String productoIdk;

    @Id
    private String articuloIdk;

    @Version
    private int version;
    
    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = Producto.class)
    private Producto producto;

    @ManyToOne(targetEntity = Articulo.class)
    private Articulo articulo;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ArticuloProducto() {

    }

    public ArticuloProducto(String productoIdk, String articuloIdk, Producto producto, Articulo articulo, DatosAuditoria atributosAuditoria) {
        this.productoIdk = productoIdk;
        this.articuloIdk = articuloIdk;
        this.producto = producto;
        this.articulo = articulo;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    public String getProductoIdk() {
        return productoIdk;
    }

    public void setProductoIdk(String productoIdk) {
        this.productoIdk = productoIdk;
    }

    public String getArticuloIdk() {
        return articuloIdk;
    }

    public void setArticuloIdk(String articuloIdk) {
        this.articuloIdk = articuloIdk;
    }


    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Producto getProducto() {
        return this.producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Articulo getArticulo() {
        return this.articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloProducto other = (ArticuloProducto) obj;
        if (this.producto != other.producto && (this.producto == null || !this.producto.equals(other.producto))) {
            return false;
        }
        if (this.articulo != other.articulo && (this.articulo == null || !this.articulo.equals(other.articulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ArticuloProducto{" + "producto=" + producto + ", articulo=" + articulo + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
