package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_MAT_ArticuloEtiqueta")
@XmlRootElement
@NamedQueries({                          
   @NamedQuery(name = "ArticuloEtiqueta.findEtiquetasBy_idA", query = ""
           + "  SELECT d "
           + "    FROM ArticuloEtiqueta d "
           + "   WHERE d.articuloIdk = :articuloIdk "),
    
   @NamedQuery(name = "ArticuloEtiqueta.findArticuloEtiquetaBy_idA_idE", query = ""
           + "  SELECT d "
           + "    FROM ArticuloEtiqueta d "
           + "   WHERE d.articuloIdk = :articuloIdk "
           + "     AND d.etiquetaIdk = :etiquetaIdk "),
   
   @NamedQuery(name = "ArticuloEtiqueta.deleteArticuloEtiquetaBy_idA_idE", query = ""
           + "  DELETE "
           + "    FROM ArticuloEtiqueta d "
           + "   WHERE d.articuloIdk = :articuloIdk "
           + "     AND d.etiquetaIdk = :etiquetaIdk ")
})
public class ArticuloEtiqueta implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ArticuloEtiqueta.class.getName());


    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id@NotNull@Column(name = "articuloIdk", length = 36, nullable = false, updatable = false)
    private String articuloIdk;

    @Id@NotNull@Column(name = "etiquetaIdk", length = 36, nullable = false, updatable = false)
    private String etiquetaIdk;
    
    @Version
    private int version;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *    
    @ManyToOne(targetEntity = Articulo.class)
    private Articulo articulo;

    @ManyToOne(targetEntity = Etiqueta.class)
    private Etiqueta etiqueta;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ArticuloEtiqueta() {
    }

    public ArticuloEtiqueta(String articuloIdk, String etiquetaIdk, Articulo articulo, Etiqueta etiqueta, DatosAuditoria atributosAuditoria) {
        this.articuloIdk = articuloIdk;
        this.etiquetaIdk = etiquetaIdk;
        this.articulo = articulo;
        this.etiqueta = etiqueta;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getArticuloIdk() {
        return articuloIdk;
    }

    public void setArticuloIdk(String articuloIdk) {
        this.articuloIdk = articuloIdk;
    }

    public String getEtiquetaIdk() {
        return etiquetaIdk;
    }

    public void setEtiquetaIdk(String etiquetaIdk) {
        this.etiquetaIdk = etiquetaIdk;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }


    public Etiqueta getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(Etiqueta etiqueta) {
        this.etiqueta = etiqueta;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.articuloIdk);
        hash = 19 * hash + Objects.hashCode(this.etiquetaIdk);
        hash = 19 * hash + Objects.hashCode(this.articulo);
        hash = 19 * hash + Objects.hashCode(this.etiqueta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloEtiqueta other = (ArticuloEtiqueta) obj;
        if (!Objects.equals(this.articuloIdk, other.articuloIdk)) {
            return false;
        }
        if (!Objects.equals(this.etiquetaIdk, other.etiquetaIdk)) {
            return false;
        }
        if (!Objects.equals(this.articulo, other.articulo)) {
            return false;
        }
        if (!Objects.equals(this.etiqueta, other.etiqueta)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ArticuloEtiqueta{" + "articuloIdk=" + articuloIdk + ", etiquetaIdk=" + etiquetaIdk + ", version=" + version + ", articulo=" + articulo + ", etiqueta=" + etiqueta + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
