package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_LGT_SocZonGeo",
        indexes = {
            @Index(name = "index_SocZonGeo_sociedadid", columnList = "sociedadid", unique = false)},
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"zonaGeograficaid", "sociedadid"})}
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SocZonGeo.findSocZonGeoBy_idZidS", query = " "
            + " SELECT d "
            + "   FROM SocZonGeo d "
            + "  WHERE d.zonaGeograficaid = :zonaGeograficaid "
            + "    AND d.sociedadid = :sociedadid"
    ),
    
    @NamedQuery(name = "SocZonGeo.findSocZonGeoBy_idS", query = " "
            + " SELECT d "
            + "   FROM SocZonGeo d "
            + "  WHERE d.sociedadid = :sociedadid"
    ),
        
   @NamedQuery(name = "SocZonGeo.findsocZonasGeograficasOrderBy_idS_N", query = ""
           + "   SELECT d "
           + "     FROM SocZonGeo d "
           + " ORDER BY d.sociedadid, d.zonaGeografica.nivelInt, d.zonaGeografica.idPadreJerarquia " ),

   @NamedQuery(name = "SocZonGeo.findSocZonGeoBy_idS_idZ_Estado", query = ""
           + "   SELECT d "
           + "     FROM SocZonGeo d "
            + "  WHERE d.sociedad.idSociedad                = :sociedadId"
            + "    AND d.zonaGeografica.idZonaGeografica    = :zonaGeograficaId"
            + "    AND d.atributosAuditoria.estadoRegistro  = :estado"
    )
   
})
public class SocZonGeo implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SocZonGeo.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSocZonGeo;

    @Version
    private int version;
    
    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * 
    @NotNull
    private String zonaGeograficaid;

    @NotNull
    private String sociedadid;

    private Boolean asignada;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * 
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl sociedad;

    @ManyToOne(targetEntity = ZonaGeografica.class)
    private ZonaGeografica zonaGeografica;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SocZonGeo() {
    }

    public SocZonGeo(String idSocZonGeo, String zonaGeograficaid, String sociedadid, Boolean asignada, SociedadRpl sociedad, ZonaGeografica zonaGeografica, DatosAuditoria atributosAuditoria) {
        this.idSocZonGeo = idSocZonGeo;
        this.zonaGeograficaid = zonaGeograficaid;
        this.sociedadid = sociedadid;
        this.asignada = asignada;
        this.sociedad = sociedad;
        this.zonaGeografica = zonaGeografica;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getIdSocZonGeo() {
        return idSocZonGeo;
    }

    public void setIdSocZonGeo(String idSocZonGeo) {
        this.idSocZonGeo = idSocZonGeo;
    }

    public String getZonaGeograficaid() {
        return zonaGeograficaid;
    }

    public void setZonaGeograficaid(String zonaGeograficaid) {
        this.zonaGeograficaid = zonaGeograficaid;
    }

    public String getSociedadid() {
        return sociedadid;
    }

    public void setSociedadid(String sociedadid) {
        this.sociedadid = sociedadid;
    }

    public Boolean getAsignada() {
        return asignada;
    }

    public void setAsignada(Boolean asignada) {
        this.asignada = asignada;
    }

    public SociedadRpl getSociedad() {
        return this.sociedad;
    }

    public void setSociedad(SociedadRpl sociedad) {
        this.sociedad = sociedad;
    }

    public ZonaGeografica getZonaGeografica() {
        return this.zonaGeografica;
    }

    public void setZonaGeografica(ZonaGeografica zonaGeografica) {
        this.zonaGeografica = zonaGeografica;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.sociedad != null ? this.sociedad.hashCode() : 0);
        hash = 29 * hash + (this.zonaGeografica != null ? this.zonaGeografica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SocZonGeo other = (SocZonGeo) obj;
        if (this.sociedad != other.sociedad && (this.sociedad == null || !this.sociedad.equals(other.sociedad))) {
            return false;
        }
        if (this.zonaGeografica != other.zonaGeografica && (this.zonaGeografica == null || !this.zonaGeografica.equals(other.zonaGeografica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SocZonGeo{" + "idSocZonGeo=" + idSocZonGeo + ", zonaGeograficaid=" + zonaGeograficaid + ", sociedadid=" + sociedadid + ", asignada=" + asignada + ", sociedad=" + sociedad + ", zonaGeografica=" + zonaGeografica + ", version=" + version + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
