package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class FormatoVtaLangPK implements Serializable {

    @NotNull
    private String formatoVtaId;

    @NotNull
    private String idioma;

    public FormatoVtaLangPK() {
    }

    public FormatoVtaLangPK(String formatoVtaId, String idioma) {
        this.formatoVtaId = formatoVtaId;
        this.idioma = idioma;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.formatoVtaId);
        hash = 17 * hash + Objects.hashCode(this.idioma);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormatoVtaLangPK other = (FormatoVtaLangPK) obj;
        if (!Objects.equals(this.formatoVtaId, other.formatoVtaId)) {
            return false;
        }
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FormatoVtaLangPK{" + "formatoVtaId=" + formatoVtaId + ", idioma=" + idioma + '}';
    }

    


}
