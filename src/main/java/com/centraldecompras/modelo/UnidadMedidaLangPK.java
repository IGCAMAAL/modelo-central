package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class UnidadMedidaLangPK implements Serializable {

    @NotNull
    private String unidadMedidaId;

    @NotNull
    private String idioma;

    public UnidadMedidaLangPK() {
    }

    public UnidadMedidaLangPK(String unidadMedidaId, String idioma) {
        this.unidadMedidaId = unidadMedidaId;
        this.idioma = idioma;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.unidadMedidaId);
        hash = 59 * hash + Objects.hashCode(this.idioma);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadMedidaLangPK other = (UnidadMedidaLangPK) obj;
        if (!Objects.equals(this.unidadMedidaId, other.unidadMedidaId)) {
            return false;
        }
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnidadMedidaLangPK{" + "unidadMedidaId=" + unidadMedidaId + ", idioma=" + idioma + '}';
    }




}
