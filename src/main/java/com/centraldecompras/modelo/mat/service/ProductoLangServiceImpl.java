package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoLangService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductoLangServiceImpl implements ProductoLangService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductoLangServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(ProductoLang productoLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(productoLang);
        } catch (Exception ex) {
            if (findProductoLangBy_IdP_Lang(productoLang.getProductoId(), productoLang.getIdioma()) != null) {
                throw new PreexistingEntityException("productoLang " + productoLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(ProductoLang productoLang) throws PreexistingEntityException, Exception {
        try {
            em.persist(productoLang);
        } catch (Exception ex) {
            if (findProductoLangBy_IdP_Lang(productoLang.getProductoId(), productoLang.getIdioma()) != null) {
                throw new PreexistingEntityException("productoLang " + productoLang + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ProductoLang productoLang) throws NonexistentEntityException, Exception {
        try {
            productoLang = em.merge(productoLang);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = productoLang.getProductoId();
                if (findProductoLangBy_IdP_Lang(productoLang.getProductoId(), productoLang.getIdioma()) == null) {
                    throw new NonexistentEntityException("The productoLang with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ProductoLang productoLang) throws NonexistentEntityException {
        //TODO 
    }

    public List<ProductoLang> findProductoLangEntities() {
        return findProductoLangEntities(true, -1, -1);
    }

    public List<ProductoLang> findProductoBLangEntities(int maxResults, int firstResult) {
        return findProductoLangEntities(false, maxResults, firstResult);
    }

    private List<ProductoLang> findProductoLangEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ProductoLang.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public ProductoLang findProductoLang(String id) {

        return em.find(ProductoLang.class, id);

    }

    public int getProductoLangCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ProductoLang> rt = cq.from(ProductoLang.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

    public List<ProductoLang> findProductoLangBy_P(Producto producto) {
        List<ProductoLang> reply = null;
        reply = (List<ProductoLang>) em.createNamedQuery("ProductoLang.findProductoLangBy_P")
                .setParameter("producto", producto)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<ProductoLang> findProductoLangBy_P_Lang(ProductoLang productoLang) {
        List<ProductoLang> reply = null;
        reply = (List<ProductoLang>) em.createNamedQuery("ProductoLang.findProductoLangBy_P_Lang")
                .setParameter("productoLang", productoLang.getProductoEntity())
                .setParameter("idioma", productoLang.getIdioma())
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<ProductoLang> findProductoLangBy_IdP(String productoId) {
        List<ProductoLang> reply = null;
        try {
            reply = (List<ProductoLang>) em.createNamedQuery("ProductoLang.findProductoLangBy_IdP")
                    .setParameter("productoId", productoId)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para productoId=" + productoId);
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public ProductoLang findProductoLangBy_IdP_Lang(String productoId, String idioma) {
        ProductoLang productoLang = null;
        try {
            productoLang = (ProductoLang) em.createNamedQuery("ProductoLang.findProductoLangBy_IdP_Lang")
                    .setParameter("productoId", productoId)
                    .setParameter("idioma", idioma)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para productoId=" + productoId);
        }
        return productoLang;
    }

    public int deleteProductoLangBy_IdP_Lang(String productoId, String idioma) {
        int num = 0;
        try {
            num = em.createNamedQuery("ProductoLang.deleteProductoLangBy_IdP_Lang")
                    .setParameter("productoId", productoId)
                    .setParameter("idioma", idioma)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para productoId=" + productoId);
        }
        return num;
    }

}
