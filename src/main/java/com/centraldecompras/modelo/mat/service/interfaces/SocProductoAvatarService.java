package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocProductoAvatar;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface SocProductoAvatarService {

    void create(SocProductoAvatar socProductoAvatar) throws PreexistingEntityException, Exception;

    void edit(SocProductoAvatar socProductoAvatar) throws NonexistentEntityException, Exception;

    SocProductoAvatar findSocProductoAvatarBy_idS_idP(String sociedadId, String productoId);

    int deleteSocProductoAvatarBy_idS_idP(String sociedadId, String productoId);

    List<String[]> findSocProductoAvatarTypeAhead(int tipo, String cadena, String estado, int cantidadInt, List<Integer> nivelesList, String idioma, String sociedadIdk);
}
