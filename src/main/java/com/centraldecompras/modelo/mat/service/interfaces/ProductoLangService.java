package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoLang;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ProductoLangService {

    ProductoLang findProductoLang(String id);

    void create(ProductoLang productoLang) throws PreexistingEntityException, Exception;

    void create_C(ProductoLang productoLang) throws PreexistingEntityException, Exception;

    void edit(ProductoLang productoLang) throws NonexistentEntityException, Exception;

    List<ProductoLang> findProductoLangBy_P(Producto formatoVta);

    List<ProductoLang> findProductoLangBy_P_Lang(ProductoLang productoLang);

    ProductoLang findProductoLangBy_IdP_Lang(String productoId, String idioma);

    List<ProductoLang> findProductoLangBy_IdP(String productoId);

    int deleteProductoLangBy_IdP_Lang(String productoId, String idioma);
}
