package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.ArticuloEtiqueta;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ArticuloEtiquetaService { 

    void create(ArticuloEtiqueta articuloEtiqueta) throws PreexistingEntityException, Exception;

    void edit(ArticuloEtiqueta articuloEtiqueta) throws NonexistentEntityException, Exception;

    void destroy(ArticuloEtiqueta articuloEtiqueta) throws NonexistentEntityException;

    List<ArticuloEtiqueta> findArticuloEtiquetaEntities();

    List<ArticuloEtiqueta> findArticuloEtiquetaEntities(int maxResults, int firstResult);

    List<ArticuloEtiqueta> findArticuloEtiquetaEntities(boolean all, int maxResults, int firstResult);

    int getArticuloEtiquetaCount();

    ArticuloEtiqueta findArticuloEtiquetaBy_idA_idE(String articuloIdk, String etiquetaIdk);

    List<ArticuloEtiqueta> findEtiquetasBy_idA(String articuloIdk);

    int deleteArticuloEtiquetaBy_idA_idE(String articuloIdk, String etiquetaIdk);

}
