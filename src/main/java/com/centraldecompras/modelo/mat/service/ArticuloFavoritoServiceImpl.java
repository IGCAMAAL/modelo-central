package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.ArticuloFavorito;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloFavoritoService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class ArticuloFavoritoServiceImpl implements ArticuloFavoritoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloFavoritoServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(ArticuloFavorito articuloFavorito) throws PreexistingEntityException, Exception { 
        try {
            em.persist(articuloFavorito);
        } catch (Exception ex) {
            if (findArticuloFavoritoBy_idS_idA_idU(
                    articuloFavorito.getSociedadIdk(),
                    articuloFavorito.getArticuloIdk(),
                    articuloFavorito.getUsuarioIdk()
            ) != null) {
                throw new PreexistingEntityException("ArticuloFavorito " + articuloFavorito.toString() + " already exists.", ex); 
            }
            throw ex;
        }
    }

    public void edit(ArticuloFavorito articuloFavorito) throws NonexistentEntityException, Exception {
        try {
            articuloFavorito = em.merge(articuloFavorito);  
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findArticuloFavoritoBy_idS_idA_idU(
                    articuloFavorito.getSociedadIdk(),
                    articuloFavorito.getArticuloIdk(),
                    articuloFavorito.getUsuarioIdk()
                ) == null) {
                    throw new NonexistentEntityException("ArticuloFavorito " + articuloFavorito.toString() + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ArticuloFavorito articuloFavorito) throws NonexistentEntityException {  
        //TODO 
    }

    public List<ArticuloFavorito> findArticuloFavoritoEntities() {
        return findArticuloFavoritoEntities(true, -1, -1);
    }

    public List<ArticuloFavorito> findArticuloFavoritoEntities(int maxResults, int firstResult) {
        return findArticuloFavoritoEntities(false, maxResults, firstResult);
    }

    public List<ArticuloFavorito> findArticuloFavoritoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ArticuloFavorito.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getArticuloFavoritoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ArticuloFavorito> rt = cq.from(ArticuloFavorito.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public ArticuloFavorito findArticuloFavoritoBy_idS_idA_idU(String sociedadIdk, String articuloIdk,  String usuarioIdk) {
        ArticuloFavorito reply = null;
        try {
            reply = (ArticuloFavorito) em.createNamedQuery("ArticuloFavorito.findArticuloFavoritoBy_idS_idA_idU")
                    .setParameter("sociedadIdk", sociedadIdk)
                    .setParameter("articuloIdk", articuloIdk)
                    .setParameter("usuarioIdk", usuarioIdk)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación articulo, asociado, usuario dada:  " + articuloIdk + " / " + sociedadIdk + " / " + usuarioIdk);
        }
        return reply;
    }

    public int deleteArticuloFavoritoBy_idS_idA_idU(String sociedadIdk, String articuloIdk, String usuarioIdk) {
        int reply = 0;
        try {
            reply = em.createNamedQuery("ArticuloFavorito.deleteArticuloFavoritoBy_idS_idA_idU")
                    .setParameter("sociedadIdk", sociedadIdk)
                    .setParameter("articuloIdk", articuloIdk)
                    .setParameter("usuarioIdk", usuarioIdk)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación articulo, asociado, usuario dada:  " + articuloIdk + " / " + sociedadIdk + " / " + usuarioIdk);
        }
        return reply;
    }


}
