package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.ArticuloProducto;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface ArticuloProductoService {

    ArticuloProducto findArticuloProductoBy_A_P(String articuloId, String productoId);

    Producto findProductosBy_A(String articuloId);

    int deleteArticuloProductoBy_A_P(String articuloId, String productoId) throws NonexistentEntityException;

    void create(ArticuloProducto articuloProducto) throws PreexistingEntityException, Exception;

    void edit(ArticuloProducto articuloProducto) throws NonexistentEntityException, Exception;

    List<ArticuloProducto> findArticuloProductoEntities(boolean all, int maxResults, int firstResult);

    List<String[]> findArticuloProductoTypeAhead(int tipo, String cadena, String estado, int cantidadInt, String idioma, String sociedadIdk);

    List<String[]> findArticuloProductoAvatarTypeAhead(int tipo, String cadena, String estado, int cantidadInt, String idioma, String sociedadIdk);
    
}
