/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.ArticuloProducto;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloProductoService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel
 */
@Service
public class ArticuloProductoServiceImpl implements ArticuloProductoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloProductoServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(ArticuloProducto articuloProducto) throws PreexistingEntityException, Exception {
        try {
            em.persist(articuloProducto);
        } catch (Exception ex) {
            if (findArticuloProductoBy_A_P(articuloProducto.getArticulo().getIdArticulo(), articuloProducto.getProducto().getIdProducto()) != null) {
                throw new PreexistingEntityException("ArticuloProducto " + articuloProducto.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ArticuloProducto articuloProducto) throws NonexistentEntityException, Exception {
        try {
            articuloProducto = em.merge(articuloProducto);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findArticuloProductoBy_A_P(articuloProducto.getArticulo().getIdArticulo(), articuloProducto.getProducto().getIdProducto()) == null) {
                    throw new NonexistentEntityException("The articuloProducto " + articuloProducto.toString() + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ArticuloProducto articuloProducto) throws NonexistentEntityException {
        //TODO
    }

    public List<ArticuloProducto> findArticuloProductoEntities() {
        return findArticuloProductoEntities(true, -1, -1);
    }

    public List<ArticuloProducto> findArticuloProductoEntities(int maxResults, int firstResult) {
        return findArticuloProductoEntities(false, maxResults, firstResult);
    }

    public List<ArticuloProducto> findArticuloProductoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ArticuloProducto.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getArticuloProductoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ArticuloProducto> rt = cq.from(ArticuloProducto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public ArticuloProducto findArticuloProductoBy_A_P(String articuloId, String productoId) {
        ArticuloProducto reply = null;
        try {
            reply = (ArticuloProducto) em.createNamedQuery("ArticuloProducto.findArticuloProductoBy_A_P")
                    .setParameter("articuloId", articuloId)
                    .setParameter("productoId", productoId)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación Articulo / Producto dada:  " + articuloId + " / " + productoId);
        }
        return reply;
    }

    public Producto findProductosBy_A(String articuloId) {
        Producto reply = null;
        try {
            reply = (Producto) em.createNamedQuery("ArticuloProducto.findProductosBy_A")
                    .setParameter("articuloId", articuloId)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado Productos asociados al articulo:  " + articuloId);
        }
        return reply;
    }

    public int deleteArticuloProductoBy_A_P(String articuloId, String productoId) {
        int reply = 0;
        try {
            reply = em.createNamedQuery("ArticuloProducto.deleteArticuloProductoBy_A_P")
                    .setParameter("articuloId", articuloId)
                    .setParameter("productoId", productoId)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación Articulo / Producto dada:  " + productoId + " / " + productoId);
        }
        return reply;
    }

    public List<String[]> findArticuloProductoTypeAhead(int tipo, String cadena, String estado, int cantidadInt, String idioma, String sociedadIdk) {
        List<String[]> reply = new ArrayList();

        Query query = null;

        StringBuffer descripcionSql = new StringBuffer();
        StringBuffer descripcionExcludeSql = new StringBuffer();
        try {
            if (tipo == 1) {
                // Empieza
                descripcionSql.append(cadena).append("%");
                if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoEmpiezaTypeAheadTodo")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("idioma", idioma)
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);

                } else {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoEmpiezaTypeAheadSelEstado")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("estadoRegistro", estado)
                            .setParameter("idioma", idioma)
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);
                }

            } else {
                // Contiene
                descripcionSql.append("%").append(cadena).append("%");
                descripcionExcludeSql.append(cadena).append("%");

                if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoContieneTypeAheadTodo")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("idioma", idioma)
                            .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);

                } else {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoContieneTypeAheadSelEstado")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("idioma", idioma)
                            .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                            .setParameter("estadoRegistro", estado)
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);
                }
            }

            reply = query.getResultList();

        } catch (Exception ex) {
            try {
                ex.printStackTrace();
                throw new Exception("findArticuloProductoTypeAhead. " + ex.getMessage());
            } catch (Exception ex1) {
                Logger.getLogger(ArticuloProductoServiceImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }

        }

        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<String[]> findArticuloProductoAvatarTypeAhead(int tipo, String cadena, String estado, int cantidadInt, String idioma, String sociedadIdk) {
        List<String[]> reply = new ArrayList();

        Query query = null;

        StringBuffer descripcionSql = new StringBuffer();
        StringBuffer descripcionExcludeSql = new StringBuffer();
        try {
            if (tipo == 1) {
                // Empieza
                descripcionSql.append(cadena).append("%");
                if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoAvatarEmpiezaTypeAheadTodo")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("idioma", idioma)
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);

                } else {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoAvatarEmpiezaTypeAheadSelEstado")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("estadoRegistro", estado)
                            .setParameter("idioma", idioma)
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);
                }

            } else {
                // Contiene
                descripcionSql.append("%").append(cadena).append("%");
                descripcionExcludeSql.append(cadena).append("%");

                if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoAvatarContieneTypeAheadTodo")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("idioma", idioma)
                            .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);

                } else {
                    query = em.createNamedQuery("ArticuloProducto.findArticuloProductoAvatarContieneTypeAheadSelEstado")
                            .setParameter("descripcion", descripcionSql.toString())
                            .setParameter("idioma", idioma)
                            .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                            .setParameter("estadoRegistro", estado)
                            .setParameter("sociedadIdk", sociedadIdk)
                            .setMaxResults(cantidadInt);
                }
            }

            reply = query.getResultList();

        } catch (Exception ex) {
            try {
                ex.printStackTrace();
                throw new Exception("findArticuloProductoAvatarTypeAhead. " + ex.getMessage());
            } catch (Exception ex1) {
                Logger.getLogger(ArticuloProductoServiceImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }

        }

        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<Object[]> findArticuloProductoAvatarTypeAheadNuevo(int tipo, Map<String, Object> parametrosFiltro) {
        List<Object[]> reply = new ArrayList();
        try {
            StringBuffer cadenaArtSql = new StringBuffer();
            StringBuffer cadenaExcludeSql = new StringBuffer();

            Boolean count = Boolean.FALSE;
            Boolean typeAheadArt = Boolean.TRUE;
            Boolean etiquetas = (parametrosFiltro.containsKey("productosConEtiqueta")) ? Boolean.TRUE : Boolean.FALSE;
            Boolean estados = (parametrosFiltro.containsKey("estados")) ? Boolean.TRUE : Boolean.FALSE;

            if (tipo == 1) {  /* ------- Empieza ------------ */

                cadenaArtSql.append((String) parametrosFiltro.get("cadenaArt")).append("%");
            } else {  /* ------- Contiene ------------ */

                cadenaArtSql.append("%").append((String) parametrosFiltro.get("cadenaArt")).append("%");
                cadenaExcludeSql.append((String) parametrosFiltro.get("cadenaArt")).append("%");
                parametrosFiltro.put("cadenaExcludeSql", cadenaExcludeSql.toString());
            }
            parametrosFiltro.put("cadenaArtSql", cadenaArtSql.toString());

            reply = (List<Object[]>) findArticulosFor_Sel_TAhead_Qry_WithFilterDin(parametrosFiltro, count, etiquetas, estados, typeAheadArt, tipo).getResultList();

            if (reply == null) {
                reply = new ArrayList<>();
            }

        } catch (Exception ex) {
            log.warn(ex);
            throw (ex);
        }
        return reply;
    }

    private Query findArticulosFor_Sel_TAhead_Qry_WithFilterDin(Map<String, Object> parametrosFiltro, Boolean count, Boolean etiquetas, Boolean estados, Boolean typeAheadArt, int tipo) {

        StringBuffer qrySbf = new StringBuffer();

        qrySbf.append(" "
                + " SELECT ap.articuloIdk, ap.atributosAuditoria.estadoRegistro,          ap.atributosAuditoria.ultimaAccion ,"
                + "        ap.atributosAuditoria.fechaPrevistaActivacion, ap.atributosAuditoria.fechaPrevistaDesactivacion,         ap.productoIdk "
                + "   FROM ArticuloProducto            ap, "
                + "        ArticuloLangNomb            al  "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "  WHERE ap.producto.route                               LIKE        '%' || :productoId  || '.%'                          "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND   al.articuloIdk                                 =         ap.articulo.idArticulo                                 "
                + "        AND   al.idioma                                      =         :idioma                                                "
                //
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND  ap.articulo.sociedad.idSociedad                 =         :sociedadId                                                                             " // Productos que sirve el proveedor 
                //   -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
                + "        AND  ap.producto.route                               NOT IN      (  SELECT spe.route                                                                  " // que no estén excluidos en la jerarquia de asignación de productos.
                + "                                                                              FROM SocProductoExcl spe                                                        " //
                + "                                                                             WHERE spe.sociedadId    =   ap.articulo.sociedad.idSociedad                      " //                
                + "                                                                         )                                                                                    " //          
                //
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND   :hoy                                           BETWEEN             ap.producto.atributosAuditoria.fechaPrevistaActivacion                       "
                + "                                                                 AND CASE  WHEN  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999 "
                + "                                                                           ELSE  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion                    "
                + "                                                                     END                                                                                      "
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND   :hoy                                           BETWEEN             ap.articulo.atributosAuditoria.fechaPrevistaActivacion                       "
                + "                                                                 AND CASE  WHEN  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN 99999999  "
                + "                                                                           ELSE  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion                    "
                + "                                                                 END                                                                                          "
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND   :hoy                                           BETWEEN             ap.atributosAuditoria.fechaPrevistaActivacion                                "
                + "                                                                 AND CASE  WHEN  ap.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN 99999999           "
                + "                                                                           ELSE  ap.atributosAuditoria.fechaPrevistaDesactivacion                             "
                + "                                                                 END                                                                                          "
                //                                                              - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                + "        AND  ap.producto.atributosAuditoria.estadoRegistro   IN          (:estados)                                                                           " // Productos activs                                                        
                + "        AND  ap.articulo.atributosAuditoria.estadoRegistro   IN          (:estados)                                                                           " // Articulos activos                                
                + "        AND  ap.atributosAuditoria.estadoRegistro            IN          (:estados)                                                                           " // Relacion almacen / sociedad / producto activa                       
                //
                + "");

        qrySbf.append("QccQ_etiquetas_QccQ");
        qrySbf.append("QccQ_traduccionNombArticulo_QccQ");
        qrySbf.append("QccQ_excludeNombArticulo_QccQ");

        // -----------------------------------------------------------------------------------------------------------------------------------------------        
        String qryString = qrySbf.toString();

        if (etiquetas) {
            qryString = qryString.replaceAll("QccQ_etiquetas_QccQ", "    AND   ap.producto.idProducto                          IN       (:productosConEtiqueta)      ");
        } else {
            qryString = qryString.replaceAll("QccQ_etiquetas_QccQ", "");
        }

        if (typeAheadArt) {
            qryString = qryString.replaceAll("QccQ_traduccionNombArticulo_QccQ", "    AND  al.traduccionNomb                  LIKE      :cadenaArtSql                ");
            if (tipo == 2) {
                qryString = qryString.replaceAll("QccQ_excludeNombArticulo_QccQ", "       AND  al.traduccionNomb              NOT LIKE  :cadenaExcludeSql            ");
            } else {
                qryString = qryString.replaceAll("QccQ_excludeNombArticulo_QccQ", "");
            }
        } else {
            qryString = qryString.replaceAll("QccQ_traduccionNombArticulo_QccQ", "");
            qryString = qryString.replaceAll("QccQ_excludeNombArticulo_QccQ", "");
        }

        // -----------------------------------------------------------------------------------------------------------------------------------------------
        Query query = em.createQuery(qryString);
        query.setParameter("idioma", (String) parametrosFiltro.get("idioma"));
        query.setParameter("cadenaArtSql", (String) parametrosFiltro.get("cadenaArtSql"));        
        query.setParameter("hoy", (Integer) parametrosFiltro.get("hoy"));
        query.setParameter("sociedadId", (List<String>) parametrosFiltro.get("sociedadId"));

        if (etiquetas) {
            query.setParameter("productosConEtiqueta", (List<String>) parametrosFiltro.get("productosConEtiqueta"));
        }

        if (estados) {
            query.setParameter("estados", (List<String>) parametrosFiltro.get("estados"));
        }
        if (tipo == 2) {
            query.setParameter("cadenaExcludeSql", (String) parametrosFiltro.get("cadenaArtSql"));
        }

        query.setMaxResults((Integer) parametrosFiltro.get("cantidad"));
        query.setFirstResult(1);

        log.info("........... AhoraVAlaQRY .............................. AhoraVAlaQRY .............................. AhoraVAlaQRY .............................. AhoraVAlaQRY ...................");
        return query;
    }

}
