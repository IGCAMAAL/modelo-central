package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.ArticuloLangDesc;
import com.centraldecompras.modelo.ArticuloLangNomb;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangNombService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ArticuloLangNombServiceImpl implements ArticuloLangNombService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloLangNombServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(ArticuloLangNomb articuloLangNomb) throws PreexistingEntityException, Exception {
        try {
            em.persist(articuloLangNomb);
        } catch (Exception ex) {
            if (findArticuloLangNombBy_IdA_Lang(articuloLangNomb.getArticuloIdk(), articuloLangNomb.getIdioma()) != null) {
                throw new PreexistingEntityException("articuloLangNomb " + articuloLangNomb.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void create(ArticuloLangNomb articuloLangNomb) throws PreexistingEntityException, Exception {
        try {
            em.persist(articuloLangNomb);
        } catch (Exception ex) {
            if (findArticuloLangNombBy_IdA_Lang(articuloLangNomb.getArticuloIdk(), articuloLangNomb.getIdioma()) != null) {
                throw new PreexistingEntityException("articuloLangNomb " + articuloLangNomb.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ArticuloLangNomb articuloLangNomb) throws NonexistentEntityException, Exception {
        try {
            articuloLangNomb = em.merge(articuloLangNomb);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = articuloLangNomb.getArticuloIdk();
                if (findArticuloLangNombBy_IdA_Lang(articuloLangNomb.getArticuloIdk(), articuloLangNomb.getIdioma()) == null) {
                    throw new NonexistentEntityException("The articuloLangNomb with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ArticuloLangNomb articuloLangNomb) throws NonexistentEntityException {
        //TODO 
    }

    public List<ArticuloLangNomb> findArticuloLangNombEntities() {
        return findArticuloLangNombEntities(true, -1, -1);
    }

    public List<ArticuloLangNomb> findArticuloBLangNombEntities(int maxResults, int firstResult) {
        return findArticuloLangNombEntities(false, maxResults, firstResult);
    }

    private List<ArticuloLangNomb> findArticuloLangNombEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ArticuloLangNomb.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public ArticuloLangNomb findArticuloLangNomb(String id) {

        return em.find(ArticuloLangNomb.class, id);

    }

    public int getArticuloLangCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ArticuloLangNomb> rt = cq.from(ArticuloLangNomb.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

    public List<ArticuloLangNomb> findArticuloLangNombBy_A(Articulo articulo) {
        List<ArticuloLangNomb> reply = null;
        reply = (List<ArticuloLangNomb>) em.createNamedQuery("ArticuloLangNomb.findArticuloLangNombBy_A")
                .setParameter("articulo", articulo)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<ArticuloLangNomb> findArticuloLangNombBy_A_Lang(ArticuloLangNomb articuloLang) {
        List<ArticuloLangNomb> reply = null;
        reply = (List<ArticuloLangNomb>) em.createNamedQuery("ArticuloLangNomb.findArticuloLangNombBy_A_Lang")
                .setParameter("articuloLang", articuloLang.getArticuloIdk())
                .setParameter("idioma", articuloLang.getIdioma())
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<ArticuloLangNomb> findArticuloLangNombBy_IdA(String articuloId) {
        List<ArticuloLangNomb> reply = null;
        try {
            reply = (List<ArticuloLangNomb>) em.createNamedQuery("ArticuloLangNomb.findArticuloLangNombBy_IdA")
                    .setParameter("articuloId", articuloId)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para productoId=" + articuloId);
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public ArticuloLangNomb findArticuloLangNombBy_IdA_Lang(String articuloId, String idioma) {
        ArticuloLangNomb articuloLangNomb = null;
        try {
            articuloLangNomb = (ArticuloLangNomb) em.createNamedQuery("ArticuloLangNomb.findArticuloLangNombBy_IdA_Lang")
                    .setParameter("articuloId", articuloId)
                    .setParameter("idioma", idioma)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para articuloId=" + articuloId);
        }
        return articuloLangNomb;
    }

    public int deleteArticuloLangNombBy_IdA_Lang(String articuloId, String idioma) {
        int num = 0;
        try {
            num = em.createNamedQuery("ArticuloLangNomb.deleteArticuloLangNombBy_IdA_Lang")
                    .setParameter("articuloId", articuloId)
                    .setParameter("idioma", idioma)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para articuloId=" + articuloId);
        }
        return num;
    }

    public List<Object[]> findArticuloLangNombBy_idS_i(String sociedadId, String idioma) {
        List<Object[]> reply = new ArrayList<>();

        if (sociedadId != null && !sociedadId.equals("")) {
            reply = (List<Object[]>) em.createNamedQuery("ArticuloLangNomb.findArticuloBy_idS_i")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("idioma", idioma)
                    .getResultList();
        } else {
            reply = (List<Object[]>) em.createNamedQuery("ArticuloLangNomb.findArticuloBy_i")
                    .setParameter("idioma", idioma)
                    .getResultList();
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

}
