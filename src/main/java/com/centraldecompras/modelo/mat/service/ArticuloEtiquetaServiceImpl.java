package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.ArticuloEtiqueta;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloEtiquetaService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class ArticuloEtiquetaServiceImpl implements ArticuloEtiquetaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloEtiquetaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(ArticuloEtiqueta articuloEtiqueta) throws PreexistingEntityException, Exception {  
        try {
            em.persist(articuloEtiqueta);
        } catch (Exception ex) {
            if (findArticuloEtiquetaBy_idA_idE(articuloEtiqueta.getArticulo().getIdArticulo(), articuloEtiqueta.getEtiqueta().getIdEtiqueta() ) != null) {
                throw new PreexistingEntityException("ProductoEtiqueta " + articuloEtiqueta.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ArticuloEtiqueta articuloEtiqueta) throws NonexistentEntityException, Exception {  
        try {
            articuloEtiqueta = em.merge(articuloEtiqueta);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findArticuloEtiquetaBy_idA_idE(articuloEtiqueta.getArticulo().getIdArticulo(), articuloEtiqueta.getEtiqueta().getIdEtiqueta() ) == null) {
                    throw new NonexistentEntityException("ProductoEtiqueta " + articuloEtiqueta.toString() + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ArticuloEtiqueta articuloEtiqueta) throws NonexistentEntityException {
        //TODO 
    }

    public List<ArticuloEtiqueta> findArticuloEtiquetaEntities() {
        return findArticuloEtiquetaEntities(true, -1, -1);
    }

    public List<ArticuloEtiqueta> findArticuloEtiquetaEntities(int maxResults, int firstResult) {
        return findArticuloEtiquetaEntities(false, maxResults, firstResult);
    }

    public List<ArticuloEtiqueta> findArticuloEtiquetaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ArticuloEtiqueta.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getArticuloEtiquetaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ArticuloEtiqueta> rt = cq.from(ArticuloEtiqueta.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public ArticuloEtiqueta findArticuloEtiquetaBy_idA_idE(String articuloIdk, String etiquetaIdk) {
        ArticuloEtiqueta reply = null;
        try {
            reply = (ArticuloEtiqueta) em.createNamedQuery("ArticuloEtiqueta.findArticuloEtiquetaBy_idA_idE")
                    .setParameter("articuloIdk", articuloIdk)
                    .setParameter("etiquetaIdk", etiquetaIdk)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación Articulo / etiqueta dada:  " + articuloIdk+ " / " + etiquetaIdk);
        }
        return reply;
    }
    
    public List<ArticuloEtiqueta> findEtiquetasBy_idA(String articuloIdk) {
        List<ArticuloEtiqueta> reply = null;
        try {
            reply = (List<ArticuloEtiqueta>) em.createNamedQuery("ArticuloEtiqueta.findEtiquetasBy_idA")
                    .setParameter("articuloIdk", articuloIdk)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado Etiqueras asociadas al articulo:  " + articuloIdk);
        }
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }
    
    public int deleteArticuloEtiquetaBy_idA_idE(String articuloIdk, String etiquetaIdk) {
        int reply = 0;
        try {
            reply = em.createNamedQuery("ArticuloEtiqueta.deleteArticuloEtiquetaBy_idA_idE")
                    .setParameter("articuloIdk", articuloIdk)
                    .setParameter("etiquetaIdk", etiquetaIdk)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación Articulo / Etiqueta dada:  " + articuloIdk + " / " + etiquetaIdk);
        }
        return reply;
    }

}
