/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.Etiqueta;
import com.centraldecompras.modelo.ProductoEtiqueta;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ProductoEtiquetaService {

    void create(ProductoEtiqueta productoEtiqueta) throws PreexistingEntityException, Exception;

    void edit(ProductoEtiqueta productoEtiqueta) throws NonexistentEntityException, Exception;

    void destroy(ProductoEtiqueta articuloProducto) throws NonexistentEntityException;

    List<ProductoEtiqueta> findProductoEtiquetaEntities();

    List<ProductoEtiqueta> findProductoEtiquetaEntities(int maxResults, int firstResult);

    List<ProductoEtiqueta> findProductoEtiquetaEntities(boolean all, int maxResults, int firstResult);

    int getArticuloProductoCount();

    ProductoEtiqueta findProductoEtiquetaBy_idP_idE(String productoIdk, String etiquetaIdk);

    List<ProductoEtiqueta> findEtiquetasBy_idP(String productoIdk);

    int deleteProductoEtiquetaBy_idP_idE(String productoIdk, String etiquetaIdk);

}
