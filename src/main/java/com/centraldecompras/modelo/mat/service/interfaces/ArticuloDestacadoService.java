package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.ArticuloDestacado;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface ArticuloDestacadoService { 

    void create(ArticuloDestacado articuloDestacado) throws PreexistingEntityException, Exception;

    void edit(ArticuloDestacado articuloDestacado) throws NonexistentEntityException, Exception;

    void destroy(ArticuloDestacado articuloDestacado) throws NonexistentEntityException;

    List<ArticuloDestacado> findArticuloDestacadoEntities();

    List<ArticuloDestacado> findArticuloDestacadoEntities(int maxResults, int firstResult);

    List<ArticuloDestacado> findArticuloDestacadoEntities(boolean all, int maxResults, int firstResult);

    int getArticuloDestacadoCount();

    ArticuloDestacado findArticuloDestacadoBy_idS_idA(String sociedadIdk, String articuloIdk);

    int deleteArticuloDestacadoBy_idS_idA(String sociedadIdk, String articuloIdk);

}
