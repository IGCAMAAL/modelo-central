/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.Producto;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ProductoService {

    void create(Producto producto);

    void create_C(Producto producto);

    void edit(Producto producto) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    Producto findProducto(String id);

    List<Producto> findProductoEntities();

    List<Producto> findProductosBy_ParentId(Producto producto);

    List<String[]> findProductoTypeAhead(int tipo, String cadena, String estado, int cantidadInt, List<Integer> nivelesList, String idioma);
    
    List<String[]> findProductosBy_n_i_d_(int niveleInt, String idioma, String idPadreJerarquia, String estado);
    
    Producto findProductoBy_n_d_t(int niveleInt, String idioma, String traduccionDesc);
    
     List<Producto> findProductosBy_idP(String productoId) ;

}
