/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class ArticuloServiceImpl implements ArticuloService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(Articulo articulo) throws PreexistingEntityException, Exception {
        em.persist(articulo);
    }

    public void create(Articulo articulo) throws PreexistingEntityException, Exception {
        em.persist(articulo);
    }

    public void edit(Articulo articulo) throws NonexistentEntityException, Exception {
        try {
            articulo = em.merge(articulo);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = articulo.getIdArticulo();
                if (findArticulo(id) == null) {
                    throw new NonexistentEntityException("The articulo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Articulo articulo;
        try {
            articulo = em.getReference(Articulo.class, id);
            articulo.getIdArticulo();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The articulo with id " + id + " no longer exists.", enfe);
        }
        em.remove(articulo);
    }

    public List<Articulo> findArticuloEntities() {
        return findArticuloEntities(true, -1, -1);
    }

    public List<Articulo> findArticuloEntities(int maxResults, int firstResult) {
        return findArticuloEntities(false, maxResults, firstResult);
    }

    @Override
    public List<Articulo> findArticuloEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Articulo.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Articulo findArticulo(String id) {
        return em.find(Articulo.class, id);
    }

    public int getArticuloCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Articulo> rt = cq.from(Articulo.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Articulo> findArticuloBy_idP_idS(String productoIdk, String sociedadId) {
        List<Articulo> reply = new ArrayList<>();

        reply = (List<Articulo>) em.createNamedQuery("Articulo.findArticuloBy_idP_idS")
                .setParameter("productoIdk", productoIdk)
                .setParameter("sociedadId", sociedadId)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }

        return reply;
    }

    @Override
    public List<String> findArticuloSel_PA(List<String> estadoAprobacion) {
        List<String> reply = new ArrayList<>();

        reply = (List<String>) em.createNamedQuery("Articulo.findArticuloSel_PA")
                .setParameter("estadoAprobacion", estadoAprobacion)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }

        return reply;
    }

    @Override
    public List<String[]> findArticuloSel_PA_R(List<String> estadoAprobacion) {
        List<String[]> reply = new ArrayList<>();

        reply = (List<String[]>) em.createNamedQuery("Articulo.findArticuloSel_PA_R")
                .setParameter("estadoAprobacion", estadoAprobacion)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }

        return reply;
    }
    
    @Override
    public Map<String, Object> findPagArticulosPASoc(Map<String, Object> filtro) {
        Map<String, Object> reply = new HashMap<>();
        List<Articulo> articulos = new ArrayList<>();

        String sociedadId = (String) filtro.get("sociedadId");
        String estadosAprobacionParam = (String) filtro.get("estadoAprobacion");
        int numeroRegistrosPagina = (int) filtro.get("numeroRegistrosPagina");
        int paginaActual = (int) filtro.get("paginaActual");
        String idioma = (String) filtro.get("idioma");

        
        List<String> estadosAprobacion = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(estadosAprobacionParam, "|");
        while (st.hasMoreTokens()) {
            estadosAprobacion.add(st.nextToken());
        }
        
        int ultimaPagina = 0;

        Long resultadosmax = findContArticulosPASoc(sociedadId, estadosAprobacion);

        int leidos = resultadosmax.intValue();
        ultimaPagina = (leidos + numeroRegistrosPagina - 1) / numeroRegistrosPagina;

        if (paginaActual > ultimaPagina) {
            paginaActual = ultimaPagina;
        }

        try {
            articulos = (List<Articulo>) em.createNamedQuery("Articulo.findPagArticulosPASoc")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("idioma", idioma)
                    .setParameter("estadoAprobacion", estadosAprobacion)
                    .setFirstResult((paginaActual - 1) * numeroRegistrosPagina)
                    .setMaxResults(numeroRegistrosPagina)
                    .getResultList();

        } catch (Exception ex) {
            log.warn(ex + ".............findPagArticulosPASoc No se ha encontrado articulos ptes de aprobar para la sociedad" + sociedadId);
        }

        reply.put("totalPaginas", ultimaPagina);
        reply.put("paginaActual", paginaActual);
        reply.put("totalRegistros", leidos);
        reply.put("articulos", articulos);

        return reply;
    }

    public Long findContArticulosPASoc(String sociedadId, List<String> estadoAprobacion) {
        List<Long> reply = null;

        reply = (List<Long>) em.createNamedQuery("Articulo.findContArticulosPASoc")
                .setParameter("estadoAprobacion", estadoAprobacion)
                .setParameter("sociedadId", sociedadId)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }

        return reply.get(0);
    }

}
