/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ciberado
 */
public interface ArticuloService {

    Articulo findArticulo(String id);

    void create(Articulo articulo) throws PreexistingEntityException, Exception;

    void create_C(Articulo articulo) throws PreexistingEntityException, Exception;

    void edit(Articulo articulo) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    List<Articulo> findArticuloEntities(boolean all, int maxResults, int firstResult);

    List<Articulo> findArticuloBy_idP_idS(String articuloId, String sociedadId);

    List<String> findArticuloSel_PA(List<String> estadoAprobacion);

    List<String[]> findArticuloSel_PA_R(List<String> estadoAprobacion);
    
    Map<String, Object> findPagArticulosPASoc(Map<String, Object> filtro);

}
