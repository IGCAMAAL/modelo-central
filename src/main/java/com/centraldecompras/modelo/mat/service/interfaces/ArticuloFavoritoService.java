package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.ArticuloFavorito;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

public interface ArticuloFavoritoService { 

    void create(ArticuloFavorito articuloFavorito) throws PreexistingEntityException, Exception;

    void edit(ArticuloFavorito articuloFavorito) throws NonexistentEntityException, Exception;

    void destroy(ArticuloFavorito articuloFavorito) throws NonexistentEntityException;

    List<ArticuloFavorito> findArticuloFavoritoEntities();

    List<ArticuloFavorito> findArticuloFavoritoEntities(int maxResults, int firstResult);

    List<ArticuloFavorito> findArticuloFavoritoEntities(boolean all, int maxResults, int firstResult);

    int getArticuloFavoritoCount();

    ArticuloFavorito findArticuloFavoritoBy_idS_idA_idU(String articuloIdk, String asociadoIdk, String usuarioIdk);

    int deleteArticuloFavoritoBy_idS_idA_idU(String articuloIdk, String asociadoIdk, String usuarioIdk);

}
