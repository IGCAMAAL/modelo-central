/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface SocProductoService {

    void create(SocProducto socProducto) throws PreexistingEntityException, Exception;

    void edit(SocProducto socProducto) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    SocProducto findSocProducto(String id);

    SocProducto findSocProductoBy_idPidS(String productoid, String sociedadid);

    SocProducto findSocProductoBy_idPidS_Estado(String productoid, String sociedadid, String estado);

    List<SocProducto> findSocProductoBy_idS(String sociedadid);

    List<SocProducto> findSocProductoEntities(boolean all, int maxResults, int firstResult);

    List<String[]> findSocProductoTypeAhead(int tipo, String cadena, String estado, int cantidadInt, List<Integer> nivelesList, String idioma, String idSociedad);

    void create_C(SocProducto socProducto) throws PreexistingEntityException, Exception;

    SocProducto findSocProductoBy_idS_idP_Estado(String sociedadId, String productoId, String estado);
    
    List<String> findSociedadesSirvenProdcuto(String productoId, List<String> estado, Boolean asignada) ;
}
