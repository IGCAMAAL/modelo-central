package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.SocProductoExcl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface SocProductoExcludesService {

    public void create(SocProductoExcl socProductoExcl) throws PreexistingEntityException, Exception ;

    public void edit(SocProductoExcl socProductoExcl) throws NonexistentEntityException, Exception ;

    public List<SocProductoExcl> findSocProductoExcludesEntities() ;

    public List<SocProductoExcl> findSocProductoExcludesEntities(int maxResults, int firstResult) ;

    public int getSocProductoExcludesCount() ;

    public SocProductoExcl findSocProductoExcludesBy_idS_r(String almacenId, String route) ;

    public int destroySocProductoExcludesBy_idA_idU_r(String almacenId, String route) throws NonexistentEntityException ;

}
