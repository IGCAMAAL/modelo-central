package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.ArticuloDestacado;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloDestacadoService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class ArticuloDestacadoServiceImpl implements ArticuloDestacadoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloDestacadoServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(ArticuloDestacado articuloDestacado) throws PreexistingEntityException, Exception {
        try {
            em.persist(articuloDestacado);
        } catch (Exception ex) {
            if (findArticuloDestacadoBy_idS_idA(
                    articuloDestacado.getSociedadIdk(),
                    articuloDestacado.getArticuloIdk()
            ) != null) {
                throw new PreexistingEntityException("ArticuloDestacado " + articuloDestacado.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ArticuloDestacado articuloDestacado) throws NonexistentEntityException, Exception {
        try {
            articuloDestacado = em.merge(articuloDestacado);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findArticuloDestacadoBy_idS_idA(
                        articuloDestacado.getSociedadIdk(),
                        articuloDestacado.getArticuloIdk()
                ) == null) {
                    throw new NonexistentEntityException("ArticuloDestacado " + articuloDestacado.toString() + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ArticuloDestacado articuloDestacado) throws NonexistentEntityException {
        //TODO 
    }

    public List<ArticuloDestacado> findArticuloDestacadoEntities() {
        return findArticuloDestacadoEntities(true, -1, -1);
    }

    public List<ArticuloDestacado> findArticuloDestacadoEntities(int maxResults, int firstResult) {
        return findArticuloDestacadoEntities(false, maxResults, firstResult);
    }

    public List<ArticuloDestacado> findArticuloDestacadoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ArticuloDestacado.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getArticuloDestacadoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ArticuloDestacado> rt = cq.from(ArticuloDestacado.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public ArticuloDestacado findArticuloDestacadoBy_idS_idA(String sociedadIdk, String articuloIdk) {
        ArticuloDestacado reply = null;
        try {
            reply = (ArticuloDestacado) em.createNamedQuery("ArticuloDestacado.findArticuloDestacadoBy_idS_idA")
                    .setParameter("sociedadIdk", sociedadIdk)
                    .setParameter("articuloIdk", articuloIdk)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación articulo, destacado, dada:  " + articuloIdk + " / " + sociedadIdk);
        }
        return reply;
    }

    public int deleteArticuloDestacadoBy_idS_idA(String sociedadIdk, String articuloIdk) {
        int reply = 0;
        try {
            reply = em.createNamedQuery("ArticuloDestacado.deleteArticuloDestacadoBy_idS_idA")
                    .setParameter("sociedadIdk", sociedadIdk)
                    .setParameter("articuloIdk", articuloIdk)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación articulo, destacado dada:  " + articuloIdk + " / " + sociedadIdk);
        }
        return reply;
    }

}
