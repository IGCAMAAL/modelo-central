package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.ArticuloLangNomb;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ArticuloLangNombService {

    ArticuloLangNomb findArticuloLangNomb(String id);

    void create(ArticuloLangNomb articuloLangNomb) throws PreexistingEntityException, Exception;

    void create_C(ArticuloLangNomb articuloLangNomb) throws PreexistingEntityException, Exception;

    void edit(ArticuloLangNomb articuloLangNomb) throws NonexistentEntityException, Exception;

    List<ArticuloLangNomb> findArticuloLangNombBy_A(Articulo articulo);

    List<ArticuloLangNomb> findArticuloLangNombBy_A_Lang(ArticuloLangNomb articuloLangNomb);

    ArticuloLangNomb findArticuloLangNombBy_IdA_Lang(String articuloId, String idioma);

    List<ArticuloLangNomb> findArticuloLangNombBy_IdA(String articuloId);

    int deleteArticuloLangNombBy_IdA_Lang(String articuloId, String idioma);
    
    List<Object[]> findArticuloLangNombBy_idS_i(String sociedadId, String idioma);
}
