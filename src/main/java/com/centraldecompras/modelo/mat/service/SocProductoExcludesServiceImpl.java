package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.SocProductoExcl;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoExcludesService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class SocProductoExcludesServiceImpl implements SocProductoExcludesService { 

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocProductoExcludesServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(SocProductoExcl socProductoExcl) throws PreexistingEntityException, Exception {
        try { 
            em.persist(socProductoExcl);
        } catch (Exception ex) {
            if (findSocProductoExcludesBy_idS_r(socProductoExcl.getSociedadId(), socProductoExcl.getRoute()) != null) {
                throw new PreexistingEntityException("socProductoExcl " + socProductoExcl.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(SocProductoExcl socProductoExcl) throws NonexistentEntityException, Exception {
        try {
            socProductoExcl = em.merge(socProductoExcl); 
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findSocProductoExcludesBy_idS_r(socProductoExcl.getSociedadId(), socProductoExcl.getRoute()) == null) {
                    throw new PreexistingEntityException("socProductoExcl " + socProductoExcl.toString() + " no longer exists.", ex);
                }
            }
            throw ex;
        }
    }

    public List<SocProductoExcl> findSocProductoExcludesEntities() {
        return SocProductoExcludesServiceImpl.this.findSocProductoExcludesEntities(true, -1, -1);
    } 

    public List<SocProductoExcl> findSocProductoExcludesEntities(int maxResults, int firstResult) {
        return SocProductoExcludesServiceImpl.this.findSocProductoExcludesEntities(false, maxResults, firstResult);
    }

    private List<SocProductoExcl> findSocProductoExcludesEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SocProductoExcl.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getSocProductoExcludesCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SocProductoExcl> rt = cq.from(SocProductoExcl.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public SocProductoExcl findSocProductoExcludesBy_idS_r(String sociedadId, String route) {
        List<SocProductoExcl> reply = null;
        try {
            reply = (List<SocProductoExcl>) em.createNamedQuery("SocProductoExcl.findSocProductoExcludesBy_idS_r")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("route", route)
                    .getResultList();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        } 
        if(reply.size()>0){
            return reply.get(0); 
        } else {
            return null;
        }
    }

    public int destroySocProductoExcludesBy_idA_idU_r(String sociedadId, String route) throws NonexistentEntityException {
        int reply = 0;
        try {
            reply = (int) em.createNamedQuery("SocProductoExcl.destroySocProductoExcludesBy_idS_r")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("route", route)
                    .executeUpdate();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return reply;
    }

}
