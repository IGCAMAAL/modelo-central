package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.ArticuloLangDesc;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloLangDescService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ArticuloLangDescServiceImpl implements ArticuloLangDescService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ArticuloLangDescServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(ArticuloLangDesc articuloLangDesc) throws PreexistingEntityException, Exception {
        try {
            em.persist(articuloLangDesc);
        } catch (Exception ex) {
            if (findArticuloLangDescBy_IdA_Lang(articuloLangDesc.getArticuloIdk(), articuloLangDesc.getIdioma()) != null) {
                throw new PreexistingEntityException("articuloLang " + articuloLangDesc.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }
    
    public void create(ArticuloLangDesc articuloLangDesc) throws PreexistingEntityException, Exception {
        try {
            em.persist(articuloLangDesc);
        } catch (Exception ex) {
            if (findArticuloLangDescBy_IdA_Lang(articuloLangDesc.getArticuloIdk(), articuloLangDesc.getIdioma()) != null) {
                throw new PreexistingEntityException("articuloLang " + articuloLangDesc.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ArticuloLangDesc articuloLang) throws NonexistentEntityException, Exception {
        try {
            articuloLang = em.merge(articuloLang);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = articuloLang.getArticuloIdk();
                if (findArticuloLangDescBy_IdA_Lang(articuloLang.getArticuloIdk(), articuloLang.getIdioma()) == null) {
                    throw new NonexistentEntityException("The articuloLang with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ArticuloLangDesc articuloLangDesc) throws NonexistentEntityException {
        //TODO 
    }

    public List<ArticuloLangDesc> findArticuloLangDescEntities() {
        return findArticuloLangDescEntities(true, -1, -1);
    }

    public List<ArticuloLangDesc> findArticuloBLangDescEntities(int maxResults, int firstResult) {
        return findArticuloLangDescEntities(false, maxResults, firstResult);
    }

    private List<ArticuloLangDesc> findArticuloLangDescEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ArticuloLangDesc.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public ArticuloLangDesc findArticuloLangDesc(String id) {

        return em.find(ArticuloLangDesc.class, id);

    }

    public int getArticuloLangCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ArticuloLangDesc> rt = cq.from(ArticuloLangDesc.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

    public List<ArticuloLangDesc> findArticuloLangDescBy_A(Articulo articulo) {
        List<ArticuloLangDesc> reply = null;
        reply = (List<ArticuloLangDesc>) em.createNamedQuery("ArticuloLangDesc.findArticuloLangDescBy_A")
                .setParameter("articulo", articulo)
                .getResultList();
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<ArticuloLangDesc> findArticuloLangDescBy_A_Lang(ArticuloLangDesc articuloLang) {
        List<ArticuloLangDesc> reply = null;
        reply = (List<ArticuloLangDesc>) em.createNamedQuery("ArticuloLangDesc.findArticuloLangDescBy_A_Lang")
                .setParameter("articuloLang", articuloLang.getArticuloIdk())
                .setParameter("idioma", articuloLang.getIdioma())
                .getResultList();
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<ArticuloLangDesc> findArticuloLangDescBy_IdA(String articuloId) {
        List<ArticuloLangDesc> reply = null;
        try {
            reply = (List<ArticuloLangDesc>) em.createNamedQuery("ArticuloLangDesc.findArticuloLangDescBy_IdA")
                    .setParameter("articuloId", articuloId)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para productoId=" + articuloId);
        } 
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }

    public ArticuloLangDesc findArticuloLangDescBy_IdA_Lang(String articuloId, String idioma) {
        ArticuloLangDesc reply = null;
        try {
            reply = (ArticuloLangDesc) em.createNamedQuery("ArticuloLangDesc.findArticuloLangDescBy_IdA_Lang")
                    .setParameter("articuloId", articuloId)
                    .setParameter("idioma", idioma)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para articuloId=" + articuloId);
        } 
        return reply;
    }

    public int deleteArticuloLangDescBy_IdA_Lang(String articuloId, String idioma) {
        int num = 0;
        try {
            num = em.createNamedQuery("ArticuloLangDesc.deleteArticuloLangDescBy_IdA_Lang")
                    .setParameter("articuloId", articuloId)
                    .setParameter("idioma", idioma)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para articuloId=" + articuloId);
        }
        return num;
    }

    public List<Object[]> findArticuloLangDescBy_idS_i(String sociedadId, String idioma) {
        List<Object[]> reply = new ArrayList<>();

        if (sociedadId != null && !sociedadId.equals("")) {
            reply = (List<Object[]>) em.createNamedQuery("ArticuloLangDesc.findArticuloBy_idS_i")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("idioma", idioma)
                    .getResultList();
        } else {
            reply = (List<Object[]>) em.createNamedQuery("ArticuloLangDesc.findArticuloBy_i")
                    .setParameter("idioma", idioma)
                    .getResultList();
        }
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }
}
