package com.centraldecompras.modelo.mat.service.interfaces;

import com.centraldecompras.modelo.Articulo;
import com.centraldecompras.modelo.ArticuloLangDesc;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ArticuloLangDescService {

    ArticuloLangDesc findArticuloLangDesc(String id);

    void create(ArticuloLangDesc articuloLangDesc) throws PreexistingEntityException, Exception;

    void create_C(ArticuloLangDesc articuloLangDesc) throws PreexistingEntityException, Exception;

    void edit(ArticuloLangDesc articuloLangDesc) throws NonexistentEntityException, Exception;

    List<ArticuloLangDesc> findArticuloLangDescBy_A(Articulo articulo);

    List<ArticuloLangDesc> findArticuloLangDescBy_A_Lang(ArticuloLangDesc articuloLangDesc);

    ArticuloLangDesc findArticuloLangDescBy_IdA_Lang(String articuloId, String idioma);

    List<ArticuloLangDesc> findArticuloLangDescBy_IdA(String articuloId);

    int deleteArticuloLangDescBy_IdA_Lang(String articuloId, String idioma);
    
    List<Object[]> findArticuloLangDescBy_idS_i(String sociedadId, String idioma);
}
