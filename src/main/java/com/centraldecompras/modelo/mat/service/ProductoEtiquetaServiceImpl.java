package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.ProductoEtiqueta;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoEtiquetaService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel
 */
@Service
public class ProductoEtiquetaServiceImpl implements ProductoEtiquetaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductoEtiquetaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(ProductoEtiqueta productoEtiqueta) throws PreexistingEntityException, Exception { 
        try {
            em.persist(productoEtiqueta);
        } catch (Exception ex) {
            if (findProductoEtiquetaBy_idP_idE(productoEtiqueta.getProducto().getIdProducto(), productoEtiqueta.getEtiqueta().getIdEtiqueta() ) != null) {
                throw new PreexistingEntityException("ProductoEtiqueta " + productoEtiqueta.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ProductoEtiqueta productoEtiqueta) throws NonexistentEntityException, Exception { 
        try {
            productoEtiqueta = em.merge(productoEtiqueta);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findProductoEtiquetaBy_idP_idE(productoEtiqueta.getProducto().getIdProducto(), productoEtiqueta.getEtiqueta().getIdEtiqueta() ) != null) {
                    throw new NonexistentEntityException("ProductoEtiqueta " + productoEtiqueta.toString() + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(ProductoEtiqueta articuloProducto) throws NonexistentEntityException {
        //TODO
    }

    public List<ProductoEtiqueta> findProductoEtiquetaEntities() {
        return findProductoEtiquetaEntities(true, -1, -1);
    }

    public List<ProductoEtiqueta> findProductoEtiquetaEntities(int maxResults, int firstResult) {
        return findProductoEtiquetaEntities(false, maxResults, firstResult);
    }

    public List<ProductoEtiqueta> findProductoEtiquetaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ProductoEtiqueta.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getArticuloProductoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ProductoEtiqueta> rt = cq.from(ProductoEtiqueta.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public ProductoEtiqueta findProductoEtiquetaBy_idP_idE(String productoIdk, String etiquetaIdk) {
        ProductoEtiqueta reply = null;
        try {
            reply = (ProductoEtiqueta) em.createNamedQuery("ProductoEtiqueta.findProductoEtiquetaBy_idP_idE")
                    .setParameter("productoIdk", productoIdk)
                    .setParameter("etiquetaIdk", etiquetaIdk)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la combinación Producto / etiqueta dada:  " + productoIdk+ " / " + etiquetaIdk);
        }
        return reply;
    }
    
    public List<ProductoEtiqueta> findEtiquetasBy_idP(String productoIdk) {
        List<ProductoEtiqueta> reply = null;
        try {
            reply = (List<ProductoEtiqueta>) em.createNamedQuery("ProductoEtiqueta.findEtiquetasBy_idP")
                    .setParameter("productoIdk", productoIdk)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se han encontrado Etiqueras asociadas al producto:  " + productoIdk);
        }
        if(reply==null){
            reply = new ArrayList<>();
        }
        return reply;
    }
    
    public int deleteProductoEtiquetaBy_idP_idE(String productoIdk, String etiquetaIdk) {
        int reply = 0;
        try {
            reply = em.createNamedQuery("ProductoEtiqueta.deleteProductoEtiquetaBy_idP_idE")
                    .setParameter("productoIdk", productoIdk)
                    .setParameter("etiquetaIdk", etiquetaIdk)
                    .executeUpdate();
        } catch (Exception ex) {
            System.out.println(".............No se ha encontrado la combinación Producto / Etiqueta dada:  " + productoIdk + " / " + etiquetaIdk);
        }
        return reply;
    }

}
