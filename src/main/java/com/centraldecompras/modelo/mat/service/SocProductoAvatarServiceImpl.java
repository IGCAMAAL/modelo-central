package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SocProductoAvatar;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoAvatarService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

@Service
public class SocProductoAvatarServiceImpl implements SocProductoAvatarService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocProductoAvatarServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(SocProductoAvatar socProductoAvatar) throws PreexistingEntityException, Exception {
        try {
            em.persist(socProductoAvatar);
        } catch (Exception ex) {
            if (findSocProductoAvatarBy_idS_idP(socProductoAvatar.getSociedadId(), socProductoAvatar.getProductoId()) != null) {
                throw new PreexistingEntityException("SocProductoAvatar " + socProductoAvatar.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(SocProductoAvatar socProductoAvatar) throws NonexistentEntityException, Exception {
        try {
            socProductoAvatar = em.merge(socProductoAvatar);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                if (findSocProductoAvatarBy_idS_idP(socProductoAvatar.getSociedadId(), socProductoAvatar.getProductoId()) == null) {
                    throw new NonexistentEntityException("The SocProductoAvatar " + socProductoAvatar.toString() + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public List<SocProductoAvatar> findSocProductoAvatarEntities() {
        return findSocProductoAvatarEntities(true, -1, -1);
    }

    public List<SocProductoAvatar> findSocProductoAvatarEntities(int maxResults, int firstResult) {
        return findSocProductoAvatarEntities(false, maxResults, firstResult);
    }

    private List<SocProductoAvatar> findSocProductoAvatarEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SocProductoAvatar.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public int getProductoLangCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SocProductoAvatar> rt = cq.from(SocProductoAvatar.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public SocProductoAvatar findSocProductoAvatarBy_idS_idP(String sociedadId, String productoId) {
        SocProductoAvatar reply = null;
        try {
            reply = (SocProductoAvatar) em.createNamedQuery("SocProductoAvatar.findSocProductoAvatarBy_idS_idP")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("productoId", productoId)
                    .getSingleResult();
        } catch (Exception e) {
            // Notthing todo
        }
        return reply;
    }

    public int deleteSocProductoAvatarBy_idS_idP(String sociedadId, String productoId) {
        int num = 0;
        try {
            num = em.createNamedQuery("SocProductoAvatar.deleteSocProductoAvatarBy_idS_idP")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("productoId", productoId)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado Idioma para productoId=" + productoId);
        }
        return num;
    }

    public List<String[]> findSocProductoAvatarTypeAhead(int tipo, String cadena, String estado, int cantidadInt, List<Integer> nivelesList, String idioma, String sociedadIdk) {
        List<String[]> reply = new ArrayList();
        
        Query query = null;

        StringBuffer descripcionSql = new StringBuffer();
        StringBuffer descripcionExcludeSql = new StringBuffer();

        if (tipo == 1) {
            // Empieza
            descripcionSql.append(cadena).append("%");
            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("SocProductoAvatar.findSocProductoAvatarEmpiezaTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("SocProductoAvatar.findSocProductoAvatarEmpiezaTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("estadoRegistro", estado)
                        .setParameter("idioma", idioma)
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);
            }

        } else {
            // Contiene
            descripcionSql.append("%").append(cadena).append("%");
            descripcionExcludeSql.append(cadena).append("%");

            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("SocProductoAvatar.findSocProductoAvatarContieneTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("SocProductoAvatar.findSocProductoAvatarContieneTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);
            }
        }

        reply = query.getResultList();
        
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    
}
