package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoService;
import com.centraldecompras.zglobal.enums.ElementEnum;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SocProductoServiceImpl implements SocProductoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SocProductoServiceImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public void create(SocProducto socProducto) throws PreexistingEntityException, Exception {
        try {
            em.persist(socProducto);
        } catch (Exception ex) {
            if (findSocProducto(socProducto.getSociedad()) != null) {
                throw new PreexistingEntityException("SocProducto " + socProducto + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(SocProducto socProducto) throws NonexistentEntityException, Exception {
        try {
            socProducto = em.merge(socProducto);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                SociedadRpl id = socProducto.getSociedad();
                if (findSocProducto(id) == null) {
                    throw new NonexistentEntityException("The socProducto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        SocProducto socProducto;
        try {
            socProducto = em.getReference(SocProducto.class, id);
            socProducto.getSociedad();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The socProducto with id " + id + " no longer exists.", enfe);
        }
        em.remove(socProducto);
    }

    public List<SocProducto> findSocProductoEntities() {
        return findSocProductoEntities(true, -1, -1);
    }

    public List<SocProducto> findSocProductoEntities(int maxResults, int firstResult) {
        return findSocProductoEntities(false, maxResults, firstResult);
    }

    public List<SocProducto> findSocProductoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SocProducto.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public SocProducto findSocProducto(String id) {
        return em.find(SocProducto.class, id);
    }

    public SocProducto findSocProducto(SociedadRpl id) {
        return findSocProducto(id.getIdSociedad());
    }

    public int getSocProductoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SocProducto> rt = cq.from(SocProducto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public SocProducto findSocProductoBy_idPidS(String productoid, String sociedadid) {
        SocProducto socProducto = null;
        try {
            socProducto = (SocProducto) em.createNamedQuery("SocProducto.findSocProductoBy_idPidS")
                    .setParameter("productoid", productoid)
                    .setParameter("sociedadid", sociedadid)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la relacion Producto / Sociedad= " + productoid + " / " + sociedadid);
        }
        return socProducto;
    }

    public SocProducto findSocProductoBy_idPidS_Estado(String productoid, String sociedadid, String estado) {
        SocProducto socProducto = null;
        try {
            socProducto = (SocProducto) em.createNamedQuery("SocProducto.findSocProductoBy_idPidS_Estado")
                    .setParameter("productoid", productoid)
                    .setParameter("sociedadid", sociedadid)
                    .setParameter("estado", estado)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la relacion Producto / Sociedad= " + productoid + " / " + sociedadid);
        }
        return socProducto;
    }

    public List<SocProducto> findSocProductoBy_idS(String sociedadid) {
        List<SocProducto> socProducto = null;
        try {
            socProducto = (List<SocProducto>) em.createNamedQuery("SocProducto.findSocProductoBy_idS")
                    .setParameter("sociedadid", sociedadid)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado ninguan Zona Geografica para la Sociedad= " + sociedadid);
        }
        return socProducto;
    }

    public List<String[]> findSocProductoTypeAhead(int tipo, String cadena, String estado, int cantidadInt, List<Integer> nivelesList, String idioma, String sociedadIdk) {
        List<String[]> reply = new ArrayList();

        Query query = null;

        StringBuffer descripcionSql = new StringBuffer();
        StringBuffer descripcionExcludeSql = new StringBuffer();

        if (tipo == 1) {
            // Empieza
            descripcionSql.append(cadena).append("%");
            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("SocProducto.findSocProductoEmpiezaTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("SocProducto.findSocProductoEmpiezaTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("estadoRegistro", estado)
                        .setParameter("idioma", idioma)
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);
            }

        } else {
            // Contiene
            descripcionSql.append("%").append(cadena).append("%");
            descripcionExcludeSql.append(cadena).append("%");

            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("SocProducto.findSocProductoContieneTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("SocProducto.findSocProductoContieneTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setParameter("sociedadIdk", sociedadIdk)
                        .setMaxResults(cantidadInt);
            }
        }

        reply = query.getResultList();

        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    @Transactional
    public void create_C(SocProducto socProducto) throws PreexistingEntityException, Exception {
        try {
            em.persist(socProducto);
        } catch (Exception ex) {
            if (findSocProducto(socProducto.getSociedad()) != null) {
                throw new PreexistingEntityException("SocProducto " + socProducto + " already exists.", ex);
            }
            throw ex;
        }
    }

    public List<Producto> findAlmacenUsuarioProductoBy_idA_idU_r(SocProducto _socProducto) {
        List<Producto> productos = new ArrayList();
        try {
            productos = (List<Producto>) em.createNamedQuery("Producto.findAlmacenUsuarioProductoBy_idA_idU_r")
                    .setParameter("productoId", _socProducto.getProducto().getIdProducto())
                    .getResultList();
        } catch (Exception ex) {
            log.info(ex + ".............No se han encontrado Productos = " + _socProducto.getProducto().getIdProducto());
        }
        return productos;
    }

    public SocProducto findSocProductoBy_idS_idP_Estado(String sociedadId, String productoId, String estado) {
        SocProducto socProducto = null;
        try {
            socProducto = (SocProducto) em.createNamedQuery("SocProducto.findSocProductoBy_idS_idP_Estado")
                    .setParameter("almacenId", sociedadId)
                    .setParameter("productoId", productoId)
                    .setParameter("estado", estado)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + ".............No se ha encontrado la relacion Almacen / Usuaeio / Producto = " + productoId + " - " + sociedadId + " - " + estado);
        }
        return socProducto;
    }

    public List<String> findSociedadesSirvenProdcuto(String productoId, List<String> estado, Boolean asignada) {
        List<String> socProductos = new ArrayList();

        StringBuffer qrySbf = new StringBuffer();

        qrySbf.append("  SELECT sp.sociedad.idSociedad                                                     "
                + "        FROM SocProducto         sp                                                     "
                + "       WHERE  sp.producto.route                      LIKE  '%' || :productoId  || '.%'  "
                + "         AND  sp.asignada                               =  :true                        "
                + "         AND  sp.atributosAuditoria.estadoRegistro      =  :estado                      "
                + "");

        String qryString = qrySbf.toString();

        Query query = em.createQuery(qryString);
        query.setParameter("productoId", productoId);
        query.setParameter("estado", estado);
        query.setParameter("true", asignada);        
        
        return query.getResultList();
    }

}
