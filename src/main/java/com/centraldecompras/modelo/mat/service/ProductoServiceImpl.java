/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.mat.service;

import com.centraldecompras.modelo.AlmacenUsuarioProducto;
import com.centraldecompras.zglobal.enums.ElementEnum;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class ProductoServiceImpl implements ProductoService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ProductoServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(Producto producto) {
        em.persist(producto);
    }

    public void create(Producto producto) {
        em.persist(producto);
    }

    public void edit(Producto producto) throws NonexistentEntityException, Exception {
        try {
            producto = em.merge(producto);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = producto.getIdProducto();
                if (findProducto(id) == null) {
                    throw new NonexistentEntityException("The producto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Producto producto;
        try {
            producto = em.getReference(Producto.class, id);
            producto.getIdProducto();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The producto with id " + id + " no longer exists.", enfe);
        }
        em.remove(producto);
    }

    public List<Producto> findProductoEntities() {
        return findProductoEntities(true, -1, -1);
    }

    public List<Producto> findProductoEntities(int maxResults, int firstResult) {
        return findProductoEntities(false, maxResults, firstResult);
    }

    private List<Producto> findProductoEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Producto.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Producto findProducto(String id) {
        return em.find(Producto.class, id);
    }

    public int getProductoCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Producto> rt = cq.from(Producto.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Producto> findProductosBy_ParentId(Producto producto) {
        List<Producto> reply = new ArrayList();
        reply = (List<Producto>) em.createNamedQuery("Producto.findProductosBy_ParentId")
                .setParameter("padreJerarquia", producto)
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<String[]> findProductoTypeAhead(int tipo, String cadena, String estado, int cantidadInt, List<Integer> nivelesList, String idioma) {
        List<String[]> reply = new ArrayList();
        
        Query query = null;

        StringBuffer descripcionSql = new StringBuffer();
        StringBuffer descripcionExcludeSql = new StringBuffer();

        if (tipo == 1) {
            // Empieza
            descripcionSql.append(cadena).append("%");
            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("Producto.findProductoEmpiezaTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("Producto.findProductoEmpiezaTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("estadoRegistro", estado)
                        .setParameter("idioma", idioma)
                        .setMaxResults(cantidadInt);
            }

        } else {
            // Contiene
            descripcionSql.append("%").append(cadena).append("%");
            descripcionExcludeSql.append(cadena).append("%");

            if (estado.equals(ElementEnum.KApp.TODOS.getKApp())) {
                query = em.createNamedQuery("Producto.findProductoContieneTypeAheadTodo")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setMaxResults(cantidadInt);

            } else {
                query = em.createNamedQuery("Producto.findProductoContieneTypeAheadSelEstado")
                        .setParameter("descripcion", descripcionSql.toString())
                        .setParameter("nivelesInt", nivelesList)
                        .setParameter("idioma", idioma)
                        .setParameter("descipcionExclude", descripcionExcludeSql.toString())
                        .setParameter("estadoRegistro", estado)
                        .setMaxResults(cantidadInt);
            }
        }

        reply = query.getResultList();
        
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<String[]> findProductosBy_n_i_d_t(int niveleInt, String idioma, String traduccionDesc, String idPadreJerarquia) {
        List<String[]> reply = new ArrayList();

        reply = (List<String[]>) em.createNamedQuery("Producto.findProductosBy_n_i_d_t")
                .setParameter("niveleInt", niveleInt)
                .setParameter("idPadreJerarquia", idPadreJerarquia)
                .setParameter("idioma", idioma)
                .setParameter("traduccionDesc", "%" + traduccionDesc + "%")
                .getResultList();
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public List<String[]> findProductosBy_n_i_d_(int niveleInt, String idioma, String idPadreJerarquia, String estado) {
        List<String[]> reply = new ArrayList();

        if (estado != null && (estado.equals(KApp.ACTIVO.getKApp() ) || estado.equals(KApp.DESACTIVADO.getKApp() )) ) {
            reply = (List<String[]>) em.createNamedQuery("Producto.findProductosBy_n_i_d_e")
                    .setParameter("niveleInt", niveleInt)
                    .setParameter("idPadreJerarquia", idPadreJerarquia)
                    .setParameter("idioma", idioma)
                    .setParameter("estado", estado)
                    .getResultList();
        } else {
            reply = (List<String[]>) em.createNamedQuery("Producto.findProductosBy_n_i_d")
                    .setParameter("niveleInt", niveleInt)
                    .setParameter("idPadreJerarquia", idPadreJerarquia)
                    .setParameter("idioma", idioma)
                    .getResultList();
        }
        if (reply == null) {
            reply = new ArrayList<>();
        }
        return reply;
    }

    public Producto findProductoBy_n_d_t(int niveleInt, String idioma, String traduccionDesc) {
        Producto reply = new Producto();

        reply = (Producto) em.createNamedQuery("Producto.findProductoBy_n_d_t")
                .setParameter("niveleInt", niveleInt)
                .setParameter("idioma", idioma)
                .setParameter("traduccionDesc", traduccionDesc)
                .getSingleResult();

        return reply;
    }
    
    public List<Producto> findProductosBy_idP(String productoId) {
        List<Producto> productos = new ArrayList();
        try {
            productos = (List<Producto>) em.createNamedQuery("Producto.findProductosBy_idP")
                    .setParameter("productoId", productoId)
                    .getResultList();
        } catch (Exception ex) {
            log.info(ex + ".............No se han encontrado Productos = " + productoId);
        }
        return productos; 
    }    

}
