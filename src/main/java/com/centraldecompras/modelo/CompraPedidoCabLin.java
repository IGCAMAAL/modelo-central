package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_PUR_CompraPedidoCabLin")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompraPedidoCabLin.findAll", query = ""
            + "SELECT p "
            + "FROM CompraPedidoCabLin p"),
    
    @NamedQuery(name = "CompraPedidoCabLin.findByIdPedido", query = ""
            + "SELECT p "
            + "FROM CompraPedidoCabLin p "
            + "WHERE p.pedido.id = :compraPedidoIdk"),
    
    @NamedQuery(name = "CompraPedidoCabLin.findUltimaLinCompraPedidoCabLin", query = ""
            + " SELECT max(d.compraPedidoCabLin) "
            + "   FROM CompraPedidoCabLin d "
            + "  WHERE d.pedido.id = :compraPedidoIdk" ),
    
    @NamedQuery(name = "CompraPedidoCabLin.findCompraPedidoCabLin", query = ""
            + " SELECT d "
            + "   FROM CompraPedidoCabLin d "
            + "  WHERE d.pedido.id = :compraPedidoIdk "
            + "    AND d.compraPedidoCabLin = :compraPedidoCabLin ")
})

public class CompraPedidoCabLin implements Serializable {

    private static final long serialVersionUID = 1L;

    // Atributos ID -----------------------------------------------------------------------------------------------    
    @Id
    @Basic
    @ManyToOne(targetEntity = CompraPedido.class)
    private CompraPedido pedido;
    
    @Id
    @Basic
    @Column(name = "compraPedidoCabLin")
    private int compraPedidoCabLin;
    
    @Version
    private int version;

    // Atributos Basicos--------------------------------------------------------------------------------------------
    @Embedded
    DatosAuditoria atributosAuditoria;

    // 2 Constructores ---------------------------------------------------------------------------------------------
    public CompraPedidoCabLin() {
    }

    public CompraPedidoCabLin(CompraPedido pedido, int compraPedidoCabLin, DatosAuditoria atributosAuditoria) {
        this.pedido = pedido;
        this.compraPedidoCabLin = compraPedidoCabLin;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Metodos  ID -----------------------------------------------------------------------------------------------     
    public CompraPedido getPedido() {
        return pedido;
    }
 
    public void setPedido(CompraPedido pedido) {
        this.pedido = pedido;
    }

    public int getCompraPedidoCabLin() {
        return compraPedidoCabLin;
    }

    public void setCompraPedidoCabLin(int compraPedidoCabLin) {
        this.compraPedidoCabLin = compraPedidoCabLin;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


    // Metodos  Basicos ----------------------------------------------------------------------------------------   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Metodos  Entity ----------------------------------------------------------------------------------------

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.pedido);
        hash = 59 * hash + this.compraPedidoCabLin;
        hash = 59 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraPedidoCabLin other = (CompraPedidoCabLin) obj;
        if (!Objects.equals(this.pedido, other.pedido)) {
            return false;
        }
        if (this.compraPedidoCabLin != other.compraPedidoCabLin) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CompraPedidoCabLin{" + "pedido=" + pedido + ", compraPedidoCabLin=" + compraPedidoCabLin + ", version=" + version + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
