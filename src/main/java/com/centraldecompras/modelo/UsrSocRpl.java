package com.centraldecompras.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_RPL_UsrSocRpl")
@XmlRootElement

public class UsrSocRpl implements Serializable {

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idUsrSoc;

    @Version
    private int version;

    public UsrSocRpl() {

    }

    public UsrSocRpl(String idUsrSoc) {
        this.idUsrSoc = idUsrSoc;
    }
  
    public String getIdUsrSoc() {
        return this.idUsrSoc;
    }

    public void setIdUsrSoc(String idUsrSoc) {
        this.idUsrSoc = idUsrSoc;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

}
