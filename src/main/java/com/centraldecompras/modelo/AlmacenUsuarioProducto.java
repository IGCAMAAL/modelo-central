package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_LGT_AlmacenUsuarioProducto",
        indexes = {
            @Index(name = "index_AlmacenUsuarioProducto_RAU", columnList = "tipoReg"),
            @Index(name = "index_AlmacenUsuarioProducto_RAU", columnList = "almacen"),
            @Index(name = "index_AlmacenUsuarioProducto_RAU", columnList = "usuario"),

            @Index(name = "index_AlmacenUsuarioProducto_RA", columnList = "tipoReg"),
            @Index(name = "index_AlmacenUsuarioProducto_RA", columnList = "almacen"),
        },
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"almacen", "usuario", "producto"})
        }
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AlmacenUsuarioProducto.findAlmacenUsuarioProductoBy_AUP", query = ""
            + "  SELECT d "
            + "    FROM AlmacenUsuarioProducto d "
            + "   WHERE d.almacen = :almacen "
            + "     AND d.usuario = :usuario "
            + "     AND d.producto = :producto "
    ),
    @NamedQuery(name = "AlmacenUsuarioProducto.findAlmacenUsuarioProductoBy_AU", query = ""
            + "  SELECT d "
            + "    FROM AlmacenUsuarioProducto d "
            + "   WHERE d.tipoReg = :tipoReg "
            + "     AND d.almacen = :almacen "
            + "     AND d.usuario = :usuario "
            + "     AND d.atributosAuditoria.estadoRegistro IN (:estadoRegistro)                "
    ),
    @NamedQuery(name = "AlmacenUsuarioProducto.findAlmacenUsuarioProductoBy_A", query = ""
            + "  SELECT d "
            + "    FROM AlmacenUsuarioProducto d "
            + "   WHERE d.tipoReg = :tipoReg "
            + "     AND d.almacen = :almacen "
            + "     AND d.atributosAuditoria.estadoRegistro IN (:estadoRegistro) "
    ),

    @NamedQuery(name = "AlmacenUsuarioProducto.AlmacenUsuarioProductoBy_idA_idU_idP_Estado", query = ""
            + "  SELECT d "
            + "    FROM AlmacenUsuarioProducto d "
            + "   WHERE d.almacen.id = :almacenId "
            + "     AND d.usuario.id = :ususarioId "
            + "     AND d.producto.id = :productoId "
            + "     AND d.atributosAuditoria.estadoRegistro = :estado "
    )

})
public class AlmacenUsuarioProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(AlmacenUsuarioProducto.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @Basic(optional = false)
    @ManyToOne(targetEntity = Almacen.class)
    private Almacen almacen;

    @Id
    @Basic(optional = false)
    @ManyToOne(targetEntity = Persona.class)
    private Persona usuario;

    @Id
    @Basic(optional = false)
    @ManyToOne(targetEntity = Producto.class)
    private Producto producto;

    @Version
    private int version;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
    @Column(length = 3)
    private String tipoReg;

    private Boolean asignada;

    private Boolean verPrecio;

    @Column(length = 1)           /* B=mas barato, O=por obligacion, P=por referencia, L=libre */

    private String modoCompra;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public AlmacenUsuarioProducto() {

    }

    public AlmacenUsuarioProducto(Almacen almacen, Persona usuario, Producto producto, Boolean asignada, Boolean verPrecio, String modoCompra, DatosAuditoria atributosAuditoria) {
        this.almacen = almacen;
        this.usuario = usuario;
        this.producto = producto;
        this.asignada = asignada;
        this.verPrecio = verPrecio;
        this.modoCompra = modoCompra;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    
    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Persona getUsuario() {
        return usuario;
    }

    public void setUsuario(Persona usuario) {
        this.usuario = usuario;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Boolean isAsignada() {
        return asignada;
    }

    public void setAsignada(Boolean asignada) {
        this.asignada = asignada;
    }

    public Boolean isVerPrecio() {
        return verPrecio;
    }

    public void setVerPrecio(Boolean verPrecio) {
        this.verPrecio = verPrecio;
    }

    public String getModoCompra() {
        return modoCompra;
    }

    public void setModoCompra(String modoCompra) {
        this.modoCompra = modoCompra;
    }

    public String getTipoReg() {
        return tipoReg;
    }

    public void setTipoReg(String tipoReg) {
        this.tipoReg = tipoReg;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.almacen);
        hash = 53 * hash + Objects.hashCode(this.usuario);
        hash = 53 * hash + Objects.hashCode(this.producto);
        hash = 53 * hash + Objects.hashCode(this.tipoReg);
        hash = 53 * hash + Objects.hashCode(this.asignada);
        hash = 53 * hash + Objects.hashCode(this.verPrecio);
        hash = 53 * hash + Objects.hashCode(this.modoCompra);
        hash = 53 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlmacenUsuarioProducto other = (AlmacenUsuarioProducto) obj;
        if (!Objects.equals(this.almacen, other.almacen)) {
            return false;
        }
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        if (!Objects.equals(this.producto, other.producto)) {
            return false;
        }
        if (!Objects.equals(this.tipoReg, other.tipoReg)) {
            return false;
        }
        if (!Objects.equals(this.asignada, other.asignada)) {
            return false;
        }
        if (!Objects.equals(this.verPrecio, other.verPrecio)) {
            return false;
        }
        if (!Objects.equals(this.modoCompra, other.modoCompra)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlmacenUsuarioProducto{" + "almacen=" + almacen + ", usuario=" + usuario + ", producto=" + producto + ", version=" + version + ", tipoReg=" + tipoReg + ", asignada=" + asignada + ", verPrecio=" + verPrecio + ", modoCompra=" + modoCompra + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
