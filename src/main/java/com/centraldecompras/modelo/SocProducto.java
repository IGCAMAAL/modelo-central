package com.centraldecompras.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_MAT_SocProducto",
        indexes = {
            @Index(name = "index_SocProducto_sociedadid", columnList = "sociedadIdk", unique = false)},
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"productoIdk", "sociedadIdk"})}
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SocProducto.findSocProductoBy_idPidS", query = " "
            + " SELECT d "
            + "   FROM SocProducto d "
            + "  WHERE d.productoIdk = :productoid "
            + "    AND d.sociedadIdk = :sociedadid"
    ),

    @NamedQuery(name = "SocProducto.findSocProductoBy_idPidS_Estado", query = " "
            + " SELECT d "
            + "   FROM SocProducto d "
            + "  WHERE d.productoIdk = :productoid "
            + "    AND d.sociedadIdk = :sociedadid "
            + "    AND d.atributosAuditoria.estadoRegistro = :estado "
    ),

    @NamedQuery(name = "SocProducto.findSocProductoBy_idS", query = " "
            + " SELECT d "
            + "   FROM SocProducto d "
            + "  WHERE d.sociedadIdk = :sociedadid"
    ),

    @NamedQuery(name = "SocProducto.findSocProductoEmpiezaTypeAheadTodo",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion, "
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProducto s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoIdk = p.idProducto "
            + "   AND s.productoIdk = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND s.sociedadIdk = :sociedadIdk  "
    ),

    @NamedQuery(name = "SocProducto.findSocProductoEmpiezaTypeAheadSelEstado",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProducto s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoIdk = p.idProducto "
            + "   AND s.productoIdk = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND p.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND s.sociedadIdk = :sociedadIdk  "
    ),

    @NamedQuery(name = "SocProducto.findSocProductoContieneTypeAheadTodo",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProducto s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoIdk = p.idProducto "
            + "   AND s.productoIdk = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND d.traduccionDesc NOT LIKE :descipcionExclude"
            + "   AND s.sociedadIdk = :sociedadIdk  "
    ),

    @NamedQuery(name = "SocProducto.findSocProductoContieneTypeAheadSelEstado",
            query = " SELECT p.idProducto,  p.nivelInt, p.padreJerarquia.idProducto, "
            + "       p.atributosAuditoria.estadoRegistro, p.atributosAuditoria.ultimaAccion ,"
            + "       p.atributosAuditoria.fechaPrevistaActivacion ,p.atributosAuditoria.fechaPrevistaDesactivacion "
            + "  FROM SocProducto s, "
            + "       Producto p, "
            + "       ProductoLang d "
            + " WHERE s.productoIdk = p.idProducto "
            + "   AND s.productoIdk = d.productoId "
            + "   AND p.nivelInt IN (:nivelesInt) "
            + "   AND p.atributosAuditoria.estadoRegistro = :estadoRegistro "
            + "   AND d.idioma = :idioma "
            + "   AND d.traduccionDesc LIKE :descripcion "
            + "   AND d.traduccionDesc NOT LIKE :descipcionExclude  "
            + "   AND s.sociedadIdk = :sociedadIdk  "
    )
})

public class SocProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SocProducto.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSocProducto;

    @Version
    private int version;

    // Atributos basicos * * * * * * * * * * * * * * * * * * * * * * * * * 
    @NotNull
    private String productoIdk;

    @NotNull
    private String sociedadIdk;

    private Boolean asignada;

    // Atributos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    int cargaDirectaArticulos;      // 0=NO puede cargar articulos;  1=Carga con aprobacion,        2=Carga Directa (Sin aprobacion)
    int modPreciosBaja;             // 0=NO puede modificar precios; 1=Modicacion con aprobacion,   2=Modificacion directa
    @NotNull
    BigDecimal porcentajeMaxBaja;
    int modPreciosAlta;             // 0=N= puede modificar precios; 1=Modicacion con aprobacion,   2=Modificacion directa
    @NotNull
    BigDecimal porcentajeMaxAlta;
    @NotNull
    BigDecimal rappel;

    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * 
    @ManyToOne(targetEntity = Producto.class)
    private Producto producto;

    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl sociedad;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public SocProducto() {
    }

    public SocProducto(String idSocProducto, String productoIdk, String sociedadIdk, Boolean asignada, int cargaDirectaArticulos, int modPreciosBaja, BigDecimal porcentajeMaxBaja, int modPreciosAlta, BigDecimal porcentajeMaxAlta, BigDecimal rappel, Producto producto, SociedadRpl sociedad, DatosAuditoria atributosAuditoria) {
        this.idSocProducto = idSocProducto;
        this.productoIdk = productoIdk;
        this.sociedadIdk = sociedadIdk;
        this.asignada = asignada;
        this.cargaDirectaArticulos = cargaDirectaArticulos;
        this.modPreciosBaja = modPreciosBaja;
        this.porcentajeMaxBaja = porcentajeMaxBaja;
        this.modPreciosAlta = modPreciosAlta;
        this.porcentajeMaxAlta = porcentajeMaxAlta;
        this.rappel = rappel;
        this.producto = producto;
        this.sociedad = sociedad;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getIdSocProducto() {
        return idSocProducto;
    }

    public void setIdSocProducto(String idSocProducto) {
        this.idSocProducto = idSocProducto;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getCargaDirectaArticulos() {
        return cargaDirectaArticulos;
    }

    public void setCargaDirectaArticulos(int cargaDirectaArticulos) {
        this.cargaDirectaArticulos = cargaDirectaArticulos;
    }

    public int getModPreciosBaja() {
        return modPreciosBaja;
    }

    public void setModPreciosBaja(int modPreciosBaja) {
        this.modPreciosBaja = modPreciosBaja;
    }

    public BigDecimal getPorcentajeMaxBaja() {
        return porcentajeMaxBaja;
    }

    public void setPorcentajeMaxBaja(BigDecimal porcentajeMaxBaja) {
        this.porcentajeMaxBaja = porcentajeMaxBaja;
    }

    public int getModPreciosAlta() {
        return modPreciosAlta;
    }

    public void setModPreciosAlta(int modPreciosAlta) {
        this.modPreciosAlta = modPreciosAlta;
    }

    public BigDecimal getPorcentajeMaxAlta() {
        return porcentajeMaxAlta;
    }

    public BigDecimal getRappel() {
        return rappel;
    }

    public void setRappel(BigDecimal rappel) {
        this.rappel = rappel;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public void setPorcentajeMaxAlta(BigDecimal porcentajeMaxAlta) {
        this.porcentajeMaxAlta = porcentajeMaxAlta;
    }

    public String getProductoIdk() {
        return productoIdk;
    }

    public void setProductoIdk(String productoIdk) {
        this.productoIdk = productoIdk;
    }

    public String getSociedadIdk() {
        return sociedadIdk;
    }

    public void setSociedadIdk(String sociedadIdk) {
        this.sociedadIdk = sociedadIdk;
    }

    public Boolean getAsignada() {
        return asignada;
    }

    public void setAsignada(Boolean asignada) {
        this.asignada = asignada;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public SociedadRpl getSociedad() {
        return this.sociedad;
    }

    public void setSociedad(SociedadRpl sociedad) {
        this.sociedad = sociedad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.productoIdk);
        hash = 97 * hash + Objects.hashCode(this.sociedadIdk);
        hash = 97 * hash + Objects.hashCode(this.asignada);
        hash = 97 * hash + this.cargaDirectaArticulos;
        hash = 97 * hash + this.modPreciosBaja;
        hash = 97 * hash + Objects.hashCode(this.porcentajeMaxBaja);
        hash = 97 * hash + this.modPreciosAlta;
        hash = 97 * hash + Objects.hashCode(this.porcentajeMaxAlta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SocProducto other = (SocProducto) obj;
        if (!Objects.equals(this.productoIdk, other.productoIdk)) {
            return false;
        }
        if (!Objects.equals(this.sociedadIdk, other.sociedadIdk)) {
            return false;
        }
        if (!Objects.equals(this.asignada, other.asignada)) {
            return false;
        }
        if (this.cargaDirectaArticulos != other.cargaDirectaArticulos) {
            return false;
        }
        if (this.modPreciosBaja != other.modPreciosBaja) {
            return false;
        }
        if (!Objects.equals(this.porcentajeMaxBaja, other.porcentajeMaxBaja)) {
            return false;
        }
        if (this.modPreciosAlta != other.modPreciosAlta) {
            return false;
        }
        if (!Objects.equals(this.porcentajeMaxAlta, other.porcentajeMaxAlta)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SocProducto{" + "idSocProducto=" + idSocProducto + ", version=" + version + ", productoIdk=" + productoIdk + ", sociedadIdk=" + sociedadIdk + ", asignada=" + asignada + ", cargaDirectaArticulos=" + cargaDirectaArticulos + ", modPreciosBaja=" + modPreciosBaja + ", porcentajeMaxBaja=" + porcentajeMaxBaja + ", modPreciosAlta=" + modPreciosAlta + ", porcentajeMaxAlta=" + porcentajeMaxAlta + ", rappel=" + rappel + ", producto=" + producto + ", sociedad=" + sociedad + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
