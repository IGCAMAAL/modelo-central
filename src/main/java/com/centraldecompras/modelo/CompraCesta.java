package com.centraldecompras.modelo;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.zglobal.enums.LongFields;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_PUR_CompraCesta")
@XmlRootElement
@NamedQueries({
    /*
    @NamedQuery(name = "Cesta.findAll", query = ""
            + " SELECT c "
            + "   FROM CompraCesta c"),
    
    @NamedQuery(name = "Cesta.findByIdCesta", query = ""
            + " SELECT c "
            + "   FROM CompraCesta c "
            + "  WHERE c.idCesta = :idCesta")
    */
    /*
    @NamedQuery(name = "CompraCesta.findCompraCesta_CountWithFilter_dSUE", query = ""
            + "   SELECT c "
            + "     FROM CompraCesta c "
            + "    WHERE c.atributosAuditoria.created BETWEEN :createdIni AND :createdFin "                        
            + "      AND c.sociedad     = :sociedad "
            + "      AND c.persona      = :persona "
            + "      AND c.cestaEstado IN (:cestaEstado) "   
            + " ORDER BY c.atributosAuditoria.created DESC, c.cestaDescripcion  "
            + ") "               
    ),
    @NamedQuery(name = "CompraCesta.findCompraCestaWithFilter_dSUE_COUNT", query = ""
            + "   SELECT count(c) "
            + "     FROM CompraCesta c "
            + "    WHERE c.atributosAuditoria.created BETWEEN :createdIni AND :createdFin "                        
            + "      AND c.sociedad     = :sociedad "
            + "      AND c.persona      = :persona "
            + "      AND c.cestaEstado IN (:cestaEstado) "   
    ),    
   */
  /*  
    @NamedQuery(name = "CompraCesta.findArticulosForSelect_Pag_WithFilter", query
            = " SELECT ap,  aln"
                    
            + "   FROM ArticuloProducto                ap, "                    
            + "        AlmacenUsuarioProducto         aup, "                    
            + "        ArticuloLangNomb               aln, "
            + "        SocZonGeo                      szg  "    
                    
            + "  WHERE   ap.producto.route                             LIKE   '%.' || aup.producto.idProducto || '.%'    "
                    
            + "    AND   ap.producto.idProducto                          IN   (:productosConEtiqueta)                    "                    
                    
            + "    AND   ap.producto.route                           NOT IN   ( SELECT aupe.route                                      "
            + "                                                                   FROM AlmacenUsuProdExcl aupe                         "
            + "                                                                  WHERE aupe.route LIKE '%.' || aup.producto.idProducto || '.%' "
            + "                                                               )                                                        "   
                    
            + "    AND   ap.articulo.idArticulo                           = aln.articuloIdk     "                     
            + "    AND   ap.articulo.sociedad.id                          = szg.sociedad.id     "                          

            + "    AND   aup.almacen.id                                   =    :almacenId       "
            + "    AND   aup.usuario.id                                   =    :usuarioId       "
            + "    AND   aup.producto.idProducto                          =    :productoId      "                    
                    
            + "    AND   szg.zonaGeografica.route                      LIKE    :zonaIdRoute     "                                                            
                    
            + "    AND  aln.idioma                                        =    :idioma          "
            + "    AND  aln.traduccionNomb                             LIKE    :traduccionNomb  "
                    
            + "    AND   ap.producto.atributosAuditoria.estadoRegistro   IN   (:estados)                                                                             "  
            + "    AND   :hoy                                       BETWEEN             ap.producto.atributosAuditoria.fechaPrevistaActivacion                       "
                    + "                                                 AND CASE  WHEN  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999 "
                    + "                                                           ELSE  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion                    " 
                    + "                                                      END                                                                                     "                     
                    
            + "    AND   ap.articulo.atributosAuditoria.estadoRegistro   IN  (:estados)                                                                              "  
            + "    AND   :hoy                                       BETWEEN             ap.articulo.atributosAuditoria.fechaPrevistaActivacion                       "
                    + "                                                 AND CASE  WHEN  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN 99999999  "
                    + "                                                           ELSE  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion                    "
                    + "                                                      END                                                                                     "             
    ),
*/
    
    /*
    @NamedQuery(name = "CompraCesta.findArticulosForSelect_Coun_WithFilter", query
            = " SELECT COUNT(ap) "
                    
            + "   FROM ArticuloProducto                ap, "                    
            + "        AlmacenUsuarioProducto         aup, "                    
            + "        ArticuloLangNomb               aln, "
            + "        SocZonGeo                      szg  "    
                    
                    
            + "  WHERE   ap.producto.route                             LIKE   '%.' || aup.producto.idProducto || '.%'    "
                    
            + "    AND   ap.producto.idProducto                          IN   (:productosConEtiqueta)                    "                    
                    
            + "    AND   ap.producto.route                           NOT IN   ( SELECT aupe.route                                      "
            + "                                                                   FROM AlmacenUsuProdExcl aupe                         "
            + "                                                                  WHERE aupe.route LIKE '%.' || aup.producto.idProducto || '.%' "
            + "                                                               )                                                        "   
                    
            + "    AND   ap.articulo.idArticulo                           = aln.articuloIdk     "                     
            + "    AND   ap.articulo.sociedad.id                          = szg.sociedad.id     "                          

            + "    AND   aup.almacen.id                                   =    :almacenId       "
            + "    AND   aup.usuario.id                                   =    :usuarioId       "
            + "    AND   aup.producto.idProducto                          =    :productoId      "                    
                    
            + "    AND   szg.zonaGeografica.route                      LIKE    :zonaIdRoute     "                                                            
                    
            + "    AND  aln.idioma                                        =    :idioma          "
            + "    AND  aln.traduccionNomb                             LIKE    :traduccionNomb  "
                    
            + "    AND   ap.producto.atributosAuditoria.estadoRegistro   IN    (:estados)                                                                            "  
            + "    AND   :hoy                                       BETWEEN             ap.producto.atributosAuditoria.fechaPrevistaActivacion                       "
                    + "                                                 AND CASE  WHEN  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN  99999999 "
                    + "                                                           ELSE  ap.producto.atributosAuditoria.fechaPrevistaDesactivacion                    " 
                    + "                                                      END                                                                                     "                     
                    
            + "    AND   ap.articulo.atributosAuditoria.estadoRegistro   IN  (:estados)                                                                              "  
            + "    AND   :hoy                                       BETWEEN             ap.articulo.atributosAuditoria.fechaPrevistaActivacion                       "
                    + "                                                 AND CASE  WHEN  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion = 0 THEN 99999999  "
                    + "                                                           ELSE  ap.articulo.atributosAuditoria.fechaPrevistaDesactivacion                    "
                    + "                                                      END                                                                                     "
    )
    
    */

})
public class CompraCesta implements Serializable {

    private static final long serialVersionUID = 1L;

    // Atributos ID -----------------------------------------------------------------------------------------------    
    @Id
    @Basic(optional = false)
    @Column(name = "ID", length = LongFields.id, updatable = false)
    private String idCesta;

    @Version
    private int version;
    
    // Atributos Básicos ------------------------------------------------------------------------------------------  
    @Column(length = LongFields.compraEstado)
    @Basic(optional = false)
    String cestaEstado;
    
    @Column(length = 150)    
    String cestaDescripcion;
    
    boolean cestaFavorita;
    
    int fechaEntregaCesta;
    
    @Column(length = LongFields.moneda)
    @Basic(optional = false)
    String moneda;
    // Atributos Relacion ------------------------------------------------------------------------------------------        
    @ManyToOne(targetEntity = Sociedad.class)
    @Basic(optional = false)
    private Sociedad sociedad;

    @ManyToOne(targetEntity = Persona.class)
    @Basic(optional = false)
    private Persona persona;
    
    // Atributos de auditoria --------------------------------------------------------------------------------------
    @Embedded
    private DatosAuditoria atributosAuditoria;
    
    // 2 Constructores ---------------------------------------------------------------------------------------------
    public CompraCesta() {
    }

    public CompraCesta(String idCesta, String cestaEstado, String cestaDescripcion, boolean cestaFavorita, int fechaEntregaCesta, String moneda, Sociedad sociedad, Persona persona, DatosAuditoria atributosAuditoria) {
        this.idCesta = idCesta;
        this.cestaEstado = cestaEstado;
        this.cestaDescripcion = cestaDescripcion;
        this.cestaFavorita = cestaFavorita;
        this.fechaEntregaCesta = fechaEntregaCesta;
        this.moneda = moneda;
        this.sociedad = sociedad;
        this.persona = persona;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Metodos  ID -----------------------------------------------------------------------------------------------     
    public String getIdCesta() {
        return idCesta;
    }

    public void setIdCesta(String idCesta) {
        this.idCesta = idCesta;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Metodos  Basicos ----------------------------------------------------------------------------------------   
    public String getCestaEstado() {
        return cestaEstado;
    }

    public void setCestaEstado(String cestaEstado) {
        this.cestaEstado = cestaEstado;
    }

    public boolean isCestaFavorita() {
        return cestaFavorita;
    }

    public void setCestaFavorita(boolean cestaFavorita) {
        this.cestaFavorita = cestaFavorita;
    }

    public int getFechaEntregaCesta() {
        return fechaEntregaCesta;
    }

    public void setFechaEntregaCesta(int fechaEntregaCesta) {
        this.fechaEntregaCesta = fechaEntregaCesta;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getCestaDescripcion() {
        return cestaDescripcion;
    }

    public void setCestaDescripcion(String cestaDescripcion) {
        this.cestaDescripcion = cestaDescripcion;
    }

    // Metodos  Relacion ----------------------------------------------------------------------------------------   
    public Sociedad getSociedad() {
        return sociedad;
    }

    public void setSociedad(Sociedad sociedad) {
        this.sociedad = sociedad;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Metodos  Entity ----------------------------------------------------------------------------------------

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idCesta);
        hash = 97 * hash + Objects.hashCode(this.cestaEstado);
        hash = 97 * hash + Objects.hashCode(this.cestaDescripcion);
        hash = 97 * hash + (this.cestaFavorita ? 1 : 0);
        hash = 97 * hash + this.fechaEntregaCesta;
        hash = 97 * hash + Objects.hashCode(this.moneda);
        hash = 97 * hash + Objects.hashCode(this.sociedad);
        hash = 97 * hash + Objects.hashCode(this.persona);
        hash = 97 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CompraCesta other = (CompraCesta) obj;
        if (!Objects.equals(this.idCesta, other.idCesta)) {
            return false;
        }
        if (!Objects.equals(this.cestaEstado, other.cestaEstado)) {
            return false;
        }
        if (!Objects.equals(this.cestaDescripcion, other.cestaDescripcion)) {
            return false;
        }
        if (this.cestaFavorita != other.cestaFavorita) {
            return false;
        }
        if (this.fechaEntregaCesta != other.fechaEntregaCesta) {
            return false;
        }
        if (!Objects.equals(this.moneda, other.moneda)) {
            return false;
        }
        if (!Objects.equals(this.sociedad, other.sociedad)) {
            return false;
        }
        if (!Objects.equals(this.persona, other.persona)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CompraCesta{" + "idCesta=" + idCesta + ", version=" + version + ", cestaEstado=" + cestaEstado + ", cestaDescripcion=" + cestaDescripcion + ", cestaFavorita=" + cestaFavorita + ", fechaEntregaCesta=" + fechaEntregaCesta + ", moneda=" + moneda + ", sociedad=" + sociedad + ", persona=" + persona + ", atributosAuditoria=" + atributosAuditoria + '}';
    }




    
}
