package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Miguel
 */
@Entity
@Table(name = "CC_MAT_ArticuloLangDesc",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"articuloIdk", "idioma", "traduccionDesc"})
        }
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArticuloLangDesc.findArticuloLangDescBy_A", query = ""
        + " SELECT d "
        + "   FROM ArticuloLangDesc d "
        + "  WHERE d.articuloEntity = :articulo" ),
    
    @NamedQuery(name = "ArticuloLangDesc.findArticuloLangDescBy_A_Lang", query = ""
        + " SELECT d "
        + "   FROM ArticuloLangDesc d "
        + "  WHERE d.articuloEntity = :articulo "
        + "    AND d.idioma = :idioma" ),
    
    @NamedQuery(name = "ArticuloLangDesc.findArticuloLangDescBy_IdA_Lang", query = ""
            + " SELECT d "
            + "   FROM ArticuloLangDesc d "
            + "  WHERE d.articuloIdk = :articuloId "
            + "    AND d.idioma = :idioma" ),
    
    @NamedQuery(name = "ArticuloLangDesc.findArticuloLangDescBy_IdA", query = ""
            + " SELECT d "
            + "   FROM ArticuloLangDesc d "
            + "  WHERE d.articuloIdk = :articuloId"),
    
    @NamedQuery(name = "ArticuloLangDesc.deleteArticuloLangDescBy_IdA_Lang", query = ""
            + " DELETE "
            + "   FROM ArticuloLangDesc d "
            + "  WHERE d.articuloIdk = :articuloId "
            + "    AND d.idioma = :idioma" ),
    
    @NamedQuery(name = "ArticuloLangDesc.findArticuloBy_i", query = 
            " SELECT l.articuloEntity.idArticulo,              l.idioma,                            l.traduccionDesc, "
          + "        l.articuloEntity.uniMinVta,               l.articuloEntity.precioUniMed,       l.articuloEntity.unidadFormatoVta, "
          + "        l.articuloEntity.codiParaProveedor,       l.articuloEntity.codigoEAN,          fvl.traduccionDesc,   "
          + "        l.articuloEntity.sociedad.id,             p.productoIdk,                       l.articuloEntity.atributosAuditoria.estadoRegistro, "
          + "        l.articuloEntity.atributosAuditoria.fechaPrevistaActivacion, l.articuloEntity.atributosAuditoria.fechaPrevistaDesactivacion,  "
          + "        p.producto.idPadreJerarquia "
          + "   FROM ArticuloLangDesc l, "  
          + "        ArticuloProducto p, " 
          + "        FormatoVtaLang fvl "       
          + "  WHERE l.articuloEntity.idArticulo = p.articuloIdk "
          + "    AND l.articuloEntity.formatoVta.id = fvl.formatoVtaId " 
          + "    AND fvl.idioma                  = :idioma "    
          + "    AND l.idioma                    = :idioma "     
          + " "),
    
    @NamedQuery(name = "ArticuloLangDesc.findArticuloBy_idS_i", query = 
            " SELECT l.articuloEntity.idArticulo,              l.idioma,                             l.traduccionDesc, "
          + "        l.articuloEntity.uniMinVta,               l.articuloEntity.precioUniMed,        l.articuloEntity.unidadFormatoVta, "
          + "        l.articuloEntity.codiParaProveedor,       l.articuloEntity.codigoEAN,           l.articuloEntity.formatoVta.tipoEnvase,  "
          + "        l.articuloEntity.sociedad.id,             p.productoIdk,                        l.articuloEntity.atributosAuditoria.estadoRegistro, "
          + "        l.articuloEntity.atributosAuditoria.fechaPrevistaActivacion, l.articuloEntity.atributosAuditoria.fechaPrevistaDesactivacion,  "
          + "        p.producto.idPadreJerarquia "
          + "   FROM ArticuloLangDesc l, "  
          + "        ArticuloProducto p "  
          + "  WHERE l.articuloEntity.idArticulo  = p.articuloIdk "
          + "    AND l.articuloEntity.sociedad.id = :sociedadId "
          + "    AND l.idioma                     = :idioma "     
          + " ")
})
public class ArticuloLangDesc implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ArticuloLangDesc.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id@Basic@Column(name = "articuloIdk", length = 36, nullable = false, updatable = false)
    private String articuloIdk;

    @Id@Basic
    private String idioma;
    
    @Version
    private int version;
    
    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Basic
    private String traduccionDesc;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity=Articulo.class)
    private Articulo articuloEntity;

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;
    
    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ArticuloLangDesc() {
    }

    public ArticuloLangDesc(String articuloIdk, String idioma, String traduccionDesc, Articulo articuloEntity, DatosAuditoria atributosAuditoria) {
        this.articuloIdk = articuloIdk;
        this.idioma = idioma;
        this.traduccionDesc = traduccionDesc;
        this.articuloEntity = articuloEntity;
        this.atributosAuditoria = atributosAuditoria;
    }
    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getArticuloIdk() {
        return articuloIdk;
    }

    public void setArticuloIdk(String articuloIdk) {
        this.articuloIdk = articuloIdk;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getTraduccionDesc() {
        return traduccionDesc;
    }

    public void setTraduccionDesc(String traduccionDesc) {
        this.traduccionDesc = traduccionDesc;
    }
    
    // Métodos relacion * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public Articulo getArticuloEntity() {
        return articuloEntity;
    }

    public void setArticuloEntity(Articulo articuloEntity) {
        this.articuloEntity = articuloEntity;
    }
    
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.articuloIdk);
        hash = 67 * hash + Objects.hashCode(this.idioma);
        hash = 67 * hash + Objects.hashCode(this.traduccionDesc);
        hash = 67 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloLangDesc other = (ArticuloLangDesc) obj;
        if (!Objects.equals(this.articuloIdk, other.articuloIdk)) {
            return false;
        }
        if (!Objects.equals(this.idioma, other.idioma)) {
            return false;
        }
        if (!Objects.equals(this.traduccionDesc, other.traduccionDesc)) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ArticuloLang{" + "articuloId=" + articuloIdk + ", idioma=" + idioma + ", version=" + version + ", traduccionDesc=" + traduccionDesc + ", articuloEntity=" + articuloEntity + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
