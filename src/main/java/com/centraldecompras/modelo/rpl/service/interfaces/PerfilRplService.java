package com.centraldecompras.modelo.rpl.service.interfaces;

import com.centraldecompras.modelo.PerfilRpl;

public interface PerfilRplService {

    void create(PerfilRpl perfilRpl);
    void create_C(PerfilRpl perfilRpl);
    int getPerfilRplCount();
}
