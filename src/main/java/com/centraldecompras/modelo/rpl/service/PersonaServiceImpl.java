/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.rpl.service;

import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class PersonaServiceImpl implements PersonaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PersonaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(Persona persona) {
        em.persist(persona);
    }

    public void edit(Persona persona) throws NonexistentEntityException, Exception {
        try {
            persona = em.merge(persona);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = persona.getIdPersona();
                if (findPersona(id) == null) {
                    throw new NonexistentEntityException("The persona with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Persona persona;
        try {
            persona = em.getReference(Persona.class, id);
            persona.getIdPersona();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The persona with id " + id + " no longer exists.", enfe);
        }
        em.remove(persona);
    }

    public List<Persona> findPersonaEntities() {
        return findPersonaEntities(true, -1, -1);
    }

    public List<Persona> findPersonaEntities(int maxResults, int firstResult) {
        return findPersonaEntities(false, maxResults, firstResult);
    }

    private List<Persona> findPersonaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Persona.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Persona findPersona(String id) {
        return em.find(Persona.class, id);
    }

    public int getPersonaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Persona> rt = cq.from(Persona.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Persona> findPersonasByIdWeb(String nivel, String userName, String pass) {
        List<Persona> reply = new ArrayList();
        reply = (List<Persona>) em
                .createNamedQuery("Persona.findPersonasByIdWeb")
                .setParameter("userName", userName)
                .getResultList();
        if(reply == null){
            reply = new ArrayList();
        }
        return reply;
    }

    public Persona findPersonaByIdWeb(String userName) {
        Persona persona = null;
        try {
            persona = (Persona) em
                    .createNamedQuery("Persona.findPersonaByIdWeb")
                    .setParameter("userName", userName)
                    .getSingleResult();
        } catch (Exception ex) {
            // Nothing to do
        }
        return persona;
    }

    @Transactional
    public void create_C(Persona persona) {
        em.persist(persona);
    }

}
