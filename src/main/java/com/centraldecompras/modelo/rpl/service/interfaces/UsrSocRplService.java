package com.centraldecompras.modelo.rpl.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UsrSocRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;

public interface UsrSocRplService {

    void create(UsrSocRpl usrSocRpl) throws PreexistingEntityException, Exception;

    void create_C(UsrSocRpl usrSocRpl);

    void destroy(String id) throws NonexistentEntityException;
}
