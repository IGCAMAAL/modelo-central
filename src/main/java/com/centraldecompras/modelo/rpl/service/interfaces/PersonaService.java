package com.centraldecompras.modelo.rpl.service.interfaces;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.Persona;
import java.util.List;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public interface PersonaService {

    void create(Persona persona);
    
    void edit(Persona persona) throws NonexistentEntityException, Exception ;
    
    void destroy(String id) throws NonexistentEntityException;
    
    Persona findPersona(String id);

    List<Persona>  findPersonasByIdWeb(String nivel, String idWeb, String pass);
    
    Persona findPersonaByIdWeb(String persona)  throws UsernameNotFoundException;
    
    void create_C(Persona persona);

}
