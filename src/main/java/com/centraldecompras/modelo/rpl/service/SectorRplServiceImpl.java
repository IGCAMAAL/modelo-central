package com.centraldecompras.modelo.rpl.service;


import com.centraldecompras.modelo.SectorRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.rpl.service.interfaces.SectorRplService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class SectorRplServiceImpl implements SectorRplService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SectorRplServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(SectorRpl sectorRpl) {
        em.persist(sectorRpl);
    }
    
    public void create(SectorRpl sectorRpl) {
        em.persist(sectorRpl);
    }

    public SectorRpl findSectorRpl(String id) {
        return em.find(SectorRpl.class, id);
    }
}
