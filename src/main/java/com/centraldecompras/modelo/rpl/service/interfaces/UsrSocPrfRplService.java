package com.centraldecompras.modelo.rpl.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.UsrSocPrfRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;

public interface UsrSocPrfRplService {

    void create(UsrSocPrfRpl usrSocPrfRpl) throws PreexistingEntityException, Exception;

    UsrSocPrfRpl findUsrSocPrf(String id);

    void create_C(UsrSocPrfRpl usrSocPrfRpl);

    void destroy(String id) throws NonexistentEntityException;
}
