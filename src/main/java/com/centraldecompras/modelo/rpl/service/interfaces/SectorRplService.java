package com.centraldecompras.modelo.rpl.service.interfaces;

import com.centraldecompras.modelo.SectorRpl;


public interface SectorRplService {

    void create(SectorRpl sectorRpl);
    void create_C(SectorRpl sectorRpl);
    SectorRpl findSectorRpl(String id);
    
}
