package com.centraldecompras.modelo.rpl.service;


import com.centraldecompras.modelo.SectoresAutorizadosRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.rpl.service.interfaces.SectoresAutorizadosRplService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class SectoresAutorizadosRplServiceImpl implements SectoresAutorizadosRplService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SectoresAutorizadosRplServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;
    @Transactional
    public void create_C(SectoresAutorizadosRpl sectoresAutorizadosRpl) {
        em.persist(sectoresAutorizadosRpl);
    }

    public void create(SectoresAutorizadosRpl sectoresAutorizadosRpl) {
        em.persist(sectoresAutorizadosRpl);
    }

    public SectoresAutorizadosRpl findSectoresAutorizadosRpl(String id) {
        return em.find(SectoresAutorizadosRpl.class, id);
    }
}
