package com.centraldecompras.modelo.rpl.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.UsrSocRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.rpl.service.interfaces.UsrSocRplService;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsrSocRplServiceImpl implements UsrSocRplService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UsrSocRplServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(UsrSocRpl usrSocRpl) {
        em.persist(usrSocRpl);
    }

    public void create(UsrSocRpl usrSocRpl) {
        em.persist(usrSocRpl);
    }

    public void edit(UsrSocRpl usrSocRpl) throws NonexistentEntityException, Exception {
        try {
            usrSocRpl = em.merge(usrSocRpl);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usrSocRpl.getIdUsrSoc();
                if (findUsrSocRpl(id) == null) {
                    throw new NonexistentEntityException("The prsSoc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        UsrSocRpl usrSocRpl;
        try {
            usrSocRpl = em.getReference(UsrSocRpl.class, id);
            usrSocRpl.getIdUsrSoc();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The prsSoc with id " + id + " no longer exists.", enfe);
        }
        em.remove(usrSocRpl);
    }

    public UsrSocRpl findUsrSocRpl(String id) {
        return em.find(UsrSocRpl.class, id);
    }

}
