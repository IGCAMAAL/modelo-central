/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.rpl.service;

import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class SociedadRplServiceImpl implements SociedadRplService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SociedadRplServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(SociedadRpl sociedadRpl) throws PreexistingEntityException, Exception {

        try {
            em.persist(sociedadRpl);
        } catch (Exception ex) {
            if (findSociedadRpl(sociedadRpl.getIdSociedad()) != null) {
                throw new PreexistingEntityException("Sociedad " + sociedadRpl + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(SociedadRpl sociedadRpl) throws NonexistentEntityException, Exception {
        try {
            SociedadRpl persistentSociedad = em.find(SociedadRpl.class, sociedadRpl.getIdSociedad());
            sociedadRpl = em.merge(sociedadRpl);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = sociedadRpl.getIdSociedad();
                if (findSociedadRpl(id) == null) {
                    throw new NonexistentEntityException("The sociedad with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        SociedadRpl sociedad;
        try {
            sociedad = em.getReference(SociedadRpl.class, id);
            sociedad.getIdSociedad();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The sociedad with id " + id + " no longer exists.", enfe);
        }

        em.remove(sociedad);
    }

    public SociedadRpl findSociedadRpl(String id) {
        return em.find(SociedadRpl.class, id);
    }
    
    @Transactional
    public void create_C(SociedadRpl sociedadRpl) throws PreexistingEntityException, Exception {
        try {
            em.persist(sociedadRpl);
        } catch (Exception ex) {
            if (findSociedadRpl(sociedadRpl.getIdSociedad()) != null) {
                throw new PreexistingEntityException("Sociedad " + sociedadRpl + " already exists.", ex);
            }
            throw ex;
        }
    }

}
