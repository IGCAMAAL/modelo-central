package com.centraldecompras.modelo.rpl.service.interfaces;

import com.centraldecompras.modelo.SectorRpl;
import com.centraldecompras.modelo.SectoresAutorizadosRpl;


public interface SectoresAutorizadosRplService {

    void create(SectoresAutorizadosRpl sectoresAutorizadosRpl);
    void create_C(SectoresAutorizadosRpl sectoresAutorizadosRpl);
    SectoresAutorizadosRpl findSectoresAutorizadosRpl(String id);
    
}
