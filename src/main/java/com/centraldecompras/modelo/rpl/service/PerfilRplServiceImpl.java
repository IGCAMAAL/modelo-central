package com.centraldecompras.modelo.rpl.service;

import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.modelo.PerfilRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.rpl.service.interfaces.PerfilRplService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class PerfilRplServiceImpl implements PerfilRplService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PerfilRplServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(PerfilRpl perfilRpl) {
        em.persist(perfilRpl);
    }
    
    public void create(PerfilRpl perfilRpl) {
        em.persist(perfilRpl);
    }
    
    public int getPerfilRplCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<PerfilRpl> rt = cq.from(PerfilRpl.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
