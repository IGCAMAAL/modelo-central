package com.centraldecompras.modelo.rpl.service;

import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.modelo.UsrSocPrfRpl;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.rpl.service.interfaces.UsrSocPrfRplService;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsrSocPrfRplServiceImpl implements UsrSocPrfRplService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UsrSocPrfRplServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create_C(UsrSocPrfRpl usrSocPrfRpl) {
        em.persist(usrSocPrfRpl);
    }

    public void create(UsrSocPrfRpl usrSocPrfRpl) {
        em.persist(usrSocPrfRpl);
    }

    public void edit(UsrSocPrfRpl usrSocPrfRpl) throws NonexistentEntityException, Exception {
        try {
            usrSocPrfRpl = em.merge(usrSocPrfRpl);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usrSocPrfRpl.getIdUsrSocPrf();
                if (findUsrSocPrf(id) == null) {
                    throw new NonexistentEntityException("The usrSocPrf with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        UsrSocPrfRpl usrSocPrfRpl;
        try {
            usrSocPrfRpl = em.getReference(UsrSocPrfRpl.class, id);
            usrSocPrfRpl.getIdUsrSocPrf();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The usrSocPrf with id " + id + " no longer exists.", enfe);
        }

        em.remove(usrSocPrfRpl);
    }

    public UsrSocPrfRpl findUsrSocPrf(String id) {
        return em.find(UsrSocPrfRpl.class, id);
    }

}
