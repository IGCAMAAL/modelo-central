package com.centraldecompras.modelo.rpl.service.interfaces;

import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;

public interface SociedadRplService {

    void create(SociedadRpl sociedadRpl) throws PreexistingEntityException, Exception;
    
    void edit(SociedadRpl sociedadRpl) throws NonexistentEntityException, Exception;
        
    void destroy(String id) throws NonexistentEntityException ;
    
    SociedadRpl findSociedadRpl(String id);

    void create_C(SociedadRpl sociedadRpl) throws PreexistingEntityException, Exception;

}
