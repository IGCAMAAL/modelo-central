package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_RPL_SectorRpl")
@XmlRootElement

public class SectorRpl implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(SectorRpl.class.getName());

    
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idSectorRpl;
    
    @Version
    private int version;

    @Basic
    private int ultimoNivel;    

    // Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public SectorRpl() {
    }

    public SectorRpl(String uuid) {
        this.idSectorRpl = uuid;
    }

    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getIdSectorRpl() {
        return idSectorRpl;
    }

    public void setIdSectorRpl(String idSectorRpl) {
        this.idSectorRpl = idSectorRpl;
    }

    public int getUltimoNivel() {
        return ultimoNivel;
    }

    public void setUltimoNivel(int ultimoNivel) {
        this.ultimoNivel = ultimoNivel;
    }
    


    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *       

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.idSectorRpl);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SectorRpl other = (SectorRpl) obj;
        if (!Objects.equals(this.idSectorRpl, other.idSectorRpl)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PerfilRpl{" + "idPerfil=" + idSectorRpl + ", version=" + version + '}';
    }

}
