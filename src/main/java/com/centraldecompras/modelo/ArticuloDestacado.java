package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CC_MAT_ArticuloDestacado")
@XmlRootElement
@NamedQueries({                          
   
   @NamedQuery(name = "ArticuloDestacado.findArticuloDestacadoBy_idS_idA", query = ""
           + "  SELECT d "
           + "    FROM ArticuloDestacado d "
           + "   WHERE d.sociedadIdk = :sociedadIdk "
           + "     AND d.articuloIdk = :articuloIdk "),
   
   @NamedQuery(name = "ArticuloDestacado.deleteArticuloDestacadoBy_idS_idA", query = ""
           + "  DELETE "
           + "    FROM ArticuloDestacado d "
           + "   WHERE d.sociedadIdk = :sociedadIdk "
           + "     AND d.articuloIdk = :articuloIdk ")
})
public class ArticuloDestacado implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ArticuloDestacado.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "sociedadIdk", length = 36, nullable = false, updatable = false)
    private String sociedadIdk;
    
    @Id
    @NotNull
    @Column(name = "articuloIdk", length = 36, nullable = false, updatable = false)
    private String articuloIdk;
    
    @Version
    private int version;

    // Atributos básicos * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    private boolean destacado;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *

    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Constructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ArticuloDestacado() {
    }

    public ArticuloDestacado(String sociedadIdk, String articuloIdk, boolean destacado, DatosAuditoria atributosAuditoria) {
        this.sociedadIdk = sociedadIdk;
        this.articuloIdk = articuloIdk; 
        this.destacado = destacado;
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public String getSociedadIdk() {
        return sociedadIdk;
    }

    public void setSociedadIdk(String sociedadIdk) {
        this.sociedadIdk = sociedadIdk;
    }

    public String getArticuloIdk() {
        return articuloIdk;
    }

    public void setArticuloIdk(String articuloIdk) {
        this.articuloIdk = articuloIdk;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    public boolean isDestacado() {
        return destacado;
    }

    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    public void setDestacado(boolean destacado) {
        this.destacado = destacado;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.sociedadIdk);
        hash = 89 * hash + Objects.hashCode(this.articuloIdk);
        hash = 89 * hash + (this.destacado ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.atributosAuditoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArticuloDestacado other = (ArticuloDestacado) obj;
        if (!Objects.equals(this.sociedadIdk, other.sociedadIdk)) {
            return false;
        }
        if (!Objects.equals(this.articuloIdk, other.articuloIdk)) {
            return false;
        }
        if (this.destacado != other.destacado) {
            return false;
        }
        if (!Objects.equals(this.atributosAuditoria, other.atributosAuditoria)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ArticuloAsociado{" + "sociedadIdk=" + sociedadIdk + ", articuloIdk=" + articuloIdk + ", version=" + version + ", destacado=" + destacado + ", atributosAuditoria=" + atributosAuditoria + '}';
    }

}
