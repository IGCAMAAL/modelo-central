package com.centraldecompras.modelo;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CC_LGT_almacenes")
@XmlRootElement
@NamedQueries({                          
   @NamedQuery(name = "Almacen.findAlmacenesBy_idS", query = ""
           + "  SELECT a "
           + "    FROM Almacen a "
           + "   WHERE a.sociedad.idSociedad = :sociedadIdk ")
})
public class Almacen implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(Almacen.class.getName());

    // id * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Id
    @NotNull
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    private String idAlmacen;

    @Version
    private int version;

    private String nombreAlmacen;
    
    private String tipoAlmacen;
       
    private String descripcionAlmacen;
    
    // Atributos de relacion * * * * * * * * * * * * * * * * * * * * * * * * * *
    @ManyToOne(targetEntity = ZonaGeografica.class)
    private ZonaGeografica zonaGeografica;
    
    @ManyToOne(targetEntity = SociedadRpl.class)
    private SociedadRpl sociedad;
    
    // Atributos de auditoria * * * * * * * * * * * * * * * * * * * * * * * * * 
    @Embedded
    private DatosAuditoria atributosAuditoria;

    // 2 Contructores * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public Almacen() {
    }

    public Almacen(String idAlmacen, String nombreAlmacen, String tipoAlmacen, ZonaGeografica zonaGeografica, SociedadRpl sociedad, DatosAuditoria atributosAuditoria) {
        this.idAlmacen = idAlmacen;
        this.nombreAlmacen = nombreAlmacen;
        this.tipoAlmacen = tipoAlmacen;
        this.zonaGeografica = zonaGeografica;
        this.sociedad = sociedad;
        this.atributosAuditoria = atributosAuditoria;
    }
    
    // Métodos id * * * * * * * * * * * * * * * * * * * * * * * * * * * * *      
    public String getIdAlmacen() {
        return this.idAlmacen;
    }

    public void setIdAlmacen(String idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    // Métodos Basicos * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public String getNombreAlmacen() {
        return nombreAlmacen;
    }

    public void setNombreAlmacen(String nombreAlmacen) {
        this.nombreAlmacen = nombreAlmacen;
    }

    public String getTipoAlmacen() {
        return tipoAlmacen;
    }

    public void setTipoAlmacen(String tipoAlmacen) {    
        this.tipoAlmacen = tipoAlmacen;
    }

    // Métodos Relaciones * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public ZonaGeografica getZonaGeografica() {
        return this.zonaGeografica;
    }

    public void setZonaGeografica(ZonaGeografica zonaGeografica) {
        this.zonaGeografica = zonaGeografica;
    }

    public SociedadRpl getSociedad() {
        return sociedad;
    }

    public void setSociedad(SociedadRpl sociedad) {
        this.sociedad = sociedad;
    }

    // Métodos auditoria * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    public DatosAuditoria getAtributosAuditoria() {
        return atributosAuditoria;
    }

    public void setAtributosAuditoria(DatosAuditoria atributosAuditoria) {
        this.atributosAuditoria = atributosAuditoria;
    }

    public String getDescripcionAlmacen() {
        return descripcionAlmacen;
    }

    public void setDescripcionAlmacen(String descripcionAlmacen) {
        this.descripcionAlmacen = descripcionAlmacen;
    }

    
    // Métodos Entity  * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Almacen other = (Almacen) obj;
        if (!Objects.equals(this.nombreAlmacen, other.nombreAlmacen)) {
            return false;
        }
        if (!Objects.equals(this.tipoAlmacen, other.tipoAlmacen)) {
            return false;
        }
        if (!Objects.equals(this.zonaGeografica, other.zonaGeografica)) {
            return false;
        }
        return true;
    }
/*
    @Override
    public String toString() {
        return "Almacen{" + "idAlmacen=" + idAlmacen + ", version=" + version + ", nombreAlmacen=" + nombreAlmacen + 
                ", tipoAlmacen=" + tipoAlmacen + ",[ zonaGeografica=" + zonaGeografica.toString() + "], [atributosAuditoria=" 
                + atributosAuditoria.toString() + "]}";
    }
*/
    
}
