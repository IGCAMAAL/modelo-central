package com.centraldecompras.modelo.ven.service;

import com.centraldecompras.modelo.DiasEntrega;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.ven.service.interfaces.DiasEntregaService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel
 */
@Service
public class DiasEntregaServiceImpl implements DiasEntregaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DiasEntregaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(DiasEntrega diasEntrega) throws PreexistingEntityException, Exception {
        try {
            em.persist(diasEntrega);
        } catch (Exception ex) {
            if (findDiasEntregaBy_S_Z(diasEntrega.getSociedad(), diasEntrega.getZonaGeografica()) != null) {
                throw new PreexistingEntityException("Comentario " + diasEntrega.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(DiasEntrega diasEntrega) throws NonexistentEntityException, Exception {
        try {
            diasEntrega = em.merge(diasEntrega);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                DiasEntrega diasEntregaN = findDiasEntregaBy_S_Z(diasEntrega.getSociedad(), diasEntrega.getZonaGeografica());
                if (diasEntregaN == null) {
                    throw new NonexistentEntityException("diasEntregaN  " + diasEntrega.toString() + " ya no existe.", ex);
                }
            }
            throw ex;
        }
    }

    public DiasEntrega findDiasEntrega(String id) {
        return em.find(DiasEntrega.class, id);
    }

    @Override
    public List<DiasEntrega> findDiasEntregaBy_idS(String sociedadid) {
        List<DiasEntrega> reply = null;

        try {
            reply = (List<DiasEntrega>) em.createNamedQuery("DiasEntrega.findDiasEntregaBy_idS")
                    .setParameter("sociedadid", sociedadid)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + "Sociedad {0} no tiene informada dias de entrega. " + sociedadid);
        }
        
        if (reply == null) {
            reply = new ArrayList();
        }
        
        return reply;
    }

    public DiasEntrega findDiasEntregaBy_S_Z(SociedadRpl sociedad, ZonaGeografica zonaGeografica) {
        DiasEntrega reply = null;

        try {
            reply = (DiasEntrega) em.createNamedQuery("DiasEntrega.findDiasEntregaBy_S_Z")
                    .setParameter("sociedad", sociedad)
                    .setParameter("zonaGeografica", zonaGeografica)
                    .getSingleResult();
        } catch (Exception ex) {
            log.warn(ex + "DiasEntrega: Sociedad / Zonageografica no encontrada. " + sociedad.toString() + " --- " + zonaGeografica.toString());
        }

        return reply;
    }

    public int destroyBy_S_Z(SociedadRpl sociedad, ZonaGeografica zonaGeografica) throws NonexistentEntityException {
        int reply = 0;

        try {
            reply = em.createNamedQuery("DiasEntrega.destroyBy_S_Z")
                    .setParameter("sociedad", sociedad)
                    .setParameter("zonaGeografica", zonaGeografica)
                    .executeUpdate();
        } catch (Exception ex) {
            log.warn(ex + "DiasEntrega: Sociedad / Zonageografica no encontrada. " + sociedad.toString() +" --- " + zonaGeografica.toString());
        }

        return reply;
    }

}
