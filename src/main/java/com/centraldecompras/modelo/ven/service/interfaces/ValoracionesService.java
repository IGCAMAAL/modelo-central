package com.centraldecompras.modelo.ven.service.interfaces;

import com.centraldecompras.modelo.Valoraciones;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ValoracionesService {

    void create(Valoraciones valoraciones) throws PreexistingEntityException, Exception;

    void edit(Valoraciones valoraciones) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    Valoraciones findValoraciones(String id);
    
    List<Valoraciones> findValoracionesBy_idS(String sociedadOpinadaid);
}
