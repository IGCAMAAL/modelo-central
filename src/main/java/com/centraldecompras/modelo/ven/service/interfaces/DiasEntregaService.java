package com.centraldecompras.modelo.ven.service.interfaces;

import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.modelo.DiasEntrega;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface DiasEntregaService {

    void create(DiasEntrega diasEntrega) throws PreexistingEntityException, Exception;

    void edit(DiasEntrega diasEntrega) throws NonexistentEntityException, Exception;

    DiasEntrega findDiasEntrega(String id);

    List<DiasEntrega> findDiasEntregaBy_idS(String sociedadid);

    DiasEntrega findDiasEntregaBy_S_Z(SociedadRpl sociedad, ZonaGeografica zonaGeografica);

    int destroyBy_S_Z(SociedadRpl sociedad, ZonaGeografica zonaGeografica) throws NonexistentEntityException;
}
