package com.centraldecompras.modelo.ven.service;

import com.centraldecompras.modelo.Comentarios;
import com.centraldecompras.modelo.Valoraciones;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.ven.service.interfaces.ValoracionesService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel
 */
@Service
public class ValoracionesServiceImpl implements ValoracionesService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ValoracionesServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(Valoraciones valoraciones) throws PreexistingEntityException, Exception {
        try {
            em.persist(valoraciones);
        } catch (Exception ex) {
            if (findValoraciones(valoraciones.getIdValoracion()) != null) {
                throw new PreexistingEntityException("Valoracion " + valoraciones.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(Valoraciones valoraciones) throws NonexistentEntityException, Exception {
        try {
            valoraciones = em.merge(valoraciones);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = valoraciones.getIdValoracion();
                if (findValoraciones(id) == null) {
                    throw new NonexistentEntityException("El Valoracion con id " + id + " ya no existe.", ex);
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Valoraciones valoraciones;
        try {
            valoraciones = em.getReference(Valoraciones.class, id);
            valoraciones.getIdValoracion();
        } catch (EntityNotFoundException ex) {
            throw new NonexistentEntityException("El Valoracion con id " + id + " ya no existe.", ex);
        }
        em.remove(valoraciones);
    }

    public Valoraciones findValoraciones(String id) {
        return em.find(Valoraciones.class, id);
    }

    public List<Valoraciones> findValoracionesBy_idS(String sociedadOpinadaid) {
        List<Valoraciones> reply = null;

        try {
            reply = (List<Valoraciones>) em.createNamedQuery("Valoraciones.findValoracionesBy_idS")
                    .setParameter("sociedadOpinadaid", sociedadOpinadaid)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + "Sociedades sin comentarios. " + sociedadOpinadaid); 
        }

        if (reply == null) {
            reply = new ArrayList();
        }

        return reply;
    }
}
