/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.ven.service;

import com.centraldecompras.modelo.Oferta;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.ven.service.interfaces.OfertaService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Miguel
 */
@Service
public class OfertaServiceImpl implements OfertaService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(OfertaServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void create(Oferta oferta) throws PreexistingEntityException, Exception {
        em.persist(oferta);
    }

    /**
     *
     * @param oferta
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Oferta oferta) throws NonexistentEntityException, Exception {
        try {
            oferta = em.merge(oferta);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = oferta.getIdOferta();
                if (findOferta(id) == null) {
                    throw new NonexistentEntityException("The oferta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     *
     * @param id
     * @throws NonexistentEntityException
     */
    public void destroy(String id) throws NonexistentEntityException {
        Oferta oferta;
        try {
            oferta = em.getReference(Oferta.class, id);
            oferta.getIdOferta();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The articulo with id " + id + " no longer exists.", enfe);
        }
        em.remove(oferta);
    }

    public List<Oferta> findOfertaEntities() {
        return findOfertaEntities(true, -1, -1);
    }

    public List<Oferta> findOfertaEntities(int maxResults, int firstResult) {
        return findOfertaEntities(false, maxResults, firstResult);
    }

    public List<Oferta> findOfertaEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Oferta.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     *
     * @param id
     * @return
     */
    public Oferta findOferta(String id) {
        return em.find(Oferta.class, id);
    }

    public int getOfertaCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Oferta> rt = cq.from(Oferta.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Oferta> findOfertasBy_idS_Activas(String sociedadId, List<String> estadosAprobacion, int fechaActual) {
        List<Oferta> reply = null;

        try {
            reply = (List<Oferta>) em.createNamedQuery("Oferta.findOfertasBy_idS_Activas")
                    .setParameter("sociedadId", sociedadId)
                    .setParameter("estadosAprobacion", estadosAprobacion)
                    .setParameter("fechaActual", fechaActual)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + "Ofertas Activas no encontradas para la sociedad . " + sociedadId);
        }
        if (reply == null) {
            reply = new ArrayList();
        }
        return reply;
    }

    public List<Oferta> findOfertasBy_idS_ALL(String sociedadId) {
        List<Oferta> reply = null;

        try {
            reply = (List<Oferta>) em.createNamedQuery("Oferta.findOfertasBy_idS_ALL")
                    .setParameter("sociedadId", sociedadId)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + "Ofertas no encontradas para la sociedad . " + sociedadId);
        }
        
        if (reply == null) {
            reply = new ArrayList();
        }
        return reply;
    }

}
