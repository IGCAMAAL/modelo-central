package com.centraldecompras.modelo.ven.service;

import com.centraldecompras.modelo.Comentarios;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;

import com.centraldecompras.modelo.ven.service.interfaces.ComentariosService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Miguel
 */
@Service
public class ComentariosServiceImpl implements ComentariosService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ComentariosServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(Comentarios comentario) throws PreexistingEntityException, Exception {
        try {
            em.persist(comentario);
        } catch (Exception ex) {
            if (findComentarios(comentario.getIdComentario()) != null) {
                throw new PreexistingEntityException("Comentario " + comentario.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(Comentarios comentarios) throws NonexistentEntityException, Exception {
        try {
            comentarios = em.merge(comentarios);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = comentarios.getIdComentario();
                if (findComentarios(id) == null) {
                    throw new NonexistentEntityException("El Comentario con id " + id + " ya no existe.", ex);
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        Comentarios comentarios;
        try {
            comentarios = em.getReference(Comentarios.class, id);
            comentarios.getIdComentario();
        } catch (EntityNotFoundException ex) {
            throw new NonexistentEntityException("El Comentario con id " + id + " ya no existe.", ex);
        }
        em.remove(comentarios);
    }

    public Comentarios findComentarios(String id) {
        return em.find(Comentarios.class, id);
    }

    public List<Comentarios> findComentariosBy_idS(String sociedadOpinadaid) {
        List<Comentarios> reply = null;

        try {
            reply = (List<Comentarios>) em.createNamedQuery("Comentarios.findComentariosBy_idS")
                    .setParameter("sociedadOpinadaid", sociedadOpinadaid)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + "Sociedades {0} sin comentarios. " + sociedadOpinadaid); 
        }
        
        if (reply == null) {
            reply = new ArrayList();
        }
        
        return reply;
    }
}
