/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.modelo.ven.service.interfaces;

import com.centraldecompras.modelo.Oferta;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface OfertaService {

    Oferta findOferta(String id);

    void create(Oferta oferta) throws PreexistingEntityException, Exception;

    void edit(Oferta oferta) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    List<Oferta> findOfertaEntities(boolean all, int maxResults, int firstResult);
    
    List<Oferta> findOfertasBy_idS_Activas(String sociedadId, List<String> estadosAprobacion, int fechaActual);

    List<Oferta> findOfertasBy_idS_ALL(String sociedadId);
}
