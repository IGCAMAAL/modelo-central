package com.centraldecompras.modelo.ven.service;

import com.centraldecompras.modelo.ConDatFra;
import com.centraldecompras.modelo.DiasEntrega;
import com.centraldecompras.modelo.lgt.service.AlmacenUsuarioProductoServiceImpl;
import com.centraldecompras.modelo.ven.service.interfaces.ConDatFraService;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;

@Service
public class ConDatFraServiceImpl implements ConDatFraService {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ConDatFraServiceImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;

    public void create(ConDatFra conDatFra) throws PreexistingEntityException, Exception {
        try {
            em.persist(conDatFra);
        } catch (Exception ex) {
            if (findConDatFra(conDatFra.getIdConDatFra()) != null) {
                throw new PreexistingEntityException("Condiciones/Datos facturacion " + conDatFra.toString() + " already exists.", ex);
            }
            throw ex;
        }
    }

    public void edit(ConDatFra conDatFra) throws NonexistentEntityException, Exception {
        try {
            conDatFra = em.merge(conDatFra);
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = conDatFra.getIdConDatFra();
                if (findConDatFra(id) == null) {
                    throw new NonexistentEntityException("Condiciones/Datos facturacion con id " + id + " ya no existe.", ex);
                }
            }
            throw ex;
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        ConDatFra conDatFra;
        try {
            conDatFra = em.getReference(ConDatFra.class, id);
            conDatFra.getIdConDatFra();
        } catch (EntityNotFoundException ex) {
            throw new NonexistentEntityException("Condiciones/Datos facturacion con id " + id + " ya no existe.", ex);
        }
        em.remove(conDatFra);
    }

    public ConDatFra findConDatFra(String id) {
        return em.find(ConDatFra.class, id);
    }

    public List<ConDatFra> findConDatFraBy_idS(String sociedadid) {
        List<ConDatFra> reply = null;

        try {
            reply = (List<ConDatFra>) em.createNamedQuery("ConDatFra.findConDatFraBy_idS")
                    .setParameter("sociedadid", sociedadid)
                    .getResultList();
        } catch (Exception ex) {
            log.warn(ex + "Sociedad no tiene informada condiciones o datos facturacion. " + sociedadid);
        }
        
        if (reply == null) {
            reply = new ArrayList();
        }
        return reply;
    }
}
