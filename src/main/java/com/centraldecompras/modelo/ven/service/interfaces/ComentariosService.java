package com.centraldecompras.modelo.ven.service.interfaces;

import com.centraldecompras.modelo.Comentarios;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ComentariosService {

    void create(Comentarios comentarios) throws PreexistingEntityException, Exception;

    void edit(Comentarios comentarios) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    Comentarios findComentarios(String id);

    List<Comentarios> findComentariosBy_idS(String sociedadid);
}
