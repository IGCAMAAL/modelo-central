package com.centraldecompras.modelo.ven.service.interfaces;

import com.centraldecompras.modelo.ConDatFra;
import com.centraldecompras.zglobal.exceptions.NonexistentEntityException;
import com.centraldecompras.zglobal.exceptions.PreexistingEntityException;
import java.util.List;

/**
 *
 * @author ciberado
 */
public interface ConDatFraService {

    void create(ConDatFra conDatFra) throws PreexistingEntityException, Exception;

    void edit(ConDatFra conDatFra) throws NonexistentEntityException, Exception;

    void destroy(String id) throws NonexistentEntityException;

    ConDatFra findConDatFra(String id);
    
    List<ConDatFra> findConDatFraBy_idS(String sociedadid);
}
