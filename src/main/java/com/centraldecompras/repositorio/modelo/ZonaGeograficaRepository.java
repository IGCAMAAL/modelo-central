package com.centraldecompras.repositorio.modelo;


import com.centraldecompras.modelo.ZonaGeografica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ZonaGeograficaRepository extends JpaRepository<ZonaGeografica, String> {

}
