package com.centraldecompras.repositorio.acceso;

import com.centraldecompras.acceso.Sociedad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SociedadRepository extends JpaRepository<Sociedad, String> {

}
