package com.centraldecompras.repositorio.acceso;

import com.centraldecompras.acceso.Sector;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorRepository extends JpaRepository<Sector, String> {

}
