package com.centraldecompras.repositorio.acceso;

import com.centraldecompras.acceso.UserCentral;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserCentralRepository extends JpaRepository<UserCentral, String> {

}
