package com.centraldecompras.repositorio.acceso;

import com.centraldecompras.acceso.UsrSocPrf;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsrSocPrfRepository extends JpaRepository<UsrSocPrf, String> {

}
