package com.centraldecompras.repositorio.acceso;

import com.centraldecompras.acceso.UsrSoc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsrSocRepository extends JpaRepository<UsrSoc, String> {

}
