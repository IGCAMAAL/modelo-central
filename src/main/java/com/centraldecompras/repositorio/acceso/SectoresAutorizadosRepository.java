package com.centraldecompras.repositorio.acceso;

import com.centraldecompras.acceso.SectoresAutorizados;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectoresAutorizadosRepository extends JpaRepository<SectoresAutorizados, String> {

}
