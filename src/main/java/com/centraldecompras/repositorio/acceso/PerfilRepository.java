package com.centraldecompras.repositorio.acceso;

import com.centraldecompras.acceso.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerfilRepository extends JpaRepository<Perfil, String> {

}
