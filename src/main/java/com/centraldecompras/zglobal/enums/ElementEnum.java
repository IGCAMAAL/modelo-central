package com.centraldecompras.zglobal.enums;

public class ElementEnum {

    public enum KApp {

        // Códigos Padre
// Códigos Padre
        ADMIN("ADMIN"),
        PRODUCTO_UNIVERSAL("PRODUCTO_UNIVERSAL"),
        ZONA_GEOGRAFICA_UNIVERSAL("ZONA_GEOGRAFICA_UNIVERSAL"),
        // Para selección sin filtro        
        TODO("TODO"),
        TODOS("TODOS"),
        SELECCIONA(".Selec."),
        // Estados de registro
        ACTIVO("AC"),
        DESACTIVADO("DA"),
        BLOQUEADO("BL"),
        // Metodos de acceso        
        GET("GET"),
        POST("POST"),
        PUT("PUT"),
        DELETE("DELETE"),
        DUMMY_METHOD("DUMMY"),
        // Tipos media   
        CATALOGO("CT"),
        CATALOGO_ARTICULO("CA"),
        FOTO_LOGO("FL"),
        FOTO_USUARIO("FU"),
        FOTO_PRODUCTO("FP"),
        FOTO_ARTICULO("FA"),
        // Estados
        SUCCESS("SUCCESS"),
        ERROR("ERROR"),
        // Conceptos de entrega
        CONDICIONES("CN"),
        DATOS_FACTURA("DF"),
        // Idioma catalan
        CATALAN("ca"),
        // Nombres archivos exportados a Excel
        PRODUCTOS("Productos"),
        ARTICULOS_PRODUCTOS("ArticulosProductos"),
        SOC_USU_PERF("SociedadesUsuariosPerfiles"),
        ZONAS_GEOGRAFICAS("ZonasGeografica"),
        // Extensiones Excel
        XLS("xls"),
        XLSX("xlsx"),
        // Attributos MAP
        NAME_FILE("NAME_FILE"),
        EXTENSION_FILE("EXTENSION_FILE"),
        PICTURE("PICTURE"),
        // Clasificacion articulos según asociado_administrador y asociado_comprador
        ART_DESTACADO("10"),
        ART_DEST_FAVO("20"),
        ART_FAVORITO("30"),
        ART_SIN_CLASIFICAR("70"),
        // Estados de aprobacion alta articulos y modificaciones precios
        PENDIENTE("PT"),
        PEND_ALTA_ART("PAA"),
        PEND_MOD_PRECIO("PMP"),
        APROBADO("AP"),
        NO_APROBADOD("NA"),
        ARTICULO_NUEVO("AN"),
        PRECIO_MODIFICADO("PM"),

        TOTAL_PTE_APROBACION("TOTAL_PTE_APROBACION"),
        
        // Estados cesta
        CESTA_ABIERTA("OPE"),     // Permite insertar y modificar lineas
        CESTA_CERRADA("CLO"),     // Se ha realizado la distribución en pedidos
        CESTA_ANULADA("ANU"),     // Se anula la cesta y las lineas de la cesta. Mejor borrar de la base de datos.
        CESTA_BLOQUEADA("BLQ"),   
        
        // Estados pedidos
        //PEDIDO_ABIERTO("OPE"),      // 
        //PEDIDO_ANULADA("ANU"),      //
        
        // La distribucion de la cesta en pedidos genera los siguientes estados de pedido
        PEDIDO_ACEPTADO("ACE"),     
        PEDIDO_PTE_ACEP("PTE"),       // Posteriormente pasa a ACE o RECHAZADO
        PEDIDO_RECHAZADO("RCZ"),      // No se borra de la bbddd      
        
        //PEDIDO_ENVIADO("ENV"),      //
        //PEDIDO_CONF_PARC("CFP"),    
        //PEDIDO_CNF_TOTAL("CFT"),    
        
        PEDIDO_CERRADO("CLO"),

        // Estados pedidos
        LINEA_ABIERTA("OPE"),       // Están en cesta y en pedido abierto y ACE, PTE, RCZ
        LINEA_ASIGNADA("ASG"),       // Linea asignada a pedido
        LINEA_NODISPO("LND"),       // El proveedor dice que no la entrega
        LINEA_ANULADA("ANU"),       //  Mejor borrar de la base de datos.             
        //LINEA_CERRADA("CLO"),       //   
        //LINEA_BLOQUEADA("BLQ"),     // Puede ocurrir que un comprador no tenga autorización para un producto. Entonces se bloquea la linas.           
        
        // Asignaciones de un almacén
        SOCIEDAD("S"),
        PERFIL("P"),
        DUMMY("DUMMY"),
        
        // Modo de compra        
        MC_OBLIGACION("O"),
        MC_LIBRE("L"),
        MC_REFERENCIA("P"),
        MC_MAS_BARATO("B"),
        
        // Estados Avatar
        AVATAR_EXCLUIDO("Excluido"),
        AVATAR_ASIGNADO("Asignado"),
        AVATAR_NEXC_NASG("NExcNAsg"),    
        
        // Tipos de numeradores
        NUM_FRA("FRA"),
        NUM_PDD("PDD"),
        NUM_ABO("ABO"),            
        ;

        private final String kApp;

        KApp(String consApp) {
            this.kApp = consApp;
        }

        public String getKApp() {
            return kApp;
        }

    }

    /*
     public static void main(String[] args) {
     for (ConsApp o : ConsApp.values()) {
     System.out.printf("El String %s tiene la clave %s \n", o, o.getObjUniversal());
     }
        
     log.info("Elementos sueltos");
     log.info(".............." + ConsApp.PRODUCTO.getConsApp());
     log.info(".............." + ConsApp.ZONAGEOGRAFICA.getConsApp());
     }
     */
}
