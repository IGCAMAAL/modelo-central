package com.centraldecompras.zglobal.enums;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Miguel
 */
public class UtilsMethods {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(UtilsMethods.class.getName());

    public static String convertDateToString(Date date, SimpleDateFormat sdf) {
        String s = sdf.format(date);
        return s;
    }

    public static Integer convertDateToInteger(Date date, SimpleDateFormat sdf) {
        String s = sdf.format(date);
        return Integer.parseInt(s);
    }  
    
    public static int convertDateToInt(Date date, SimpleDateFormat sdf) {
        String s = sdf.format(date);
        return Integer.parseInt(s);
    }  
    
    public static Date convertStringToDate(String dateString, SimpleDateFormat sdf) throws ParseException {
        Date date = sdf.parse(dateString);
        return date;
    }
 
}
