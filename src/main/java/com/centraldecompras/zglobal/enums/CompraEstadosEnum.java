package com.centraldecompras.zglobal.enums;

public class CompraEstadosEnum {
/*
    public enum KAcesta {

        CESTA_ABIERTA (
                "COP",
                ".","Y","Y","Y",    // CESTA:   Abrir,  Cerrar, Anular, Bloquear
                "Y",                // PEDIDO:  Add,    Update, Delete
                "Y",                // LINEA:   Add,    Update, Delete
                "Cesta Abierta. Permite: [En cesta: Cerrar, Anular y Bloquear],[En pedidos: add, upd, del],[En lineas: add, upd, del]"),
        CESTA_CERRADA (
                "CCL",
                ".",".",".",".",
                ".",
                ".",
                "Una cesta cerrada tiene creados los pedidos y asignados a proveedor"), 
        CESTA_ANULADA  (
                "CAN",
                ".",".",".",".",
                ".",
                ".",
                "Cesta anulada"), 
        CESTA_BLOQUEADA (
                "CBL",
                "Y","Y","Y",".",    // CESTA: Abrir, Cerrar, Anular, Bloquear
                ".",
                ".",
                "Cesta bloqueada por proceso de distribucion en pedidos. Permite: [En cesta: Abrir, Cerrar y Anular]"); //       

        private final String estado;
        private final String abrirC;    private final String cerrarC;   private final String anularC;   private final String bloquearC;
        private final String crudP;      
        private final String crudL;      
        private final String desc;
        
        KAcesta(String estado, 
                String abrirC, String cerrarC, String anularC, String bloquearC, 
                String crudP,  
                String crudL,  
                String desc) {
            this.estado = estado;
            this.abrirC = abrirC;   this.cerrarC = cerrarC;  this.anularC = anularC;  this.bloquearC = bloquearC;
            this.crudP = crudP;     
            this.crudL = crudL;    
            this.desc = desc;
        }

        public String getEstado() {return estado;}
        
        public String getAbrirC() {return abrirC; }
        public String getCerrarC() {return cerrarC;}
        public String getAnularC() {return anularC;}
        public String getBloquearC() {return bloquearC;}
        
        public String getCrudP() {return crudP; }
        public String getCrudL() {return crudL; }
        public String getDesc() {return desc;}
        
    }

    public enum KApedido {

        PEDIDO_ABIERTO (
                "POP",
                ".","Y","Y",".","Y",".",".",".","Y",    // PEDIDO: Abrir, Anular, SolicitarAprobacion, Aprobar, Enviar, Rechazar, ConfParcial, ConfTotal, Cerrar
                "Y","Y","Y",                            // Añadir, modificar, delete pedido
                "Y","Y","Y",                            // Añadir, modificar, delete linea
                "Se puede modificar cualquier parte del pedido"), //  
        PEDIDO_ANULADO  (
                "PAN",
                ".",".",".",".",".",".",".",".",".",
                ".",".",".",
                ".",".",".",
                "Solo se pueden anular los pedidos en estado POP. Los anulados quedan muertos para siempre."), //  
        PEDIDO_PTE_APROBACION (
                "PPA",
                ".","Y",".","Y",".","Y",".",".",".",
                ".",".",".",
                ".",".",".",
                "Pedidos que requieren la aprobación del nivel superior"), //  .
        PEDIDO_APROBADO (
                "PAP",
                ".","Y",".",".","Y","Y",".",".",".",
                ".",".",".",
                ".",".",".",
                "Pedido aprobado. Los PPA son aprobados por nivel superior"), //  
        PEDIDO_ENVIADO (
                "PSN",
                ".",".",".",".",".","Y","Y","Y",".",
                ".",".",".",
                ".",".",".",
                "Pedido enviado a proveedor. Ya no se pueden modificar por el comprador"), //  .
        PEDIDO_RECHAZADO (
                "PRJ",
                ".","Y",".",".",".",".",".",".",".",
                ".",".",".",
                ".",".",".",
                "El proveedor puede rechazar los pedidos"), //  
        PEDIDO_CONFIRMADO_PARCIAL (
                "PCP",
                ".",".",".",".",".",".","Y","Y",".",
                ".",".",".",
                ".",".",".",
                "El comprador informa que ha recibido parte de las lineas de pedido"), //    
        PEDIDO_CONFIRMADO_TOTAL (
                "PCT",
                ".",".",".",".",".",".",".",".","Y",
                ".",".",".",
                ".",".",".",
                "El comprador informa que ha recibido la totalidad de las lineas del pedido"), //  
        PEDIDO_CLOSE (
                "PCL",
                ".",".",".",".",".",".",".",".",".",
                ".",".",".",
                ".",".",".",
                "Pedido cerrado"); 

        private final String estado;
        private final String abrirP;        private final String anularP;       private final String solAP;
        private final String aprobarP;      private final String enviarP;       private final String rechazarP;
        private final String confPP;        private final String confTP;        private final String cerrarP;
        private final String addP;          private final String updP;          private final String delP;
        private final String addL;          private final String updL;          private final String delL;
        private final String desc;
        
        KApedido(
                String estado, 
                String abrirP,      String anularP, String solAP,
                String aprobarP,    String enviarP, String rechazarP,
                String confPP,      String confTP,  String cerrarP,
                String addP,        String updP,    String delP, 
                String addL,        String updL,    String delL, 
                String desc) {
            
            this.estado = estado;
            this.abrirP = abrirP;       this.anularP = anularP; this.solAP = solAP;
            this.aprobarP = aprobarP;   this.enviarP = enviarP; this.rechazarP = rechazarP;
            this.confPP = confPP;       this.confTP = confTP;   this.cerrarP = cerrarP;
            this.addP = addP;           this.updP = updP;       this.delP = delP;
            this.addL = addL;           this.updL = updL;       this.delL = delL;
            this.desc = desc;
        }

        public String getEstado() { return estado;}
        
        public String getAbrirP() {return abrirP;}      
        public String getAnularP() {return anularP;}  
        public String getSolAP() {return solAP;} 
        public String getAprobarP() {return aprobarP;}     
        public String getEnviarP() {return enviarP;}  
        public String getRechazarP() {return rechazarP;} 
        public String getConfPP() {return confPP;}      
        public String getConfTP() {return confTP;}   
        public String getCerrarP() {return cerrarP;} 

        public String getAddP() { return addP; }
        public String getUpdP() { return updP; }
        public String getDelP() { return delP; }
        
        public String getAddL() { return addL; }
        public String getUpdL() { return updL;}
        public String getDelL() { return delL;}
        public String getDesc() { return desc; }
        
    }

    public enum KAlinea {

        LINEA_ABIERTA  (
                "LOP",
                ".","Y","Y",    // Add, Update, Delete
                "Linea abierta. Permite update y delete"), 
        LINEA_RECHAZADA (
                "LRJ",
                ".",".",".",
                "El proveedor puede rechazar alguna linea"), //  
        LINEA_ANULADA  (
                "LAN",
                ".",".",".",
                "En principio este estado no puede existir"), // . 
        LINEA_ENVIADA (
                "LSN",
                ".",".",".",
                "En principio este estado no puede existir"), // . 
        LINEA_CONFIRMADA_PARCIAL (
                "LCP",
                ".",".",".",
                "El comprador informa que ha recibido parte de la linea de pedido"), //    
        LINEA_CONFIRMADA_TOTAL (
                "LCT",
                ".",".",".",
                "El comprador informa que ha recibido la totalidad de la linea del pedido"), //  
        LINEA_CERRADA  (
                "LCL",
                ".",".",".",
                "Linea cerrada"); //       

        private final String estado;
        private final String add;
        private final String upd;
        private final String del;
        private final String desc;
        
        KAlinea(String estado, String add, String upd, String del, String desc) {
            this.estado = estado;
            this.add = add;
            this.upd = upd;
            this.del = del;
            this.desc = desc;
        }

        public String getEstado() {return estado;}
        public String getAdd() {return add;}
        public String getUpd() {return upd;}
        public String getDel() {return del;}
        public String getDesc() {return desc;}
        
    }
*/
}
