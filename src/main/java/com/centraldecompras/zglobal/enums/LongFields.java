package com.centraldecompras.zglobal.enums;


public interface LongFields {

    final static int id = 36;
    final static int estadoRegistro = 2;
    final static int ultimaAccion = 7;
    final static int fecha8N = 8;
    final static int compraEstado = 3;
    final static int moneda = 3;
    
    final static int N2 = 2;
    final static int N3 = 3;
    final static int N4 = 4;
    final static int N17 = 17;
}
