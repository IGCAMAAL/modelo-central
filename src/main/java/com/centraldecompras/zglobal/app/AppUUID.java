/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.centraldecompras.zglobal.app;

import java.util.UUID;

/**
 *
 * @author Miguel
 */
public interface AppUUID {

    // Creación de Personas -------------------------------------------------------------------------  
    static String prs1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prs2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prs3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prs4 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de sociedades -----------------------------------------------------------------------
    static String soc1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String soc2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String soc3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String soc4 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de perfiles ------------------------------------------------    
    static String prf1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prf2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prf3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prf4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prf5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prf6 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Relacion persona / sociedad ----------------------------- 
    static String prsSoc1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prsSoc2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prsSoc3 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de persona / sociedad / perfil ----------------------------- 
    static String uuid61 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uuid62 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uuid63 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uuid64 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uuid65 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uuid66 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uuid67 = UUID.randomUUID().toString().replaceAll("-", "");

    
    // Creación de Contactos ----------------------------------------------------------------
    static String con1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String con2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String con3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String con4 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Contactos Tipo-----------------------------------
    static String conTip11 = UUID.randomUUID().toString().replaceAll("-", "");
    static String conTip12 = UUID.randomUUID().toString().replaceAll("-", "");
    static String conTip21 = UUID.randomUUID().toString().replaceAll("-", "");
    static String conTip22 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Direccin -----------------------------------------------------------------
    static String dir1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dir2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dir3 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Direccin Google Type-----------------------------
    static String dirGooTyp11 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooTyp12 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooTyp21 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooTyp22 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Direccin Google Componente-----------------------
    static String dirGooCmp11 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooCmp12 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooCmp21 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooCmp22 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Direccin Google Componente Type------------------
    static String dirGooCmpTyp11 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooCmpTyp12 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooCmpTyp21 = UUID.randomUUID().toString().replaceAll("-", "");
    static String dirGooCmpTyp22 = UUID.randomUUID().toString().replaceAll("-", "");

    // -------- Zonas geograficas -------------------------------------------------------------------
    static String zon1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zon2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zon3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zon4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zon5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zon6 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zon7 = UUID.randomUUID().toString().replaceAll("-", "");

    static String alm1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String alm2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String alm3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String alm4 = UUID.randomUUID().toString().replaceAll("-", "");

    static String soZo1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String soZo2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String soZo3 = UUID.randomUUID().toString().replaceAll("-", "");
    
    // Creación de formatos de venta  -----------------------------
    static String fmtVta1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String fmtVta2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String fmtVta3 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Formato Venta Language ----------------------------- 
    static String fmtVtaLang1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String fmtVtaLang2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String fmtVtaLang3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String fmtVtaLang4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String fmtVtaLang5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String fmtVtaLang6 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Genero Jerarquia ----------------------------- 
    static String gnrJer1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJer2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJer3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJer4 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Genero Jerarquia Language ----------------------------- 
    static String gnrJerLang1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJerLang2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJerLang3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJerLang4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJerLang5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJerLang6 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJerLang7 = UUID.randomUUID().toString().replaceAll("-", "");
    static String gnrJerLang8 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Sector ----------------------------- 
    static String sct1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sct2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sct3 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Sectores Autorizados ----------------------------- 
    static String sctAut1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut6 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut7 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut8 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut9 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut10 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut11 = UUID.randomUUID().toString().replaceAll("-", "");
    static String sctAut12 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de articulo ----------------------------- 
    static String art1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String art2 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de producto ----------------------------- 
    static String prd1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prd2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prd3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String prd4 = UUID.randomUUID().toString().replaceAll("-", "");
    
    // Creación de sociedad / producto ----------------------------- 
    static String socPrd1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String socPrd2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String socPrd3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String socPrd4 = UUID.randomUUID().toString().replaceAll("-", "");
    
    // Creación de articulo / producto ----------------------------- 
    static String artPrd1 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de jerarquia ----------------------------- 
    static String jer1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String jer2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String jer3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String jer4 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Unidad Formato venta ----------------------------- 
    static String uFmtVta1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVta2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVta3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVta4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVta5 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Unidad Formato venta Lang----------------------------- 
    static String uFmtVtaLang1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang6 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang7 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang8 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang9 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uFmtVtaLang10 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Unidad de Medida ----------------------------- 
    static String uMed1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMed2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMed3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMed4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMed5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMed6 = UUID.randomUUID().toString().replaceAll("-", "");

    // Creación de Unidad de Medida Lang ----------------------------- 
    static String uMedLang1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang4 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang5 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang6 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang7 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang8 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang9 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang10 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang11 = UUID.randomUUID().toString().replaceAll("-", "");
    static String uMedLang12 = UUID.randomUUID().toString().replaceAll("-", "");

    // -------- Almacenes autorizados--------------------------------------------
    static String almAut1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String almAut2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String almAut3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String almAut4 = UUID.randomUUID().toString().replaceAll("-", "");

    // --------  Sociedades / Almacenes -----------------------------------------   
    static String socAlm1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String socAlm2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String socAlm3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String socAlm4 = UUID.randomUUID().toString().replaceAll("-", "");

    // -------- Zonas geograficas autorizadas--------------------------------------------    
    static String zonAut1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zonAut2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zonAut3 = UUID.randomUUID().toString().replaceAll("-", "");
    static String zonAut4 = UUID.randomUUID().toString().replaceAll("-", "");

    // -------- Cesta Cabecera--------------------------------------------
    static String ces1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String ces2 = UUID.randomUUID().toString().replaceAll("-", "");

    // -------- Pedido Cabecera --------------------------------------------
    static String pedCab1 = UUID.randomUUID().toString().replaceAll("-", "");
    static String pedCab2 = UUID.randomUUID().toString().replaceAll("-", "");
    static String pedCab3 = UUID.randomUUID().toString().replaceAll("-", "");

}
