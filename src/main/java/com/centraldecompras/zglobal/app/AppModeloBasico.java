package com.centraldecompras.zglobal.app;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.UserCentralServiceImpl;
import com.centraldecompras.acceso.service.interfaces.PerfilService;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.acceso.service.interfaces.SectoresAutorizadosService;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoService;
import com.centraldecompras.modelo.PerfilRpl;
import com.centraldecompras.modelo.SectorRpl;
import com.centraldecompras.modelo.SectoresAutorizadosRpl;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.UsrSocPrfRpl;
import com.centraldecompras.modelo.UsrSocRpl;
import com.centraldecompras.modelo.rpl.service.interfaces.PerfilRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.SectorRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.SectoresAutorizadosRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.UsrSocPrfRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.UsrSocRplService;
import static com.centraldecompras.zglobal.app.AppModelo.atributosAuditoriaAcceso;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@Configuration
@EnableAutoConfiguration

@ComponentScan(value = {
    "com.centraldecompras.acceso",
    "com.centraldecompras.modelo",
    "com.javiermoreno.springboot.modelo"
})
@EntityScan(basePackages = {
    "com.centraldecompras.acceso",
    "com.centraldecompras.modelo",
    "com.javiermoreno.springboot.modelo"
})

@EnableTransactionManagement
public class AppModeloBasico implements AppUUID {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UserCentralServiceImpl.class.getName());
    
    static int fechaNacimiento = 19981231;
    static int fechaPrevistaActivacion = 0;
    static int fechaPrevistaDesactivacion = 0;
    static DatosAuditoria atributosAuditoria = new DatosAuditoria("AC", "A", "Usu", fechaPrevistaActivacion, fechaPrevistaDesactivacion);
    static DatosAuditoriaAcceso atributosAuditoriaAcceso = new DatosAuditoriaAcceso("AC", "A", "Usu", fechaPrevistaActivacion, fechaPrevistaDesactivacion);

    /*
     public static void main(String[] args) {
     log.warn("####################################### ....................Inicio.");
     preparacionDatosBasicos(args);
     }
     */
    public static void preparacionDatosBasicos(String[] args) {
        BasicConfigurator.configure();
        ConfigurableApplicationContext context = new SpringApplicationBuilder(new Object[0])
                .showBanner(true)
                .sources(new Class[]{AppModeloBasico.class})
                .run(args);

        Date date = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(5, 1);

        try {
            date = simpleDateFormat.parse("13-07-1999");
        } catch (ParseException ex) {
            Logger.getLogger(AppModeloBasico.class.getName()).log(Level.SEVERE, null, ex);
        }

        String Sprs1 = AppUUID.prs1;

        String Ssoc1 = AppUUID.soc1;

        String Sprf1 = AppUUID.prf1;
        String Sprf2 = AppUUID.prf2;
        String Sprf3 = AppUUID.prf3;
        String Sprf4 = AppUUID.prf4;
        String Sprf5 = AppUUID.prf5;
        String Sprf6 = AppUUID.prf6;

        String SprsSoc1 = AppUUID.prsSoc1;

        String Suuid61 = AppUUID.uuid61;

        String Ssct1 = AppUUID.sct1;
        String Ssct2 = AppUUID.sct2;

        String SsctAut1 = AppUUID.sctAut1;

        if (args.length == 1 && args[0].equals("cargaDatosBasicos")) {
            creaDatosBasicos(
                    context,
                    fechaNacimiento,
                    fechaPrevistaActivacion,
                    fechaPrevistaDesactivacion,
                    Sprs1, Ssoc1, Sprf1, Sprf2, Sprf3, Sprf4, Sprf5, Sprf6,
                    SprsSoc1, Suuid61, Ssct1, Ssct2, SsctAut1
            );

        }
    }

    @Transactional
    public static void creaDatosBasicos(ConfigurableApplicationContext context, int fechaNacimiento, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion,
            String prs1, String soc1, String prf1, String prf2, String prf3, String prf4, String prf5, String prf6, String prsSoc1, String uuid61, String Ssct1, String Ssct2, String SsctAut1
    ) {

        SociedadService sociedadService = (SociedadService) context.getBean(SociedadService.class);
        UserCentralService userCentralService = (UserCentralService) context.getBean(UserCentralService.class);
        PerfilService perfilService = (PerfilService) context.getBean(PerfilService.class);
        UsrSocService usrSocService = (UsrSocService) context.getBean(UsrSocService.class);
        UsrSocPrfService usrSocPrfService = (UsrSocPrfService) context.getBean(UsrSocPrfService.class);
        SectorService sectorService = (SectorService) context.getBean(SectorService.class);
        SectoresAutorizadosService sectoresAutorizadosService = (SectoresAutorizadosService) context.getBean(SectoresAutorizadosService.class);

        SociedadRplService sociedadRplService = (SociedadRplService) context.getBean(SociedadRplService.class);        
        PersonaService personaService = (PersonaService) context.getBean(PersonaService.class);
        PerfilRplService perfilRplService = (PerfilRplService) context.getBean(PerfilRplService.class);
        UsrSocRplService usrSocRplService = (UsrSocRplService) context.getBean(UsrSocRplService.class);
        UsrSocPrfRplService usrSocPrfRplService = (UsrSocPrfRplService) context.getBean(UsrSocPrfRplService.class);
        SectorRplService sectorRplService = (SectorRplService) context.getBean(SectorRplService.class);
        SectoresAutorizadosRplService sectoresAutorizadosRplService = (SectoresAutorizadosRplService) context.getBean(SectoresAutorizadosRplService.class);
        
        NivelProductoService nivelProductoService = (NivelProductoService) context.getBean(NivelProductoService.class);
        ProductoService productoService = (ProductoService) context.getBean(ProductoService.class);
        ZonaGeograficaService zonaGeograficaService = (ZonaGeograficaService) context.getBean(ZonaGeograficaService.class);

        try {
            //sociedadService.create_C(new Sociedad(KApp.ADMIN.getKApp(), "admin", "admin", "admin", null, atributosAuditoriaAcceso));
            //userCentralService.create_C(new UserCentral(prs1, "admin", "admin", "mail1@gmail.com", atributosAuditoriaAcceso));
            //usrSocService.create_C(new UsrSoc(prsSoc1, userCentralService.findUserCentral(prs1), sociedadService.findSociedad(KApp.ADMIN.getKApp()), atributosAuditoriaAcceso));            
            
            sociedadService.create_C(new Sociedad(soc1, "NIF", "nifnrf-1", "nombre Sociedad 1", null, atributosAuditoriaAcceso));
            userCentralService.create_C(new UserCentral(prs1, "admin", "admin", "mail1@gmail.com", atributosAuditoriaAcceso));
            usrSocService.create_C(new UsrSoc(prsSoc1, userCentralService.findUserCentral(prs1), sociedadService.findSociedad(soc1), atributosAuditoriaAcceso));
            
            perfilService.create_C(new Perfil(prf1, "DUMMY ", "Perfil para relación Persona/Empresa", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf2, "ADMINA", "Administrador aplicación", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf3, "ASOADM", "Asociado Administrador", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf4, "ASOCOM", "Asociado Comprador", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf5, "PRVDST", "Distribuidor", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf6, "PRVFAB", "Fabricante", atributosAuditoriaAcceso));

            usrSocPrfService.create_C(new UsrSocPrf(uuid61, true, "mail", usrSocService.findUsrSoc(prsSoc1), perfilService.findPerfil(prf2), null, atributosAuditoriaAcceso));

            //sectorService.create_C(new Sector(Ssct1, "DUMMY", 4,atributosAuditoriaAcceso));
            sectorService.create_C(new Sector(Ssct2, "Hotelero", "",  "", "root", "root", "localhost", 8000, 8001, 4, atributosAuditoriaAcceso));
            sectorService.create_C(new Sector(Ssct2, "Hotelero", "",  "", "root", "bragasdeesparto", "ec2-54-76-103-159.eu-west-1.compute.amazonaws.com", 8000, 8001, 4, atributosAuditoriaAcceso));
            sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut1, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0),usrSocPrfService.findUsrSocPrf(uuid61), sectorService.findSector(Ssct2), atributosAuditoriaAcceso));

             
            // - - - - - - - - - - - - - - - - - -
            atributosAuditoria.setUsuarioUltimaAccion("SetUp");
            sociedadRplService.create_C(new SociedadRpl(KApp.ADMIN.getKApp()));
            personaService.create_C(new Persona(prs1, "admin", "nombre-admin", "apellido1-admin", "apellido2-admin", "cifpasport-1", fechaNacimiento, "email1_admin@gmail.com", atributosAuditoria));
            usrSocRplService.create_C(new UsrSocRpl(prsSoc1));
            
            SectorRpl sectorRpl2 = new SectorRpl(Ssct2);
            sectorRplService.create_C(sectorRpl2);

            perfilRplService.create_C(new PerfilRpl(prf1));
            perfilRplService.create_C(new PerfilRpl(prf2));
            perfilRplService.create_C(new PerfilRpl(prf3));
            perfilRplService.create_C(new PerfilRpl(prf4));
            perfilRplService.create_C(new PerfilRpl(prf5));
            perfilRplService.create_C(new PerfilRpl(prf6));

            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid61, true));
            //sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut1));
            
            nivelProductoService.create_C(new NivelProducto(1, "Familia", sectorRplService.findSectorRpl(Ssct2), atributosAuditoria));
            nivelProductoService.create_C(new NivelProducto(2, "SubFamilia", sectorRplService.findSectorRpl(Ssct2), atributosAuditoria));
            nivelProductoService.create_C(new NivelProducto(3, "Categoria", sectorRplService.findSectorRpl(Ssct2), atributosAuditoria));
            nivelProductoService.create_C(new NivelProducto(4, "SubCategoria", sectorRplService.findSectorRpl(Ssct2), atributosAuditoria));
            // sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut2));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(UUID.randomUUID().toString().replaceAll("-", "")));    
            

            zonaGeograficaService.create_C(new ZonaGeografica(KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp(), 0, " ", " ", " ", null, atributosAuditoria));    
            
        } catch (Exception ex) {
            Logger.getLogger(AppModelo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
