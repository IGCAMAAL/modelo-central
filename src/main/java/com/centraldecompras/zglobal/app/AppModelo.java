package com.centraldecompras.zglobal.app;

import com.centraldecompras.acceso.DatosAuditoriaAcceso;
import com.centraldecompras.acceso.Perfil;
import com.centraldecompras.acceso.Sector;
import com.centraldecompras.acceso.SectoresAutorizados;
import com.centraldecompras.acceso.Sociedad;
import com.centraldecompras.acceso.UserCentral;
import com.centraldecompras.acceso.UsrSoc;
import com.centraldecompras.acceso.UsrSocPrf;
import com.centraldecompras.acceso.service.UserCentralServiceImpl;
import com.centraldecompras.acceso.service.interfaces.PerfilService;
import com.centraldecompras.acceso.service.interfaces.SectorService;
import com.centraldecompras.acceso.service.interfaces.SectoresAutorizadosService;
import com.centraldecompras.acceso.service.interfaces.SociedadService;
import com.centraldecompras.acceso.service.interfaces.UserCentralService;
import com.centraldecompras.acceso.service.interfaces.UsrSocPrfService;
import com.centraldecompras.acceso.service.interfaces.UsrSocService;
import com.centraldecompras.modelo.rpl.service.interfaces.PersonaService;
import com.centraldecompras.modelo.Contacto;
import com.centraldecompras.modelo.DatosAuditoria;
import com.centraldecompras.modelo.Direccion;
import com.centraldecompras.modelo.DireccionGoogleComponente;
import com.centraldecompras.modelo.DireccionGoogleType;
import com.centraldecompras.modelo.cmo.service.interfaces.ContactoService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleComponenteService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionGoogleTypeService;
import com.centraldecompras.modelo.cmo.service.interfaces.DireccionService;
import com.centraldecompras.modelo.Persona;
import com.centraldecompras.modelo.Almacen;
import com.centraldecompras.modelo.SocFab;
import com.centraldecompras.modelo.SocZonGeo;
import com.centraldecompras.modelo.ZonaGeografica;
import com.centraldecompras.modelo.ZonasGeograficasAutorizadas;
import com.centraldecompras.modelo.lgt.service.interfaces.AlmacenService;
import com.centraldecompras.modelo.lgt.service.interfaces.SocFabService;
import com.centraldecompras.modelo.lgt.service.interfaces.SocZonGeoService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonaGeograficaService;
import com.centraldecompras.modelo.lgt.service.interfaces.ZonasGeograficasAutorizadasService;
import com.centraldecompras.modelo.Producto;
import com.centraldecompras.modelo.ProductoLang;
import com.centraldecompras.modelo.mat.service.interfaces.ArticuloService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoLangService;
import com.centraldecompras.modelo.mat.service.interfaces.ProductoService;
import com.centraldecompras.modelo.FormatoVta;
import com.centraldecompras.modelo.FormatoVtaLang;
import com.centraldecompras.modelo.NivelProducto;
import com.centraldecompras.modelo.NivelProductoLang;
import com.centraldecompras.modelo.UnidadFormatoVta;
import com.centraldecompras.modelo.UnidadFormatoVtaLang;
import com.centraldecompras.modelo.UnidadMedida;
import com.centraldecompras.modelo.UnidadMedidaLang;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.FormatoVtaService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoLangService;
import com.centraldecompras.modelo.prm.service.interfaces.NivelProductoService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadFormatoVtaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadFormatoVtaService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaLangService;
import com.centraldecompras.modelo.prm.service.interfaces.UnidadMedidaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraCestaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraLineaService;
import com.centraldecompras.modelo.pur.service.interfaces.CompraPedidoPieLinService;
import com.centraldecompras.modelo.PerfilRpl;
import com.centraldecompras.modelo.SectorRpl;
import com.centraldecompras.modelo.SectoresAutorizadosRpl;
import com.centraldecompras.modelo.SocProducto;
import com.centraldecompras.modelo.SociedadRpl;
import com.centraldecompras.modelo.UsrSocPrfRpl;
import com.centraldecompras.modelo.UsrSocRpl;
import com.centraldecompras.modelo.mat.service.interfaces.SocProductoService;
import com.centraldecompras.modelo.rpl.service.interfaces.PerfilRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.SectorRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.SectoresAutorizadosRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.SociedadRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.UsrSocPrfRplService;
import com.centraldecompras.modelo.rpl.service.interfaces.UsrSocRplService;
import com.centraldecompras.zglobal.enums.ElementEnum.KApp;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.transaction.annotation.Transactional;
import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration

@ComponentScan(value = {
    "com.centraldecompras.acceso",
    "com.centraldecompras.modelo",
    "com.javiermoreno.springboot.modelo"
})
@EntityScan(basePackages = {
    "com.centraldecompras.acceso",
    "com.centraldecompras.modelo",
    "com.javiermoreno.springboot.modelo"
})

@EnableTransactionManagement
public class AppModelo implements AppUUID {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UserCentralServiceImpl.class.getName());
    
    static int fechaNacimiento = 19981231;
    static int fechaPrevistaActivacion = 0;
    static int fechaPrevistaDesactivacion = 0;
    static DatosAuditoria atributosAuditoria = new DatosAuditoria("AC", "A", "Usu", fechaPrevistaActivacion, fechaPrevistaDesactivacion);
    static DatosAuditoriaAcceso atributosAuditoriaAcceso = new DatosAuditoriaAcceso("AC", "A", "Usu", fechaPrevistaActivacion, fechaPrevistaDesactivacion);

    public static void main(String[] args) throws Exception {
        log.warn("####################################### ....................Inicio.");
        
        preparacionDatos(args);
    }

    public static void preparacionDatos(String[] args) throws Exception {
        BasicConfigurator.configure();
        ConfigurableApplicationContext context = new SpringApplicationBuilder(new Object[0])
                .showBanner(true)
                .sources(new Class[]{AppModelo.class})
                .run(args);

        Date date = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(5, 1);

        try {
            date = simpleDateFormat.parse("13-07-1999");
        } catch (ParseException ex) {
            Logger.getLogger(AppModelo.class.getName()).log(Level.SEVERE, null, ex);
        }

        String Sprs1 = AppUUID.prs1;
        String Sprs2 = AppUUID.prs2;
        String Sprs3 = AppUUID.prs3;
        String Sprs4 = AppUUID.prs4;

        String Ssoc1 = AppUUID.soc1;
        String Ssoc2 = AppUUID.soc2;
        String Ssoc3 = AppUUID.soc3;
        String Ssoc4 = AppUUID.soc4;

        String Sprf1 = AppUUID.prf1;
        String Sprf2 = AppUUID.prf2;
        String Sprf3 = AppUUID.prf3;
        String Sprf4 = AppUUID.prf4;
        String Sprf5 = AppUUID.prf5;
        String Sprf6 = AppUUID.prf6;

        String SprsSoc1 = AppUUID.prsSoc1;
        String SprsSoc2 = AppUUID.prsSoc2;
        String SprsSoc3 = AppUUID.prsSoc3;

        String Suuid61 = AppUUID.uuid61;
        String Suuid62 = AppUUID.uuid62;
        String Suuid63 = AppUUID.uuid63;
        String Suuid64 = AppUUID.uuid64;
        String Suuid65 = AppUUID.uuid65;
        String Suuid66 = AppUUID.uuid66;
        String Suuid67 = AppUUID.uuid67;

        //String Ssct1 = AppUUID.sct1;
        String Ssct2 = AppUUID.sct2;
        //String Ssct3 = AppUUID.sct3;

        String SsctAut1 = AppUUID.sctAut1;
        String SsctAut2 = AppUUID.sctAut2;
        String SsctAut3 = AppUUID.sctAut3;
        String SsctAut4 = AppUUID.sctAut4;
        String SsctAut5 = AppUUID.sctAut5;
        String SsctAut6 = AppUUID.sctAut6;
        String SsctAut7 = AppUUID.sctAut7;
        String SsctAut8 = AppUUID.sctAut8;
        String SsctAut9 = AppUUID.sctAut9;
        String SsctAut10 = AppUUID.sctAut10;
        String SsctAut11 = AppUUID.sctAut11;
        String SsctAut12 = AppUUID.sctAut12;

        if (args.length == 1 && args[0].equals("cargacondatos")) {
            creaDatosAccesos(
                    context,
                    fechaNacimiento,
                    fechaPrevistaActivacion,
                    fechaPrevistaDesactivacion,
                    Sprs1, Sprs2, Sprs3, Sprs4, Ssoc1, Ssoc2, Ssoc3, Ssoc4, Sprf1, Sprf2, Sprf3, Sprf4, Sprf5, Sprf6, SprsSoc1, SprsSoc2, SprsSoc3,
                    Suuid61, Suuid62, Suuid63, Suuid64, Suuid65, Suuid66, Suuid67,
                    /* Ssct1, */ Ssct2, /* Ssct3, */
                    SsctAut1, SsctAut2, SsctAut3, SsctAut4,
                    SsctAut5, SsctAut6, SsctAut7, SsctAut8,
                    SsctAut9, SsctAut10, SsctAut11, SsctAut12
            );
            creaDatos(
                    context,
                    fechaNacimiento,
                    fechaPrevistaActivacion,
                    fechaPrevistaDesactivacion,
                    Sprs1, Sprs2, Sprs3, Sprs4, Ssoc1, Ssoc2, Ssoc3, Ssoc4, Sprf1, Sprf2, Sprf3, Sprf4, Sprf5, Sprf6, SprsSoc1, SprsSoc2, SprsSoc3,
                    Suuid61, Suuid62, Suuid63, Suuid64, Suuid65, Suuid66, Suuid67,
                   /* Ssct1, */ Ssct2, /* Ssct3, */
                    SsctAut1, SsctAut2, SsctAut3, SsctAut4,
                    SsctAut5, SsctAut6, SsctAut7, SsctAut8,
                    SsctAut9, SsctAut10, SsctAut11, SsctAut12
            );

        }
    }

    @Transactional
    public static void creaDatosAccesos(ConfigurableApplicationContext context, int fechaNacimiento, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion,
            String prs1, String prs2, String prs3, String prs4, String soc1, String soc2, String soc3, String soc4,
            String prf1, String prf2, String prf3, String prf4, String prf5, String prf6, String prsSoc1, String prsSoc2, String prsSoc3,
            String uuid61, String uuid62, String uuid63, String uuid64, String uuid65, String uuid66, String uuid67,
           /* String Ssct1,*/ String Ssct2, /* String Ssct3, */
            String SsctAut1, String SsctAut2, String SsctAut3, String SsctAut4,
            String SsctAut5, String SsctAut6, String SsctAut7, String SsctAut8,
            String SsctAut9, String SsctAut10, String SsctAut11, String SsctAut12
    ) {
        SociedadService sociedadService = (SociedadService) context.getBean(SociedadService.class);
        UserCentralService userCentralService = (UserCentralService) context.getBean(UserCentralService.class);
        PerfilService perfilService = (PerfilService) context.getBean(PerfilService.class);
        UsrSocService usrSocService = (UsrSocService) context.getBean(UsrSocService.class);
        UsrSocPrfService usrSocPrfService = (UsrSocPrfService) context.getBean(UsrSocPrfService.class);
        SectorService sectorService = (SectorService) context.getBean(SectorService.class);
        SectoresAutorizadosService sectoresAutorizadosService = (SectoresAutorizadosService) context.getBean(SectoresAutorizadosService.class);

        try {

            //sociedadService.create_C(new Sociedad(KApp.ADMIN.getKApp(), "admin", "admin", "admin", null, atributosAuditoriaAcceso));
            //userCentralService.create_C(new UserCentral(prs1, "admin", "admin", "mail1@gmail.com", atributosAuditoriaAcceso));
            //usrSocService.create_C(new UsrSoc(prsSoc1, userCentralService.findUserCentral(prs1), sociedadService.findSociedad(KApp.ADMIN.getKApp()), atributosAuditoriaAcceso));            
            // Creación de sociedades ------------------------------------------------------------------------
            sociedadService.create_C(new Sociedad(soc1, "NIF", "nifnrf-1", "nombre Sociedad 1", null, atributosAuditoriaAcceso));
            sociedadService.create_C(new Sociedad(soc2, "NIF", "nifnrf-2", "nombre Sociedad 2", null, atributosAuditoriaAcceso));
            sociedadService.create_C(new Sociedad(soc3, "NIF", "nifnrf-3", "nombre Sociedad 3", null, atributosAuditoriaAcceso));
            sociedadService.create_C(new Sociedad(soc4, "NIF", "nifnrf-4", "nombre Sociedad 4", null, atributosAuditoriaAcceso));

            // Creación de usuarios ------------------------------------------------------------------------
            userCentralService.create_C(new UserCentral(prs1, "admin", "admin", "mail1@gmail.com", atributosAuditoriaAcceso));
            userCentralService.create_C(new UserCentral(prs2, "idWeb-2", "password-2", "mail2@gmail.com", atributosAuditoriaAcceso));
            userCentralService.create_C(new UserCentral(prs3, "idWeb-3", "password-3", "mail3@gmail.com", atributosAuditoriaAcceso));
            userCentralService.create_C(new UserCentral(prs4, "idWeb-4", "password-4", "mail4@gmail.com", atributosAuditoriaAcceso));

            // Creación de Relacion persona / sociedad ------------------------------------------------    
            usrSocService.create_C(new UsrSoc(prsSoc1, userCentralService.findUserCentral(prs1), sociedadService.findSociedad(soc1), atributosAuditoriaAcceso));
            usrSocService.create_C(new UsrSoc(prsSoc2, userCentralService.findUserCentral(prs1), sociedadService.findSociedad(soc2), atributosAuditoriaAcceso));
            usrSocService.create_C(new UsrSoc(prsSoc3, userCentralService.findUserCentral(prs2), sociedadService.findSociedad(soc2), atributosAuditoriaAcceso));

            // Creación de perfiles --------------------------------------------
            perfilService.create_C(new Perfil(prf1, "DUMMY ", "Perfil para relación Persona/Empresa", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf2, "ADMINA", "Administrador aplicación", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf3, "ASOADM", "Asociado Administrador", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf4, "ASOCOM", "Asociado Comprador", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf5, "PRVDST", "Distribuidor", atributosAuditoriaAcceso));
            perfilService.create_C(new Perfil(prf6, "PRVFAB", "Fabricante", atributosAuditoriaAcceso));

            // Creación de persona / sociedad / perfil ------------------------------------------------  
            Boolean verdad = true;
            //usrSocPrfService.create_C(new UsrSocPrf(uuid61, verdad, usrSocService.findUsrSoc(prsSoc1), perfilService.findPerfil(prf1), null, atributosAuditoriaAcceso));
            usrSocPrfService.create_C(new UsrSocPrf(uuid64, verdad, "mail", usrSocService.findUsrSoc(prsSoc1), perfilService.findPerfil(prf2), null, atributosAuditoriaAcceso));
            //usrSocPrfService.create_C(new UsrSocPrf(uuid65, verdad, usrSocService.findUsrSoc(prsSoc1), perfilService.findPerfil(prf3), null, atributosAuditoriaAcceso));
            usrSocPrfService.create_C(new UsrSocPrf(uuid62, verdad, "mail", usrSocService.findUsrSoc(prsSoc2), perfilService.findPerfil(prf1), null, atributosAuditoriaAcceso));
            usrSocPrfService.create_C(new UsrSocPrf(uuid66, verdad, "mail", usrSocService.findUsrSoc(prsSoc2), perfilService.findPerfil(prf3), null, atributosAuditoriaAcceso));
            usrSocPrfService.create_C(new UsrSocPrf(uuid63, verdad, "mail", usrSocService.findUsrSoc(prsSoc3), perfilService.findPerfil(prf1), null, atributosAuditoriaAcceso));
            usrSocPrfService.create_C(new UsrSocPrf(uuid67, verdad, "mail", usrSocService.findUsrSoc(prsSoc3), perfilService.findPerfil(prf3), null, atributosAuditoriaAcceso));

            //Sector sector1 = new Sector(Ssct1, "DUMMY", 4, atributosAuditoriaAcceso);
           // Sector sector2 = new Sector(Ssct2, "Hotelero", "",  "", "root", "bragasdeesparto", "ec2-54-76-103-159.eu-west-1.compute.amazonaws.com", 8000, 8001, 4, atributosAuditoriaAcceso);
            Sector sector2 = new Sector(Ssct2, "Hotelero", "",  "", "root", "root", "localhost", 8000, 8001, 4, atributosAuditoriaAcceso);
            //Sector sector3 = new Sector(Ssct3, "Automoción", 4, atributosAuditoriaAcceso);
            //sectorService.create_C(sector1);
            sectorService.create_C(sector2);
            //sectorService.create_C(sector3);

           // sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut1, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid61), sectorService.findSector(Ssct1), atributosAuditoriaAcceso));
            sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut2, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid61), sectorService.findSector(Ssct2), atributosAuditoriaAcceso));
            //sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut3, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid62), sectorService.findSector(Ssct1), atributosAuditoriaAcceso));
            sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut4, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid62), sectorService.findSector(Ssct2), atributosAuditoriaAcceso));
            //sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut5, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid63), sectorService.findSector(Ssct1), atributosAuditoriaAcceso));
            sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut6, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid63), sectorService.findSector(Ssct2), atributosAuditoriaAcceso));
            //sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut7, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid64), sectorService.findSector(Ssct1), atributosAuditoriaAcceso));
            sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut8, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid64), sectorService.findSector(Ssct2), atributosAuditoriaAcceso));
            //sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut9, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid65), sectorService.findSector(Ssct1), atributosAuditoriaAcceso));
            sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut10, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid65), sectorService.findSector(Ssct2), atributosAuditoriaAcceso));
            //sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut11, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid66), sectorService.findSector(Ssct1), atributosAuditoriaAcceso));
            sectoresAutorizadosService.create_C(new SectoresAutorizados(sctAut12, 0, 0, new BigDecimal(0.0), 0, new BigDecimal(0.0), usrSocPrfService.findUsrSocPrf(uuid66), sectorService.findSector(Ssct2), atributosAuditoriaAcceso));

        } catch (Exception ex) {
            Logger.getLogger(AppModelo.class.getName()).log(Level.SEVERE, null, ex);
        }

        log.warn("################################............Fin.");
    }

    @Transactional
    public static void creaDatos(ConfigurableApplicationContext context, int fechaNacimiento, int fechaPrevistaActivacion, int fechaPrevistaDesactivacion,
            String prs1, String prs2, String prs3, String prs4, String soc1, String soc2, String soc3, String soc4,
            String prf1, String prf2, String prf3, String prf4, String prf5, String prf6, String prsSoc1, String prsSoc2, String prsSoc3,
            String uuid61, String uuid62, String uuid63, String uuid64, String uuid65, String uuid66, String uuid67,
            /* String Ssct1, */ String Ssct2, /* String Ssct3, */
            String SsctAut1, String SsctAut2, String SsctAut3, String SsctAut4,
            String SsctAut5, String SsctAut6, String SsctAut7, String SsctAut8,
            String SsctAut9, String SsctAut10, String SsctAut11, String SsctAut12
    ) throws Exception {

        PersonaService personaService = (PersonaService) context.getBean(PersonaService.class);
        SociedadRplService sociedadRplService = (SociedadRplService) context.getBean(SociedadRplService.class);
        PerfilRplService perfilRplService = (PerfilRplService) context.getBean(PerfilRplService.class);
        UsrSocRplService usrSocRplService = (UsrSocRplService) context.getBean(UsrSocRplService.class);
        UsrSocPrfRplService usrSocPrfRplService = (UsrSocPrfRplService) context.getBean(UsrSocPrfRplService.class);

        ContactoService contactoService = (ContactoService) context.getBean(ContactoService.class);
        DireccionService direccionService = (DireccionService) context.getBean(DireccionService.class);
        DireccionGoogleComponenteService direccionGoogleComponenteService = (DireccionGoogleComponenteService) context.getBean(DireccionGoogleComponenteService.class);
        DireccionGoogleTypeService direccionGoogleTypeService = (DireccionGoogleTypeService) context.getBean(DireccionGoogleTypeService.class);

        FormatoVtaService formatoVtaService = (FormatoVtaService) context.getBean(FormatoVtaService.class);
        FormatoVtaLangService formatoVtaLangService = (FormatoVtaLangService) context.getBean(FormatoVtaLangService.class);
        NivelProductoService nivelProductoService = (NivelProductoService) context.getBean(NivelProductoService.class);
        NivelProductoLangService nivelProductoLangService = (NivelProductoLangService) context.getBean(NivelProductoLangService.class);
        UnidadMedidaLangService unidadMedidaLangService = (UnidadMedidaLangService) context.getBean(UnidadMedidaLangService.class);

        SectorRplService sectorRplService = (SectorRplService) context.getBean(SectorRplService.class);
        SectoresAutorizadosRplService sectoresAutorizadosRplService = (SectoresAutorizadosRplService) context.getBean(SectoresAutorizadosRplService.class);
        UnidadMedidaService unidadMedidaService = (UnidadMedidaService) context.getBean(UnidadMedidaService.class);
        ProductoLangService productoLangService = (ProductoLangService) context.getBean(ProductoLangService.class);

        ProductoService productoService = (ProductoService) context.getBean(ProductoService.class);;

        ZonaGeograficaService zonaGeograficaService = (ZonaGeograficaService) context.getBean(ZonaGeograficaService.class);
        AlmacenService almacenService = (AlmacenService) context.getBean(AlmacenService.class);

        UnidadFormatoVtaService unidadFormatoVtaService = (UnidadFormatoVtaService) context.getBean(UnidadFormatoVtaService.class);
        UnidadFormatoVtaLangService unidadFormatoVtaLangService = (UnidadFormatoVtaLangService) context.getBean(UnidadFormatoVtaLangService.class);

        ArticuloService articuloService = (ArticuloService) context.getBean(ArticuloService.class);

    //    SocAlmService socAlmService = (SocAlmService) context.getBean(SocAlmService.class);
        SocFabService socFabService = (SocFabService) context.getBean(SocFabService.class);
        SocZonGeoService socZonGeoService = (SocZonGeoService) context.getBean(SocZonGeoService.class);
        ZonasGeograficasAutorizadasService zonasGeograficasAutorizadasService = (ZonasGeograficasAutorizadasService) context.getBean(ZonasGeograficasAutorizadasService.class);
        SocProductoService socProductoService = (SocProductoService) context.getBean(SocProductoService.class);

        CompraCestaService compraCestaService = (CompraCestaService) context.getBean(CompraCestaService.class);
        CompraPedidoService compraPedidoService = (CompraPedidoService) context.getBean(CompraPedidoService.class);
        CompraLineaService compraLineaService = (CompraLineaService) context.getBean(CompraLineaService.class);
        CompraPedidoPieLinService compraPedidoPieLinService = (CompraPedidoPieLinService) context.getBean(CompraPedidoPieLinService.class);

        try {

            // Creación de persona ------------------------------------------------------------------------
            atributosAuditoria.setUsuarioUltimaAccion("SetUp");

            //sociedadRplService.create_C(new SociedadRpl(KApp.ADMIN.getKApp()));
            //personaService.create_C(new Persona(prs1, "admin", "nombre-admin", "apellido1-admin", "apellido2-admin", "cifpasport-1", fechaNacimiento, "email1_admin@gmail.com", atributosAuditoria));
            //usrSocRplService.create_C(new UsrSocRpl(prsSoc1));
            // Creación de sociedades REPLICA ------------------------------------------------------------------------
            sociedadRplService.create_C(new SociedadRpl(soc1));
            sociedadRplService.create_C(new SociedadRpl(soc2));
            sociedadRplService.create_C(new SociedadRpl(soc3));
            sociedadRplService.create_C(new SociedadRpl(soc4));

            personaService.create_C(new Persona(prs1, "admin", "nombre-1", "apellido1-1", "apellido2-1", "cifpasport-1", fechaNacimiento, "email1@gmail.com", atributosAuditoria));
            atributosAuditoria.setUsuarioUltimaAccion(prs1);
            personaService.create_C(new Persona(prs2, "idWeb-2", "nombre-2", "apellido1-2", "apellido2-2", "cifpasport-2", fechaNacimiento, "email1@gmail.com", atributosAuditoria));
            personaService.create_C(new Persona(prs3, "idWeb-3", "nombre-3", "apellido1-3", "apellido2-3", "cifpasport-3", fechaNacimiento, "email1@gmail.com", atributosAuditoria));
            personaService.create_C(new Persona(prs4, "idWeb-4", "nombre-2", "apellido1-4", "apellido2-4", "cifpasport-4", fechaNacimiento, "email1@gmail.com", atributosAuditoria));

            // Creación de Relacion persona / sociedad ------------------------------------------------    
            usrSocRplService.create_C(new UsrSocRpl(prsSoc1));
            usrSocRplService.create_C(new UsrSocRpl(prsSoc2));
            usrSocRplService.create_C(new UsrSocRpl(prsSoc3));

            // Creación de perfiles --------------------------------------------
            perfilRplService.create_C(new PerfilRpl(prf1));
            perfilRplService.create_C(new PerfilRpl(prf2));
            perfilRplService.create_C(new PerfilRpl(prf3));
            perfilRplService.create_C(new PerfilRpl(prf4));
            perfilRplService.create_C(new PerfilRpl(prf5));
            perfilRplService.create_C(new PerfilRpl(prf6));

            // Creación de persona / sociedad / perfil ------------------------------------------------  
            Boolean verdad = true;
            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid61, verdad));
            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid62, verdad));
            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid63, verdad));
            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid64, verdad));
            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid65, verdad));
            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid66, verdad));
            usrSocPrfRplService.create_C(new UsrSocPrfRpl(uuid67, verdad));

            //SectorRpl sectorRpl1 = new SectorRpl(Ssct1);
            SectorRpl sectorRpl2 = new SectorRpl(Ssct2);
            //SectorRpl sectorRpl3 = new SectorRpl(Ssct3);
            //sectorRplService.create_C(sectorRpl1);
            sectorRplService.create_C(sectorRpl2);
            //sectorRplService.create_C(sectorRpl3);

            // Creación de zonas geograficas ------------------------------------------------------------------------
            zonaGeograficaService.create_C(new ZonaGeografica(KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp(), 0, " ", " ", " ", null, atributosAuditoria));                                                  // 1
            zonaGeograficaService.create_C(new ZonaGeografica(AppUUID.zon1, 1, "Continente", "Europa", KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp(), zonaGeograficaService.findZonaGeografica(KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp()), atributosAuditoria));                                                  // 1
            zonaGeograficaService.create_C(new ZonaGeografica(AppUUID.zon2, 1, "Continente", "Antartico", KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp(), zonaGeograficaService.findZonaGeografica(KApp.ZONA_GEOGRAFICA_UNIVERSAL.getKApp()), atributosAuditoria));            // 2
            zonaGeograficaService.create_C(new ZonaGeografica(AppUUID.zon3, 2, "Pais", "España", AppUUID.zon1, zonaGeograficaService.findZonaGeografica(AppUUID.zon1), atributosAuditoria));
            zonaGeograficaService.create_C(new ZonaGeografica(AppUUID.zon4, 3, "Com Aut", "Catalunya", AppUUID.zon2, zonaGeograficaService.findZonaGeografica(AppUUID.zon3), atributosAuditoria));
            zonaGeograficaService.create_C(new ZonaGeografica(AppUUID.zon5, 4, "Provincia", "Barcelona", AppUUID.zon3, zonaGeograficaService.findZonaGeografica(AppUUID.zon4), atributosAuditoria));
            zonaGeograficaService.create_C(new ZonaGeografica(AppUUID.zon6, 4, "Provincia", "Gerona", AppUUID.zon3, zonaGeograficaService.findZonaGeografica(AppUUID.zon4), atributosAuditoria));
            zonaGeograficaService.create_C(new ZonaGeografica(AppUUID.zon7, 4, "Provincia", "Lerida", AppUUID.zon3, zonaGeograficaService.findZonaGeografica(AppUUID.zon4), atributosAuditoria));

            // Creación de almacenes ------------------------------------------------------------------------
            almacenService.create_C(new Almacen(AppUUID.alm1, "Nombre Almacen alm1", "DMM", zonaGeograficaService.findZonaGeografica(AppUUID.zon1), sociedadRplService.findSociedadRpl(soc1), atributosAuditoria));
            almacenService.create_C(new Almacen(AppUUID.alm2, "Nombre Almacen alm2", "DMM", zonaGeograficaService.findZonaGeografica(AppUUID.zon2), sociedadRplService.findSociedadRpl(soc1),atributosAuditoria));
            almacenService.create_C(new Almacen(AppUUID.alm3, "Nombre Almacen alm3", "DMM", zonaGeograficaService.findZonaGeografica(AppUUID.zon3), sociedadRplService.findSociedadRpl(soc1),atributosAuditoria));
            almacenService.create_C(new Almacen(AppUUID.alm4, "Nombre Almacen alm4", "DMM", zonaGeograficaService.findZonaGeografica(AppUUID.zon4), sociedadRplService.findSociedadRpl(soc1),atributosAuditoria));

            // Creación de Contactos ------------------------------------------------------------------------------------------------------------------
            contactoService.create_C(new Contacto(AppUUID.con1, "tipo", "valor", "descripcion", "nombre Dani1", "cargo Dani1", true, false, null, null, null, atributosAuditoria));
            contactoService.create_C(new Contacto(AppUUID.con2, "telefono_movil", "546354654", "Duro para negociar", "nombre Dani2", "cargo Dani2", true, true, personaService.findPersona(prs2), null, null, atributosAuditoria));
            contactoService.create_C(new Contacto(AppUUID.con3, "telefono_fijo", "912040111", "El de la tia buena", "nombre Dani3", "cargo Dani3", false, true, null, sociedadRplService.findSociedadRpl(soc3), null, atributosAuditoria));
            contactoService.create_C(new Contacto(AppUUID.con4, "telefono_trabajo", "541251211", "Este sabe negociar", "nombre Dani4", "cargo Dani4", false, false, personaService.findPersona(prs4), sociedadRplService.findSociedadRpl(soc4), null, atributosAuditoria));

            // Creación de Direcciones  ------------------------------------------------------------------------------------------------------------------
            direccionService.create_C(new Direccion(AppUUID.dir1, "Tipo-DS", "Direccion formateada", 12.14545445454545, 7.1211212111222, true, false, "descripcion dani", personaService.findPersona(prs1), null, null, atributosAuditoria));
            direccionService.create_C(new Direccion(AppUUID.dir2, "Tipo-DS", "Direccion formateada", 12.14545445454545, 7.1211212111222, false, true, "descripcion dani", null, sociedadRplService.findSociedadRpl(soc2), null, atributosAuditoria));
            direccionService.create_C(new Direccion(AppUUID.dir3, "Tipo-DS", "Direccion formateada", 12.14545445454545, 7.1211212111222, true, true, "descripcion dani", null, null, almacenService.findAlmacen(AppUUID.alm3), atributosAuditoria));

            // Creación de Direcciones / Types -----------------------------------------------------------------------------------------------------------
            direccionGoogleTypeService.create_C(new DireccionGoogleType(AppUUID.dirGooTyp11, "Type 11", null, dirGooTyp11, null, direccionService.findDireccion(AppUUID.dir1), atributosAuditoria));
            direccionGoogleTypeService.create_C(new DireccionGoogleType(AppUUID.dirGooTyp12, "Type 12", null, dirGooTyp12, null, direccionService.findDireccion(AppUUID.dir1), atributosAuditoria));

            // Creación de Direcciones / Componentes ----------------------------11111,-------------------------------------------------------------------------
            direccionGoogleComponenteService.create_C(new DireccionGoogleComponente(AppUUID.dirGooCmp11, "Short Name-11", "Long name-11", direccionService.findDireccion(AppUUID.dir1), null, atributosAuditoria));
            direccionGoogleComponenteService.create_C(new DireccionGoogleComponente(AppUUID.dirGooCmp12, "Short Name-12", "Long name-12", direccionService.findDireccion(AppUUID.dir1), null, atributosAuditoria));
            direccionGoogleComponenteService.create_C(new DireccionGoogleComponente(AppUUID.dirGooCmp21, "Short Name-21", "Long name-21", direccionService.findDireccion(AppUUID.dir2), null, atributosAuditoria));
            direccionGoogleComponenteService.create_C(new DireccionGoogleComponente(AppUUID.dirGooCmp22, "Short Name-22", "Long name-22", direccionService.findDireccion(AppUUID.dir2), null, atributosAuditoria));

            // Creación de Direcciones / Componentes / Types ---------------------------------------------------------------------------------------------
            direccionGoogleTypeService.create_C(new DireccionGoogleType(AppUUID.dirGooCmpTyp11, "Type 21", dirGooCmpTyp11, null, direccionGoogleComponenteService.findDireccionGoogleComponente(AppUUID.dirGooCmp11), null, atributosAuditoria));
            direccionGoogleTypeService.create_C(new DireccionGoogleType(AppUUID.dirGooCmpTyp12, "Type 22", dirGooCmpTyp12, null, direccionGoogleComponenteService.findDireccionGoogleComponente(AppUUID.dirGooCmp11), null, atributosAuditoria));
            direccionGoogleTypeService.create_C(new DireccionGoogleType(AppUUID.dirGooCmpTyp21, "Type 21", dirGooCmpTyp21, null, direccionGoogleComponenteService.findDireccionGoogleComponente(AppUUID.dirGooCmp22), null, atributosAuditoria));
            direccionGoogleTypeService.create_C(new DireccionGoogleType(AppUUID.dirGooCmpTyp22, "Type 22", dirGooCmpTyp22, null, direccionGoogleComponenteService.findDireccionGoogleComponente(AppUUID.dirGooCmp22), null, atributosAuditoria));

            formatoVtaService.create_C(new FormatoVta(AppUUID.fmtVta1, 0, "Botella P", "Botella de plastico", atributosAuditoria));
            formatoVtaService.create_C(new FormatoVta(AppUUID.fmtVta2, 0, "Brick Galleta", "Brick de galleta", atributosAuditoria));
            formatoVtaService.create_C(new FormatoVta(AppUUID.fmtVta3, 0, "Lata metal", "Apertura manual", atributosAuditoria));

            formatoVtaLangService.create_C(new FormatoVtaLang(AppUUID.fmtVta1, "es", 0, "Botella P es", formatoVtaService.findFormatoVta(AppUUID.fmtVta1), atributosAuditoria));
            formatoVtaLangService.create_C(new FormatoVtaLang(AppUUID.fmtVta2, "es", 0, "Brick Galleta es", formatoVtaService.findFormatoVta(AppUUID.fmtVta2), atributosAuditoria));
            formatoVtaLangService.create_C(new FormatoVtaLang(AppUUID.fmtVta3, "es", 0, "Lata metal es", formatoVtaService.findFormatoVta(AppUUID.fmtVta3), atributosAuditoria));
            formatoVtaLangService.create_C(new FormatoVtaLang(AppUUID.fmtVta1, "ca", 0, "Botella P ca", formatoVtaService.findFormatoVta(AppUUID.fmtVta1), atributosAuditoria));
            formatoVtaLangService.create_C(new FormatoVtaLang(AppUUID.fmtVta2, "ca", 0, "Brick Galleta ca", formatoVtaService.findFormatoVta(AppUUID.fmtVta2), atributosAuditoria));
            formatoVtaLangService.create_C(new FormatoVtaLang(AppUUID.fmtVta3, "ca", 0, "Lata metal ca", formatoVtaService.findFormatoVta(AppUUID.fmtVta3), atributosAuditoria));

            NivelProducto nivelProducto1 = new NivelProducto(1, "Familia", sectorRpl2, atributosAuditoria);
            NivelProducto nivelProducto2 = new NivelProducto(2, "SubFamilia", sectorRpl2, atributosAuditoria);
            NivelProducto nivelProducto3 = new NivelProducto(3, "Categoria", sectorRpl2, atributosAuditoria);
            NivelProducto nivelProducto4 = new NivelProducto(4, "SubCategoria", sectorRpl2, atributosAuditoria);
            nivelProductoService.create_C(nivelProducto1);
            nivelProductoService.create_C(nivelProducto2);
            nivelProductoService.create_C(nivelProducto3);
            nivelProductoService.create_C(nivelProducto4);

            nivelProductoLangService.create_C(new NivelProductoLang(1, "es", "Familia es", nivelProducto1, atributosAuditoria));
            nivelProductoLangService.create_C(new NivelProductoLang(2, "es", "SubFamilia es", nivelProducto2, atributosAuditoria));
            nivelProductoLangService.create_C(new NivelProductoLang(3, "es", "Categoria es", nivelProducto3, atributosAuditoria));
            nivelProductoLangService.create_C(new NivelProductoLang(4, "es", "SubCategoria es", nivelProducto4, atributosAuditoria));
            nivelProductoLangService.create_C(new NivelProductoLang(1, "ca", "Familia ca", nivelProducto1, atributosAuditoria));
            nivelProductoLangService.create_C(new NivelProductoLang(2, "ca", "SubFamilia ca", nivelProducto2, atributosAuditoria));
            nivelProductoLangService.create_C(new NivelProductoLang(3, "ca", "Categoria ca", nivelProducto3, atributosAuditoria));
            nivelProductoLangService.create_C(new NivelProductoLang(4, "ca", "SubCategoria ca", nivelProducto4, atributosAuditoria));

            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut1));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut2));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut3));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut4));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut5));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut6));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut7));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut8));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut9));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut10));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut11));
            sectoresAutorizadosRplService.create_C(new SectoresAutorizadosRpl(SsctAut12));

            //----------------------            
            UnidadMedida unidadMedida1 = new UnidadMedida(AppUUID.uMed1, 0, "LT   ", "Litros", atributosAuditoria);
            UnidadMedida unidadMedida2 = new UnidadMedida(AppUUID.uMed2, 0, "GR   ", "Gramos", atributosAuditoria);
            UnidadMedida unidadMedida3 = new UnidadMedida(AppUUID.uMed3, 0, "KG   ", "KiloGramos", atributosAuditoria);
            UnidadMedida unidadMedida4 = new UnidadMedida(AppUUID.uMed4, 0, "M    ", "Metros", atributosAuditoria);
            UnidadMedida unidadMedida5 = new UnidadMedida(AppUUID.uMed5, 0, "KM   ", "KiloMetros", atributosAuditoria);
            UnidadMedida unidadMedida6 = new UnidadMedida(AppUUID.uMed6, 0, "M2   ", "Metros cuadrados", atributosAuditoria);
            unidadMedidaService.create_C(unidadMedida1);
            unidadMedidaService.create_C(unidadMedida2);
            unidadMedidaService.create_C(unidadMedida3);
            unidadMedidaService.create_C(unidadMedida4);
            unidadMedidaService.create_C(unidadMedida5);
            unidadMedidaService.create_C(unidadMedida6);

            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed1, "es", 0, "Litros es", unidadMedidaService.findUnidadMedida(AppUUID.uMed1), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed2, "es", 0, "Gramos es", unidadMedidaService.findUnidadMedida(AppUUID.uMed2), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed3, "es", 0, "KiloGramos es", unidadMedidaService.findUnidadMedida(AppUUID.uMed3), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed4, "es", 0, "Metros es", unidadMedidaService.findUnidadMedida(AppUUID.uMed4), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed5, "es", 0, "KiloMetros es", unidadMedidaService.findUnidadMedida(AppUUID.uMed5), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed6, "es", 0, "Metros cuadrados es", unidadMedidaService.findUnidadMedida(AppUUID.uMed6), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed1, "ca", 0, "Litros es", unidadMedidaService.findUnidadMedida(AppUUID.uMed1), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed2, "ca", 0, "Gramos es", unidadMedidaService.findUnidadMedida(AppUUID.uMed2), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed3, "ca", 0, "KiloGramos es", unidadMedidaService.findUnidadMedida(AppUUID.uMed3), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed4, "ca", 0, "Metros es", unidadMedidaService.findUnidadMedida(AppUUID.uMed4), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed5, "ca", 0, "KiloMetros es", unidadMedidaService.findUnidadMedida(AppUUID.uMed5), atributosAuditoria));
            unidadMedidaLangService.create_C(new UnidadMedidaLang(AppUUID.uMed6, "ca", 0, "Metros cuadrados es", unidadMedidaService.findUnidadMedida(AppUUID.uMed6), atributosAuditoria));


            //----------------------
            ProductoLang productoLang1 = new ProductoLang(AppUUID.prd1, "es", "Prod 1 castellano", productoService.findProducto(AppUUID.prd1), atributosAuditoria);
            ProductoLang productoLang2 = new ProductoLang(AppUUID.prd2, "es", "Prod 2 castellano", productoService.findProducto(AppUUID.prd2), atributosAuditoria);
            ProductoLang productoLang3 = new ProductoLang(AppUUID.prd3, "es", "Prod 3 castellano", productoService.findProducto(AppUUID.prd3), atributosAuditoria);
            ProductoLang productoLang4 = new ProductoLang(AppUUID.prd4, "es", "Prod 4 castellano", productoService.findProducto(AppUUID.prd4), atributosAuditoria);
            ProductoLang productoLang5 = new ProductoLang(AppUUID.prd1, "ca", "Prod 1 castellano", productoService.findProducto(AppUUID.prd1), atributosAuditoria);
            ProductoLang productoLang6 = new ProductoLang(AppUUID.prd2, "ca", "Prod 2 castellano", productoService.findProducto(AppUUID.prd2), atributosAuditoria);
            ProductoLang productoLang7 = new ProductoLang(AppUUID.prd3, "ca", "Prod 3 castellano", productoService.findProducto(AppUUID.prd3), atributosAuditoria);
            ProductoLang productoLang8 = new ProductoLang(AppUUID.prd4, "ca", "Prod 4 castellano", productoService.findProducto(AppUUID.prd4), atributosAuditoria);
            productoLangService.create_C(productoLang1);
            productoLangService.create_C(productoLang2);
            productoLangService.create_C(productoLang3);
            productoLangService.create_C(productoLang4);
            productoLangService.create_C(productoLang5);
            productoLangService.create_C(productoLang6);
            productoLangService.create_C(productoLang7);
            productoLangService.create_C(productoLang8);

            //----------------------
            UnidadFormatoVta unidadFormatoVta1 = new UnidadFormatoVta(AppUUID.uFmtVta1, 0, BigDecimal.valueOf(0.5D), "Medio Litro", atributosAuditoria);
            UnidadFormatoVta unidadFormatoVta2 = new UnidadFormatoVta(AppUUID.uFmtVta2, 0, BigDecimal.valueOf(1L), "1 Litro", atributosAuditoria);
            UnidadFormatoVta unidadFormatoVta3 = new UnidadFormatoVta(AppUUID.uFmtVta3, 0, BigDecimal.valueOf(1.5D), "Litro y medio", atributosAuditoria);
            UnidadFormatoVta unidadFormatoVta4 = new UnidadFormatoVta(AppUUID.uFmtVta4, 0, BigDecimal.valueOf(2L), "2 Litros ", atributosAuditoria);
            UnidadFormatoVta unidadFormatoVta5 = new UnidadFormatoVta(AppUUID.uFmtVta5, 0, BigDecimal.valueOf(5L), "5 Kilos", atributosAuditoria);
            unidadFormatoVtaService.create_C(unidadFormatoVta1);
            unidadFormatoVtaService.create_C(unidadFormatoVta2);
            unidadFormatoVtaService.create_C(unidadFormatoVta3);
            unidadFormatoVtaService.create_C(unidadFormatoVta4);
            unidadFormatoVtaService.create_C(unidadFormatoVta5);

            //----------------------            
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta1, "es", 0, "Medio Litro es", unidadFormatoVta1, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta2, "es", 0, "1 Litro es", unidadFormatoVta2, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta3, "es", 0, "Litro y medio es", unidadFormatoVta3, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta4, "es", 0, "2 Litros es", unidadFormatoVta4, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta5, "es", 0, "5 Kilos es", unidadFormatoVta2, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta1, "ca", 0, "Medio Litro ca", unidadFormatoVta1, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta2, "ca", 0, "1 Litro ca", unidadFormatoVta2, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta3, "ca", 0, "Litro y medio ca", unidadFormatoVta3, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta4, "ca", 0, "2 Litros ca", unidadFormatoVta4, atributosAuditoria));
            unidadFormatoVtaLangService.create_C(new UnidadFormatoVtaLang(AppUUID.uFmtVta5, "ca", 0, "5 Kilos ca", unidadFormatoVta5, atributosAuditoria));

            //productoService.create_C(new Producto(AppUUID.prd1, "EUR", "nombreProducto", jerarquiaService.findJerarquia(AppUUID.jer4), unidadMedidaService.findUnidadMedida(AppUUID.uMed1), atributosAuditoria));
            // viejo: articuloService.create_C(new Articulo(AppUUID.art1, new BigDecimal(500), new BigDecimal(3.54D), formatoVtaService.findFormatoVta(AppUUID.fmtVta2), unidadFormatoVtaService.findUnidadFormatoVta(AppUUID.uFmtVta1), sociedadRplService.findSociedadRpl(soc1), atributosAuditoria));
           
            /*articuloService.create_C(new Articulo(
                    AppUUID.art1, 
                    new BigDecimal(500), 
                    new BigDecimal(3.54D), 
                    new BigDecimal(3.54D), 
                    " ", 
                    BigDecimal.valueOf(0.5D), 
                    
                    "Decripcion Articulo", 
                    "Nombre Articulo", 
                    "codigo del proveedor", 
                    "codigo ean", 
                    KApp.APROBADO.getKApp(), 
                    formatoVtaService.findFormatoVta(AppUUID.fmtVta2), 

                    sociedadService.findSociedad(soc1), 
                    atributosAuditoria));
            
            articuloService.create_C(new Articulo(
                    AppUUID.art2, 
                    new BigDecimal(450), 
                    new BigDecimal(3.54D), 
                    new BigDecimal(3.54D), 
                    " ", 
                    BigDecimal.valueOf(1.5D), 
                    
                    "Decripcion Articulo", 
                    "Nombre Articulo", 
                    "codigo del proveedor", 
                    "codigo ean", 
                    KApp.APROBADO.getKApp(),
                    formatoVtaService.findFormatoVta(AppUUID.fmtVta2), 

                    sociedadService.findSociedad(soc1), 
                    atributosAuditoria));
            */
            //articuloProductoService.create_C(new ArticuloProducto(productoService.findProducto(AppUUID.prd4), articuloService.findArticulo(AppUUID.art1), atributosAuditoria));

            // -------- Almacenes autorizados--------------------------------------------    


            // -------- Sociedades / Almacenes--------------------------------------------                
           // socAlmService.create_C(new SocAlm(AppUUID.socAlm1, sociedadRplService.findSociedadRpl(soc1), almacenService.findAlmacen(AppUUID.alm1), atributosAuditoria));
           // socAlmService.create_C(new SocAlm(AppUUID.socAlm2, sociedadRplService.findSociedadRpl(soc2), almacenService.findAlmacen(AppUUID.alm2), atributosAuditoria));
           // socAlmService.create_C(new SocAlm(AppUUID.socAlm3, sociedadRplService.findSociedadRpl(soc1), almacenService.findAlmacen(AppUUID.alm3), atributosAuditoria));
           // socAlmService.create_C(new SocAlm(AppUUID.socAlm4, sociedadRplService.findSociedadRpl(soc2), almacenService.findAlmacen(AppUUID.alm4), atributosAuditoria));

            // -------- Sociedades / Fabricas --------------------------------------------      
            socFabService.create_C(new SocFab(sociedadRplService.findSociedadRpl(soc1), sociedadRplService.findSociedadRpl(soc3), atributosAuditoria));

            // -------- Sociedades / Zonas geográficas -------------------------------------------- 
            socZonGeoService.create_C(new SocZonGeo(AppUUID.soZo1, AppUUID.zon4, AppUUID.soc1, Boolean.getBoolean("1"), sociedadRplService.findSociedadRpl(soc1), zonaGeograficaService.findZonaGeografica(AppUUID.zon4), atributosAuditoria));
            socZonGeoService.create_C(new SocZonGeo(AppUUID.soZo2, AppUUID.zon7, AppUUID.soc2, Boolean.getBoolean("1"), sociedadRplService.findSociedadRpl(soc2), zonaGeograficaService.findZonaGeografica(AppUUID.zon7), atributosAuditoria));
            socZonGeoService.create_C(new SocZonGeo(AppUUID.soZo3, AppUUID.zon3, AppUUID.soc3, Boolean.getBoolean("0"), sociedadRplService.findSociedadRpl(soc3), zonaGeograficaService.findZonaGeografica(AppUUID.zon3), atributosAuditoria));

            // -------- Zonas geografocas autorizados--------------------------------------------    
            zonasGeograficasAutorizadasService.create_C(new ZonasGeograficasAutorizadas(AppUUID.zonAut1, personaService.findPersona(prs1), zonaGeograficaService.findZonaGeografica(AppUUID.zon4), atributosAuditoria));
            zonasGeograficasAutorizadasService.create_C(new ZonasGeograficasAutorizadas(AppUUID.zonAut2, personaService.findPersona(prs1), zonaGeograficaService.findZonaGeografica(AppUUID.zon5), atributosAuditoria));
            zonasGeograficasAutorizadasService.create_C(new ZonasGeograficasAutorizadas(AppUUID.zonAut3, personaService.findPersona(prs1), zonaGeograficaService.findZonaGeografica(AppUUID.zon6), atributosAuditoria));
            zonasGeograficasAutorizadasService.create_C(new ZonasGeograficasAutorizadas(AppUUID.zonAut4, personaService.findPersona(prs2), zonaGeograficaService.findZonaGeografica(AppUUID.zon3), atributosAuditoria));

            // -------- Productos asignados a sociedad --------------------------------------------   
            socProductoService.create_C(new SocProducto(AppUUID.socPrd1, AppUUID.prd1, AppUUID.soc1, verdad, 0, 0, new BigDecimal(0), 0, new BigDecimal(0), new BigDecimal(0), productoService.findProducto(AppUUID.prd1), sociedadRplService.findSociedadRpl(soc1), atributosAuditoria));
            socProductoService.create_C(new SocProducto(AppUUID.socPrd2, AppUUID.prd2, AppUUID.soc1, verdad, 0, 0, new BigDecimal(0), 0, new BigDecimal(0), new BigDecimal(0), productoService.findProducto(AppUUID.prd2), sociedadRplService.findSociedadRpl(soc1), atributosAuditoria));
            socProductoService.create_C(new SocProducto(AppUUID.socPrd3, AppUUID.prd3, AppUUID.soc1, verdad, 0, 0, new BigDecimal(0), 0, new BigDecimal(0), new BigDecimal(0), productoService.findProducto(AppUUID.prd3), sociedadRplService.findSociedadRpl(soc1), atributosAuditoria));
            socProductoService.create_C(new SocProducto(AppUUID.socPrd4, AppUUID.prd2, AppUUID.soc3, verdad, 0, 0, new BigDecimal(0), 0, new BigDecimal(0), new BigDecimal(0), productoService.findProducto(AppUUID.prd2), sociedadRplService.findSociedadRpl(soc3), atributosAuditoria));

            // -------- Cesta de pedido --------------------------------------------
            atributosAuditoria = new DatosAuditoria("AC", "A", "Usu", fechaPrevistaActivacion, fechaPrevistaDesactivacion);
            /*
            cestaService.create_C(new CompraCesta(AppUUID.ces1, atributosAuditoria));
            cestaService.create_C(new CompraCesta(AppUUID.ces2, atributosAuditoria));

            // -------- Linea de cesta de pedido -----------------------------------
            CompraCesta cesta;
            CestaLin cestaLin;
            Long ultimaLinea;

            cestaLin = new CestaLin(cestaService.findCesta(AppUUID.ces1), atributosAuditoria);
            ultimaLinea = cestaLinService.findUltimaLineaCesta(cestaLin.getIdCesta());
            cestaLin.setLineaCesta(ultimaLinea + 1);
            cestaLinService.create_C(cestaLin);

            cestaLin = new CestaLin(cestaService.findCesta(AppUUID.ces2), atributosAuditoria);
            ultimaLinea = cestaLinService.findUltimaLineaCesta(cestaLin.getIdCesta());
            cestaLin.setLineaCesta(ultimaLinea + 1);
            cestaLinService.create_C(cestaLin);

            // -------- Cabecera de pedido -----------------------------------------

            pedidoCabService.create_C(new CompraPedido(AppUUID.pedCab1, new ArrayList<CompraLinea>(), new ArrayList<CompraPedidoPieLin>(), atributosAuditoria));
            pedidoCabService.create_C(new CompraPedido(AppUUID.pedCab2, new ArrayList<CompraLinea>(), new ArrayList<CompraPedidoPieLin>(), atributosAuditoria));
            pedidoCabService.create_C(new CompraPedido(AppUUID.pedCab3, new ArrayList<CompraLinea>(), new ArrayList<CompraPedidoPieLin>(), atributosAuditoria));

            // -------- Linea y pie de pedido --------------------------------------
            CompraPedido pedidoCab;
            CompraLinea pedidoLin;
            CompraPedidoPieLin pedidoPie;

            pedidoCab = pedidoCabService.findPedidoCab(AppUUID.pedCab1);

            pedidoLin = new CompraLinea(pedidoCab.getIdPedido(), pedidoCab, 1L);
            ultimaLinea = pedidoLinService.findUltimaLineaPedido(pedidoLin.getIdPedido());
            pedidoLin.setAtributosAuditoria(atributosAuditoria);
            pedidoLin.setLineaPedido(ultimaLinea + 1);
            pedidoLinService.create_C(pedidoLin);

            pedidoLin = new CompraLinea(pedidoCab.getIdPedido(), pedidoCab, 1L);
            ultimaLinea = pedidoLinService.findUltimaLineaPedido(pedidoLin.getIdPedido());
            pedidoLin.setAtributosAuditoria(atributosAuditoria);
            pedidoLin.setLineaPedido(ultimaLinea + 1);
            pedidoLinService.create_C(pedidoLin);

            pedidoPie = new CompraPedidoPieLin(pedidoCab.getIdPedido(), pedidoCab);
            ultimaLinea = pedidoPieService.findUltimaLinPiePedido(pedidoPie.getIdPedido());
            pedidoPie.setAtributosAuditoria(atributosAuditoria);
            pedidoPie.setLineaPie(ultimaLinea + 1);
            pedidoPieService.create_C(pedidoPie);

            pedidoCab = pedidoCabService.findPedidoCab(AppUUID.pedCab2);

            pedidoLin = new CompraLinea(pedidoCab.getIdPedido(), pedidoCab, 1L);
            ultimaLinea = pedidoLinService.findUltimaLineaPedido(pedidoLin.getIdPedido());
            pedidoLin.setAtributosAuditoria(atributosAuditoria);
            pedidoLin.setLineaPedido(ultimaLinea + 1);
            pedidoLinService.create_C(pedidoLin);

            pedidoPie = new CompraPedidoPieLin(pedidoCab.getIdPedido(), pedidoCab);
            ultimaLinea = pedidoPieService.findUltimaLinPiePedido(pedidoPie.getIdPedido());
            pedidoPie.setAtributosAuditoria(atributosAuditoria);
            pedidoPie.setLineaPie(ultimaLinea + 1);
            pedidoPieService.create_C(pedidoPie);
*/
            
        } catch (Exception ex) {
            Logger.getLogger(AppModelo.class.getName()).log(Level.SEVERE, null, ex);
        }

        log.warn("################################............Fin.");
    }

}
