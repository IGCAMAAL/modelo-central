
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_aut_sociedades    (id,               tipoidentificador, nifnrf,     nombre,              fabrica, estado_registro, ultima_accion,  usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("SOCIEDAD_ADMIN", "NIF",             "nifnrf-1", "nombre Sociedad 1", null,    "AC",            "A",            "PERSONA_ADMIN",       CURDATE()+0,               0,                            0      , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_aut_users         (user_id,         username, md5password,  email,             estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PERSONA_ADMIN", "admin",  "admin",      "mail1@gmail.com", "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_aut_usr_soc       (id,             user_central,    sociedad,         estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PERSOC_ADMIN", "PERSONA_ADMIN", "SOCIEDAD_ADMIN", "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0      , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_aut_perfiles      (id,             cod_perfil, desc_perfil,                            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PRF_DUMMY",    "DUMMY ",   "Perfil para relación Persona/Empresa", "AC",            "A",           "PERSONA_ADMIN",        CURDATE()+0,               0,                            0       , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_aut_perfiles      (id,             cod_perfil, desc_perfil,                            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PRF_ADMINA", "ADMINA", "Administrador aplicación",                 "AC",            "A",           "PERSONA_ADMIN",        CURDATE()+0,               0,                            0      , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_aut_perfiles      (id,             cod_perfil, desc_perfil,                            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PRF_ASOADM", "ASOADM", "Asociado Administrador",                  "AC",            "A",           "PERSONA_ADMIN",        CURDATE()+0,               0,                            0      , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_aut_perfiles      (id,             cod_perfil, desc_perfil,                            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PRF_ASOCOM", "ASOCOM", "Asociado Comprador",                      "AC",            "A",           "PERSONA_ADMIN",        CURDATE()+0,               0,                            0       , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_aut_perfiles      (id,             cod_perfil, desc_perfil,                            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PRF_PRVDST", "PRVDST", "Distribuidor",                       "AC",            "A",           "PERSONA_ADMIN",        CURDATE()+0,                    0,                            0      , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_aut_perfiles      (id,             cod_perfil, desc_perfil,                            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PRF_PRVFAB", "PRVFAB", "Fabricante",                              "AC",            "A",           "PERSONA_ADMIN",        CURDATE()+0,               0,                            0       , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_aut_usr_soc_prf   (id,                estado_sesion, mail,   prs_soc,        perfil,       asociado_administrador, estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("PERSOCPRF_ADMIN", true,          "mail", "PERSOC_ADMIN", "PRF_ADMINA", null,                   "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_aut_sector        (id,                descripcion_sector, driver_class_name, urldb, username, password,          host_destino,                                         puerto_destino, puerto_destinossl, ultimo_nivel, estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                              ("SECTOR_HOTELERO", "Hotelero",          "",                "",    "root",   "root",           "localhost",                                          8000,           8001,              4,            "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null);

-- INSERT INTO centralcomprasdemo.cc_aut_sector        (id,                descripcion_sector, driver_class_name, urldb, username, password,           host_destino,                                         puerto_destino, puerto_destinossl, ultimo_nivel, estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
-- VALUES                                              ("SECTOR_HOTELERO", "Hotelero",          "",                "",    "root",  "bragasdeesparto",  "ec2-54-76-103-159.eu-west-1.compute.amazonaws.com",  8000,           8001,              4,            "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null);
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_aut_sectores_autorizados  (id,           carga_directa_articulos, mod_precios_baja, porcentaje_max_baja, mod_precios_alta, porcentaje_max_alta, prs_soc_prf,       sector,            estado_registro,  ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                                      ("SECAUT_01",  0,                       0,                0.0, 0,                 0.0, "PERSOCPRF_ADMIN", "SECTOR_HOTELERO", "AC",            "A",          "PERSONA_ADMIN",  CURDATE()+0,                     0,                         0                     , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_rpl_sociedad_rpl          (id, version) 
VALUES                                                      ("ADMIN", 0);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_rpl_personas              (id,              user_name, nombre,         apellido1,         apellido2,         cifpasport,     fecha_nacimiento, email,                    estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                                      ("PERSONA_ADMIN", "admin",   "nombre-admin", "apellido1-admin", "apellido2-admin", "cifpasport-1", 19000101,  "email1_admin@gmail.com", "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0      , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_rpl_personas              (id,              user_name, nombre,         apellido1,         apellido2,         cifpasport,     fecha_nacimiento, email,                    estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                                      ("DUMMY", "dummy",   "dummy",        "dummy",           "dummy",           "dummy",        19000101,        "dummy",                   "DA",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0      , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_rpl_usr_soc_rpl           (id, version)
VALUES                                                      ("PERSOC_ADMIN", 0);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_rpl_sector_rpl            (id, version, ultimo_nivel)
VALUES                                                      ("SECTOR_HOTELERO", 0, 4);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_rpl_perfil_rpl            (id, version) VALUES ("PRF_DUMMY", 0);
INSERT INTO centralcomprasdemo.cc_rpl_perfil_rpl            (id, version) VALUES ("PRF_ADMINA", 0);
INSERT INTO centralcomprasdemo.cc_rpl_perfil_rpl            (id, version) VALUES ("PRF_ASOADM", 0);
INSERT INTO centralcomprasdemo.cc_rpl_perfil_rpl            (id, version) VALUES ("PRF_ASOCOM", 0);
INSERT INTO centralcomprasdemo.cc_rpl_perfil_rpl            (id, version) VALUES ("PRF_PRVDST", 0);
INSERT INTO centralcomprasdemo.cc_rpl_perfil_rpl            (id, version) VALUES ("PRF_PRVFAB", 0);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_rpl_usr_soc_prf_rpl       (id, estado_sesion, version) VALUES ("PERSOCPRF_ADMIN", true, 0);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_prm_nivel_producto        (id, descripcion_nivel_producto, sector,            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created,deleted,updated)
VALUES                                                      (1,  "Familia",                  "SECTOR_HOTELERO", "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_prm_nivel_producto        (id, descripcion_nivel_producto, sector,            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created,deleted,updated)
VALUES                                                      (2,  "SubFamilia",               "SECTOR_HOTELERO", "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0      , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_prm_nivel_producto        (id, descripcion_nivel_producto, sector,            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created,deleted,updated)
VALUES                                                      (3,  "Categoria",                "SECTOR_HOTELERO", "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0      , now(),   null,       null);

INSERT INTO centralcomprasdemo.cc_prm_nivel_producto        (id, descripcion_nivel_producto, sector,            estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created,deleted,updated)
VALUES                                                      (4,  "SubCategoria",              "SECTOR_HOTELERO", "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
-- SectoresAutorizadosRpl(UUID.randomUUID().toString().replaceAll("-", "")));   
-- SectoresAutorizadosRpl(uuid) 

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_mat_producto              (id,                   nivel_int, id_padre_jerarquia, id_nivel_producto, nivel_producto, padre_jerarquia, sector,          descripcion,     estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated, unidad_medida)
VALUES                                                      ("PRODUCTO_UNIVERSAL", 0,         " ",                0,                 null,           null,           "SECTOR_HOTELERO", "Descripcion",  "AC",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null,      null);    

INSERT INTO centralcomprasdemo.cc_mat_producto              (id,                   nivel_int, id_padre_jerarquia, id_nivel_producto, nivel_producto, padre_jerarquia, sector,          descripcion,     estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated, unidad_medida)
VALUES                                                      ("DUMMY",     0,         " ",                0,                 null,           null,           "SECTOR_HOTELERO", "Dummy",  "DA",            "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0       , now(),   null,       null,      null);    

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_lgt_zona_geografica       (id,                          nivel_int, etiqueta, descripcion, id_padre_jerarquia, padre_jerarquia, estado_registro, ultima_accion, usuario_ultima_accion, fecha_prevista_activacion, fecha_prevista_desactivacion, version, created, deleted, updated)
VALUES                                                      ("ZONA_GEOGRAFICA_UNIVERSAL", 0,         " ",      " ",          " ",               null,            "AC",             "A",           "PERSONA_ADMIN",       CURDATE()+0,               0,                            0      , now(),   null,       null);   

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - - - - - - - - -
INSERT INTO centralcomprasdemo.cc_cmo_tipo_iva (id, created, deleted, estado_registro, fecha_prevista_activacion, fecha_prevista_desactivacion, ultima_accion, updated, usuario_ultima_accion, clave_iva, porcentaje_importe, porcentaje_iva, version) 
	VALUES ('prueba', '2015-06-21 17:32:13.0', '2015-06-21 17:33:02.0', 'AC', 0, 0, 'POST', '2015-06-21 17:33:02.0', 'IDUSER', '', 100.00, 21.00, 0);